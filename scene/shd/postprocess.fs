#version 300 es
precision highp float;

uniform sampler2D u_ColorImage;
uniform sampler2D u_BluredImage;
uniform vec2 u_TextureSize;

in vec2 io_UV;
out vec4 o_Color;

// vec3 textureDistorted(
//       in sampler2D tex,
//       in vec2 texcoord,
//       in vec2 direction, // direction of distortion
//       in vec3 distortion // per-channel distortion factor
//    ) {
//       return vec3(
//          texture(tex, texcoord + direction * distortion.r).r,
//          texture(tex, texcoord + direction * distortion.g).g,
//          texture(tex, texcoord + direction * distortion.b).b
//       );
//  }

 void main() {
     // o_Color = texture(u_ColorImage, io_UV);
     vec3 sourceColor = texture(u_ColorImage, io_UV).rgb;
     o_Color = vec4(sourceColor, 1.0f);
 }

// void main() {
//     vec2 texcoord = -io_UV + vec2(1.0f);
//     vec2 texelSize = 1.0f / u_TextureSize;
//
//     vec2 ghostVec = (vec2(0.5) - texcoord) * 1.5f;
//
//     vec3 result = vec3(0.0);
//     for (int i = 0; i < 3; i++) {
//         vec2 offset = fract(texcoord + ghostVec * float(i));
//
//         float weight = length(vec2(0.5) - offset) / length(vec2(0.5));
//         weight = pow(1.0 - weight, 10.0);
//
//         result += textureDistorted(u_BluredImage, offset, ghostVec, vec3(0.0f, 0.2f, 0.4f)).rgb * weight;
//
//         // result += texture(u_BluredImage, offset).rgb * weight;
//     }
//
//     // vec2 haloVec = normalize(ghostVec) * 2.0;
//     // float weight = length(vec2(0.5) - fract(texcoord + haloVec)) / length(vec2(0.5));
//     // weight = pow(1.0 - weight, 5.0);
//     // result += texture(u_BluredImage, texcoord + haloVec).rgb * weight;
//
//     // o_Color = vec4(result, 1.0f);
//
//     //
//     // vec2 ghostVec = vec2(0.5) - texcoord;
//     //
//     // vec4 result = vec4(0.0);
//     // for (int i = 0; i < 2; ++i) {
//     //     vec2 offset = fract(io_UV + ghostVec * float(i));
//     //     result += max(vec4(0.0f), texture(u_ColorImage, offset) - vec4(1.0f));
//     // }
//     //
//     // // o_Color = vec4(result.xyz, 1.0f);
//     vec3 sourceColor = texture(u_ColorImage, io_UV).rgb;
//     // // vec3 bluredColor = texture(u_BluredImage, io_UV).rgb;
//     // //
//     vec3 color = (sourceColor + result.xyz) / 2.0f;
//     color = color / (color + vec3(1.0f));
//     color = pow(color, vec3(1.0f / 2.2f));
//     // //
//     o_Color = vec4(color, 1.0f);
// }
