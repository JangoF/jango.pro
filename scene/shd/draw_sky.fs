#version 300 es
precision highp float;

uniform samplerCube u_Sky;

in vec3 io_Normal;
out vec4 o_Color;

void main() {
    // o_Color = vec4(normalize(io_Normal), 1.0f);
    o_Color = vec4(texture(u_Sky, normalize(io_Normal)).rgb, 1.0f);
}
