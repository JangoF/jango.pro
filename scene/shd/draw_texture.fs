#version 300 es
precision highp float;
precision highp sampler2DShadow;

uniform sampler2DShadow u_Texture;

in vec2 io_UV;
out vec4 o_Color;

void main() {
    vec2 coord = io_UV; // * 2.0f - 1.0f;
    float color = textureProj(u_Texture, vec4(coord.x, coord.y, 1.0f, 1.0f)); //texture(u_Texture, coord);

    o_Color = vec4(vec3(pow(color, 1.0f)), 1.0f);
}
