#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec3 i_Normal;
layout(location = 2) in vec3 i_Binormal;
layout(location = 3) in vec3 i_Tangent;
layout(location = 4) in vec2 i_Texcoord;

uniform mat4 u_Model;
uniform mat4 u_ViewProjection;

void main() {
    gl_Position = u_ViewProjection * u_Model * i_Position;
}
