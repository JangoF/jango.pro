#version 300 es
precision highp float;

in vec4 io_Position;

out vec2 o_Color;

void main() {
    float depth = io_Position.z / io_Position.w * 0.5 + 0.5;

    float moment1 = depth;
    float moment2 = depth * depth;

    float dx = dFdx(depth);
    float dy = dFdy(depth);

    moment2 += 0.25 * (dx * dx + dy * dy);

    o_Color = vec2(moment1, moment2);
}
