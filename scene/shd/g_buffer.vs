#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec3 i_Normal;
layout(location = 2) in vec3 i_Binormal;
layout(location = 3) in vec3 i_Tangent;
layout(location = 4) in vec2 i_Texcoord;

uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;

out vec4 io_Position;
out vec3 io_Normal;
out vec2 io_Texcoord;
out mat3 io_TBN;

void main() {
    io_Position = i_Position;
    io_Normal   = i_Normal;
    io_Texcoord = i_Texcoord;

    io_TBN = mat3(
        normalize(vec3(u_Model * vec4(i_Tangent,  0.0))),
        normalize(vec3(u_Model * vec4(i_Binormal, 0.0))),
        normalize(vec3(u_Model * vec4(i_Normal,   0.0)))
    );

    gl_Position = u_Projection * u_View * u_Model * i_Position;
}
