#version 300 es
precision highp float;
// precision highp sampler2DShadow;

// G_Buffer:

uniform sampler2D u_Position;
uniform sampler2D u_Normal_Origin;
uniform sampler2D u_Normal;
uniform sampler2D u_AlbedoColor;
uniform sampler2D u_EmissiveColor;
uniform sampler2D u_MetallicRoughnessEmissive;
uniform sampler2D u_AO;

// Информация о источнике света:

layout(std140) uniform LIGHT {
    vec4 u_Light_Color_Intensity;
    vec4 u_Light_Position_ShadowOpacity;
};

// uniform vec4 u_Light_Color_Intensity;//6
// uniform vec4 u_Light_Position_ShadowOpacity;//7

// Информация о тени:

uniform samplerCube u_Shadow_Depth;//8

// uniform sampler2DShadow u_Shadow_Depth_XP;
// uniform sampler2DShadow u_Shadow_Depth_XN;

// uniform sampler2DShadow u_Shadow_Depth_YP;
// uniform sampler2DShadow u_Shadow_Depth_YN;

// uniform sampler2DShadow u_Shadow_Depth_ZP;
// uniform sampler2DShadow u_Shadow_Depth_ZN;

// uniform mat4 u_Shadow_View_XP;
// uniform mat4 u_Shadow_View_XN;
//
// uniform mat4 u_Shadow_View_YP;
// uniform mat4 u_Shadow_View_YN;
//
// uniform mat4 u_Shadow_View_ZP;
// uniform mat4 u_Shadow_View_ZN;

// uniform mat4 u_Shadow_Projection;

// Прочая информация:

uniform vec3 u_CameraPosition;//9

in vec2 io_UV;
out vec4 o_Color;

const float PI = 3.14159265359;

vec3 fresnelSchlickEquation(vec3 F0, vec3 V, vec3 H) {
    return F0 + (1.0f - F0) * pow(1.0f - max(dot(V, H), 0.0f), 5.0f);
}

float normalDistributionFunction(vec3 N, vec3 H, float roughness) {
    float A = roughness * roughness;
    return pow(A, 2.0f) / (PI * pow(pow(max(dot(N, H), 0.0f), 2.0f) * (pow(A, 2.0f) - 1.0f) + 1.0f, 2.0f));
}

float geometryFunction(vec3 N, vec3 V, float roughness) {
    float A = roughness * roughness;
    float K = A / 2.0f;

    return max(0.0f, dot(N, V)) / (max(0.0f, dot(N, V)) * (1.0f - K) + K);
}

float smithMethod(vec3 N, vec3 V, vec3 L, float roughness) {
    return geometryFunction(N, V, roughness) * geometryFunction(N, L, roughness);
}

vec3 calculateInRadiance(vec3 surfacePosition, vec3 lightPosition, vec3 lightColor) {
    float distanceToLight = length(lightPosition - surfacePosition);
    return lightColor * (1.0f / (distanceToLight * distanceToLight));
}

vec3 calculateOutRadiance(vec3 cameraPosition, vec3 surfacePosition, vec3 lightPosition, vec3 lightColor, vec3 albedo, vec3 normal, float metallic, float roughness) {
    vec3 N = normalize(normal);
    vec3 V = normalize(cameraPosition - surfacePosition);
    vec3 L = normalize(lightPosition - surfacePosition);
    vec3 H = normalize(V + L);

    float NV = max(dot(N, V), 0.0f);
    float NL = max(dot(N, L), 0.0f);
    float NH = max(dot(N, H), 0.0f);

    vec3 inRadiance = calculateInRadiance(surfacePosition, lightPosition, lightColor);

    vec3 F0 = mix(vec3(0.04f), albedo, metallic);
    vec3 F = fresnelSchlickEquation(F0, V, H);

    float NDF = normalDistributionFunction(N, H, roughness);
    float G   = smithMethod(N, V, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0f * NL * NV + 0.00001f;
    vec3 specular     = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;

    return (kD * (albedo / PI) + specular) * inRadiance * NL;
}

void main() {
    if (texture(u_Position, io_UV).rgb == vec3(0.0f, 0.0f, 0.0f)) {
        o_Color = vec4(vec3(0.0f), 1.0f);
        return;
    }

    vec3 pos = texture(u_Position,      io_UV).rgb;
    vec3 nor = texture(u_Normal,        io_UV).rgb;
    vec3 alb = texture(u_AlbedoColor,   io_UV).rgb;
    vec3 ecl = texture(u_EmissiveColor, io_UV).rgb;
    vec3 aom = texture(u_AO,            io_UV).rgb;

    vec3 fromLightToPos = pos - u_Light_Position_ShadowOpacity.xyz;
    float result = texture(u_Shadow_Depth, fromLightToPos).r;

    float factor = length(fromLightToPos) > result ? 0.0f : 1.0f;

    float met = texture(u_MetallicRoughnessEmissive, io_UV).r;
    float rou = min(max(texture(u_MetallicRoughnessEmissive, io_UV).g, 0.1f), 0.8f);
    float ein = texture(u_MetallicRoughnessEmissive, io_UV).b;

    vec3 color = calculateOutRadiance(u_CameraPosition, pos, u_Light_Position_ShadowOpacity.xyz, vec3(1.0f, 0.5f, 0.0f) * u_Light_Color_Intensity.a, alb, nor, met, rou) * factor;
    o_Color = vec4(color, 1.0f);
    // o_Color = vec4(vec3(texture(u_AO,      io_UV).a), 1.0f);
}
