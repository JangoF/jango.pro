#version 300 es
layout(location = 0) in vec2 i_Position;
layout(location = 1) in vec2 i_UV;

uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;

out vec2 io_UV;

void main() {
    io_UV = i_UV;
    gl_Position = u_Projection * u_View * u_Model * vec4(i_Position, 0.0f, 1.0f);
}
