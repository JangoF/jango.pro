#version 300 es
precision mediump float;

uniform samplerCube u_Texture;
in vec3 io_Normal;
out vec4 o_Color;

void main() {
    o_Color = vec4(texture(u_Texture, io_Normal).rgb, 1.0);
}
