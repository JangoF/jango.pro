#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec2 i_Texcoord;
layout(location = 2) in vec3 i_Normal;

uniform mat4 u_ViewProjection;
uniform mat4 u_ViewProjectionLight;

out vec4 io_Position;
out vec2 io_Texcoord;
out vec3 io_Normal;

out vec4 io_PositionLightSpace;

void main() {
    io_Position = i_Position;
    io_Texcoord = i_Texcoord;
    io_Normal   = i_Normal;

    io_PositionLightSpace = u_ViewProjectionLight * io_Position;
    gl_Position = u_ViewProjection * i_Position;
}
