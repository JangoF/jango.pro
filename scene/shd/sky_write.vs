#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec2 i_Texcoord;

uniform mat3 u_View;

out vec3 io_NormalXP;
out vec3 io_NormalXN;

out vec3 io_NormalYP;
out vec3 io_NormalYN;

out vec3 io_NormalZP;
out vec3 io_NormalZN;

mat3 lookAt(vec3 inverseTarget, vec3 up) {
    vec3 FW = inverseTarget;
    vec3 LF = cross(up, FW);
    vec3 UP = cross(FW, LF);

    return mat3(
        LF.x, UP.x, FW.x,
        LF.y, UP.y, FW.y,
        LF.z, UP.z, FW.z
    );
}

void main() {
    vec3 normal = vec3(i_Texcoord * 2.0 - 1.0, -1.0);

    io_NormalXP = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, -1.0,  0.0)) * normal * u_View;
    io_NormalXN = lookAt(vec3( 1.0, 0.0,  0.0), vec3(0.0, -1.0,  0.0)) * normal * u_View;

    io_NormalYP = lookAt(vec3(0.0,  1.0,  0.0), vec3(0.0,  0.0, -1.0)) * normal * u_View;
    io_NormalYN = lookAt(vec3(0.0, -1.0,  0.0), vec3(0.0,  0.0,  1.0)) * normal * u_View;

    io_NormalZP = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, -1.0,  0.0)) * normal * u_View;
    io_NormalZN = lookAt(vec3(0.0,  0.0,  1.0), vec3(0.0, -1.0,  0.0)) * normal * u_View;

    gl_Position = i_Position;
}
