#version 300 es
precision highp float;
precision highp sampler2DShadow;

uniform sampler2D u_GBufferDepth;
uniform sampler2D u_GBufferPosition;
uniform sampler2D u_GBufferNormal;
uniform sampler2D u_GBufferNormalOriginal;

uniform sampler2DShadow u_ShadowDepthDetail;
uniform sampler2DShadow u_ShadowDepthOverall;

uniform mat4 u_ShadowVPDetail;
uniform mat4 u_ShadowVPOverall;

uniform vec3        u_SkyDirection;
uniform samplerCube u_SkyDiffuse;

in vec2 io_Texcoord;
out vec4 o_Color;

vec4 textureProjectionCustom(vec4 position, mat4 matrix) {
    vec4 result = matrix * position;
    result /= result.w;
    return result * 0.5 + 0.5;
}

float calculateShadowFactor(vec4 position) {
    vec4 positionInSpaceShadowDetail  = textureProjectionCustom(position, u_ShadowVPDetail);
    vec4 positionInSpaceShadowOverall = textureProjectionCustom(position, u_ShadowVPOverall);

    if (positionInSpaceShadowDetail.x < 0.99 &&
        positionInSpaceShadowDetail.x > 0.01 &&
        positionInSpaceShadowDetail.y < 0.99 &&
        positionInSpaceShadowDetail.y > 0.01) {

        positionInSpaceShadowDetail.z -= 0.004;

        int count = 1;
        float result = 0.0;
        for (int x = -count; x <= count; x++) {
            for (int y = -count; y <= count; y++) {
                vec2 offset = vec2(x, y) / vec2(512.0);
                result += textureProj(u_ShadowDepthDetail, positionInSpaceShadowDetail + vec4(offset, 0.0, 0.0));
            }
        }

        return result / (float(count + count + 1) * float(count + count + 1));
    }

    if (positionInSpaceShadowOverall.x < 0.99 &&
        positionInSpaceShadowOverall.x > 0.01 &&
        positionInSpaceShadowOverall.y < 0.99 &&
        positionInSpaceShadowOverall.y > 0.01) {

        positionInSpaceShadowOverall.z -= 0.005;
        return textureProj(u_ShadowDepthOverall, positionInSpaceShadowOverall);
    }

    return 1.0;
}

vec3 calculateDiffuseColor(float depth, vec3 position, vec3 normal) {
    return vec3(max(dot(u_SkyDirection, normal), 0.0));
}

void main() {
    float GBDepth = texture(u_GBufferDepth, io_Texcoord).r;
    if (GBDepth > 0.999) { o_Color = vec4(0.0); return; } // Если в данной точке нет геометрии - просто выходим.

    vec3 GBPosition = texture(u_GBufferPosition, io_Texcoord).rgb;
    vec3 GBNormal = texture(u_GBufferNormal, io_Texcoord).rgb;
    vec3 GBNormalOriginal = texture(u_GBufferNormalOriginal, io_Texcoord).rgb;

    float shadowFactor = dot(u_SkyDirection, GBNormalOriginal) < 0.0 ? 0.0 : calculateShadowFactor(vec4(GBPosition, 1.0));
    vec3 deffuseColor = calculateDiffuseColor(GBDepth, GBPosition, GBNormal);

    vec3 finalColor = deffuseColor * shadowFactor;
    o_Color = vec4(finalColor, 1.0);
}
