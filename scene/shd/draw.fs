#version 300 es
precision highp float;

uniform sampler2D u_Position;
uniform sampler2D u_Normal;
uniform sampler2D u_AlbedoColor;
uniform sampler2D u_EmissiveColor;
uniform sampler2D u_MetallicRoughnessEmissive;
uniform sampler2D u_AO;

uniform vec3 u_CameraPosition;

struct PointLightInfo {
    vec4 lightColor;
    vec4 lightPosition;
};

struct DirectionalLightInfo {
    vec4 lightColor;
    vec4 lightRotation;
};

struct SpotLightInfo {
    vec4 lightColor;
    vec4 lightAngle;
    vec4 lightPosition;
    vec4 lightRotation;
};

layout(std140) uniform ListPointLight {
    PointLightInfo lightPointList[__RV_POINT_LIGHT_COUNT__];
};

layout(std140) uniform ListDirectionalLight {
    DirectionalLightInfo lightDirectionalList[__RV_DIRECTIONAL_LIGHT_COUNT__];
};

layout(std140) uniform ListSpotLight {
    SpotLightInfo lightSpotList[__RV_SPOT_LIGHT_COUNT__];
};

in vec2 io_UV;
out vec4 o_Color;

const float PI = 3.14159265359;

mat3 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c);
}

vec3 fresnelSchlickEquation(vec3 F0, vec3 V, vec3 H) {
    return F0 + (1.0f - F0) * pow(1.0f - max(dot(V, H), 0.0f), 5.0f);
}

float normalDistributionFunction(vec3 N, vec3 H, float roughness) {
    float A = roughness * roughness;
    return pow(A, 2.0f) / (PI * pow(pow(max(dot(N, H), 0.0f), 2.0f) * (pow(A, 2.0f) - 1.0f) + 1.0f, 2.0f));
}

float geometryFunction(vec3 N, vec3 V, float roughness) {
    float A = roughness * roughness;
    float K = A / 2.0f;

    return max(0.0f, dot(N, V)) / (max(0.0f, dot(N, V)) * (1.0f - K) + K);
}

float smithMethod(vec3 N, vec3 V, vec3 L, float roughness) {
    return geometryFunction(N, V, roughness) * geometryFunction(N, L, roughness);
}

vec3 calculateInRadiance(vec3 surfacePosition, vec3 lightPosition, vec3 lightColor) {
    float distanceToLight = length(lightPosition - surfacePosition);
    return lightColor * (1.0f / (distanceToLight * distanceToLight));
}

vec3 calculateOutRadiance(vec3 cameraPosition, vec3 surfacePosition, vec3 lightPosition, vec3 lightColor, vec3 albedo, vec3 normal, float metallic, float roughness) {
    vec3 N = normalize(normal);
    vec3 V = normalize(cameraPosition - surfacePosition);
    vec3 L = normalize(lightPosition - surfacePosition);
    vec3 H = normalize(V + L);

    float NV = max(dot(N, V), 0.0f);
    float NL = max(dot(N, L), 0.0f);
    float NH = max(dot(N, H), 0.0f);

    vec3 inRadiance = calculateInRadiance(surfacePosition, lightPosition, lightColor);

    vec3 F0 = mix(vec3(0.04f), albedo, metallic);
    vec3 F = fresnelSchlickEquation(F0, V, H);

    float NDF = normalDistributionFunction(N, H, roughness);
    float G   = smithMethod(N, V, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0f * NL * NV + 0.00001f;
    vec3 specular     = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;

    return (kD * (albedo / PI) + specular) * inRadiance * NL;
}

vec3 calculateOutRadianceDirectional(vec3 cameraPosition, vec3 surfacePosition, vec3 lightRotation, vec3 lightColor, vec3 albedo, vec3 normal, float metallic, float roughness) {
    vec3 N = normalize(normal);
    vec3 V = normalize(cameraPosition - surfacePosition);
    vec3 L = normalize(-lightRotation);
    vec3 H = normalize(V + L);

    float NV = max(dot(N, V), 0.0f);
    float NL = max(dot(N, L), 0.0f);
    float NH = max(dot(N, H), 0.0f);

    vec3 inRadiance = lightColor;

    vec3 F0 = mix(vec3(0.04f), albedo, metallic);
    vec3 F = fresnelSchlickEquation(F0, V, H);

    float NDF = normalDistributionFunction(N, H, roughness);
    float G   = smithMethod(N, V, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0f * NL * NV + 0.00001f;
    vec3 specular     = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;

    return (kD * (albedo / PI) + specular) * inRadiance * NL;
}

vec3 calculateOutRadianceSpot(vec3 cameraPosition, vec3 surfacePosition, vec2 lightAngle, vec3 lightPosition, vec3 lightRotation, vec3 lightColor, vec3 albedo, vec3 normal, float metallic, float roughness) {

    vec3 lightDirection = normalize(lightRotation);
    vec3 fromLightToSurface = normalize(lightPosition - surfacePosition);
    // return vec3(abs(dot(lightDirection, fromLightToSurface)));

    vec3 N = normalize(normal);
    vec3 V = normalize(cameraPosition - surfacePosition);
    vec3 L = normalize(lightPosition - surfacePosition);
    vec3 H = normalize(V + L);

    float NV = max(dot(N, V), 0.0f);
    float NL = max(dot(N, L), 0.0f);
    float NH = max(dot(N, H), 0.0f);

    vec3 formLightToPoint = normalize(surfacePosition - lightPosition);
    float dotLV_LTP = max(dot(lightRotation, formLightToPoint), 0.0f);

    float innerAngle = max(cos(radians(lightAngle.x / 2.0f)), 0.0f);
    float outerAngle = max(cos(radians(lightAngle.y / 2.0f)), 0.0f);

    float value = smoothstep(outerAngle, innerAngle, dotLV_LTP);
    vec3 inRadiance = value * calculateInRadiance(surfacePosition, lightPosition, lightColor);

    vec3 F0 = mix(vec3(0.04f), albedo, metallic);
    vec3 F = fresnelSchlickEquation(F0, V, H);

    float NDF = normalDistributionFunction(N, H, roughness);
    float G   = smithMethod(N, V, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0f * NL * NV + 0.00001f;
    vec3 specular     = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;

    return (kD * (albedo / PI) + specular) * inRadiance * NL;
}

void main() {
    if (texture(u_Position, io_UV).rgb == vec3(0.0f, 0.0f, 0.0f)) {
        o_Color = vec4(vec3(0.0f), 1.0f);
        return;
    }

    vec3 pos = texture(u_Position,      io_UV).rgb;
    vec3 nor = texture(u_Normal,        io_UV).rgb;
    vec3 alb = texture(u_AlbedoColor,   io_UV).rgb;
    vec3 ecl = texture(u_EmissiveColor, io_UV).rgb;
    vec3 aom = texture(u_AO, io_UV).rgb;

    float met = texture(u_MetallicRoughnessEmissive, io_UV).r;
    float rou = min(max(texture(u_MetallicRoughnessEmissive, io_UV).g, 0.1f), 0.8f);
    float ein = texture(u_MetallicRoughnessEmissive, io_UV).b;

    vec3 outRadiance = vec3(0.0f);
    for (int i = 0; i < __RV_POINT_LIGHT_COUNT__; ++i) {
        outRadiance += calculateOutRadiance(u_CameraPosition, pos, lightPointList[i].lightPosition.xyz, vec3(1.0f, 0.5f, 0.0f) * lightPointList[i].lightColor.a, alb, nor, met, rou);
    }

    for (int i = 0; i < __RV_DIRECTIONAL_LIGHT_COUNT__; ++i) {
        outRadiance += calculateOutRadianceDirectional(u_CameraPosition, pos, lightDirectionalList[i].lightRotation.xyz, lightDirectionalList[i].lightColor.rgb * lightDirectionalList[i].lightColor.a * 0.2f, alb, nor, met, rou);
    }

    for (int i = 0; i < __RV_SPOT_LIGHT_COUNT__; ++i) {
        outRadiance += calculateOutRadianceSpot(u_CameraPosition, pos, lightSpotList[i].lightAngle.xy, lightSpotList[i].lightPosition.xyz, lightSpotList[i].lightRotation.xyz, vec3(1.0f, 0.0f, 0.5f) * lightSpotList[i].lightColor.a * 2.0f, alb, nor, met, rou);
    }

    vec3 color = outRadiance;
    o_Color = vec4(color, 1.0f);
}
