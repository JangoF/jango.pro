#version 300 es
precision highp float;

in vec3 io_Position;
in vec3 io_Normal;
in vec2 io_UV;
in mat3 io_TBN;

uniform vec3 u_CameraPosition;

uniform sampler2D u_BaseColor;
uniform sampler2D u_Metallic;
uniform sampler2D u_Normal;
uniform sampler2D u_Roughness;

out vec4 o_Color;

const float PI = 3.14159265359;
const vec3 u_LightPosition[] = vec3[2]( vec3(-1.0f, 1.0f, 1.0f), vec3(1.0f, -1.0f, -1.0f) );
const vec3 u_LightColor[] = vec3[2]( vec3(2.0f), vec3(8.0f) );

vec3 cook_Torrance(vec3 N, vec3 V, vec3 L, vec3 M, float metallic, float roughness) {
    return vec3(1.0f);
}

void main() {
    vec3 N = normalize(texture(u_Normal, io_UV).rgb);
    vec3 V = normalize(u_CameraPosition - io_Position);

    o_Color = vec4(vec3(1.0f), 1.0f);
}

// void main() {
//     // vec3 N = texture(u_Normal, io_UV).rgb;
//     // N = normalize(N * 2.0f - 1.0f);
//     // N = normalize(io_TBN * N);
//     //
//     // o_Color = vec4(vec3(N), 1.0f);
// }

// void main() {
//     vec3 N = normalize(io_Normal);
//     vec3 V = normalize(u_CameraPosition - io_Position);
//
//     vec3 color = texture(u_Roughness, io_UV).xyz;
//
//     o_Color = vec4(color, 1.0f);
//
//     // o_Color = vec4(vec3(N), 1.0f);
//
//     // float io_Roughness = io_Roughness_T;
//     //
//     // vec3 N = normalize(io_Normal); // Нормаль к поверхности.
//     // vec3 V = normalize(u_CameraPosition - io_Position); // Вектор с поверхности в камеру.
//     //
//     // vec3 Lo = vec3(0.0f); // Итоговое излучение точки.
//     // for (int i = 0; i < 2; ++i) {
//     //     vec3 L = normalize(u_LightPosition[i] - io_Position); // Вектор с поверхности к источнику света.
//     //     vec3 H = normalize(V + L); // Медианный вектор между V и L.
//     //
//     //     float distanceToLight = length(u_LightPosition[i] - io_Position); // Дистанция до источника света.
//     //     float lightAttenuation = 1.0f / (distanceToLight * distanceToLight); // Затухание источника света (обратный квадрат расстояния).
//     //
//     //     vec3 pointRadiance = u_LightColor[i] * lightAttenuation; // Энергетическая Яркость для данного источника в данной точке.
//     //
//     //     vec3 F0 = mix(vec3(0.04), io_Color, io_Metallic);
//     //     vec3 F = fresnelSchlickEquation(max(0.0, dot(H, V)), F0);
//     //
//     //     float NDF = normalDistributionFunction(N, H, io_Roughness);
//     //     float G = smithMethod(N, V, L, io_Roughness);
//     //
//     //     vec3 num = NDF * G * F;
//     //     float denom = 4.0f * max(dot(N, V), 0.0f) * max(dot(N, L), 0.0f) + 0.001f;
//     //     vec3 specular = num / denom;
//     //
//     //     vec3 kS = F;
//     //     vec3 kD = vec3(1.0f) - kS;
//     //
//     //     kD *= 1.0f - io_Metallic;
//     //
//     //     float nDotL = max(dot(N, L), 0.0f);
//     //     Lo += (kD * io_Color / PI + specular) * pointRadiance * nDotL;
//     // }
//     //
//     // vec3 ambient = vec3(0.002f) * io_Color;
//     // vec3 color = ambient + Lo + io_EmissiveColor * io_EmissiveIntencity;
//     //
//     // color = color / (color + vec3(1.0f));
//     // color = pow(color, vec3(1.0f / 2.2f));
//     //
//     // o_Color = vec4(color, 1.0);
// }

// float normalDistributionFunction(vec3 normal, vec3 median, float roughness) {
// // float distributionGGX(vec3 normal, vec3 median, float roughness) {
//     float alpha = roughness * roughness;
//     float v0 = pow(max(dot(normal, median), 0.0f), 2.0f);
//     float v1 = pow(alpha, 2.0f) - 1.0f;
//     float v2 = pow(v0 * v1 + 1.0f, 2.0f);
//
//     return pow(alpha, 2.0f) / (PI * v2);
//
//     // float ndotm = max(dot(normal, median), 0.0f);
//     // float a2 = alpha * alpha;
//     //
//     // float v1 = 1.0f / (PI * a2 * pow(ndotm, 4.0f));
//     // float v2 = exp((pow(ndotm, 2.0f) - 1.0f) / (a2 * pow(ndotm, 2.0f)));
//     //
//     // return v1 * v2;
// }

// float geometryFunction(vec3 normal, vec3 view, float roughness) {
//     float alpha = roughness * roughness;
//     float k = alpha / 2.0f;
//
//     return max(dot(normal, view), 0.0f) / (max(dot(normal, view), 0.0f) * (1.0f - k) + k);
// }
//
//
// float smithMethod(vec3 normal, vec3 view, vec3 light, float roughness) {
//     return geometryFunction(normal, view, roughness) * geometryFunction(normal, light, roughness);
// }
//
//
// vec3 fresnelSchlickEquation(float cosTheta, vec3 F0) {
//     return F0 + (1.0f - F0) * pow(1.0f - cosTheta, 5.0f);
// }

// float distributionGGX(vec3 N, vec3 H, float a) {
//     float a2     = a*a;
//     float NdotH  = max(dot(N, H), 0.0);
//     float NdotH2 = NdotH*NdotH;
//
//     float nom    = a2;
//     float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
//     denom        = PI * denom * denom;
//
//     return nom / denom;
// }

// float distributionGGX(vec3 N, vec3 H, float roughness) {
//     float a = roughness * roughness;
//     float a2 = a * a;
//     float nDotH = max(dot(N, H), 0.0f);
//     float nDotH2 = nDotH * nDotH;
//
//     float num = a2;
//     float denom = nDotH2 * (a2 - 1.0f) + 1.0f;
//     denom = PI * denom * denom;
//
//     return num / denom;
// }

// float geometrySchlickGGX(float nDotV, float roughness) {
//     float r = roughness + 1.0f;
//     float k = r * r / 8.0f;
//
//     float num = nDotV;
//     float denom = nDotV * (1.0f - k) + k;
//
//     return num / denom;
// }

// float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
//     float nDotV = max(dot(N, V), 0.0f);
//     float nDotL = max(dot(N, L), 0.0f);
//
//     float ggx2 = geometrySchlickGGX(nDotV, roughness);
//     float ggx1 = geometrySchlickGGX(nDotL, roughness);
//
//     // return min(min(ggx2, ggx1), 1.0f);
//     return ggx2 * ggx1;
// }

// vec3 fresnelSchlick(float cosTheta, vec3 F0) {
//     return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
// }
//
// float DistributionGGX(vec3 N, vec3 H, float roughness) {
//     float a      = roughness*roughness;
//     float a2     = a*a;
//     float NdotH  = max(dot(N, H), 0.0);
//     float NdotH2 = NdotH*NdotH;
//
//     float num   = a2;
//     float denom = (NdotH2 * (a2 - 1.0) + 1.0);
//     denom = PI * denom * denom;
//
//     return num / denom;
// }
//
// float GeometrySchlickGGX(float NdotV, float roughness) {
//     float r = (roughness + 1.0);
//     float k = (r*r) / 8.0;
//
//     float num   = NdotV;
//     float denom = NdotV * (1.0 - k) + k;
//
//     return num / denom;
// }
//
// float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
//     float NdotV = max(dot(N, V), 0.0);
//     float NdotL = max(dot(N, L), 0.0);
//     float ggx2  = GeometrySchlickGGX(NdotV, roughness);
//     float ggx1  = GeometrySchlickGGX(NdotL, roughness);
//
//     return ggx1 * ggx2;
// }

// void main() {
//     vec3 u_LightColor = vec3(128.0, 128.0, 128.0);
//
//     vec3  albedo = io_Color;//vec3(102.0f / 255.0f, 0.0f, 204.0f / 255.0f);
//     float metallic = io_Metallic;
//     float roughness = io_Roughness_T;
//     float ao = 1.0f;
//
//     vec3 camPos = u_CameraPosition;
//     vec3 Normal = io_Normal;
//     vec3 WorldPos = io_Position;
//
//     vec3 N = normalize(Normal);
//     vec3 V = normalize(camPos - WorldPos);
//
//     vec3 F0 = vec3(0.04);
//     F0 = mix(F0, albedo, metallic);
//
//     // выражение отражающей способности
//     vec3 Lo = vec3(0.0);
//
//     // расчет энергетической яркости для каждого источника света
//     vec3 L = normalize(u_LightPosition - WorldPos);
//     vec3 H = normalize(V + L);
//     float distance    = length(u_LightPosition - WorldPos);
//     float attenuation = 1.0 / (distance * distance);
//     vec3 radiance     = u_LightColor * attenuation;
//
//     // Cook-Torrance BRDF
//     float NDF = DistributionGGX(N, H, roughness);
//     float G   = GeometrySmith(N, V, L, roughness);
//     vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
//
//     vec3 kS = F;
//     vec3 kD = vec3(1.0) - kS;
//     kD *= 1.0 - metallic;
//
//     vec3 numerator    = NDF * G * F;
//     float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
//     vec3 specular     = numerator / max(denominator, 0.001);
//
//     // прибавляем результат к исходящей энергетической яркости Lo
//     float NdotL = max(dot(N, L), 0.0);
//     Lo += (kD * albedo / PI + specular) * radiance * NdotL;
//
//     vec3 ambient = vec3(0.03) * albedo * ao;
//     vec3 color = ambient + Lo + io_EmissiveColor * io_EmissiveIntencity;
//
//     color = color / (color + vec3(1.0));
//     color = pow(color, vec3(1.0/2.2));
//
//     o_Color = vec4(color, 1.0);
// }
