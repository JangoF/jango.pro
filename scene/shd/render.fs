#version 300 es
precision highp float;

in vec2 io_UV;
out vec4 o_Color;

void main() {
    o_Color = vec4(io_UV.x, io_UV.y, 0.0f, 1.0f);
}
