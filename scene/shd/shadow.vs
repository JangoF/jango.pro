#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec2 i_Texcoord;
layout(location = 2) in vec3 i_Normal;

uniform mat4 u_ViewProjection;

out vec4 io_Position;

void main() {
    vec4 result = u_ViewProjection * i_Position;
    io_Position = result;
    gl_Position = result;
}
