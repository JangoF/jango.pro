#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec3 i_Normal;

uniform mat4 u_ViewProjection;
out vec3 io_Normal;

void main() {
    io_Normal = i_Normal;
    gl_Position = u_ViewProjection * i_Position;
}
