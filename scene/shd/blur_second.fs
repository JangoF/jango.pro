#version 300 es
precision highp float;

uniform sampler2D u_Image;
uniform vec3 u_Blur;

in vec2 io_UV;
out vec4 o_Color;

vec4 getColor(vec2 uv) {
    vec3 color = max(vec3(0.0f), texture(u_Image, uv).rgb - vec3(0.0f));
    return vec4(color, 1.0f);
}

#define INV_SQRT_2PI_X3 1.1968268412042980338198381798031

void main() {
 float r = u_Blur.z;

 float exp_value = -4.5 / r / r;
 float sqrt_value = INV_SQRT_2PI_X3 / r;

 float sum = 0.0;
 vec4 value = vec4(0.0);

 float x = 1.0;
 while (x <= r)
 {
  float currentScale = exp(exp_value * x * x);
  sum += currentScale;

  vec2 dudv = u_Blur.xy * x;
  value += currentScale * (getColor(io_UV - dudv) +
                     getColor(io_UV + dudv) );
  x += 1.0;
 }

 float correction = 1.0 / sqrt_value - 2.0 * sum;
 value += getColor(io_UV) * correction;

 // FragColor = value * sqrt_value;
 o_Color = vec4((value * sqrt_value).rgb, 1.0f);
}

// void main() {
//     float r = u_Blur.z;
//
//     float totalScale = 1.0 + r;
//     // vec4 value = texture(u_Image, io_UV) * totalScale;
//     vec4 value = getColor(io_UV) * totalScale;
//
//     float x = 1.0;
//     while (x <= r) {
//         vec2 dudv = u_Blur.xy * x;
//         float scale = 1.0 + r - x;
//         // value += scale * (texture(u_Image, io_UV - dudv) + texture(u_Image, io_UV + dudv));
//         value += scale * (getColor(io_UV - dudv) + getColor(io_UV + dudv));
//         x += 1.0;
//     }
//
//     o_Color = vec4((value / totalScale / totalScale).rgb, 1.0f);
//     // o_Color = vec4(1.0f);
// }
