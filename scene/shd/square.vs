#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec2 i_Texcoord;

out vec2 io_Texcoord;

void main() {
    io_Texcoord = (i_Texcoord + 1.0) * 0.5;
    gl_Position = i_Position;
}
