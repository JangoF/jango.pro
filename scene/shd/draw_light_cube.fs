#version 300 es
precision highp float;

// G_Buffer:

uniform sampler2D u_Position;
uniform sampler2D u_Normal_Origin;
uniform sampler2D u_Normal;
uniform sampler2D u_AlbedoColor;
uniform sampler2D u_EmissiveColor;
uniform sampler2D u_MetallicRoughnessEmissive;
uniform sampler2D u_AO;

// Информация о источнике света:

uniform samplerCube u_LightOriginal;
uniform samplerCube u_LightIrradiance;
uniform samplerCube u_Prefilter;
uniform sampler2D u_BRDF_LUT;

// Прочая информация:

uniform vec3 u_CameraPosition;

in vec2 io_UV;
out vec4 o_Color;

const float PI = 3.14159265359;

vec3 fresnelSchlickEquation(vec3 F0, vec3 V, vec3 H) {
    return F0 + (1.0f - F0) * pow(1.0f - max(dot(V, H), 0.0f), 5.0f);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

float normalDistributionFunction(vec3 N, vec3 H, float roughness) {
    float A = roughness * roughness;
    return pow(A, 2.0f) / (PI * pow(pow(max(dot(N, H), 0.0f), 2.0f) * (pow(A, 2.0f) - 1.0f) + 1.0f, 2.0f));
}

float geometryFunction(vec3 N, vec3 V, float roughness) {
    float A = roughness * roughness;
    float K = A / 2.0f;

    return max(0.0f, dot(N, V)) / (max(0.0f, dot(N, V)) * (1.0f - K) + K);
}

float smithMethod(vec3 N, vec3 V, vec3 L, float roughness) {
    return geometryFunction(N, V, roughness) * geometryFunction(N, L, roughness);
}

vec3 calculateInRadiance(vec3 surfacePosition, vec3 lightPosition, vec3 lightColor) {
    float distanceToLight = length(lightPosition - surfacePosition);
    return lightColor * (1.0f / (distanceToLight * distanceToLight));
}

vec3 calculateOutRadianceDirectional(vec3 cameraPosition, vec3 surfacePosition, vec3 lightDirection, vec3 lightColor, vec3 albedo, vec3 normal, float metallic, float roughness) {
    vec3 N = normalize(normal);
    vec3 V = normalize(cameraPosition - surfacePosition);
    vec3 L = normalize(-lightDirection);
    vec3 H = normalize(V + L);

    float NV = max(dot(N, V), 0.0f);
    float NL = max(dot(N, L), 0.0f);
    float NH = max(dot(N, H), 0.0f);

    vec3 inRadiance = lightColor;

    vec3 F0 = mix(vec3(0.04f), albedo, metallic);
    vec3 F = fresnelSchlickEquation(F0, V, H);

    float NDF = normalDistributionFunction(N, H, roughness);
    float G   = smithMethod(N, V, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0f * NL * NV + 0.00001f;
    vec3 specular     = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;

    return (kD * (albedo / PI) + specular) * inRadiance * NL;
}

void main() {
    if (texture(u_Position, io_UV).rgb == vec3(0.0f, 0.0f, 0.0f)) {
        o_Color = vec4(0.0f);
        return;
    }

    vec3 pos = texture(u_Position,      io_UV).rgb;
    vec3 nor = texture(u_Normal,        io_UV).rgb;
    vec3 alb = texture(u_AlbedoColor,   io_UV).rgb;
    vec3 ecl = texture(u_EmissiveColor, io_UV).rgb;
    vec3 aom = texture(u_AO, io_UV).rgb;

    float met = texture(u_MetallicRoughnessEmissive, io_UV).r;
    float rou = min(max(texture(u_MetallicRoughnessEmissive, io_UV).g, 0.1f), 0.8f);
    float ein = texture(u_MetallicRoughnessEmissive, io_UV).b;

    vec3 N = normalize(nor);
    vec3 V = normalize(u_CameraPosition - pos);

    vec3 F0 = mix(vec3(0.04f), alb, met);

    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, rou);

    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - met;

    vec3 irradiance = texture(u_LightIrradiance, N).rgb;
    vec3 diffuse    = irradiance * alb;

    vec3 R = reflect(-V, N);

    const float MAX_REFLECTION_LOD = 4.0;
    vec3 prefilteredColor = textureLod(u_Prefilter, R, rou * MAX_REFLECTION_LOD).rgb;
    vec2 envBRDF  = texture(u_BRDF_LUT, vec2(max(dot(N, V), 0.0), rou)).rg;
    vec3 specular = prefilteredColor * (F * envBRDF.x + envBRDF.y);

    vec3 ambient = (kD * diffuse + specular) * aom;

    o_Color = vec4(ambient, 1.0f);
}

// void main() {
//     if (texture(u_Position, io_UV).rgb == vec3(0.0f, 0.0f, 0.0f)) {
//         o_Color = vec4(0.0f);
//         return;
//     }
//
//     vec3 pos = texture(u_Position,      io_UV).rgb;
//     vec3 nor = texture(u_Normal,        io_UV).rgb;
//     vec3 alb = texture(u_AlbedoColor,   io_UV).rgb;
//     vec3 ecl = texture(u_EmissiveColor, io_UV).rgb;
//     vec3 aom = texture(u_AO, io_UV).rgb;
//
//     float met = texture(u_MetallicRoughnessEmissive, io_UV).r;
//     float rou = min(max(texture(u_MetallicRoughnessEmissive, io_UV).g, 0.1f), 0.8f);
//     float ein = texture(u_MetallicRoughnessEmissive, io_UV).b;
//
//     vec3 N = normalize(nor);
//     vec3 V = normalize(u_CameraPosition - pos);
//     // vec3 L = normalize(-lightDirection);
//     // vec3 H = normalize(V + L);
//
//     // float NV = max(dot(N, V), 0.0f);
//     // float NL = max(dot(N, L), 0.0f);
//     // float NH = max(dot(N, H), 0.0f);
//
//     // vec3 inRadiance = lightColor;
//
//     vec3 F0 = mix(vec3(0.04f), alb, met);
//
//
//
//     vec3 kS = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, rou);
//     vec3 kD = 1.0 - kS;
//     vec3 irradiance = texture(u_LightIrradiance, nor).rgb;
//     vec3 diffuse    = irradiance * alb;
//     vec3 ambient    = (kD * diffuse) * aom;
//
//     o_Color = vec4(ambient, 1.0f);
//
//     // vec3 skyColor = texture(u_LightIrradiance, nor).rgb;
//     //
//     // o_Color = vec4(skyColor * 0.4 * met * (1.0f - rou), 1.0f);
// }
