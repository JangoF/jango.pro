#version 300 es
precision mediump float;

uniform sampler2D u_Texture;

in vec2 io_Texcoord;
out vec4 o_Color;

void main() {
     o_Color = texture(u_Texture, io_Texcoord);
}

// #version 300 es
// precision mediump float;
//
// uniform sampler2D u_Texture;
// uniform vec3 u_BlurData;
//
// in vec2 io_Texcoord;
// out vec4 o_Color;
//
// #define INV_SQRT_2PI_X3 1.1968268412042980338198381798031
//
// void main() {
//     vec2 TexCoord = io_Texcoord;
//     vec3 texel_radius = u_BlurData;
//
//     float r = texel_radius.z;
//
//      float exp_value = -4.5 / r / r;
//      float sqrt_value = INV_SQRT_2PI_X3 / r;
//
//      float sum = 0.0;
//      vec4 value = vec4(0.0);
//
//      float x = 1.0;
//      while (x <= r)
//      {
//       float currentScale = exp(exp_value * x * x);
//       sum += currentScale;
//
//       vec2 dudv = texel_radius.xy * x;
//       value += currentScale * (texture(u_Texture, TexCoord - dudv) +
//                          texture(u_Texture, TexCoord + dudv) );
//       x += 1.0;
//      }
//
//      float correction = 1.0 / sqrt_value - 2.0 * sum;
//      value += texture(u_Texture, TexCoord) * correction;
//
//      o_Color = value * sqrt_value;
// }

// void main() {
//     vec3 texelData = u_BlurData;
//     float radius = texelData.z;
//
//     float totalScale = 2.0 * radius + 1.0;
//
//     vec4 value = texture(u_Texture, io_Texcoord);
//
//     float x = 1.0;
//     while (x <= radius) {
//         vec2 dudv = texelData.xy * x;
//         value += texture(u_Texture, io_Texcoord - dudv) + texture(u_Texture, io_Texcoord + dudv);
//         x += 1.0;
//     }
//
//     o_Color = value / totalScale;
// }

// void main() {
//     vec3 texelData = u_BlurData;
//     float radius = texelData.z;
//
//     float totalScale = 1.0 + radius;
//
//     vec4 value = texture(u_Texture, io_Texcoord) * totalScale;
//
//     float x = 1.0;
//     while (x <= radius) {
//         vec2 dudv = texelData.xy * x;
//         float scale = 1.0 + radius - x;
//         value += scale * (texture(u_Texture, io_Texcoord - dudv) + texture(u_Texture, io_Texcoord + dudv));
//         x += 1.0;
//     }
//
//     o_Color = value / totalScale / totalScale;
// }

// void main() {
//     // o_Color = vec4(vec3(pow(texture(u_Texture, io_Texcoord).g, 1.0)), 1.0);
//     o_Color = vec4(texture(u_Texture, io_Texcoord).rg, 0.0, 1.0);
// }
