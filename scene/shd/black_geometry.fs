#version 300 es
precision highp float;

out vec4 o_Color;

void main() {
    o_Color = vec4(vec3(0.0), 1.0);
}
