#version 300 es
precision highp float;

uniform sampler2D u_Albedo;
uniform sampler2D u_Metallic;
uniform sampler2D u_Normal;
uniform sampler2D u_Roughness;
uniform sampler2D u_AO;

in vec3 io_Position;
in vec3 io_Normal;
in vec2 io_UV;
in mat3 io_TBN;

out vec4 o_Color[4];

void main() {
    vec3 normal = texture(u_Normal, io_UV).rgb;

    normal = normalize(normal * 2.0f - 1.0f);
    normal = (normalize(io_TBN * normal) + 1.0f) * 0.5f;

    o_Color[0] = vec4(io_Position.xyz, 1.0f);
    o_Color[1] = vec4(normal.xyz, texture(u_AO, io_UV).r);
    o_Color[2] = vec4(texture(u_Albedo, io_UV).rgb, texture(u_Metallic, io_UV).r);
    o_Color[3] = vec4(vec3(0.0f), texture(u_Roughness, io_UV).r);
}
