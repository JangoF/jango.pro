#version 300 es
precision highp float;

uniform sampler2D u_NormalMap;

in vec4 io_Position;
in vec3 io_Normal;
in vec2 io_Texcoord;
in mat3 io_TBN;

out vec4 o_Color[3];

void main() {
    vec3 normal = texture(u_NormalMap, io_Texcoord * 16.0).rgb;

    normal = normalize(normal * 2.0 - 1.0);
    normal = normalize(io_TBN * normal);

    o_Color[0] = vec4(io_Position.xyz, 1.0);
    o_Color[1] = vec4(normal.xyz, 1.0);
    o_Color[2] = vec4(io_Normal.xyz, 1.0);
}
