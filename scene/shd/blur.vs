#version 300 es
layout(location = 0) in vec4 i_Position;
layout(location = 1) in vec2 i_UV;

out vec2 io_UV;

void main() {
    io_UV = i_UV;
    gl_Position = i_Position;
}
