#version 300 es
precision highp float;

uniform sampler2D u_Texture;
uniform sampler2D u_TextureGodRays;
uniform vec2 u_SunPositionOnScreen;

uniform mat4 u_EyeVP;
uniform vec4 u_SunPosition;

uniform vec2 u_Offset;

in vec2 io_Texcoord;
out vec4 o_Color;

vec4 textureProjectionCustom(vec4 position, mat4 matrix) {
    vec4 result = matrix * position;
    result /= result.w;
    return result * 0.5 + 0.5;
}

void main() {
    vec2 sunPositionInScreenSpace = textureProjectionCustom(u_SunPosition, u_EyeVP).xy;
    vec2 deltaTextureCoordinates = vec2(io_Texcoord - sunPositionInScreenSpace);
    vec2 currentTextureCoordinates = io_Texcoord;
    deltaTextureCoordinates /= 128.0;

    float illumination = 1.0;

    for (int i = 0; i < 128; i++) {
        currentTextureCoordinates -= deltaTextureCoordinates;
        vec4 currentSample = texture(u_TextureGodRays, max(min(currentTextureCoordinates, vec2(1.0)), vec2(0.0)));
        currentSample *= illumination * 0.4;
        o_Color += currentSample;
        illumination *= 1.0;
    }

    o_Color *= 0.04;
    o_Color = o_Color + texture(u_Texture, io_Texcoord);

    // o_Color = vec4(vec3(length(io_Texcoord - u_SunPositionOnScreen)), 1.0);

    // float r = texture(u_Texture, vec2(io_Texcoord.x + u_Offset.x, io_Texcoord.y + u_Offset.y)).r;
    // float g = texture(u_Texture, vec2(io_Texcoord.x, io_Texcoord.y)).g;
    // float b = texture(u_Texture, vec2(io_Texcoord.x - u_Offset.x, io_Texcoord.y - u_Offset.y)).b;
    //
    // o_Color = vec4(r, g, b, 1.0);
}
