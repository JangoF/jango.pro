#version 300 es
precision highp float;
precision highp sampler2DShadow;

// G_Buffer:

uniform sampler2D u_Position;
uniform sampler2D u_Normal_Origin;
uniform sampler2D u_Normal;
uniform sampler2D u_AlbedoColor;
uniform sampler2D u_EmissiveColor;
uniform sampler2D u_MetallicRoughnessEmissive;
uniform sampler2D u_AO;

// Информация о источнике света:

layout(std140) uniform LIGHT {
    vec4 u_Light_Color_Intensity;
    vec4 u_Light_Target_ShadowOpacity;
};

// uniform vec4 u_Light_Color_Intensity;
// uniform vec4 u_Light_Target_ShadowOpacity;

// Информация о тени:

uniform sampler2DShadow u_Shadow_Depth;
uniform mat4 u_Shadow_ViewProjection;

// Прочая информация:

uniform vec3 u_CameraPosition;

in vec2 io_UV;
out vec4 o_Color;

const float PI = 3.14159265359;

vec3 fresnelSchlickEquation(vec3 F0, vec3 V, vec3 H) {
    return F0 + (1.0f - F0) * pow(1.0f - max(dot(V, H), 0.0f), 5.0f);
}

float normalDistributionFunction(vec3 N, vec3 H, float roughness) {
    float A = roughness * roughness;
    return pow(A, 2.0f) / (PI * pow(pow(max(dot(N, H), 0.0f), 2.0f) * (pow(A, 2.0f) - 1.0f) + 1.0f, 2.0f));
}

float geometryFunction(vec3 N, vec3 V, float roughness) {
    float A = roughness * roughness;
    float K = A / 2.0f;

    return max(0.0f, dot(N, V)) / (max(0.0f, dot(N, V)) * (1.0f - K) + K);
}

float smithMethod(vec3 N, vec3 V, vec3 L, float roughness) {
    return geometryFunction(N, V, roughness) * geometryFunction(N, L, roughness);
}

vec3 calculateInRadiance(vec3 surfacePosition, vec3 lightPosition, vec3 lightColor) {
    float distanceToLight = length(lightPosition - surfacePosition);
    return lightColor * (1.0f / (distanceToLight * distanceToLight));
}

vec3 calculateOutRadianceDirectional(vec3 cameraPosition, vec3 surfacePosition, vec3 lightDirection, vec3 lightColor, vec3 albedo, vec3 normal, float metallic, float roughness) {
    vec3 N = normalize(normal);
    vec3 V = normalize(cameraPosition - surfacePosition);
    vec3 L = normalize(-lightDirection);
    vec3 H = normalize(V + L);

    float NV = max(dot(N, V), 0.0f);
    float NL = max(dot(N, L), 0.0f);
    float NH = max(dot(N, H), 0.0f);

    vec3 inRadiance = lightColor;

    vec3 F0 = mix(vec3(0.04f), albedo, metallic);
    vec3 F = fresnelSchlickEquation(F0, V, H);

    float NDF = normalDistributionFunction(N, H, roughness);
    float G   = smithMethod(N, V, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0f * NL * NV + 0.00001f;
    vec3 specular     = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0f) - kS;
    kD *= 1.0f - metallic;

    return (kD * (albedo / PI) + specular) * inRadiance * NL;
}

vec4 textureProjectionCustom(vec4 position, mat4 matrix) {
    vec4 result = matrix * position;
    result /= result.w;
    return result * 0.5 + 0.5;
}

float calculateShadowFactor(vec4 position) {
    vec4 positionInSpaceShadowDetail  = textureProjectionCustom(position, u_Shadow_ViewProjection);

    if (positionInSpaceShadowDetail.x < 0.99 &&
        positionInSpaceShadowDetail.x > 0.01 &&
        positionInSpaceShadowDetail.y < 0.99 &&
        positionInSpaceShadowDetail.y > 0.01) {

        // positionInSpaceShadowDetail.z -= 0.0002;

        int count = 1;
        float result = 0.0;
        for (int x = -count; x <= count; x++) {
            for (int y = -count; y <= count; y++) {
                vec2 offset = vec2(x, y) / vec2(512.0);
                result += textureProj(u_Shadow_Depth, positionInSpaceShadowDetail + vec4(offset, 0.0, 0.0));
            }
        }

        return result / (float(count + count + 1) * float(count + count + 1));
    }

    return 1.0;
}

void main() {
    if (texture(u_Position, io_UV).rgb == vec3(0.0f, 0.0f, 0.0f)) {
        o_Color = vec4(vec3(0.0f), 1.0f);
        return;
    }

    vec3 pos = texture(u_Position,      io_UV).rgb;
    vec3 nor = texture(u_Normal,        io_UV).rgb;
    vec3 alb = texture(u_AlbedoColor,   io_UV).rgb;
    vec3 ecl = texture(u_EmissiveColor, io_UV).rgb;
    vec3 aom = texture(u_AO, io_UV).rgb;

    float shadowFactor = calculateShadowFactor(vec4(pos, 1.0));

    float met = texture(u_MetallicRoughnessEmissive, io_UV).r;
    float rou = min(max(texture(u_MetallicRoughnessEmissive, io_UV).g, 0.1f), 0.8f);
    float ein = texture(u_MetallicRoughnessEmissive, io_UV).b;

    vec3 color = calculateOutRadianceDirectional(u_CameraPosition, pos, u_Light_Target_ShadowOpacity.xyz, u_Light_Color_Intensity.rgb * u_Light_Color_Intensity.a * 0.2f, alb, nor, met, rou);
    color *= shadowFactor;
    o_Color = vec4(color, 1.0f);
}
