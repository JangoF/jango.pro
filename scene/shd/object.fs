#version 300 es
precision highp float;

uniform vec3 u_SunVector;
uniform sampler2D u_TexNormal;
uniform sampler2D u_TexShadow;

uniform vec2 u_ShadowMapSize;

in vec4 io_Position;
in vec2 io_Texcoord;
in vec3 io_Normal;

in vec4 io_PositionLightSpace;

out vec4 o_Color;

float texture2DCompare(sampler2D image, vec2 uv, float value) {
    return step(value, texture(image, uv).r);
}

float texture2DShadowLerp(sampler2D image, vec2 imageSize, vec2 uv, float value) {
    vec2 texelSize = vec2(1.0) / imageSize;

    vec2 fractional = fract(uv * imageSize + 0.5);
    vec2 centroidUV = floor(uv * imageSize + 0.5) / imageSize;

    float lb = texture2DCompare(image, centroidUV + texelSize * vec2(0.0, 0.0), value);
    float lt = texture2DCompare(image, centroidUV + texelSize * vec2(0.0, 1.0), value);
    float rb = texture2DCompare(image, centroidUV + texelSize * vec2(1.0, 0.0), value);
    float rt = texture2DCompare(image, centroidUV + texelSize * vec2(1.0, 1.0), value);

    float a = mix(lb, lt, fractional.y);
    float b = mix(rb, rt, fractional.y);
    float c = mix(a,   b, fractional.x);

    return c;
}

float PCF(sampler2D image, vec2 imageSize, vec2 uv, float value) {
    float result = 0.0;

    for (int x = -1; x <= 1; x++) {
        for(int y = -1; y <= 1; y++) {
            vec2 off = vec2(x, y) / imageSize;
            // result += texture2DCompare(image, uv + off, value);
            result += texture2DShadowLerp(image, imageSize, uv + off, value);
        }
    }

    return result / 9.0;
}

void main() {
    vec3 devided = (io_PositionLightSpace.xyz / io_PositionLightSpace.w) * 0.5 + 0.5;

    float depth = texture(u_TexShadow, devided.xy).r;
    float shadow = 1.0;

    if (devided.x < 1.0 && devided.y < 1.0 && devided.x > 0.0 && devided.y > 0.0) {
        // shadow = depth + 0.0008 > devided.z ? 1.0 : 0.2;
        shadow = texture2DShadowLerp(u_TexShadow, u_ShadowMapSize, devided.xy, devided.z - 0.0008);
        // shadow = PCF(u_TexShadow, u_ShadowMapSize, devided.xy, devided.z - 0.01);
        // shadow = PCF(u_TexShadow, u_ShadowMapSize, devided.xy, devided.z - 0.01);
    }

    vec3 deffuse = vec3(max(dot(u_SunVector, io_Normal), 0.0)) + 0.05;
    o_Color = vec4(vec3(deffuse * shadow), 1.0);
}

// #version 300 es
// precision mediump float;
//
// uniform vec3 u_SunVector;
// uniform sampler2D u_TexNormal;
// uniform sampler2D u_TexShadow;
//
// uniform vec2 u_ShadowMapSize;
//
// in vec4 io_Position;
// in vec2 io_Texcoord;
// in vec3 io_Normal;
//
// in vec4 io_PositionLightSpace;
//
// out vec4 o_Color;
//
// float texture2DCompare(sampler2D image, vec2 uv, float value) {
//     return step(value, texture(image, uv).r);
// }
//
// float texture2DShadowLerp(sampler2D image, vec2 imageSize, vec2 uv, float value) {
//     vec2 texelSize = vec2(1.0) / imageSize;
//
//     vec2 fractional = fract(uv * imageSize + 0.5);
//     vec2 centroidUV = floor(uv * imageSize + 0.5) / imageSize;
//
//     float lb = texture2DCompare(image, centroidUV + texelSize * vec2(0.0, 0.0), value);
//     float lt = texture2DCompare(image, centroidUV + texelSize * vec2(0.0, 1.0), value);
//     float rb = texture2DCompare(image, centroidUV + texelSize * vec2(1.0, 0.0), value);
//     float rt = texture2DCompare(image, centroidUV + texelSize * vec2(1.0, 1.0), value);
//
//     float a = mix(lb, lt, fractional.y);
//     float b = mix(rb, rt, fractional.y);
//     float c = mix(a,   b, fractional.x);
//
//     return c;
// }
//
// float PCF(sampler2D image, vec2 imageSize, vec2 uv, float value) {
//     float result = 0.0;
//
//     for (int x = -1; x <= 1; x++) {
//         for(int y = -1; y <= 1; y++) {
//             vec2 off = vec2(x, y) / imageSize;
//             // result += texture2DCompare(image, uv + off, value);
//             result += texture2DShadowLerp(image, imageSize, uv + off, value);
//         }
//     }
//
//     return result / 9.0;
// }

// float linstep(float minimum, float maximum, float value) {
// 	return clamp((value - minimum) / (maximum - minimum), 0.0, 1.0);
// }
//
// float reduceLightBleeding(float p_max, float amount) {
// 	return linstep(amount, 1.0, p_max);
// }
//
// float chebyshevUpperBound(vec2 uv, float value) {
// 	vec2 moments = texture(u_TexShadow, uv.xy).rg;
//
// 	if (moments.x > value)
// 		return 1.0;
//
//     float variance = moments.y - (moments.x * moments.x);
//     variance = max(variance, 0.0005);
//
//     float delta = value - moments.x;
// 	float p_max = smoothstep(0.20, 1.0, variance / (variance + delta * delta));
// 	return reduceLightBleeding(p_max, 0.9);
//     // return variance / (variance + delta * delta);
// }
//
// void main() {
//     vec3 devided = (io_PositionLightSpace.xyz / io_PositionLightSpace.w) * 0.5 + 0.5;
//
//     float shadow = 1.0;
//     if (devided.x < 1.0 && devided.y < 1.0 && devided.x > 0.0 && devided.y > 0.0) {
//         shadow = chebyshevUpperBound(devided.xy, devided.z);
//     }
//
//     vec3 deffuse = vec3(max(dot(u_SunVector, io_Normal), 0.0)) + 0.05;
//     o_Color = vec4(vec3(deffuse * shadow), 1.0);
// }
