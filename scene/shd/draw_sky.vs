#version 300 es
layout(location = 0) in vec3 i_Position;

layout(std140) uniform ORU {
    mat4 some;
};

out vec3 io_Position;

void main() {
    io_Position = i_Position;
    gl_Position = some * vec4(i_Position, 1.0f);
}

// #version 300 es
// layout(location = 0) in vec3 i_Position;
//
// uniform mat4 u_ViewProjection[2];
// out vec3 io_Position;
//
// void main() {
//     io_Position = i_Position;
//     gl_Position = u_ViewProjection[0] * u_ViewProjection[1] * vec4(i_Position, 1.0f);
// }


// #version 300 es
// layout(location = 0) in vec2 i_Position;
// layout(location = 1) in vec2 i_Texture;
//
// uniform mat4 u_ViewProjection;
// out vec2 io_Texture;
//
// void main() {
//     io_Texture = i_Texture;
//     // gl_Position = u_ViewProjection * vec4(i_Position, 0.0f, 1.0f);
//     gl_Position = vec4(i_Position, 0.0f, 1.0f);
// }


// #version 300 es
// layout(location = 0) in vec3 i_Position;
// // layout(location = 1) in vec2 i_Texture;
//
// uniform mat4 u_ViewProjection;
// // out vec2 io_Texture;
// out vec3 io_Position;
//
// void main() {
//     io_Position = i_Position;
//     // io_Texture = i_Texture;
//     gl_Position = u_ViewProjection * vec4(i_Position, 1.0f);
//     // gl_Position = vec4(i_Position, 0.0f, 1.0f);
// }
//
//
// // #version 300 es
// // layout(location = 0) in vec2 i_Position;
// // layout(location = 1) in vec2 i_Texture;
// //
// // uniform mat4 u_ViewProjection;
// // out vec2 io_Texture;
// //
// // void main() {
// //     io_Texture = i_Texture;
// //     // gl_Position = u_ViewProjection * vec4(i_Position, 0.0f, 1.0f);
// //     gl_Position = vec4(i_Position, 0.0f, 1.0f);
// // }
