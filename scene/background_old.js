function background(plane, resource) {

    // Получение холста и контекста:

    const canvas = document.getElementById(plane);
    const gl = canvas.getContext('webgl2');

    // Подключение расширений:

    {
        let EXT_color_buffer_float = gl.getExtension('EXT_color_buffer_float');
        let EXT_texture_filter_anisotropic = gl.getExtension('EXT_texture_filter_anisotropic');

        gl.MAX_TEXTURE_MAX_ANISOTROPY_EXT = EXT_texture_filter_anisotropic.MAX_TEXTURE_MAX_ANISOTROPY_EXT;
        gl.TEXTURE_MAX_ANISOTROPY_EXT = EXT_texture_filter_anisotropic.TEXTURE_MAX_ANISOTROPY_EXT;
    }

    // Указание параметров сцены:

    const value = new function () {
        this.settings = new function () {
            this.resolution = new function () {
                /**/ this.skyBox = new V2(512.0);
                /**/ this.shadowMapDetail = new V2(512.0);
                /**/ this.shadowMapOverall = new V2(512.0);
            }

            this.anisotropy = 16.0;
        }

        this.scene = new function () {
            this.sun = new function () {
                /**/ this.direction = new V3(0.0);
                /**/ this.camera = new SCCameraLookAt(1.0, 64.0, 5.0, -5.0, 5.0, -5.0).setPosition(0.0, 0.0, 1.0).setTarget(0.0, 0.0, 0.0);
                /**/ this.cameraOverall = new SCCameraLookAt(1.0, 55.0, 55.0, -55.0, 55.0, -55.0).setPosition(0.0, 0.0, 1.0).setTarget(0.0, 0.0, 0.0);
            }

            this.eye = new function () {
                /**/ this.cameraRotationMax = new V2(Math.toRadians(-16.0));
                /**/ this.cameraRotationRequired = new V2(0.0);
                /**/ this.camera = new SCCameraAround(0.2, 3072.0, Math.toRadians(60.0), 1.0).setPosition(0.0, 0.0, -8.0);
            }

            this.stars = new function () {
                /**/ this.count = 0.04;
                /**/ this.brightness = 0.1;
            }
        }

        this.canvas = new function () {
            /**/ this.resolution = new V2(0.0);
            /**/ this.center = new V2(0.0);
            /**/ this.resolutionMin = new V2(0.0);
        }
    }

    const scene = new function () {

        this.mesh = new function () {
            /**/ this.square = new GLMesh(gl, new Uint16Array([0, 1, 2, 2, 1, 3]), new Float32Array([-1.0, -1.0, 0.0, 0.0,
                                                                                                      1.0, -1.0, 1.0, 0.0,
                                                                                                     -1.0,  1.0, 0.0, 1.0,
                                                                                                      1.0,  1.0, 1.0, 1.0]), [2, 2]);
        }

        this.shader = new function () {
            /**/ this.square = new GLShader(gl, resource['pbr.vs']);
        }

        this.pipeline = new function (parent) {
            /**/ this.gbuffre = new GLPipeline(gl, resource['g_buffer.vs'], resource['g_buffer.fs']);
            /**/ this.depth = new GLPipeline(gl, resource['depth_draw.vs'], resource['depth_draw.fs']);
            /**/ this.skyWrite = new GLPipeline(gl, resource['sky_write.vs'], resource['sky_write.fs']);
            /**/ this.skyRead = new GLPipeline(gl, resource['sky_read.vs'], resource['sky_read.fs']);
            /**/ this.pbr = new GLPipeline(gl, parent.shader.square, resource['pbr.fs'], true);
            /**/ this.post = new GLPipeline(gl, parent.shader.square, resource['post.fs'], true);
            /**/ this.blackGeometry = new GLPipeline(gl, resource['black_geometry.vs'], resource['black_geometry.fs']);
        }(this)

        this.texture = new function () {
            /**/ this.skybox = new GLTexture(gl, gl.RGB, value.settings.resolution.skyBox.x, gl.LINEAR);
            /**/ this.normal = new GLTexture(gl, resource['normal_plate'], gl.LINEAR);

            this.normal.generateMipmap();
            this.normal.maxAnisotropy = 16;
            this.normal.minFilter = gl.LINEAR_MIPMAP_LINEAR;

            /**/ this.normalDefault = new GLTexture(gl).setTexture(gl.TEXTURE_2D, gl.RGB, 1.0, 1.0, null, gl.LINEAR);
            /**/ this.planetColor = new GLTexture(gl, resource['mars_color'], gl.LINEAR);
            /**/ this.starsColor = new GLTexture(gl).setTexture(gl.TEXTURE_CUBE_MAP, gl.RGB, value.settings.resolution.skyBox.x, [
                makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
                makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
                makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
                makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
                makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
                makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); })
            ], gl.NEAREST);
        }

        this.framebuffer = new function (parent) {
            /**/ this.gbuffer = new GLFramebuffer(gl, [
                [new GLTexture(gl, gl.RGBA32F,            canvas.width, canvas.height, gl.NEAREST), gl.COLOR_ATTACHMENT0], // Позиции.
                [new GLTexture(gl, gl.RGBA32F,            canvas.width, canvas.height, gl.NEAREST), gl.COLOR_ATTACHMENT1], // Нормали.
                [new GLTexture(gl, gl.RGBA32F,            canvas.width, canvas.height, gl.NEAREST), gl.COLOR_ATTACHMENT2], // Исходные нормали.
                [new GLTexture(gl, gl.DEPTH_COMPONENT32F, canvas.width, canvas.height, gl.NEAREST), gl.DEPTH_ATTACHMENT ]
            ]);

            /**/ this.sky = new GLFramebuffer(gl, [
                [parent.texture.skybox, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X],
                [parent.texture.skybox, gl.COLOR_ATTACHMENT1, gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
                [parent.texture.skybox, gl.COLOR_ATTACHMENT2, gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
                [parent.texture.skybox, gl.COLOR_ATTACHMENT3, gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
                [parent.texture.skybox, gl.COLOR_ATTACHMENT4, gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
                [parent.texture.skybox, gl.COLOR_ATTACHMENT5, gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
            ]);

            /**/ this.shadowDetail = new GLFramebuffer(gl, [
                [new GLTexture(gl, gl.DEPTH_COMPONENT32F, value.settings.resolution.shadowMapDetail.x, value.settings.resolution.shadowMapDetail.y, gl.LINEAR), gl.DEPTH_ATTACHMENT]
            ]);

            this.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT).compareMode = gl.COMPARE_REF_TO_TEXTURE;
            this.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT).compareFunc = gl.LEQUAL;

            /**/ this.shadowOverall = new GLFramebuffer(gl, [
                [new GLTexture(gl, gl.DEPTH_COMPONENT32F, value.settings.resolution.shadowMapOverall.x, value.settings.resolution.shadowMapOverall.y, gl.LINEAR), gl.DEPTH_ATTACHMENT]
            ]);

            this.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT).compareMode = gl.COMPARE_REF_TO_TEXTURE;
            this.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT).compareFunc = gl.LEQUAL;

            /**/ this.post = new GLFramebuffer(gl, [
                [new GLTexture(gl, gl.RGB, canvas.width, canvas.height, gl.LINEAR), gl.COLOR_ATTACHMENT0]
            ]);

            /**/ this.godRaysSky = new GLFramebuffer(gl, [
                [new GLTexture(gl, gl.RGB, canvas.width / 4.0, canvas.height / 4.0, gl.LINEAR), gl.COLOR_ATTACHMENT0]
            ]);
        }(this)

        /**/ this.skySphere = makeSceneFromFBX(gl, resource['fbx_sky'])[0];
        /**/ this.object = makeSceneFromFBX(gl, resource['fbx4']);
    }

    // Инициализация событий окна:

    function windowUpdate() {

        // Обновление размеров холста:

        canvas.width = canvas.offsetWidth;
        canvas.height = canvas.offsetHeight;

        // Обновление canvas секции:

        value.canvas.center = new V2(canvas.width * 0.5, canvas.height * 0.5);
        value.canvas.resolution = new V2(canvas.width, canvas.height);
        value.canvas.resolutionMin = Math.min(canvas.width, canvas.height);

        // Обновление scene секции:

        value.scene.eye.camera.setProjection(value.scene.eye.camera._fov, canvas.width / canvas.height);

        // Обновление framebuffer секции:

        scene.framebuffer.gbuffer.resize(canvas.width, canvas.height);
        scene.framebuffer.post.resize(canvas.width, canvas.height);
        scene.framebuffer.godRaysSky.resize(canvas.width / 4.0, canvas.height / 4.0);
    };

    window.addEventListener('resize', windowUpdate);
    windowUpdate();

    // Инициализация событий мыши:

    function mouseUpdate(event) {

        // Множители углов отклонения камеры:

        let factor_x = (event.pageX - value.canvas.center.x) / value.canvas.resolutionMin * 2.0;
        let factor_y = (event.pageY - value.canvas.center.y) / value.canvas.resolutionMin * 2.0;

        // Ограничиваем множители по максимальному и минимальному значению:

        factor_x = Math.max(Math.min(factor_x, 1.5), -1.5);
        factor_y = Math.max(Math.min(factor_y, 1.5), -1.5);

        value.scene.eye.cameraRotationRequired = new V2(value.scene.eye.cameraRotationMax.y * factor_y, value.scene.eye.cameraRotationMax.x * factor_x);
    };

    document.addEventListener('mousemove', mouseUpdate, false);
    document.addEventListener('mouseenter', mouseUpdate, false);

    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function makeStarTexture(size, density, brightness, random) {
        let count = Math.round(size * size * density);
        let data = new Uint8Array(size * size * 3);

        for (let i = 0; i < count; ++i) {
            let r = Math.floor(random() * size * size);
            let c = Math.round(255 * Math.log(1 - random()) * -brightness);

            data[r * 3 + 0] = c;
            data[r * 3 + 1] = c;
            data[r * 3 + 2] = c;
        }

        return data;
    }

    // Инициализация переодических событий:

var a = 0;
var TIME = 0.0;
var NEED_UPDATE = true

    function sunUpdate() {
        if (NEED_UPDATE == false) {
            return;
        }
        else {
            NEED_UPDATE = false
        }

        a += 0.0005;
        // Обновление вектора солнца:

        let day = new Date();
        day = (day.getSeconds() * 1000 + day.getMilliseconds()) / 120000;

        day = (44 + TIME) / 60;

        value.scene.sun.direction = new V3(Math.sin(Math.toRadians(360.0) * day * 4.0), Math.cos(Math.toRadians(360.0) * day * 4.0) + 0.5, -1).normalized;
        value.scene.sun.camera.setPosition(value.scene.sun.direction.mul(48.0));
        value.scene.sun.cameraOverall.setPosition(value.scene.sun.direction.mul(48.0));

        scene.framebuffer.sky.bind();
        gl.viewport(0, 0, scene.texture.skybox.size.x, scene.texture.skybox.size.y);

        gl.disable(gl.DEPTH_TEST);
        gl.disable(gl.CULL_FACE);

        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.clear(gl.DEPTH_BUFFER_BIT);

        gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1, gl.COLOR_ATTACHMENT2, gl.COLOR_ATTACHMENT3, gl.COLOR_ATTACHMENT4, gl.COLOR_ATTACHMENT5]);
        scene.pipeline.skyWrite.bind(
            new M3(Math.(1.5708 * 0.2)),
            scene.texture.planetColor,
            scene.texture.starsColor,
            value.scene.sun.direction,
            new V3(0.0, RADIUS_PALANET + 2.0, DISTANCE_CAMERA),
            new V4(RADIUS_PALANET, RADIUS_PALANET + RADIUS_ATMOSPHERE, HEIGHT_RAY, HEIGHT_MIE),
            new V3(K_RAY_R, K_RAY_G, K_RAY_B),
            new V3(20e-8, 20e-8, 20e-8),
            new V4(1.0, 1.0, 1.0, INTENSITY),
            VALUE_MIE
        );
        scene.mesh.square.draw();

        scene.framebuffer.shadowDetail.bind()
        gl.viewport(0, 0, scene.framebuffer.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT).size.x, scene.framebuffer.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT).size.y);

        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.clear(gl.DEPTH_BUFFER_BIT);

        gl.enable(gl.DEPTH_TEST);
        gl.enable(gl.CULL_FACE);

        gl.drawBuffers([gl.NONE]);

        scene.pipeline.depth.bind(new M4(), value.scene.sun.camera.viewProjection);

        for (let i = 0; i < scene.object.length; i++) {
            scene.pipeline.depth.setUniform(0, scene.object[i].world);
            scene.object[i].draw();
        }

        // ----

        scene.framebuffer.shadowOverall.bind()
        gl.viewport(0, 0, scene.framebuffer.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT).size.x, scene.framebuffer.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT).size.y);

        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.clear(gl.DEPTH_BUFFER_BIT);

        gl.enable(gl.DEPTH_TEST);
        gl.enable(gl.CULL_FACE);

        gl.drawBuffers([gl.NONE]);

        scene.pipeline.depth.bind(new M4(), value.scene.sun.cameraOverall.viewProjection);

        for (let i = 0; i < scene.object.length; i++) {
            scene.pipeline.depth.setUniform(0, scene.object[i].world);
            scene.object[i].draw();
        }
    };

    setInterval(sunUpdate, 256);
    requestAnimationFrame(sunUpdate);

    function eyeUpdate() {
        let rotation_x = value.scene.eye.camera.rotation.x + (value.scene.eye.cameraRotationRequired.x - value.scene.eye.camera.rotation.x) * 0.1;
        let rotation_y = value.scene.eye.camera.rotation.y + (value.scene.eye.cameraRotationRequired.y - value.scene.eye.camera.rotation.y) * 0.1;

        value.scene.eye.camera.setRotation(rotation_x, rotation_y);
        document.getElementById('elips').style.transform = value.scene.eye.camera.toCSS(canvas.width, canvas.height).css;
        document.getElementById('elips').style.visibility = 'hidden';
    }

    setInterval(eyeUpdate, 16);
    requestAnimationFrame(eyeUpdate);

    function makeSlider(name, callback) {
        var range = document.getElementById(name);
        range.addEventListener('input', function () { NEED_UPDATE = true; callback(range); }, false);
    }

    var RADIUS_PALANET = 3400e3;
    var RADIUS_ATMOSPHERE = 100e3;

    var HEIGHT_RAY = 25e3;
    var HEIGHT_MIE = 2e3;

    var VALUE_MIE = -0.75;
    var INTENSITY = 15.0;

    var DISTANCE_CAMERA = 3700000.0;

    var K_RAY_R = 100e-8;
    var K_RAY_G = 230e-8;
    var K_RAY_B = 400e-8;

    makeSlider('range_0', function (range) {
        RADIUS_ATMOSPHERE = range.value * 200e3;
        console.log(RADIUS_ATMOSPHERE);
    });

    makeSlider('range_1', function (range) {
        HEIGHT_RAY = range.value * 100e3;
        console.log(HEIGHT_RAY);
    });

    makeSlider('range_2', function (range) {
        HEIGHT_MIE = range.value * 100e3;
        console.log(HEIGHT_MIE);
    });

    makeSlider('range_3', function (range) {
        VALUE_MIE = -(0.75 + (range.value * (0.999 - 0.75)));
        console.log(VALUE_MIE);
    });

    makeSlider('range_4', function (range) {
        INTENSITY = range.value * 20.0;
        console.log(INTENSITY);
    });

    makeSlider('range_5', function (range) {
        TIME = (range.value - 0.5) * 10.0;
        console.log(TIME);
    });

    makeSlider('range_6', function (range) {
        DISTANCE_CAMERA = range.value * 10000000.0;
        console.log(DISTANCE_CAMERA);
    });

    makeSlider('range_7', function (range) {
        console.log('RADIUS_ATMOSPHERE', RADIUS_ATMOSPHERE, '\n', 'HEIGHT_RAY', HEIGHT_RAY, '\n', 'HEIGHT_MIE', HEIGHT_MIE, '\n', 'VALUE_MIE', VALUE_MIE, '\n', 'INTENSITY', INTENSITY, '\n', 'TIME', TIME, '\n', 'DISTANCE_CAMERA', DISTANCE_CAMERA);
    });

    makeSlider('range_8', function (range) {
        // K_RAY_R = (range.value + 0.5) * 100e-8;
        console.log(K_RAY_R);
    });

    makeSlider('range_9', function (range) {
        // K_RAY_G = (range.value + 0.5) * 230e-8;
        console.log(K_RAY_G);
    });

    makeSlider('range_10', function (range) {
        // K_RAY_B = (range.value + 0.5) * 400e-8;
        console.log(K_RAY_B);
    });

    // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // Функция рисования:

    gl.clearDepth(1.0);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    this.draw = function () {

        { // Заполняем GBuffer:
            scene.framebuffer.gbuffer.bind();
            gl.viewport(0, 0, scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT0).size.x, scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT0).size.y);
            gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1, gl.COLOR_ATTACHMENT2]);

            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.CULL_FACE);

            gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
            scene.pipeline.gbuffre.bind(
                new M4(),
                value.scene.eye.camera.view,
                value.scene.eye.camera.projection,
                scene.texture.normal
            );

            scene.skySphere.draw();
            for (let i = 0; i < scene.object.length; i++) {
                scene.pipeline.gbuffre.setUniform(0, scene.object[i].world);
                scene.object[i].draw();
            }
        }

        { // Рисуем текстуру для лучей Бога:
            scene.framebuffer.godRaysSky.bind();
            gl.viewport(0, 0, scene.framebuffer.godRaysSky.getAttachment(gl.COLOR_ATTACHMENT0).size.x, scene.framebuffer.godRaysSky.getAttachment(gl.COLOR_ATTACHMENT0).size.y);
            gl.drawBuffers([gl.COLOR_ATTACHMENT0]);

            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.CULL_FACE);

            gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

            scene.pipeline.skyRead.bind(
                value.scene.eye.camera.viewProjection,
                scene.framebuffer.sky.getAttachment(gl.COLOR_ATTACHMENT0)
            );
            scene.skySphere.draw();

            scene.pipeline.blackGeometry.bind(
                new M4(),
                value.scene.eye.camera.view,
                value.scene.eye.camera.projection
            );
            for (let i = 0; i < scene.object.length; i++) {
                scene.pipeline.blackGeometry.setUniform(0, scene.object[i].world);
                scene.object[i].draw();
            }
        }

        { // Рисуем небо из текстуры:
            scene.framebuffer.post.bind();
            gl.viewport(0, 0, scene.framebuffer.post.getAttachment(gl.COLOR_ATTACHMENT0).size.x, scene.framebuffer.post.getAttachment(gl.COLOR_ATTACHMENT0).size.y);
            gl.drawBuffers([gl.COLOR_ATTACHMENT0]);

            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.CULL_FACE);

            gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

            scene.pipeline.skyRead.bind(
                value.scene.eye.camera.viewProjection,
                scene.framebuffer.sky.getAttachment(gl.COLOR_ATTACHMENT0)
            );
            scene.skySphere.draw();
        }

        { // Рассчитываем цвет пикселей:
            scene.framebuffer.post.bind();
            // gl.viewport(0, 0, scene.framebuffer.post.getAttachment(gl.COLOR_ATTACHMENT0).size.x, scene.framebuffer.post.getAttachment(gl.COLOR_ATTACHMENT0).size.y);
            gl.viewport(0, 0, value.canvas.resolution.x, value.canvas.resolution.y);
            gl.drawBuffers([gl.COLOR_ATTACHMENT0]);

            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.CULL_FACE);

            gl.enable(gl.BLEND);
            gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

            scene.pipeline.pbr.bind(
                scene.framebuffer.gbuffer.getAttachment(gl.DEPTH_ATTACHMENT),
                scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT0),
                scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT1),
                scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT2),

                scene.framebuffer.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT),
                scene.framebuffer.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT),

                value.scene.sun.camera.viewProjection,
                value.scene.sun.cameraOverall.viewProjection,

                value.scene.sun.direction,
                scene.texture.skybox
            );

            scene.mesh.square.draw();
        }

        { // Производим пост-процессинг и выводим результат на экран:
            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            gl.viewport(0, 0, value.canvas.resolution.x, value.canvas.resolution.y);
            gl.drawBuffers([gl.BACK]);

            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.CULL_FACE);

            gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

            let sunPosition = new V4(value.scene.sun.direction.mul(48.0));
            sunPosition._raw[3] = 1.0;

            let eyeCameraViewProjection = value.scene.eye.camera.viewProjection;

            let sunPositionInEyeViewProjectionSpace = eyeCameraViewProjection.mul(sunPosition);

            let sunPositionInEyeViewProjectionSpaceDividedWComponent = sunPositionInEyeViewProjectionSpace.div(sunPositionInEyeViewProjectionSpace.w);

            let sunPositionInEyeScreenSpace = new V2(sunPositionInEyeViewProjectionSpaceDividedWComponent.mul(0.5).sum(0.5));

            // console.log(sunPosition._raw);

            scene.pipeline.post.bind(
                scene.framebuffer.post.getAttachment(gl.COLOR_ATTACHMENT0),
                // scene.framebuffer.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT),
                scene.framebuffer.godRaysSky.getAttachment(gl.COLOR_ATTACHMENT0),
                sunPositionInEyeScreenSpace,
                value.scene.eye.camera.viewProjection,
                sunPosition,
                [1 / canvas.width * 0, 1 / canvas.height * 0]
            );
            scene.mesh.square.draw();
        }
    }
}
