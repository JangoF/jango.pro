function scene_background(plane, resource) {

    // Получение холста и контекста:

    const canvas = document.getElementById(plane);
    const gl = canvas.getContext('webgl2');

    // Подключение расширений:

    {
        let EXT_color_buffer_float = gl.getExtension('EXT_color_buffer_float');
        let EXT_texture_filter_anisotropic = gl.getExtension('EXT_texture_filter_anisotropic');

        gl.MAX_TEXTURE_MAX_ANISOTROPY_EXT = EXT_texture_filter_anisotropic.MAX_TEXTURE_MAX_ANISOTROPY_EXT;
        gl.TEXTURE_MAX_ANISOTROPY_EXT = EXT_texture_filter_anisotropic.TEXTURE_MAX_ANISOTROPY_EXT;
    }

    // Создание объектов сцены:

    const scene = new function () {

        // this.mesh = new function () {
        //     this.square = new GLMesh(gl, new Uint16Array([0, 1, 2, 2, 1, 3]), new Float32Array([-1.0, -1.0, 0.0, 0.0,
        //                                                                                          1.0, -1.0, 1.0, 0.0,
        //                                                                                         -1.0,  1.0, 0.0, 1.0,
        //                                                                                          1.0,  1.0, 1.0, 1.0]), [2, 2]);
        // }
        //
        // this.shader = new function () {
        //     /**/ this.square = new GLShader(gl, resource['pbr.vs']);
        // }

        this.pipeline = new function (parent) {
            /**/ this.planet = new GLPipeline(gl, resource['planet.vs'], resource['planet.fs']);
            // /**/ this.depth = new GLPipeline(gl, resource['depth_draw.vs'], resource['depth_draw.fs']);
            // /**/ this.skyWrite = new GLPipeline(gl, resource['sky_write.vs'], resource['sky_write.fs']);
            // /**/ this.skyRead = new GLPipeline(gl, resource['sky_read.vs'], resource['sky_read.fs']);
            // /**/ this.pbr = new GLPipeline(gl, parent.shader.square, resource['pbr.fs'], true);
            // /**/ this.post = new GLPipeline(gl, parent.shader.square, resource['post.fs'], true);
            // /**/ this.blackGeometry = new GLPipeline(gl, resource['black_geometry.vs'], resource['black_geometry.fs']);
        }(this)

        // this.texture = new function () {
        //     /**/ this.skybox = new GLTexture(gl, gl.RGB, value.settings.resolution.skyBox.x, gl.LINEAR);
        //     /**/ this.normal = new GLTexture(gl, resource['normal_plate'], gl.LINEAR);
        //
        //     this.normal.generateMipmap();
        //     this.normal.maxAnisotropy = 16;
        //     this.normal.minFilter = gl.LINEAR_MIPMAP_LINEAR;
        //
        //     /**/ this.normalDefault = new GLTexture(gl).setTexture(gl.TEXTURE_2D, gl.RGB, 1.0, 1.0, null, gl.LINEAR);
        //     /**/ this.planetColor = new GLTexture(gl, resource['mars_color'], gl.LINEAR);
        //     /**/ this.starsColor = new GLTexture(gl).setTexture(gl.TEXTURE_CUBE_MAP, gl.RGB, value.settings.resolution.skyBox.x, [
        //         makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
        //         makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
        //         makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
        //         makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
        //         makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); }),
        //         makeStarTexture(value.settings.resolution.skyBox.x, value.scene.stars.count, value.scene.stars.brightness, function() { return Math.random(); })
        //     ], gl.NEAREST);
        // }
        //
        // this.framebuffer = new function (parent) {
        //     /**/ this.gbuffer = new GLFramebuffer(gl, [
        //         [new GLTexture(gl, gl.RGBA32F,            canvas.width, canvas.height, gl.NEAREST), gl.COLOR_ATTACHMENT0], // Позиции.
        //         [new GLTexture(gl, gl.RGBA32F,            canvas.width, canvas.height, gl.NEAREST), gl.COLOR_ATTACHMENT1], // Нормали.
        //         [new GLTexture(gl, gl.RGBA32F,            canvas.width, canvas.height, gl.NEAREST), gl.COLOR_ATTACHMENT2], // Исходные нормали.
        //         [new GLTexture(gl, gl.DEPTH_COMPONENT32F, canvas.width, canvas.height, gl.NEAREST), gl.DEPTH_ATTACHMENT ]
        //     ]);
        //
        //     /**/ this.sky = new GLFramebuffer(gl, [
        //         [parent.texture.skybox, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X],
        //         [parent.texture.skybox, gl.COLOR_ATTACHMENT1, gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
        //         [parent.texture.skybox, gl.COLOR_ATTACHMENT2, gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
        //         [parent.texture.skybox, gl.COLOR_ATTACHMENT3, gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
        //         [parent.texture.skybox, gl.COLOR_ATTACHMENT4, gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
        //         [parent.texture.skybox, gl.COLOR_ATTACHMENT5, gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
        //     ]);
        //
        //     /**/ this.shadowDetail = new GLFramebuffer(gl, [
        //         [new GLTexture(gl, gl.DEPTH_COMPONENT32F, value.settings.resolution.shadowMapDetail.x, value.settings.resolution.shadowMapDetail.y, gl.LINEAR), gl.DEPTH_ATTACHMENT]
        //     ]);
        //
        //     this.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT).compareMode = gl.COMPARE_REF_TO_TEXTURE;
        //     this.shadowDetail.getAttachment(gl.DEPTH_ATTACHMENT).compareFunc = gl.LEQUAL;
        //
        //     /**/ this.shadowOverall = new GLFramebuffer(gl, [
        //         [new GLTexture(gl, gl.DEPTH_COMPONENT32F, value.settings.resolution.shadowMapOverall.x, value.settings.resolution.shadowMapOverall.y, gl.LINEAR), gl.DEPTH_ATTACHMENT]
        //     ]);
        //
        //     this.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT).compareMode = gl.COMPARE_REF_TO_TEXTURE;
        //     this.shadowOverall.getAttachment(gl.DEPTH_ATTACHMENT).compareFunc = gl.LEQUAL;
        //
        //     /**/ this.post = new GLFramebuffer(gl, [
        //         [new GLTexture(gl, gl.RGB, canvas.width, canvas.height, gl.LINEAR), gl.COLOR_ATTACHMENT0]
        //     ]);
        //
        //     /**/ this.godRaysSky = new GLFramebuffer(gl, [
        //         [new GLTexture(gl, gl.RGB, canvas.width / 4.0, canvas.height / 4.0, gl.LINEAR), gl.COLOR_ATTACHMENT0]
        //     ]);
        // }(this)

        /**/ this.object = makeSceneFromFBX(gl, resource['planet.fbx']);
    }

    // Функция рисования:

        gl.clearDepth(1.0);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    let camera = new SCCameraLookAt(1.0, 64.0, 90.0, 1.0).setPosition(0.0, 0.0, -4.0).setTarget(0.0, 0.0, 0.0);

    this.draw = function () {
        gl.viewport(0, 0, 1024, 1024);

        // gl.enable(gl.DEPTH_TEST);
        gl.disable(gl.CULL_FACE);

        gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

        scene.pipeline.planet.bind(
            new M4(),
            camera.view,
            camera.projection
        );

        for (let i = 0; i < scene.object.length; i++) {
            // scene.pipeline.gbuffre.setUniform(0, scene.object[i].world);
            scene.object[i].draw();
        }

        // { // Заполняем GBuffer:
        //     scene.framebuffer.gbuffer.bind();
        //     gl.viewport(0, 0, scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT0).size.x, scene.framebuffer.gbuffer.getAttachment(gl.COLOR_ATTACHMENT0).size.y);
        //     gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1, gl.COLOR_ATTACHMENT2]);
        //
        //     gl.enable(gl.DEPTH_TEST);
        //     gl.disable(gl.CULL_FACE);
        //
        //     gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
        //     scene.pipeline.gbuffre.bind(
        //         new M4()
        //     );
        //
        //     scene.skySphere.draw();
        //     for (let i = 0; i < scene.object.length; i++) {
        //         scene.pipeline.gbuffre.setUniform(0, scene.object[i].world);
        //         scene.object[i].draw();
        //     }
        // }
    }
}
