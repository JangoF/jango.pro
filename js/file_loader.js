// Кажый параметр описания списка ресурсов должен быть объектом вида: { path: string, names: [array] }

function loadFiles(shaderListDescription = [], meshListDescription = [], textureListDescription = [], finishCallback) {

    let loadedResourcesCount = 0;
    let loadedResources = {};

    if (shaderListDescription.length > 0 && shaderListDescription.path[shaderListDescription.path.length - 1] != '/') {
        shaderListDescription.path += '/'
    }

    if (meshListDescription.length > 0 && meshListDescription.path[meshListDescription.path.length - 1] != '/') {
        meshListDescription.path += '/'
    }

    if (textureListDescription.length > 0 && textureListDescription.path[textureListDescription.path.length - 1] != '/') {
        textureListDescription.path += '/'
    }

    // Загрузка шейдеров

    for (let i = 0; i < shaderListDescription.names.length; i++) {
        let loadRequest = new XMLHttpRequest();
        loadRequest.onload = function () {
            loadedResourcesCount += 1;
            loadedResources[shaderListDescription.names[i]] = this.responseText;
            tryFinish();
        };

        loadRequest.open('GET', shaderListDescription.path + shaderListDescription.names[i]);
        loadRequest.send();
    }

    // Загрузка мешей:

    for (let i = 0; i < meshListDescription.names.length; i++) {
        let loadRequest = new XMLHttpRequest();
        loadRequest.onload = function () {
            loadedResourcesCount += 1;
            loadedResources[meshListDescription.names[i]] = this.responseText;
            tryFinish();
        };

        loadRequest.open('GET', meshListDescription.path + meshListDescription.names[i]);
        loadRequest.send();
    }

    // Загрузка текстур:

    for (let i = 0; i < textureListDescription.names.length; i++) {
        let array = textureListDescription.names[i].split('.');

        if (array[array.length - 1] == 'hdr') {
            let loadRequest = new XMLHttpRequest();
            loadRequest.onload = function () {
                loadedResourcesCount += 1;
                loadedResources[textureListDescription.names[i]] = new Uint8Array(this.response);
                tryFinish();
            };

            loadRequest.responseType = "arraybuffer";
            loadRequest.open('GET', textureListDescription.path + textureListDescription.names[i]);
            loadRequest.send();

        } else {
            let image = new Image();
            image.onload = function () {
                loadedResourcesCount += 1;
                loadedResources[textureListDescription.names[i]] = image;
                tryFinish();
            };

            image.src = textureListDescription.path + textureListDescription.names[i];
        }
    }

    // Следующая функция будет вызвана после завершения загрузки каждого из ресурсов:

    function tryFinish() {
        if (loadedResourcesCount !== shaderListDescription.names.length + meshListDescription.names.length + textureListDescription.names.length) {
            return
        }
        
        finishCallback(loadedResources)
    }
}
