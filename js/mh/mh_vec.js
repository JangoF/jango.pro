class VEC {
    constructor() {
        this._raw = [];

        if (arguments[0] instanceof VEC) {
            this._raw = arguments[0]._raw.clone();
        }
        else if (arguments[0] instanceof Array) {
            this._raw = arguments[0].clone();
        }
        else {
            this._raw = arguments.clone();
        }
    }

    clone() {
        return new VEC(this);
    }

    equal() {
        let vector = arguments[0] instanceof VEC ? arguments[0] : new VEC(...arguments);

        if (this._raw.length != vector._raw.length) {
            return false;
        }

        for (let i = 0; i < vector._raw.length; i++) {
            if (this._raw[i] != vector._raw[i]) {
                return false;
            }
        }

        return true;
    }

    sub() {
        let vector = arguments[0] instanceof VEC ? arguments[0] : new VEC(...arguments);
        let result = new VEC(this);

        for (let i = 0; i < Math.min(this._raw.length, vector._raw.length); i++) {
            result._raw[i] -= vector._raw[i];
        }

        return result;
    }

    sum() {
        let vector = arguments[0] instanceof VEC ? arguments[0] : new VEC(...arguments);
        let result = new VEC(this);

        for (let i = 0; i < Math.min(this._raw.length, vector._raw.length); i++) {
            result._raw[i] += vector._raw[i];
        }

        return result;
    }

    mul() {
        let vector = arguments[0] instanceof VEC ? arguments[0] : new VEC(...arguments);
        let result = new VEC(this);

        for (let i = 0; i < Math.min(this._raw.length, vector._raw.length); i++) {
            result._raw[i] *= vector._raw[i];
        }

        return result;
    }

    div() {
        let vector = arguments[0] instanceof VEC ? arguments[0] : new VEC(...arguments);
        let result = new VEC(this);

        for (let i = 0; i < Math.min(this._raw.length, vector._raw.length); i++) {
            result._raw[i] /= vector._raw[i];
        }

        return result;
    }

    crs() {
        let vector = arguments[0] instanceof VEC ? arguments[0] : new VEC(...arguments);
        return new VEC(this._raw[1] * vector._raw[2] - this._raw[2] * vector._raw[1],
                       this._raw[2] * vector._raw[0] - this._raw[0] * vector._raw[2],
                       this._raw[0] * vector._raw[1] - this._raw[1] * vector._raw[0]);
    }

    get dimension() {
        return this._raw.length;
    }
}

Array.prototype.clone = function () {
    return this.slice(0);
};
