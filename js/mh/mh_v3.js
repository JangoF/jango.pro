class V3 extends V1 {

    // Все методы требующие вектор в качестве аргумента, могут принимать его в одном из следующих видов:

    // V2           - Недостающие элементы будут заполнены нулями.
    // V3
    // V4           - Лишние элементы будут отброшены.

    // [x]          - Данным числом будут заполнены все элементы.
    // [x, y]       - Недостающие элементы будут заполнены нулями.
    // [x, y, z]
    // [x, y, z, w] - Лишние элементы будут отброшены.

    // x            - Данным числом будут заполнены все элементы.
    // x, y         - Недостающие элементы будут заполнены нулями.
    // x, y, z
    // x, y, z, w   - Лишние элементы будут отброшены.

    constructor() {
        super(arguments, 3);
    }

    // Возвращает новый объект V3 идентичный текущему:

    clone() {
        return new V3(this);
    }

    // Проверяет, идентичен ли заданный вектор текущему:

    isEqual() {
        if (arguments[0] instanceof V2 || arguments[0] instanceof V4) {
            return false;
        }

        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return this._raw[0] === vector._raw[0] && this._raw[1] === vector._raw[1] && this._raw[2] === vector._raw[2];
    }

    // Производит покомпонентное сложение векторов:

    sum() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return new V3(this._raw[0] + vector._raw[0], this._raw[1] + vector._raw[1], this._raw[2] + vector._raw[2]);
    }

    // Производит покомпонентное вычитание векторов:

    sub() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return new V3(this._raw[0] - vector._raw[0], this._raw[1] - vector._raw[1], this._raw[2] - vector._raw[2]);
    }

    // Производит покомпонентное умножение векторов:

    mul() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return new V3(this._raw[0] * vector._raw[0], this._raw[1] * vector._raw[1], this._raw[2] * vector._raw[2]);
    }

    // Производит покомпонентное деление векторов:

    div() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return new V3(this._raw[0] / vector._raw[0], this._raw[1] / vector._raw[1], this._raw[2] / vector._raw[2]);
    }

    // Возвращает скалярное произведение для текущего и заданного векторов:

    dot() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return this._raw[0] * vector._raw[0] + this._raw[1] * vector._raw[1] + this._raw[2] * vector._raw[2];
    }

    // Возвращает вектор противоположный, по направлению, текущему:

    inv() {
        return new V3(-this._raw[0], -this._raw[1], -this._raw[2]);
    }

    // Возвращает вектор идентичный по длине текущему, но имеющий положительное направление по всем осям:

    abs() {
        return new V3(Math.abs(this._raw[0]), Math.abs(this._raw[1]), Math.abs(this._raw[2]));
    }

    // Превращает текущий вектор в набор величин вращения представленных в радианах:

    toRadians() {
        return new V3(Math.toRadians(this._raw[0]), Math.toRadians(this._raw[1]), Math.toRadians(this._raw[2]));
    }

    // Производит перекрёстное умножение, предполагая что мы работаем в ЛЕВОЙ системе координат:

    crsL() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return new V3(this.z * vector.y - this.y * vector.z, this.x * vector.z - this.z * vector.x, this.y * vector.x - this.x * vector.y);
    }

    // Производит перекрёстное умножение, предполагая что мы работаем в ПРАВОЙ системе координат:

    crsR() {
        let vector = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        return new V3(this.y * vector.z - this.z * vector.y, this.z * vector.x - this.x * vector.z, this.x * vector.y - this.y * vector.x);
    }
}
