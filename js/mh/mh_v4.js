class V4 extends V1 {

    // Все методы требующие вектор в качестве аргумента, могут принимать его в одном из следующих видов:

    // V2           - Недостающие элементы будут заполнены нулями.
    // V3           - Недостающие элементы будут заполнены нулями.
    // V4

    // [x]          - Данным числом будут заполнены все элементы.
    // [x, y]       - Недостающие элементы будут заполнены нулями.
    // [x, y, z]    - Недостающие элементы будут заполнены нулями.
    // [x, y, z, w]

    // x            - Данным числом будут заполнены все элементы.
    // x, y         - Недостающие элементы будут заполнены нулями.
    // x, y, z      - Недостающие элементы будут заполнены нулями.
    // x, y, z, w

    constructor() {
        super(arguments, 4);
    }

    // Возвращает новый объект V4 идентичный текущему:

    clone() {
        return new V4(this);
    }

    // Проверяет, идентичен ли заданный вектор текущему:

    isEqual() {
        if (arguments[0] instanceof V2 || arguments[0] instanceof V3) {
            return false;
        }

        let vector = arguments[0] instanceof V4 ? arguments[0] : new V4(...arguments);
        return this._raw[0] === vector._raw[0] && this._raw[1] === vector._raw[1] && this._raw[2] === vector._raw[2] && this._raw[3] === vector._raw[3];
    }

    // Производит покомпонентное сложение векторов:

    sum() {
        let vector = arguments[0] instanceof V4 ? arguments[0] : new V4(...arguments);
        return new V4(this._raw[0] + vector._raw[0], this._raw[1] + vector._raw[1], this._raw[2] + vector._raw[2], this._raw[3] + vector._raw[3]);
    }

    // Производит покомпонентное вычитание векторов:

    sub() {
        let vector = arguments[0] instanceof V4 ? arguments[0] : new V4(...arguments);
        return new V4(this._raw[0] - vector._raw[0], this._raw[1] - vector._raw[1], this._raw[2] - vector._raw[2], this._raw[3] - vector._raw[3]);
    }

    // Производит покомпонентное умножение векторов:

    mul() {
        let vector = arguments[0] instanceof V4 ? arguments[0] : new V4(...arguments);
        return new V4(this._raw[0] * vector._raw[0], this._raw[1] * vector._raw[1], this._raw[2] * vector._raw[2], this._raw[3] * vector._raw[3]);
    }

    // Производит покомпонентное деление векторов:

    div() {
        let vector = arguments[0] instanceof V4 ? arguments[0] : new V4(...arguments);
        return new V4(this._raw[0] / vector._raw[0], this._raw[1] / vector._raw[1], this._raw[2] / vector._raw[2], this._raw[3] / vector._raw[3]);
    }

    // Возвращает скалярное произведение для текущего и заданного векторов:

    dot() {
        let vector = arguments[0] instanceof V4 ? arguments[0] : new V4(...arguments);
        return this._raw[0] * vector._raw[0] + this._raw[1] * vector._raw[1] + this._raw[2] * vector._raw[2] + this._raw[3] * vector._raw[3];
    }

    // Возвращает вектор противоположный, по направлению, текущему:

    inv() {
        return new V4(-this._raw[0], -this._raw[1], -this._raw[2], -this._raw[3]);
    }

    // Возвращает вектор идентичный по длине текущему, но имеющий положительное направление по всем осям:

    abs() {
        return new V4(Math.abs(this._raw[0]), Math.abs(this._raw[1]), Math.abs(this._raw[2]), Math.abs(this._raw[3]));
    }

    // Превращает текущий вектор в набор величин вращения представленных в радианах:

    toRadians() {
        return new V4(Math.toRadians(this._raw[0]), Math.toRadians(this._raw[1]), Math.toRadians(this._raw[2]), Math.toRadians(this._raw[3]));
    }
}
