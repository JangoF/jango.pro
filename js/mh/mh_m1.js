class M1 {

    // Аргумент list конструктора может быть одного из следующих видов:

    // [M2]
    // [M3]
    // [M4]

    // [x]
    // [x, ... n]

    // [[x]]
    // [[x, ... n]]

    // [[A, ... An]   - Где A - любой из следующих объектов: V2, V3, V4, [x ... n].
    // [[A, ... An]] - Где A - любой из следующих объектов: V2, V3, V4, [x ... n].

    // Универсальный конструктор принимающий на вход массив аргументов а так же требуемую размерность вектора:

    constructor(list, dimension) {
        this._makeUnitMatrix(dimension); // Создаём единичную матрицу.

        if (list[0] instanceof M1) { // Если на вход мы получили матрицу (любой размерности).
            this._fillFromAnyMatrix(dimension, list[0]);
        }
        else if (list[0] && isNaN(list[0]) == false) { // Если на вход мы получили набор чисел (любой длины):
            this._fillFromOneDimensionArray([...list]);
        }
        else if (list[0] instanceof Array) { // Если на вход мы получили массив (любой длины):
            this._fillFromOneDimensionArray(list[0]);
        }
        else if (list.length > 1 && (list[0] instanceof Array || list[0] instanceof V1)) { // Если на вход мы получили набор векторов или массивов чисел (любой длины):
            this._fillFromTwoDimensionArray(dimension, ...list);
        }
        else if (list[0] instanceof Array && (list[0][0] instanceof Array || list[0][0] instanceof V1)) { // Если на вход мы получили массив векторов или массивов чисел (любой длины):
            this._fillFromTwoDimensionArray(dimension, ...list[0][0]);
        }
    }

    // Создаёт единичную матрицу:

    _makeUnitMatrix(dimension) {
        this._raw = [];

        for (let i = 0; i < dimension; i++) {
            for (let j = 0; j < dimension; j++) {
                this._raw[i * dimension + j] = i == j ? 1 : 0;
            }
        }
    }

    // Размещает заданную матрицу, любой размерности (обрезая её до указанной), поверх текущей:

    _fillFromAnyMatrix(dimension, matrix) {
        let size = Math.sqrt(matrix._raw.length);
        let subs = dimension - size;
        let data = matrix._raw;

        let j = 1;
        for (let i = 1; i < this._raw.length + 1 && j < data.length + 1; i++) {
            this._raw[i - 1] = data[j - 1];

            if (subs > 0) {
                if (j % size == 0) {
                    i += Math.abs(subs);
                }
            }
            else if (subs < 0) {
                if (i % dimension == 0) {
                    j += Math.abs(subs);
                }
            }

            j += 1;
        }
    }

    // Размещает заданный массив поверх текущей матрицы. Причём, если длина массива равна единице - вся матрица будет заполнена этим значением:

    _fillFromOneDimensionArray(array) {
        if (array.length == 1) {
            this._raw.fill(array[0]);
            return;
        }

        this._raw = this._raw.placed(0, array, this._raw.length);
    }

    // Конструирует матрицу заданной размерности из некоторого количества объектов типа A (где A это один из следующих типов: V2, V3, V4, [x, ... n]):

    _fillFromTwoDimensionArray(dimension, /* A, ... An */) {
        let array = [];

        for (let i = 1; i < arguments.length; i++) {
            if (typeof(arguments[i]) == 'undefined' || arguments[i] == null) {
                break;
            }

            array.push(arguments[i] instanceof V1 ? arguments[i].raw : arguments[i]);
        }

        for (let i = 0; i < dimension * dimension; i++) {
            let floor = Math.floor(i / dimension);
            let value = array[floor][i - floor * dimension];

            if (typeof(value) !== 'undefined' && value !== null) {
                this._raw[i] = value;
            }
        }
    }

    // Производит умножение текущей матрицы, заданной размерности, на указанный набор данных, заданной размерности:

    _mul(dimension, size, data) {
        let result = [];

        for (let i = 0; i < dimension; i++) {
            for (let j = 0; j < size; j++) {

                let value = 0;
                for (let k = 0; k < dimension; k++) {
                    value += this._raw[i * dimension + k] * data[j + size * k];
                }

                result.push(value);
            }
        }

        return result;
    }

    // Возвращает текущую матрицу, представленную в виде строки:

    toString() {
        let finalString = '(';
        let dimension = Math.floor(Math.sqrt(this._raw.length));

        for (let i = 0; i < this._raw.length; i++) {
            if (finalString.last !== '(' && finalString.last !== '\n') {
                finalString += ', ';
            }

            finalString += String(this._raw[i]);

            if ((i + 1) % dimension === 0) {
                finalString += ')\n';
            }
        }

        return finalString;
    }

    // Преобразовывает матрицу (любой размерности), сначала в четырёхмерную матрицу, а затем в строку пригодную для внедрения в CSS:

    get css() {
        return 'matrix3d(' + (this._raw.length == 16 ? this._raw : new M4(this)._raw).join(',') + ')';
    }

    // Возвращает матрицу в виде одномерного массива:

    get raw() {
        return this._raw.clone();
    }
}
