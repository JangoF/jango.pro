class M2 extends M1 {

    // Конструктор может принимать на вход любой из следующих наборов данных:

    // M2           - Элементы будут размещены поверх единичной матрицы.
    // M3
    // M4           - Лишние элементы будут отброшены.

    // x            - Данным числом будут заполнены все элементы.
    // x, ... n     - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены.

    // [x]          - Данным числом будут заполнены все элементы.
    // [x, ... n]   - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены.

    // A, ... An    - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены (где A - любой из следующих объектов: V2, V3, V4, [x ... n]).
    // [A, ... An]  - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены (где A - любой из следующих объектов: V2, V3, V4, [x ... n]).

    constructor() {
        super(arguments, 2);
    }

    // Возвращает новый объект M4 идентичный текущему:

    clone() {
        return new M2(this._raw);
    }

    // Производит умножение текущей матрицы на матрицу (или то из чего её можно сконструировать) либо на вектор (или то из чего его можно сконструировать):

    mul() {
        let size = null;
        let data = null;

        if (arguments[0] instanceof V1 && arguments.length == 1) { // Если на вход мы получили вектор (любой размерности).
            size = 1;
            data = arguments[0] instanceof V2 ? arguments[0]._raw : new V2(arguments[0])._raw;
        }
        else if (arguments[0] instanceof M1 && arguments.length == 1) { // Если на вход мы получили матрицу (любой размерности).
            size = 2;
            data = arguments[0] instanceof M2 ? arguments[0]._raw : new M2(arguments[0])._raw;
        }
        else if (arguments[0] instanceof Array && arguments[0].length <= 2 || arguments.length <= 2) { // Если на вход мы получили то из чего можно собрать вектор.
            size = 1;
            data = new V2(...arguments)._raw;
        }
        else { // Если на вход мы получили то из чего можно собрать матрицу.
            size = 2;
            data = new M2(...arguments)._raw;
        }

        let result = super._mul(2, size, data);

        if (result.length == 2) {
            return new V2(result);
        }
        else if (result.length == 4) {
            return new M2(result);
        }
    }

    // Возвращает транспонированную версию текущей матрицы:

    get transposed() {
        return new M2(
            this._raw[0], this._raw[2],
            this._raw[1], this._raw[3]
        );
    }
}
