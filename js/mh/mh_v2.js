class V2 extends V1 {

    // Все методы требующие вектор в качестве аргумента, могут принимать его в одном из следующих видов:

    // V2
    // V3           - Лишние элементы будут отброшены.
    // V4           - Лишние элементы будут отброшены.

    // [x]          - Данным числом будут заполнены все элементы.
    // [x, y]
    // [x, y, z]    - Лишние элементы будут отброшены.
    // [x, y, z, w] - Лишние элементы будут отброшены.

    // x            - Данным числом будут заполнены все элементы.
    // x, y
    // x, y, z      - Лишние элементы будут отброшены.
    // x, y, z, w   - Лишние элементы будут отброшены.

    constructor() {
        super(arguments, 2);
    }

    // Возвращает новый объект V3 идентичный текущему:

    clone() {
        return new V2(this);
    }

    // Проверяет, идентичен ли заданный вектор текущему:

    isEqual() {
        if (arguments[0] instanceof V3 || arguments[0] instanceof V4) {
            return false;
        }

        let vector = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        return this._raw[0] === vector._raw[0] && this._raw[1] === vector._raw[1];
    }

    // Производит покомпонентное сложение векторов:

    sum() {
        let vector = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        return new V2(this._raw[0] + vector._raw[0], this._raw[1] + vector._raw[1]);
    }

    // Производит покомпонентное вычитание векторов:

    sub() {
        let vector = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        return new V2(this._raw[0] - vector._raw[0], this._raw[1] - vector._raw[1]);
    }

    // Производит покомпонентное умножение векторов:

    mul() {
        let vector = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        return new V2(this._raw[0] * vector._raw[0], this._raw[1] * vector._raw[1]);
    }

    // Производит покомпонентное деление векторов:

    div() {
        let vector = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        return new V2(this._raw[0] / vector._raw[0], this._raw[1] / vector._raw[1]);
    }

    // Возвращает скалярное произведение для текущего и заданного векторов:

    dot() {
        let vector = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        return this._raw[0] * vector._raw[0] + this._raw[1] * vector._raw[1];
    }

    // Возвращает вектор противоположный, по направлению, текущему:

    inv() {
        return new V2(-this._raw[0], -this._raw[1]);
    }

    // Возвращает вектор идентичный по длине текущему, но имеющий положительное направление по всем осям:

    abs() {
        return new V2(Math.abs(this._raw[0]), Math.abs(this._raw[1]));
    }

    // Превращает текущий вектор в набор величин вращения представленных в радианах:

    toRadians() {
        return new V2(Math.toRadians(this._raw[0]), Math.toRadians(this._raw[1]));
    }
}
