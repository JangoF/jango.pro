// Все матрицы представлены в транспонированном виде!

// Структура базисных осей в WebGL:

// x.x x.y x.z 0
// y.x y.y y.z 0
// z.x z.y z.z 0
// p.x p.y p.z 1

// Формирует матрицу перемещения:

Math.translation = function (x, y, z) {
    return new M4([
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        x, y, z, 1
    ]);
};

// Формирует матрицу вращения сразу по трём осям и порядком вращения X -> Y -> Z:

Math.rotation = function (x, y, z) {
    return Math.rotationZ(z).mul(Math.rotationY(y).mul(Math.rotationX(x)));
};

// Формирует матрицу вращения вокруг оси X:

Math.rotationX = function (angle) {
    const c = Math.cos(angle);
    const s = Math.sin(angle);

    return new M4([
        1, 0, 0, 0,
        0, c,-s, 0,
        0, s, c, 0,
        0, 0, 0, 1
    ]);
};

// Формирует матрицу вращения вокруг оси Y:

Math.rotationY = function (angle) {
    const c = Math.cos(angle);
    const s = Math.sin(angle);

    return new M4([
        c, 0, s, 0,
        0, 1, 0, 0,
       -s, 0, c, 0,
        0, 0, 0, 1
    ]);
};

// Формирует матрицу вращения вокруг оси Z:

Math.rotationZ = function (angle) {
    const c = Math.cos(angle);
    const s = Math.sin(angle);

    return new M4([
        c,-s, 0, 0,
        s, c, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    ]);
};

// Формирует матрицу масштабирования:

Math.scaling = function (x, y, z) {
    return new M4([
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1
    ]);
};

// Формирует матрицу вида исходя из позиции камеры, цели камеры и вектора up для данной камеры:

Math.lookAt = function(position, target, up) {
    let F = target.sub(position).normalized;
    let R = F.crsL(up).normalized;
    let U = R.crsL(F);

    let P = position.inv();

    return new M4([
        R.x,      U.x,      F.x,      0.0,
        R.y,      U.y,      F.y,      0.0,
        R.z,      U.z,      F.z,      0.0,
        R.dot(P), U.dot(P), F.dot(P), 1.0,
    ]);
};

// Формирует матрицу перспективной проекции:

Math.perspective = function(near, far, fov, aspect) {
    let ctgHalfFov = Math.ctg(fov * 0.5);
    let farSubNear = far - near;

    let x = ctgHalfFov / aspect;
    let y = ctgHalfFov;

    let a = (far + near) / farSubNear;
    let b = (-2.0 * far * near) / farSubNear;

    return new M4([
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, a, 1,
        0, 0, b, 0
    ]);
};

// Формирует матрицу ортогональной проекции:

Math.orthogonal = function(right, left, top, bottom, near, far) {
    const rsl = right - left;
    const tsb = top - bottom;
    const fsn = far - near;

    const tx = -(right + left)   / rsl;
    const ty = -(top   + bottom) / tsb;
    const tz = -(far   + near)   / fsn;

    return new M4([
        2 / rsl, 0,        0,       0,
        0,       2 / tsb,  0,       0,
        0,       0,        2 / fsn, 0,
        tx,      ty,       tz,      1
    ]);
};

// Формирует матрицу перспективной проекции, применяемую в CSS:

Math.perspectiveCSS = function(width, height, fov) {
    const result = height * 0.5 / Math.tan(fov * 0.5);

    return new M4([
        1, 0, 0,  0,
        0, 1, 0,  0,
        0, 0, 1, -1 / result,
        0, 0, 0,  1
    ]);
};

// Вспомогательные функции:

Math.ctg = function (angle) {
    return 1.0 / Math.tan(angle);
};

Math.toRadians = function(degrees) {
    return Math.PI / 180.0 * degrees;
};

Math.toDegrees = function(radians) {
    return 180.0 / Math.PI * radians;
};
