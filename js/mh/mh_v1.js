class V1 {

    // Аргумент list конструктора может быть одного из следующих видов:

    // [V2]
    // [V3]
    // [V4]

    // [[x]]
    // [[x, y]]
    // [[x, y, z]]
    // [[x, y, z, w]]

    // [x]
    // [x, y]
    // [x, y, z]
    // [x, y, z, w]

    // Универсальный конструктор принимающий на вход массив аргументов а так же требуемую размерность вектора:

    constructor(list, dimension) {

        // Создаёт массив заданной размерности, заполненный нулями.

        this._raw = [].resized(dimension, 0);

        if (list[0] instanceof V1) { // Если на вход мы получили вектор (любой размерности):
            this._raw = this._raw.placed(0, list[0]._raw, dimension);
        }
        else if (list[0] instanceof Array) { // Если на вход мы получили массив (любого размера):
            this._fillFromArray(list[0], dimension);
        }
        else if (list.length != 0) {  // Если на вход мы получили список аргументов (в любом количестве):
            this._fillFromArray([...list], dimension);
        }
    }

    // Конструирует вектор заданной размерности из массива. Причём, если длина массива равна единице - весь вектор будет заполнен этим значением:

    _fillFromArray(array, dimension) {
        if (array.length == 1) {
            this._raw.fill(array[0]);
        }

        this._raw = this._raw.placed(0, array, dimension);
    }

    // Возвращает текущий вектор, представленный в виде строки:

    toString() {
        let finalString = '(';

        for (let i = 0; i < this._raw.length; i++) {
            finalString += String(this._raw[i]);
        }

        return finalString + ')';
    }

    // Возвращает вектор в виде одномерного массива:

    get raw() {
        return this._raw.clone();
    }

    // Возвращает длину вектора, возведённую в квадрат:

    get lengthSquared() {
        return this._raw.reduce((accumulator, currentValue) => accumulator + currentValue * currentValue, 0);
    }

    // Возвращает длину вектора:

    get length() {
        return Math.sqrt(this.lengthSquared);
    }

    // Возвращает нормализованную версию текущего вектора:

    get normalized() {
        return this.div(this.length);
    }

    // Позволяет установить X компоненту вектора:

    set x(x) {
        this._raw[0] = x;
        return this;
    }

    // Позволяет установить Y компоненту вектора:

    set y(y) {
        this._raw[1] = y;
        return this;
    }

    // Позволяет установить Z компоненту вектора, если позволяет размерность вектора:

    set z(z) {
        if (this._raw.length > 2) {
            this._raw[2] = z;
        }
        return this;
    }

    // Позволяет установить W компоненту вектора, если позволяет размерность вектора:

    set w(w) {
        if (this._raw.length > 3) {
            this._raw[3] = w;
        }
        return this;
    }

    // Следующие функции ползволяют получить любой вектор, размерностью до четырёх, состоящий из комбинации компонентов текущего вектора.
    // При этом, если компонент вектора недоступен - вместо него будет подставлен ноль:

    get x() { return this._raw[0]; }
    get y() { return this._raw[1]; }
    get z() { return this._raw[2] || 0; }
    get w() { return this._raw[3] || 0; }

    get xx() { return new V2(this._raw[0], this._raw[0]); }
    get xy() { return new V2(this._raw[0], this._raw[1]); }
    get xz() { return new V2(this._raw[0], this._raw[2] || 0); }
    get xw() { return new V2(this._raw[0], this._raw[3] || 0); }

    get yx() { return new V2(this._raw[1], this._raw[0]); }
    get yy() { return new V2(this._raw[1], this._raw[1]); }
    get yz() { return new V2(this._raw[1], this._raw[2] || 0); }
    get yw() { return new V2(this._raw[1], this._raw[3] || 0); }

    get zx() { return new V2(this._raw[2] || 0, this._raw[0]); }
    get zy() { return new V2(this._raw[2] || 0, this._raw[1]); }
    get zz() { return new V2(this._raw[2] || 0, this._raw[2] || 0); }
    get zw() { return new V2(this._raw[2] || 0, this._raw[3] || 0); }

    get wx() { return new V2(this._raw[3] || 0, this._raw[0]); }
    get wy() { return new V2(this._raw[3] || 0, this._raw[1]); }
    get wz() { return new V2(this._raw[3] || 0, this._raw[2] || 0); }
    get ww() { return new V2(this._raw[3] || 0, this._raw[3] || 0); }


    get xxx() { return new V3(this._raw[0], this._raw[0], this._raw[0]); }
    get xxy() { return new V3(this._raw[0], this._raw[0], this._raw[1]); }
    get xxz() { return new V3(this._raw[0], this._raw[0], this._raw[2] || 0); }
    get xxw() { return new V3(this._raw[0], this._raw[0], this._raw[3] || 0); }

    get xyx() { return new V3(this._raw[0], this._raw[1], this._raw[0]); }
    get xyy() { return new V3(this._raw[0], this._raw[1], this._raw[1]); }
    get xyz() { return new V3(this._raw[0], this._raw[1], this._raw[2] || 0); }
    get xyw() { return new V3(this._raw[0], this._raw[1], this._raw[3] || 0); }

    get xzx() { return new V3(this._raw[0], this._raw[2] || 0, this._raw[0]); }
    get xzy() { return new V3(this._raw[0], this._raw[2] || 0, this._raw[1]); }
    get xzz() { return new V3(this._raw[0], this._raw[2] || 0, this._raw[2] || 0); }
    get xzw() { return new V3(this._raw[0], this._raw[2] || 0, this._raw[3] || 0); }

    get xwx() { return new V3(this._raw[0], this._raw[3] || 0, this._raw[0]); }
    get xwy() { return new V3(this._raw[0], this._raw[3] || 0, this._raw[1]); }
    get xwz() { return new V3(this._raw[0], this._raw[3] || 0, this._raw[2] || 0); }
    get xww() { return new V3(this._raw[0], this._raw[3] || 0, this._raw[3] || 0); }


    get yxx() { return new V3(this._raw[1], this._raw[0], this._raw[0]); }
    get yxy() { return new V3(this._raw[1], this._raw[0], this._raw[1]); }
    get yxz() { return new V3(this._raw[1], this._raw[0], this._raw[2] || 0); }
    get yxw() { return new V3(this._raw[1], this._raw[0], this._raw[3] || 0); }

    get yyx() { return new V3(this._raw[1], this._raw[1], this._raw[0]); }
    get yyy() { return new V3(this._raw[1], this._raw[1], this._raw[1]); }
    get yyz() { return new V3(this._raw[1], this._raw[1], this._raw[2] || 0); }
    get yyw() { return new V3(this._raw[1], this._raw[1], this._raw[3] || 0); }

    get yzx() { return new V3(this._raw[1], this._raw[2] || 0, this._raw[0]); }
    get yzy() { return new V3(this._raw[1], this._raw[2] || 0, this._raw[1]); }
    get yzz() { return new V3(this._raw[1], this._raw[2] || 0, this._raw[2] || 0); }
    get yzw() { return new V3(this._raw[1], this._raw[2] || 0, this._raw[3] || 0); }

    get ywx() { return new V3(this._raw[1], this._raw[3] || 0, this._raw[0]); }
    get ywy() { return new V3(this._raw[1], this._raw[3] || 0, this._raw[1]); }
    get ywz() { return new V3(this._raw[1], this._raw[3] || 0, this._raw[2] || 0); }
    get yww() { return new V3(this._raw[1], this._raw[3] || 0, this._raw[3] || 0); }


    get zxx() { return new V3(this._raw[2] || 0, this._raw[0], this._raw[0]); }
    get zxy() { return new V3(this._raw[2] || 0, this._raw[0], this._raw[1]); }
    get zxz() { return new V3(this._raw[2] || 0, this._raw[0], this._raw[2] || 0); }
    get zxw() { return new V3(this._raw[2] || 0, this._raw[0], this._raw[3] || 0); }

    get zyx() { return new V3(this._raw[2] || 0, this._raw[1], this._raw[0]); }
    get zyy() { return new V3(this._raw[2] || 0, this._raw[1], this._raw[1]); }
    get zyz() { return new V3(this._raw[2] || 0, this._raw[1], this._raw[2] || 0); }
    get zyw() { return new V3(this._raw[2] || 0, this._raw[1], this._raw[3] || 0); }

    get zzx() { return new V3(this._raw[2] || 0, this._raw[2] || 0, this._raw[0]); }
    get zzy() { return new V3(this._raw[2] || 0, this._raw[2] || 0, this._raw[1]); }
    get zzz() { return new V3(this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get zzw() { return new V3(this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get zwx() { return new V3(this._raw[2] || 0, this._raw[3] || 0, this._raw[0]); }
    get zwy() { return new V3(this._raw[2] || 0, this._raw[3] || 0, this._raw[1]); }
    get zwz() { return new V3(this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get zww() { return new V3(this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0); }


    get wxx() { return new V3(this._raw[3] || 0, this._raw[0], this._raw[0]); }
    get wxy() { return new V3(this._raw[3] || 0, this._raw[0], this._raw[1]); }
    get wxz() { return new V3(this._raw[3] || 0, this._raw[0], this._raw[2] || 0); }
    get wxw() { return new V3(this._raw[3] || 0, this._raw[0], this._raw[3] || 0); }

    get wyx() { return new V3(this._raw[3] || 0, this._raw[1], this._raw[0]); }
    get wyy() { return new V3(this._raw[3] || 0, this._raw[1], this._raw[1]); }
    get wyz() { return new V3(this._raw[3] || 0, this._raw[1], this._raw[2] || 0); }
    get wyw() { return new V3(this._raw[3] || 0, this._raw[1], this._raw[3] || 0); }

    get wzx() { return new V3(this._raw[3] || 0, this._raw[2] || 0, this._raw[0]); }
    get wzy() { return new V3(this._raw[3] || 0, this._raw[2] || 0, this._raw[1]); }
    get wzz() { return new V3(this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get wzw() { return new V3(this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get wwx() { return new V3(this._raw[3] || 0, this._raw[3] || 0, this._raw[0]); }
    get wwy() { return new V3(this._raw[3] || 0, this._raw[3] || 0, this._raw[1]); }
    get wwz() { return new V3(this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get www() { return new V3(this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0); }


    get xxxx() { return new V4(this._raw[0], this._raw[0], this._raw[0], this._raw[0]); }
    get xxxy() { return new V4(this._raw[0], this._raw[0], this._raw[0], this._raw[1]); }
    get xxxz() { return new V4(this._raw[0], this._raw[0], this._raw[0], this._raw[2] || 0); }
    get xxxw() { return new V4(this._raw[0], this._raw[0], this._raw[0], this._raw[3] || 0); }

    get xxyx() { return new V4(this._raw[0], this._raw[0], this._raw[1], this._raw[0]); }
    get xxyy() { return new V4(this._raw[0], this._raw[0], this._raw[1], this._raw[1]); }
    get xxyz() { return new V4(this._raw[0], this._raw[0], this._raw[1], this._raw[2] || 0); }
    get xxyw() { return new V4(this._raw[0], this._raw[0], this._raw[1], this._raw[3] || 0); }

    get xxzx() { return new V4(this._raw[0], this._raw[0], this._raw[2] || 0, this._raw[0]); }
    get xxzy() { return new V4(this._raw[0], this._raw[0], this._raw[2] || 0, this._raw[1]); }
    get xxzz() { return new V4(this._raw[0], this._raw[0], this._raw[2] || 0, this._raw[2] || 0); }
    get xxzw() { return new V4(this._raw[0], this._raw[0], this._raw[2] || 0, this._raw[3] || 0); }

    get xxwx() { return new V4(this._raw[0], this._raw[0], this._raw[3] || 0, this._raw[0]); }
    get xxwy() { return new V4(this._raw[0], this._raw[0], this._raw[3] || 0, this._raw[1]); }
    get xxwz() { return new V4(this._raw[0], this._raw[0], this._raw[3] || 0, this._raw[2] || 0); }
    get xxww() { return new V4(this._raw[0], this._raw[0], this._raw[3] || 0, this._raw[3] || 0); }

    get xyxx() { return new V4(this._raw[0], this._raw[1], this._raw[0], this._raw[0]); }
    get xyxy() { return new V4(this._raw[0], this._raw[1], this._raw[0], this._raw[1]); }
    get xyxz() { return new V4(this._raw[0], this._raw[1], this._raw[0], this._raw[2] || 0); }
    get xyxw() { return new V4(this._raw[0], this._raw[1], this._raw[0], this._raw[3] || 0); }

    get xyyx() { return new V4(this._raw[0], this._raw[1], this._raw[1], this._raw[0]); }
    get xyyy() { return new V4(this._raw[0], this._raw[1], this._raw[1], this._raw[1]); }
    get xyyz() { return new V4(this._raw[0], this._raw[1], this._raw[1], this._raw[2] || 0); }
    get xyyw() { return new V4(this._raw[0], this._raw[1], this._raw[1], this._raw[3] || 0); }

    get xyzx() { return new V4(this._raw[0], this._raw[1], this._raw[2] || 0, this._raw[0]); }
    get xyzy() { return new V4(this._raw[0], this._raw[1], this._raw[2] || 0, this._raw[1]); }
    get xyzz() { return new V4(this._raw[0], this._raw[1], this._raw[2] || 0, this._raw[2] || 0); }
    get xyzw() { return new V4(this._raw[0], this._raw[1], this._raw[2] || 0, this._raw[3] || 0); }

    get xywx() { return new V4(this._raw[0], this._raw[1], this._raw[3] || 0, this._raw[0]); }
    get xywy() { return new V4(this._raw[0], this._raw[1], this._raw[3] || 0, this._raw[1]); }
    get xywz() { return new V4(this._raw[0], this._raw[1], this._raw[3] || 0, this._raw[2] || 0); }
    get xyww() { return new V4(this._raw[0], this._raw[1], this._raw[3] || 0, this._raw[3] || 0); }

    get xzxx() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[0], this._raw[0]); }
    get xzxy() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[0], this._raw[1]); }
    get xzxz() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[0], this._raw[2] || 0); }
    get xzxw() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[0], this._raw[3] || 0); }

    get xzyx() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[1], this._raw[0]); }
    get xzyy() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[1], this._raw[1]); }
    get xzyz() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[1], this._raw[2] || 0); }
    get xzyw() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[1], this._raw[3] || 0); }

    get xzzx() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[2] || 0, this._raw[0]); }
    get xzzy() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[2] || 0, this._raw[1]); }
    get xzzz() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get xzzw() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get xzwx() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[3] || 0, this._raw[0]); }
    get xzwy() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[3] || 0, this._raw[1]); }
    get xzwz() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get xzww() { return new V4(this._raw[0], this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0); }

    get xwxx() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[0], this._raw[0]); }
    get xwxy() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[0], this._raw[1]); }
    get xwxz() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[0], this._raw[2] || 0); }
    get xwxw() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[0], this._raw[3] || 0); }

    get xwyx() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[1], this._raw[0]); }
    get xwyy() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[1], this._raw[1]); }
    get xwyz() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[1], this._raw[2] || 0); }
    get xwyw() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[1], this._raw[3] || 0); }

    get xwzx() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[2] || 0, this._raw[0]); }
    get xwzy() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[2] || 0, this._raw[1]); }
    get xwzz() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get xwzw() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get xwwx() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[3] || 0, this._raw[0]); }
    get xwwy() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[3] || 0, this._raw[1]); }
    get xwwz() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get xwww() { return new V4(this._raw[0], this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0); }


    get yxxx() { return new V4(this._raw[1], this._raw[0], this._raw[0], this._raw[0]); }
    get yxxy() { return new V4(this._raw[1], this._raw[0], this._raw[0], this._raw[1]); }
    get yxxz() { return new V4(this._raw[1], this._raw[0], this._raw[0], this._raw[2] || 0); }
    get yxxw() { return new V4(this._raw[1], this._raw[0], this._raw[0], this._raw[3] || 0); }

    get yxyx() { return new V4(this._raw[1], this._raw[0], this._raw[1], this._raw[0]); }
    get yxyy() { return new V4(this._raw[1], this._raw[0], this._raw[1], this._raw[1]); }
    get yxyz() { return new V4(this._raw[1], this._raw[0], this._raw[1], this._raw[2] || 0); }
    get yxyw() { return new V4(this._raw[1], this._raw[0], this._raw[1], this._raw[3] || 0); }

    get yxzx() { return new V4(this._raw[1], this._raw[0], this._raw[2] || 0, this._raw[0]); }
    get yxzy() { return new V4(this._raw[1], this._raw[0], this._raw[2] || 0, this._raw[1]); }
    get yxzz() { return new V4(this._raw[1], this._raw[0], this._raw[2] || 0, this._raw[2] || 0); }
    get yxzw() { return new V4(this._raw[1], this._raw[0], this._raw[2] || 0, this._raw[3] || 0); }

    get yxwx() { return new V4(this._raw[1], this._raw[0], this._raw[3] || 0, this._raw[0]); }
    get yxwy() { return new V4(this._raw[1], this._raw[0], this._raw[3] || 0, this._raw[1]); }
    get yxwz() { return new V4(this._raw[1], this._raw[0], this._raw[3] || 0, this._raw[2] || 0); }
    get yxww() { return new V4(this._raw[1], this._raw[0], this._raw[3] || 0, this._raw[3] || 0); }

    get yyxx() { return new V4(this._raw[1], this._raw[1], this._raw[0], this._raw[0]); }
    get yyxy() { return new V4(this._raw[1], this._raw[1], this._raw[0], this._raw[1]); }
    get yyxz() { return new V4(this._raw[1], this._raw[1], this._raw[0], this._raw[2] || 0); }
    get yyxw() { return new V4(this._raw[1], this._raw[1], this._raw[0], this._raw[3] || 0); }

    get yyyx() { return new V4(this._raw[1], this._raw[1], this._raw[1], this._raw[0]); }
    get yyyy() { return new V4(this._raw[1], this._raw[1], this._raw[1], this._raw[1]); }
    get yyyz() { return new V4(this._raw[1], this._raw[1], this._raw[1], this._raw[2] || 0); }
    get yyyw() { return new V4(this._raw[1], this._raw[1], this._raw[1], this._raw[3] || 0); }

    get yyzx() { return new V4(this._raw[1], this._raw[1], this._raw[2] || 0, this._raw[0]); }
    get yyzy() { return new V4(this._raw[1], this._raw[1], this._raw[2] || 0, this._raw[1]); }
    get yyzz() { return new V4(this._raw[1], this._raw[1], this._raw[2] || 0, this._raw[2] || 0); }
    get yyzw() { return new V4(this._raw[1], this._raw[1], this._raw[2] || 0, this._raw[3] || 0); }

    get yywx() { return new V4(this._raw[1], this._raw[1], this._raw[3] || 0, this._raw[0]); }
    get yywy() { return new V4(this._raw[1], this._raw[1], this._raw[3] || 0, this._raw[1]); }
    get yywz() { return new V4(this._raw[1], this._raw[1], this._raw[3] || 0, this._raw[2] || 0); }
    get yyww() { return new V4(this._raw[1], this._raw[1], this._raw[3] || 0, this._raw[3] || 0); }

    get yzxx() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[0], this._raw[0]); }
    get yzxy() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[0], this._raw[1]); }
    get yzxz() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[0], this._raw[2] || 0); }
    get yzxw() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[0], this._raw[3] || 0); }

    get yzyx() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[1], this._raw[0]); }
    get yzyy() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[1], this._raw[1]); }
    get yzyz() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[1], this._raw[2] || 0); }
    get yzyw() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[1], this._raw[3] || 0); }

    get yzzx() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[2] || 0, this._raw[0]); }
    get yzzy() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[2] || 0, this._raw[1]); }
    get yzzz() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get yzzw() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get yzwx() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[3] || 0, this._raw[0]); }
    get yzwy() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[3] || 0, this._raw[1]); }
    get yzwz() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get yzww() { return new V4(this._raw[1], this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0); }

    get ywxx() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[0], this._raw[0]); }
    get ywxy() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[0], this._raw[1]); }
    get ywxz() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[0], this._raw[2] || 0); }
    get ywxw() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[0], this._raw[3] || 0); }

    get ywyx() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[1], this._raw[0]); }
    get ywyy() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[1], this._raw[1]); }
    get ywyz() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[1], this._raw[2] || 0); }
    get ywyw() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[1], this._raw[3] || 0); }

    get ywzx() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[2] || 0, this._raw[0]); }
    get ywzy() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[2] || 0, this._raw[1]); }
    get ywzz() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get ywzw() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get ywwx() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[3] || 0, this._raw[0]); }
    get ywwy() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[3] || 0, this._raw[1]); }
    get ywwz() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get ywww() { return new V4(this._raw[1], this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0); }


    get zxxx() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[0], this._raw[0]); }
    get zxxy() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[0], this._raw[1]); }
    get zxxz() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[0], this._raw[2] || 0); }
    get zxxw() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[0], this._raw[3] || 0); }

    get zxyx() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[1], this._raw[0]); }
    get zxyy() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[1], this._raw[1]); }
    get zxyz() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[1], this._raw[2] || 0); }
    get zxyw() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[1], this._raw[3] || 0); }

    get zxzx() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[2] || 0, this._raw[0]); }
    get zxzy() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[2] || 0, this._raw[1]); }
    get zxzz() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[2] || 0, this._raw[2] || 0); }
    get zxzw() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[2] || 0, this._raw[3] || 0); }

    get zxwx() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[3] || 0, this._raw[0]); }
    get zxwy() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[3] || 0, this._raw[1]); }
    get zxwz() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[3] || 0, this._raw[2] || 0); }
    get zxww() { return new V4(this._raw[2] || 0, this._raw[0], this._raw[3] || 0, this._raw[3] || 0); }

    get zyxx() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[0], this._raw[0]); }
    get zyxy() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[0], this._raw[1]); }
    get zyxz() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[0], this._raw[2] || 0); }
    get zyxw() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[0], this._raw[3] || 0); }

    get zyyx() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[1], this._raw[0]); }
    get zyyy() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[1], this._raw[1]); }
    get zyyz() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[1], this._raw[2] || 0); }
    get zyyw() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[1], this._raw[3] || 0); }

    get zyzx() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[2] || 0, this._raw[0]); }
    get zyzy() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[2] || 0, this._raw[1]); }
    get zyzz() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[2] || 0, this._raw[2] || 0); }
    get zyzw() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[2] || 0, this._raw[3] || 0); }

    get zywx() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[3] || 0, this._raw[0]); }
    get zywy() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[3] || 0, this._raw[1]); }
    get zywz() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[3] || 0, this._raw[2] || 0); }
    get zyww() { return new V4(this._raw[2] || 0, this._raw[1], this._raw[3] || 0, this._raw[3] || 0); }

    get zzxx() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[0], this._raw[0]); }
    get zzxy() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[0], this._raw[1]); }
    get zzxz() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[0], this._raw[2] || 0); }
    get zzxw() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[0], this._raw[3] || 0); }

    get zzyx() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[1], this._raw[0]); }
    get zzyy() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[1], this._raw[1]); }
    get zzyz() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[1], this._raw[2] || 0); }
    get zzyw() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[1], this._raw[3] || 0); }

    get zzzx() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[0]); }
    get zzzy() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[1]); }
    get zzzz() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get zzzw() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get zzwx() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[0]); }
    get zzwy() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[1]); }
    get zzwz() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get zzww() { return new V4(this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0); }

    get zwxx() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[0], this._raw[0]); }
    get zwxy() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[0], this._raw[1]); }
    get zwxz() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[0], this._raw[2] || 0); }
    get zwxw() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[0], this._raw[3] || 0); }

    get zwyx() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[1], this._raw[0]); }
    get zwyy() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[1], this._raw[1]); }
    get zwyz() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[1], this._raw[2] || 0); }
    get zwyw() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[1], this._raw[3] || 0); }

    get zwzx() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[0]); }
    get zwzy() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[1]); }
    get zwzz() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get zwzw() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get zwwx() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[0]); }
    get zwwy() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[1]); }
    get zwwz() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get zwww() { return new V4(this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0); }


    get wxxx() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[0], this._raw[0]); }
    get wxxy() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[0], this._raw[1]); }
    get wxxz() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[0], this._raw[2] || 0); }
    get wxxw() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[0], this._raw[3] || 0); }

    get wxyx() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[1], this._raw[0]); }
    get wxyy() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[1], this._raw[1]); }
    get wxyz() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[1], this._raw[2] || 0); }
    get wxyw() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[1], this._raw[3] || 0); }

    get wxzx() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[2] || 0, this._raw[0]); }
    get wxzy() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[2] || 0, this._raw[1]); }
    get wxzz() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[2] || 0, this._raw[2] || 0); }
    get wxzw() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[2] || 0, this._raw[3] || 0); }

    get wxwx() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[3] || 0, this._raw[0]); }
    get wxwy() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[3] || 0, this._raw[1]); }
    get wxwz() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[3] || 0, this._raw[2] || 0); }
    get wxww() { return new V4(this._raw[3] || 0, this._raw[0], this._raw[3] || 0, this._raw[3] || 0); }

    get wyxx() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[0], this._raw[0]); }
    get wyxy() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[0], this._raw[1]); }
    get wyxz() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[0], this._raw[2] || 0); }
    get wyxw() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[0], this._raw[3] || 0); }

    get wyyx() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[1], this._raw[0]); }
    get wyyy() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[1], this._raw[1]); }
    get wyyz() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[1], this._raw[2] || 0); }
    get wyyw() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[1], this._raw[3] || 0); }

    get wyzx() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[2] || 0, this._raw[0]); }
    get wyzy() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[2] || 0, this._raw[1]); }
    get wyzz() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[2] || 0, this._raw[2] || 0); }
    get wyzw() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[2] || 0, this._raw[3] || 0); }

    get wywx() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[3] || 0, this._raw[0]); }
    get wywy() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[3] || 0, this._raw[1]); }
    get wywz() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[3] || 0, this._raw[2] || 0); }
    get wyww() { return new V4(this._raw[3] || 0, this._raw[1], this._raw[3] || 0, this._raw[3] || 0); }

    get wzxx() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[0], this._raw[0]); }
    get wzxy() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[0], this._raw[1]); }
    get wzxz() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[0], this._raw[2] || 0); }
    get wzxw() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[0], this._raw[3] || 0); }

    get wzyx() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[1], this._raw[0]); }
    get wzyy() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[1], this._raw[1]); }
    get wzyz() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[1], this._raw[2] || 0); }
    get wzyw() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[1], this._raw[3] || 0); }

    get wzzx() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[0]); }
    get wzzy() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[1]); }
    get wzzz() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get wzzw() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get wzwx() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[0]); }
    get wzwy() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[1]); }
    get wzwz() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get wzww() { return new V4(this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0, this._raw[3] || 0); }

    get wwxx() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[0], this._raw[0]); }
    get wwxy() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[0], this._raw[1]); }
    get wwxz() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[0], this._raw[2] || 0); }
    get wwxw() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[0], this._raw[3] || 0); }

    get wwyx() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[1], this._raw[0]); }
    get wwyy() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[1], this._raw[1]); }
    get wwyz() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[1], this._raw[2] || 0); }
    get wwyw() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[1], this._raw[3] || 0); }

    get wwzx() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[0]); }
    get wwzy() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[1]); }
    get wwzz() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[2] || 0); }
    get wwzw() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0, this._raw[3] || 0); }

    get wwwx() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[0]); }
    get wwwy() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[1]); }
    get wwwz() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[2] || 0); }
    get wwww() { return new V4(this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0, this._raw[3] || 0); }
}
