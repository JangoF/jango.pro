class M4 extends M1 {

    // Конструктор может принимать на вход любой из следующих наборов данных:

    // M2           - Элементы будут размещены поверх единичной матрицы.
    // M3           - Элементы будут размещены поверх единичной матрицы.
    // M4

    // x            - Данным числом будут заполнены все элементы.
    // x, ... n     - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены.

    // [x]          - Данным числом будут заполнены все элементы.
    // [x, ... n]   - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены.

    // A, ... An    - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены (где A - любой из следующих объектов: V2, V3, V4, [x ... n]).
    // [A, ... An]  - Элементы будут размещены поверх единичной матрицы, лишние элементы - отброшены (где A - любой из следующих объектов: V2, V3, V4, [x ... n]).

    constructor() {
        super(arguments, 4);
    }

    // Возвращает новый объект M4 идентичный текущему:

    clone() {
        return new M4(this._raw);
    }

    // Производит умножение текущей матрицы на матрицу (или то из чего её можно сконструировать) либо на вектор (или то из чего его можно сконструировать):

    mul() {
        let size = null;
        let data = null;

        if (arguments[0] instanceof V1 && arguments.length == 1) { // Если на вход мы получили вектор (любой размерности).
            size = 1;
            data = arguments[0] instanceof V4 ? arguments[0]._raw : new V4(arguments[0])._raw;
        }
        else if (arguments[0] instanceof M1 && arguments.length == 1) { // Если на вход мы получили матрицу (любой размерности).
            size = 4;
            data = arguments[0] instanceof M4 ? arguments[0]._raw : new M4(arguments[0])._raw;
        }
        else if (arguments[0] instanceof Array && arguments[0].length <= 4 || arguments.length <= 4) { // Если на вход мы получили то из чего можно собрать вектор.
            size = 1;
            data = new V4(...arguments)._raw;
        }
        else { // Если на вход мы получили то из чего можно собрать матрицу.
            size = 4;
            data = new M4(...arguments)._raw;
        }

        let result = super._mul(4, size, data);

        if (result.length == 4) {
            return new V4(result);
        }
        else if (result.length == 16) {
            return new M4(result);
        }
    }

    // Возвращает транспонированную версию текущей матрицы:

    get transposed() {
        return new M4(
            this._raw[0], this._raw[4], this._raw[8],  this._raw[12],
            this._raw[1], this._raw[5], this._raw[9],  this._raw[13],
            this._raw[2], this._raw[6], this._raw[10], this._raw[14],
            this._raw[3], this._raw[7], this._raw[11], this._raw[15]
        );
    }
}
