// Важные инварианты для исходных данных:
// Поддерживаются только PBR материалы, других в файле быть не должно.
// Поддерживается лишь по одному LayerElement каждого из типов - Normal, Binormal, Tangent.

function loadGeometryFormFBX(sourceCode) {

    // Находит все блоки с заданным именем:

    function findBlockFromName(sourceCode, blockName) {
        let regularExpression = new RegExp('[^a-z]' + blockName + ':.+{', 'igm');
        let blockContentList = [];

        let execResult = null;
        while ((execResult = regularExpression.exec(sourceCode)) !== null) {

            let braceCount = 1;
            let blockContent = '';

            for (let i = regularExpression.lastIndex; i < sourceCode.length; i++) {

                if (sourceCode[i] == '{') { braceCount++; }
                else if (sourceCode[i] == '}') { braceCount--; }

                if (braceCount === 0) {
                    break;
                }

                blockContent += sourceCode[i];
            }

            let blockHeader = execResult[0].replace(/[\{| |\n]/igm, '');
            blockContentList.push({ header: blockHeader, content: blockContent });
        }

        return blockContentList;
    }

    function parseProperty(sourceCode, propertyName) {
        let validPropertyName = propertyName.replace(/[\\]/igm, '[\\\]').replace(/[\[]/igm, '[\\[]').
                                             replace(/[\]]/igm, '[\\]]').replace(/[\/]/igm, '[\\/]').
                                             replace(/[\^]/igm, '[\\^]').replace(/[\$]/igm, '[\\$]').
                                             replace(/[\.]/igm, '[\\.]').replace(/[\|]/igm, '[\\|]').
                                             replace(/[\?]/igm, '[\\?]').replace(/[\*]/igm, '[\\*]').
                                             replace(/[\+]/igm, '[\\+]').replace(/[\(]/igm, '[\\(]').
                                             replace(/[\)]/igm, '[\\)]').replace(/[\{]/igm, '[\\{]').replace(/[\}]/igm, '[\\}]');

        let parameterList = sourceCode.match(new RegExp('[^a-z]P:.*\"' + validPropertyName + '\".+', 'igm'))[0].replace(/(P:)| |\"/igm, '').split(',');

        for (let i = 0; i < parameterList.length; i++) {
            if (parameterList[i].length !== 0 && parameterList[i].match(new RegExp('[^-e,.0-9]', 'igm')) === null) {
                parameterList[i] = Number(parameterList[i]);
            }
        }

        return parameterList;
    }

    function parseGeometryBlock(sourceCode) {

        function parseVeretxBlock(sourceCode, blockName) {
            return findBlockFromName(sourceCode, blockName)[0].content.replace(/a:/igm, '').split(',').map(Number);
        }

        function parsePositionBlock(sourceCode) {
            return parseVeretxBlock(sourceCode, 'Vertices');
        }

        function parseIndexBlock(sourceCode) {
            return parseVeretxBlock(sourceCode, 'PolygonVertexIndex').map(function(element) { return element < 0 ? -element - 1 : element });
        }

        function parseEdgeBlock(sourceCode) {
            return parseVeretxBlock(sourceCode, 'Edges');
        }

        function parseLayerElementBlock(sourceCode, blockName, firstValueListName, secondValueListName) {
            let layerElementBlock = findBlockFromName(sourceCode, blockName);
            let layerElementBlockList = [];

            for (let i = 0; i < layerElementBlock.length; i++) {
                let valueListFirst = parseVeretxBlock(layerElementBlock[i].content, firstValueListName);

                let mappingInformationType = layerElementBlock[i].content.match(new RegExp('MappingInformationType:.+\".+\"', 'im'))[0].split('"')[1];
                let referenceInformationType = layerElementBlock[i].content.match(new RegExp('ReferenceInformationType:.+\".+\"', 'im'))[0].split('"')[1];

                if (secondValueListName !== undefined) {
                    let valueListSecond = parseVeretxBlock(layerElementBlock[i].content, secondValueListName);
                    layerElementBlockList.push({ mappingInformationType: mappingInformationType, referenceInformationType: referenceInformationType, valueListFirst: valueListFirst, valueListSecond: valueListSecond });

                } else {
                    layerElementBlockList.push({ mappingInformationType: mappingInformationType, referenceInformationType: referenceInformationType, valueListFirst: valueListFirst });
                }
            }

            return layerElementBlockList;
        }

        function parseNormalBlock(sourceCode) {
            return parseLayerElementBlock(sourceCode, 'LayerElementNormal', 'Normals', 'NormalsW');
        }

        function parseBinormalBlock(sourceCode) {
            return parseLayerElementBlock(sourceCode, 'LayerElementBinormal', 'Binormals', 'BinormalsW');
        }

        function parseTangentBlock(sourceCode) {
            return parseLayerElementBlock(sourceCode, 'LayerElementTangent', 'Tangents', 'TangentsW');
        }

        function parseUVBlock(sourceCode) {
            return parseLayerElementBlock(sourceCode, 'LayerElementUV', 'UV', 'UVIndex');
        }

        function parseMaterialBlock(sourceCode) {
            return parseLayerElementBlock(sourceCode, 'LayerElementMaterial', 'Materials');
        }

        function parseLayerBlock(sourceCode) {
            let layerBlock = findBlockFromName(sourceCode, 'Layer');
            let layerBlockList = [];

            function parseElementList(sourceCode) {
                let layerBlockCurrent = sourceCode;

                let layerElementBlock = findBlockFromName(layerBlockCurrent, 'LayerElement');
                let layerElementBlockList = [];

                for (let i = 0; i < layerElementBlock.length; i++) {
                    let layerElementType = layerElementBlock[i].content.match(new RegExp('[^a-z]Type:.+\".+\"', 'im'))[0].split('"')[1];
                    let layerElementIndex = Number(layerElementBlock[i].content.match(new RegExp('[^a-z]TypedIndex:.+', 'im'))[0].split(':')[1]);

                    layerElementBlockList.push({ type: layerElementType, index: layerElementIndex });
                }

                return layerElementBlockList;
            }

            for (let i = 0; i < layerBlock.length; i++) {
                let layerID = Number(layerBlock[i].header.replace(/[^0-9]/igm, ''));
                let layerElementList = parseElementList(layerBlock[i].content);

                layerBlockList.push({ layerID: layerID, layerElement: layerElementList });
            }

            return layerBlockList;
        }

        return {
            position: parsePositionBlock(sourceCode),
            index:    parseIndexBlock(sourceCode),
            // edge:     parseEdgeBlock(sourceCode),
            // material: parseMaterialBlock(sourceCode),
            // layer:    parseLayerBlock(sourceCode),
            normal:   parseNormalBlock(sourceCode),
            binormal: parseBinormalBlock(sourceCode),
            tangent:  parseTangentBlock(sourceCode),
            uv:       parseUVBlock(sourceCode)
        };
    }

    function parseMaterialBlock(sourceCode) {
        let propertyList = findBlockFromName(sourceCode, 'Properties70')[0].content;

        // Поддерживаются только PBR материалы:

        let propertyBaseColor = parseProperty(propertyList, 'Maya|base_color');
        let propertyMetallic = parseProperty(propertyList, 'Maya|metallic');
        let propertyRoughness = parseProperty(propertyList, 'Maya|roughness');
        let propertyEmissive = parseProperty(propertyList, 'Maya|emissive');
        let propertyEmissiveIntensity = parseProperty(propertyList, 'Maya|emissive_intensity');

        return {
            color: [propertyBaseColor[propertyBaseColor.length - 3], propertyBaseColor[propertyBaseColor.length - 2], propertyBaseColor[propertyBaseColor.length - 1]],
            metallic: propertyMetallic[propertyMetallic.length - 1],
            roughness: propertyRoughness[propertyRoughness.length - 1],
            emissiveColor: [propertyEmissive[propertyEmissive.length - 3], propertyEmissive[propertyEmissive.length - 2], propertyEmissive[propertyEmissive.length - 1]],
            emissiveIntensity: propertyEmissiveIntensity[propertyEmissiveIntensity.length - 1]
        };
    }

    function makeFinalMesh(parsedGeometry, materialList) {
        function findLayerElementBlockItem(structuredLayerElementBlock, vertexIndexList, orderedIndex, dataSize) {
            let requiredIndex = 0;

            switch (structuredLayerElementBlock.mappingInformationType) {

                // Данные определены для каждого полигона:

                case 'ByPolygon':
                requiredIndex = Math.floor(orderedIndex / 3); // В качестве полигонов поддерживаются лишь треугольники.
                break;

                // Данные определены для каждого индекса в массиве индексов блока геометрии:

                case 'ByPolygonVertex':
                requiredIndex = orderedIndex;
                break;

                // Данные определены для каждой вершины в массиве вершин блока геометрии:

                case 'ByVertex', 'ByVertice':
                requiredIndex = vertexIndexList[orderedIndex];
                break;
            }

            // В случае если параметр 'ReferenceInformationType' равен 'IndexToDirect', а так же если существует массив индексов текущего блока, данные берутся через его 'призму':

            if (structuredLayerElementBlock.valueListSecond !== undefined && structuredLayerElementBlock.referenceInformationType == 'IndexToDirect') {
                requiredIndex = structuredLayerElementBlock.valueListSecond[requiredIndex];
            }

            // Формируем результирующий массив данных:

            let result = [];

            for (let i = 0; i < dataSize; i++) {
                result.push(structuredLayerElementBlock.valueListFirst[requiredIndex * dataSize + i]);
            }

            return result;
        }

        function makeMaterialData(materialList, materialIndex) {
            return [materialList[materialIndex].color, materialList[materialIndex].metallic, materialList[materialIndex].roughness, materialList[materialIndex].emissiveColor, materialList[materialIndex].emissiveIntensity];
        }

        function isVertexDataUnique(vertexDataList, vertexData) {

            for (let i = 0; i < vertexDataList.length; i += vertexData.length) {
                let isFound = true;

                for (let j = 0; j < vertexData.length; j++) {
                    if (vertexDataList[i + j] !== vertexData[j]) {
                        isFound = false;
                        break;
                    }
                }

                if (isFound === true) {
                    return Math.floor(i / vertexData.length);
                }
            }

            return -1;
        }

        // Преобразовываем, для удобства, плоский массив позиций в двумерный:

        parsedGeometry.position = parsedGeometry.position.reduce((accumulator, item, index) => (index % 3 === 0 ? accumulator.push([item]) : accumulator[accumulator.length - 1].push(item)) && accumulator, []);

        // Формируем окончательные массивы вершин и индексов:

        let finalArrayVertex = [];
        let finalArrayIndex = [];

        for (let i = 0; i < parsedGeometry.index.length; i++) {

            // Формируем данные для текущей вершины:

            let position = parsedGeometry.position[parsedGeometry.index[i]];
            let normal = findLayerElementBlockItem(parsedGeometry.normal[0], parsedGeometry.index, i, 3); // Используем лишь нулевой 'Normal' блок.
            let binormal = findLayerElementBlockItem(parsedGeometry.binormal[0], parsedGeometry.index, i, 3); // Используем лишь нулевой 'Binormal' блок.
            let tangent = findLayerElementBlockItem(parsedGeometry.tangent[0], parsedGeometry.index, i, 3); // Используем лишь нулевой 'Tangent' блок.
            let uv = findLayerElementBlockItem(parsedGeometry.uv[0], parsedGeometry.index, i, 2); // Используем лишь нулевой 'UV' блок.
            // let material = findLayerElementBlockItem(parsedGeometry.material[0], parsedGeometry.index, i, 1); // Используем лишь нулевой 'Material' блок.
            // let materialData = makeMaterialData(materialList, material);

            let currentVertexData = [
                position[0], position[1], position[2],                      // Позиция.
                normal[0], normal[1], normal[2],                            // Нормаль.
                binormal[0], binormal[1], binormal[2],                      // Бинормаль.
                tangent[0], tangent[1], tangent[2],                         // Тангент.
                uv[0], uv[1]                                                // Развёртка.
            ];

            // let currentVertexData = [
            //     position[0], position[1], position[2],                      // Позиция.
            //     normal[0], normal[1], normal[2],                            // Нормаль.
            //     materialData[0][0], materialData[0][1], materialData[0][2], // Цвет.
            //     materialData[1],                                            // Металлик.
            //     materialData[2],                                            // Шероховатость.
            //     materialData[3][0], materialData[3][1], materialData[3][2], // Цвет свечения.
            //     materialData[4]                                             // Интенсивность свечения.
            // ];

            // Если параметры вершины уникальны - добавляем их в массив и записываем текущий индекс, иначе просто записываем индекс найденной вершины:

            let newVertexIndex = isVertexDataUnique(finalArrayVertex, currentVertexData);

            if (newVertexIndex < 0) {
                finalArrayIndex.push(finalArrayVertex.length / currentVertexData.length);
                finalArrayVertex = finalArrayVertex.concat(currentVertexData);
            } else {
                finalArrayIndex.push(newVertexIndex);
            }
        }

        return { vertexBuffer: finalArrayVertex, indexBuffer: finalArrayIndex };
    }

    let objectsBlock = findBlockFromName(sourceCode, 'Objects')[0].content;

    // Получаем список готовых к использованию материалов:

    // let materialBlock = findBlockFromName(objectsBlock, 'Material');
    // let materialBlockList = [];

    // for (let i = 0; i < materialBlock.length; i++) {
    //     materialBlockList.push(parseMaterialBlock(materialBlock[i].content));
    // }

    // Получаем список готовой к использованию геометрии сцены:

    let geometryBlock = findBlockFromName(objectsBlock, 'Geometry');
    let geometryBlockList = [];

    for (let i = 0; i < geometryBlock.length; i++) {
        geometryBlockList.push(parseGeometryBlock(geometryBlock[i].content));
    }
// console.log(geometryBlockList);
    // Получаем список окончательно сформированных мешей:

    let meshList = [];
    for (let i = 0; i < geometryBlockList.length; i++) {
        meshList.push(makeFinalMesh(geometryBlockList[i], []));
    }
// console.log(meshList[0]);
    // TODO:

    return meshList[0];
}
































// OLD!





// Загружает сцену из FBX файла:

// function makeSceneFromFBX(gl, source) {
//
//     // Получает всё содержимое до конца блока от заданной позиции. Предполагается что позиция дана уже внутри блока:
//
//     function blockContentGet(source, position, open = '{', close = '}') {
//         let opened = 1;
//         let closed = 0;
//
//         // Просто проходимся по коду и считаем открывающиеся и закрывающиеся скобки. Как только их количество станет равным - возвращаем результат:
//
//         let content = '';
//         for (let i = position; i < source.length; i++) {
//             if (source[i] == open) { opened += 1; }
//             else if (source[i] == close) { closed += 1; }
//
//             if (opened == closed) {
//                 return content;
//             }
//
//             content += source[i];
//         }
//
//         return content;
//     }
//
//     // Находит единственный блок с заданным именем, и, если требуется, очищает его от мусора (определено поведение для блока индексов и всего остального):
//
//     function findSingleBlock(source, title, clear) {
//         let blockRegexp = source.match(new RegExp('[^a-z]' + title + ':.+{', 'im'));
//         let block = blockContentGet(source, blockRegexp.index + blockRegexp[0].length);
//
//         if (clear == true) {
//
//             // В случае если мы работаем с массивом индексов, то помимо очистки массива, преобразовываем крайние (являются отрицательными) индексы полигонов:
//
//             if (title == 'PolygonVertexIndex') {
//                 return block.replace(/a:/igm, '').split(',').map(function (index) { index = Number(index); return index < 0 ? index * -1 - 1 : index; });
//             }
//
//             return block.replace(/a:/igm, '').split(',').map(Number);
//         }
//
//         return block;
//     }
//
//     // Находит все блоки с заданным именем:
//
//     function findAllBlocks(source, title) {
//         let blockRegexp = new RegExp('[^a-z]' + title + ':.+{', 'igm');
//         let blockList = [];
//
//         while (blockRegexp.exec(source) !== null) {
//             blockList.push(blockContentGet(source, blockRegexp.lastIndex));
//         }
//
//         return blockList;
//     }
//
//     // Находит свойство с заданным именем:
//
//     function findProperty(source, title) {
//         return source.match(new RegExp('[^a-z]' + title + ':.+\".+\"', 'im'))[0].split('"')[1];
//     }
//
//     // Находит и разбирает блок с учётом заданных параметров:
//
//     function findAndParseBlockWitchParameters(source, title, nameBlockData, nameBlockIndex) {
//         let block = findSingleBlock(source, title, false);
//
//         let blockData = findSingleBlock(block, nameBlockData, true);
//         let blockIndex = findSingleBlock(block, nameBlockIndex, true);
//
//         let propertyMapping = findProperty(block, 'MappingInformationType');
//         let propertyReference = findProperty(block, 'ReferenceInformationType');
//
//         return { data: blockData, index: blockIndex, mapping: propertyMapping, reference: propertyReference };
//     }
//
//     // Возвращает данные из блока (Normal, UV, Tangent и т.д.), в зависимости от заданных параметров:
//
//     function elementFromBlock(block, indexBuffer, indexBufferPosition, size) {
//         let index = 0;
//
//         switch (block.mapping) {
//
//             // Данные определены для каждого полигона:
//
//             case 'ByPolygon':
//             index = floor(indexBufferPosition / 3);
//             break;
//
//             // Данные определены для каждого индекса в массиве индексов:
//
//             case 'ByPolygonVertex':
//             index = indexBufferPosition;
//             break;
//
//             // Данные определены для каждого вертекса в массиве вертексов:
//
//             case 'ByVertex', 'ByVertice':
//             index = indexBuffer[indexBufferPosition];
//             break;
//         }
//
//         // В случае если параметр 'ReferenceInformationType' равен 'IndexToDirect', данные берутся через призму массива индексов текущего блока (Normal, UV, Tangent и т.д.):
//
//         if (block.reference == 'IndexToDirect') {
//             index = block.index[index];
//         }
//
//         // Параметр 'size' указывает на размерность вектора извлекаемого из мессива:
//
//         let result = [];
//
//         for (let i = 0; i < size; i++) {
//             result.push(block.data[index * size + i]);
//         }
//
//         return result;
//     }
//
//     // Проверяет уникален ли заданный вертекс, в заданном массиве:
//
//     function isVertexDataUnique(array, vertex) {
//
//         // Каждый элемент массива 'array'(вершины) является массивом (параметры вершины (Normal, UV, Tangent и т.д.)) массивов (компоненты параметра вершины (x, y, z и т.д.)):
//
//         function isVertexEqual(one, two) {
//             for (let i = 0; i < one.length; i++) {
//                 for (let j = 0; j < one[i].length; j++) {
//                     if (one[i][j] != two[i][j]) {
//                         return false;
//                     }
//                 }
//             }
//
//             return true;
//         }
//
//         for (let i = 0; i < array.length; i++) {
//             if (isVertexEqual(array[i], vertex) == true) {
//                 return i;
//             }
//         }
//
//         return -1;
//     }
//
//     // Разворачивает массив из многоуровнего, в обычный, линейный:
//
//     function unwrapArray(array) {
//         let result = [];
//
//         for (let i = 0; i < array.length; i++) {
//             for (let j = 0; j < array[i].length; j++) {
//                 for (let k = 0; k < array[i][j].length; k++) {
//                     result.push(array[i][j][k]);
//                 }
//             }
//         }
//
//         return result;
//     }
//
//     // Создаёт SCObject на основе входящих блоков:
//
//     function makeMeshFromData(gl, vertex, index) {
//         let dataVertex = [];
//         let dataIndex  = [];
//
//         // Для начала, необходимо заново сформировать массив вершин и индексов:
//
//         for (let i = 0; i < index.length; i++) {
//             let currentVertexIndex = index[i] * 3;
//             let currentVertex      = [vertex[currentVertexIndex], vertex[currentVertexIndex + 1], vertex[currentVertexIndex + 2]];
//
//             // Формируем данные для текущей вершины:
//
//             let newVertex = [currentVertex];
//             let newVertexIndex = isVertexDataUnique(dataVertex, newVertex);
//
//             // Если параметры вершины уникальны - добавляем их в массив и записываем текущий индекс, иначе просто записываем индекс найденной вершины:
//
//             if (newVertexIndex < 0) {
//                 dataIndex.push(dataVertex.length);
//                 dataVertex.push(newVertex);
//             }
//             else {
//                 dataIndex.push(newVertexIndex);
//             }
//         }
//
//         return new SCObject(gl, new GLMesh(gl, new Uint16Array(dataIndex), new Float32Array(unwrapArray(dataVertex)), [3, 3, 3, 3, 2]), new M4());
//     }
//
//     let objectsBlock = findSingleBlock(source, 'Objects', false);
//     let geometryBlocks = findAllBlocks(objectsBlock, 'Geometry');
//
//     let result = [];
//     for (let i = 0; i < geometryBlocks.length; i++) {
//
//         // Достаём данные из текущего блока геометрии и формируем из них SCObject:
//
//         let dataVertex = findSingleBlock(geometryBlocks[i], 'Vertices', true);
//         let dataIndex  = findSingleBlock(geometryBlocks[i], 'PolygonVertexIndex', true);
//
//         result.push(makeMeshFromData(gl, dataVertex, dataIndex));
//
//         // let dataNormal   = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementNormal',   'Normals',   'NormalsW');
//         // let dataBinormal = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementBinormal', 'Binormals', 'BinormalsW');
//         // let dataTangent  = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementTangent',  'Tangents',  'TangentsW');
//         // let dataUV       = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementUV',       'UV',        'UVIndex');
//         //
//         // result.push(makeMeshFromData(gl, dataVertex, dataIndex, dataNormal, dataBinormal, dataTangent, dataUV));
//     }
//
//     return result;
// }


// // Загружает сцену из FBX файла:
//
// function makeSceneFromFBX(gl, source) {
//
//     // Получает всё содержимое до конца блока от заданной позиции. Предполагается что позиция дана уже внутри блока:
//
//     function blockContentGet(source, position, open = '{', close = '}') {
//         let opened = 1;
//         let closed = 0;
//
//         // Просто проходимся по коду и считаем открывающиеся и закрывающиеся скобки. Как только их количество станет равным - возвращаем результат:
//
//         let content = '';
//         for (let i = position; i < source.length; i++) {
//             if (source[i] == open) { opened += 1; }
//             else if (source[i] == close) { closed += 1; }
//
//             if (opened == closed) {
//                 return content;
//             }
//
//             content += source[i];
//         }
//
//         return content;
//     }
//
//     // Находит единственный блок с заданным именем, и, если требуется, очищает его от мусора (определено поведение для блока индексов и всего остального):
//
//     function findSingleBlock(source, title, clear) {
//         let blockRegexp = source.match(new RegExp('[^a-z]' + title + ':.+{', 'im'));
//         let block = blockContentGet(source, blockRegexp.index + blockRegexp[0].length);
//
//         if (clear == true) {
//
//             // В случае если мы работаем с массивом индексов, то помимо очистки массива, преобразовываем крайние (являются отрицательными) индексы полигонов:
//
//             if (title == 'PolygonVertexIndex') {
//                 return block.replace(/a:/igm, '').split(',').map(function (index) { index = Number(index); return index < 0 ? index * -1 - 1 : index; });
//             }
//
//             return block.replace(/a:/igm, '').split(',').map(Number);
//         }
//
//         return block;
//     }
//
//     // Находит все блоки с заданным именем:
//
//     function findAllBlocks(source, title) {
//         let blockRegexp = new RegExp('[^a-z]' + title + ':.+{', 'igm');
//         let blockList = [];
//
//         while (blockRegexp.exec(source) !== null) {
//             blockList.push(blockContentGet(source, blockRegexp.lastIndex));
//         }
//
//         return blockList;
//     }
//
//     // Находит свойство с заданным именем:
//
//     function findProperty(source, title) {
//         return source.match(new RegExp('[^a-z]' + title + ':.+\".+\"', 'im'))[0].split('"')[1];
//     }
//
//     // Находит и разбирает блок с учётом заданных параметров:
//
//     function findAndParseBlockWitchParameters(source, title, nameBlockData, nameBlockIndex) {
//         let block = findSingleBlock(source, title, false);
//
//         let blockData = findSingleBlock(block, nameBlockData, true);
//         let blockIndex = findSingleBlock(block, nameBlockIndex, true);
//
//         let propertyMapping = findProperty(block, 'MappingInformationType');
//         let propertyReference = findProperty(block, 'ReferenceInformationType');
//
//         return { data: blockData, index: blockIndex, mapping: propertyMapping, reference: propertyReference };
//     }
//
//     // Возвращает данные из блока (Normal, UV, Tangent и т.д.), в зависимости от заданных параметров:
//
//     function elementFromBlock(block, indexBuffer, indexBufferPosition, size) {
//         let index = 0;
//
//         switch (block.mapping) {
//
//             // Данные определены для каждого полигона:
//
//             case 'ByPolygon':
//             index = floor(indexBufferPosition / 3);
//             break;
//
//             // Данные определены для каждого индекса в массиве индексов:
//
//             case 'ByPolygonVertex':
//             index = indexBufferPosition;
//             break;
//
//             // Данные определены для каждого вертекса в массиве вертексов:
//
//             case 'ByVertex', 'ByVertice':
//             index = indexBuffer[indexBufferPosition];
//             break;
//         }
//
//         // В случае если параметр 'ReferenceInformationType' равен 'IndexToDirect', данные берутся через призму массива индексов текущего блока (Normal, UV, Tangent и т.д.):
//
//         if (block.reference == 'IndexToDirect') {
//             index = block.index[index];
//         }
//
//         // Параметр 'size' указывает на размерность вектора извлекаемого из мессива:
//
//         let result = [];
//
//         for (let i = 0; i < size; i++) {
//             result.push(block.data[index * size + i]);
//         }
//
//         return result;
//     }
//
//     // Проверяет уникален ли заданный вертекс, в заданном массиве:
//
//     function isVertexDataUnique(array, vertex) {
//
//         // Каждый элемент массива 'array'(вершины) является массивом (параметры вершины (Normal, UV, Tangent и т.д.)) массивов (компоненты параметра вершины (x, y, z и т.д.)):
//
//         function isVertexEqual(one, two) {
//             for (let i = 0; i < one.length; i++) {
//                 for (let j = 0; j < one[i].length; j++) {
//                     if (one[i][j] != two[i][j]) {
//                         return false;
//                     }
//                 }
//             }
//
//             return true;
//         }
//
//         for (let i = 0; i < array.length; i++) {
//             if (isVertexEqual(array[i], vertex) == true) {
//                 return i;
//             }
//         }
//
//         return -1;
//     }
//
//     // Разворачивает массив из многоуровнего, в обычный, линейный:
//
//     function unwrapArray(array) {
//         let result = [];
//
//         for (let i = 0; i < array.length; i++) {
//             for (let j = 0; j < array[i].length; j++) {
//                 for (let k = 0; k < array[i][j].length; k++) {
//                     result.push(array[i][j][k]);
//                 }
//             }
//         }
//
//         return result;
//     }
//
//     // Создаёт SCObject на основе входящих блоков:
//
//     function makeMeshFromData(gl, vertex, index) {
//         let dataVertex = [];
//         let dataIndex  = [];
//
//         // Для начала, необходимо заново сформировать массив вершин и индексов:
//
//         for (let i = 0; i < index.length; i++) {
//             let currentVertexIndex = index[i] * 3;
//             let currentVertex      = [vertex[currentVertexIndex], vertex[currentVertexIndex + 1], vertex[currentVertexIndex + 2]];
//
//             // Формируем данные для текущей вершины:
//
//             let newVertex = [currentVertex];
//             let newVertexIndex = isVertexDataUnique(dataVertex, newVertex);
//
//             // Если параметры вершины уникальны - добавляем их в массив и записываем текущий индекс, иначе просто записываем индекс найденной вершины:
//
//             if (newVertexIndex < 0) {
//                 dataIndex.push(dataVertex.length);
//                 dataVertex.push(newVertex);
//             }
//             else {
//                 dataIndex.push(newVertexIndex);
//             }
//         }
//
//         return new SCObject(gl, new GLMesh(gl, new Uint16Array(dataIndex), new Float32Array(unwrapArray(dataVertex)), [3, 3, 3, 3, 2]), new M4());
//     }
//
//     let objectsBlock = findSingleBlock(source, 'Objects', false);
//     let geometryBlocks = findAllBlocks(objectsBlock, 'Geometry');
//
//     let result = [];
//     for (let i = 0; i < geometryBlocks.length; i++) {
//
//         // Достаём данные из текущего блока геометрии и формируем из них SCObject:
//
//         let dataVertex = findSingleBlock(geometryBlocks[i], 'Vertices', true);
//         let dataIndex  = findSingleBlock(geometryBlocks[i], 'PolygonVertexIndex', true);
//
//         result.push(makeMeshFromData(gl, dataVertex, dataIndex));
//
//         // let dataNormal   = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementNormal',   'Normals',   'NormalsW');
//         // let dataBinormal = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementBinormal', 'Binormals', 'BinormalsW');
//         // let dataTangent  = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementTangent',  'Tangents',  'TangentsW');
//         // let dataUV       = findAndParseBlockWitchParameters(geometryBlocks[i], 'LayerElementUV',       'UV',        'UVIndex');
//         //
//         // result.push(makeMeshFromData(gl, dataVertex, dataIndex, dataNormal, dataBinormal, dataTangent, dataUV));
//     }
//
//     return result;
// }
