// Общая концепция такова: FBX файл - это набор блоков и свойств. Блоки могу содержать как свойства так и другие блоки.
// Обработка файла происходит рекурсивно, начиная с самого верхнего уровня.
// Каждую итерацию мы сначала 'вырезаем' блоки верхнего уровня из общего контента а затем парсим оставшиеся данные на предмет свойств.
// В итоге мы получаем список обработанных свойств и не обработанных блоков.
// На финальном этапе мы строим иерархию из блоков и свойств, попутно, рекурсивно, вызывая обработку для каждого не обработанного блока.

class FLFBX {
    constructor(sourceCode) {
        this._parseBlockRecursive(sourceCode.replace(/;.*$/igm, '').replace(/^\s*[\n\r]/igm, '').replace(/,[\n\r]/igm, ','), this); // Очищаем контент о комметариев, пустых строк и запускаем рекурсивную обработку.
    }

    _parseBlockRecursive(sourceCode, parent) {

        // Здесь будет храниться весь исходный код, за исключением блоков:

        let sourceCodeWithoutBlockList = '';
        let sourceCodeWithoutBlockListLastAddedCharacterIndex = 0;

        // Получаем список блоков:

        let blockRegularExpression = new RegExp('^.*:.*\{.*$', 'igm');
        let blockContentList = []; // Список найденных блоков верхнего уровня.

        let blockRegularExpressionResult = null;
        while ((blockRegularExpressionResult = blockRegularExpression.exec(sourceCode)) !== null) {

            let braceCount = 1;
            let blockContent = '';

            // Подсчитываем количество повстречавшихся нам фигурных скобок:

            for (let i = blockRegularExpression.lastIndex; i < sourceCode.length; i++) {

                if (sourceCode[i] == '{') { braceCount++; }
                else if (sourceCode[i] == '}') { braceCount--; }

                if (braceCount === 0) { // Если количество открывающихся и закрывающихся сравнялось - мы достигли конца блока:
                    break;
                }

                blockContent += sourceCode[i];
            }

            // Добавляем контент до блока к общему контенту без блоков и обновляем позицию следующего поиска:

            sourceCodeWithoutBlockList += '\n' + sourceCode.substring(sourceCodeWithoutBlockListLastAddedCharacterIndex, blockRegularExpression.lastIndex - blockRegularExpressionResult[0].length);
            sourceCodeWithoutBlockListLastAddedCharacterIndex = blockRegularExpression.lastIndex + blockContent.length + 1; // Место старта следующего поиска за скобками текущего блока.

            blockRegularExpression.lastIndex = sourceCodeWithoutBlockListLastAddedCharacterIndex;

            // Разделяем ближайшик контент до начала блока на 'название блока' и на 'заголовок':

            let blockFullHeaderClean = blockRegularExpressionResult[0].replace(/\{|\s|\"/igm, '');
            let blockFullHeaderCleanColonIndex = blockFullHeaderClean.indexOf(':');

            // Преобразовываем заголовок в удобоваримый вид:

            let blockHeaderList = blockFullHeaderClean.slice(blockFullHeaderCleanColonIndex + 1).split(',');
            for (let i = 0; i < blockHeaderList.length; i++) {

                // Что возможно преобразовываем в число:

                if (blockHeaderList[i].length !== 0 && blockHeaderList[i].match(new RegExp('[^-e,.0-9]', 'igm')) === null) {
                    blockHeaderList[i] = Number(blockHeaderList[i]);
                }
            }

            blockContentList.push({ name: blockFullHeaderClean.slice(0, blockFullHeaderCleanColonIndex), header: blockHeaderList.length == 1 ? blockHeaderList[0] : blockHeaderList, content: blockContent });
        }

        sourceCodeWithoutBlockList += '\n' + sourceCode.substring(sourceCodeWithoutBlockListLastAddedCharacterIndex);

        // Получаем список свойств:

        let propertyRegularExpression = new RegExp('^.*:.*$', 'igm');
        let propertyContentList = []; // Список найденных свойств верхнего уровня.

        let propertyRegularExpressionResult = null;
        while ((propertyRegularExpressionResult = propertyRegularExpression.exec(sourceCodeWithoutBlockList)) !== null) {

            // Разделяем свойство на 'название свойства' и на 'контент':

            let parameterListFull = propertyRegularExpressionResult[0].replace(/\s|\"|/igm, '').replace(/,[\n\r]/igm, ',');
            let parameterListFullColonIndex = parameterListFull.indexOf(':');

            // Преобразовываем свойство в удобоваримый вид:

            let parameterList = parameterListFull.slice(parameterListFullColonIndex + 1).split(',');
            for (let i = 0; i < parameterList.length; i++) {

                // Что возможно преобразовываем в число:

                if (parameterList[i].length !== 0 && parameterList[i].match(new RegExp('[^-e,.0-9]', 'igm')) === null) {
                    parameterList[i] = Number(parameterList[i]);
                }
            }

            propertyContentList.push({ name: parameterListFull.slice(0, parameterListFullColonIndex), content: parameterList.length == 1 ? parameterList[0] : parameterList });
        }

        // Формируем иерархию свойств:

        let propertyUniqueNameList = [];
        for (let i = 0; i < propertyContentList.length; i++) {

            if (parent[propertyContentList[i].name] === undefined) { // Если свойства не существует - добавляем.
                parent[propertyContentList[i].name] = { content: propertyContentList[i].content };
                propertyUniqueNameList.push(propertyContentList[i].name);

            } else if (parent[propertyContentList[i].name] instanceof Array) { // Если у нас уже несколько свойств с одинаковыми именем - добавляем новое к существующему массиву.
                parent[propertyContentList[i].name].push({ content: propertyContentList[i].content });

            } else { // Если выяснилось что у нас несколько свойств с одинаковыми именами - объединяем старое и новое в массив.
                parent[propertyContentList[i].name] = [parent[propertyContentList[i].name], { content: propertyContentList[i].content }];
            }
        }

        // Очищаем иерархию свойств от мусора:

        for (let i = 0; i < propertyUniqueNameList.length; i++) {
            if (parent[propertyUniqueNameList[i]] instanceof Array) {

                // Если у нас несколько свойств с одинаковыми именами - проходимся по каждому:

                for (let j = 0; j < parent[propertyUniqueNameList[i]].length; j++) {
                    parent[propertyUniqueNameList[i]][j] = parent[propertyUniqueNameList[i]][j].content; // Убираем уровень 'content'.
                }

            } else {
                parent[propertyContentList[i].name] = parent[propertyContentList[i].name].content; // Убираем уровень 'content'.
            }
        }

        // Формируем иерархию блоков:

        for (let i = 0; i < blockContentList.length; i++) {
            let blockParsed = {};
            this._parseBlockRecursive(blockContentList[i].content, blockParsed);

            if (blockContentList[i].header !== '') { // Если у блока есть заголовок - добавляем его.
                blockParsed['HEADER'] = blockContentList[i].header;
            }

            if (parent[blockContentList[i].name] === undefined) { // Если блока не существует - добавляем.
                parent[blockContentList[i].name] = blockParsed;

            } else if (parent[blockContentList[i].name] instanceof Array) { // Если у нас уже несколько блоков с одинаковыми именем - добавляем новый к существующему массиву.
                parent[blockContentList[i].name].push(blockParsed);

            } else { // Если выяснилось что у нас несколько блоков с одинаковыми именами - объединяем старый и новый в массив.
                parent[blockContentList[i].name] = [parent[blockContentList[i].name], blockParsed];
            }
        }
    }
}
