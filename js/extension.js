Array.prototype.resized = function (length, value = null) {
    if (length <= this.length) {
        return this.slice(0, length)
    }

    let sub = length - this.length;
    return this.concat(Array(sub).fill(value, 0, sub));
};

Array.prototype.placed = function (start, array, end = -1) {
    let result = this.slice();
    for (let i = 0; i < Math.min(array.length); i++) {
        if (i == end) {
            break;
        }
        result[start + i] = array[i];
    }
    return result;
};

Array.prototype.first = function () {
    return this[0];
};

Array.prototype.last = function () {
    return this[this.length - 1];
};

Array.prototype.add = function (value) {
    let result = this.slice();
    result.push(value);
    return result;
};

Array.prototype.clone = function () {
	return this.slice(0);
};

Array.prototype.dropFirst = function () {
    let result = this.slice();
    result.shift();
    return result;
};

Array.prototype.dropLast = function () {
    let result = this.slice();
    result.pop();
    return result;
};

String.prototype.replaceAll = function (from, to) {
    return this.split(from).join(to);
}

function requestAnimationFrameWithInterval(code, interval) {
    setInterval(() => requestAnimationFrame(code), interval);
}
