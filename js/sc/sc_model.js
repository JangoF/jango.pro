class SCModel extends SCEssence {
    constructor(mesh, translation = new V3(0.0), rotation = new V3(0.0), scaling = new V3(1.0)) {
        super(translation, rotation, scaling);
        this._mesh = mesh;
    }

    draw(mode = this._mesh._gl.TRIANGLES) {
        this._mesh.draw(mode);
    }
}
