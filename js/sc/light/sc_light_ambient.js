class SCLightAmbient {

    constructor(color = new V3(1.0), intensity = 1.0, bufferView = null) {
        this._color      = color;
        this._intensity  = intensity;
        this._bufferView = bufferView;
    }

    _constructByteData() {
        return new Float32Array(this._color.raw.add(this._intensity));
    }

    _updateBufferView() {
        if (this._bufferView !== undefined && this._bufferView !== null) {
            this._bufferView.fill(this._constructByteData());
        }
    }

    get color() {
        return this._color;
    }

    get intensity() {
        return this._intensity;
    }

    get bufferView() {
        return this._bufferView;
    }

    setColor() {
        this._color = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._updateBufferView();
        return this;
    }

    setIntensity(intensity) {
        this._intensity = intensity;
        this._updateBufferView();
        return this;
    }

    setBufferView(bufferView, isNeedUpdateBufferView = true) {
        this._bufferView = bufferView;

        if (isNeedUpdateBufferView) {
            this._updateBufferView();
        }

        return this;
    }
}
