class SCLightSpot extends SCLightAmbient {

    constructor(
        gl,
        color      = undefined,
        intensity  = undefined,
        bufferView = undefined,
        opacity    = 0.0,
        angle      = new V2(24.0, 48.0),
        direction  = new V3(0.0, 0.0, -1.0),
        position   = new V3(0.0),
        resolution = 256.0,
        pipeline   = new GLPipeline(gl, SCLightSpot.defaultVS, SCLightSpot.defaultFS),
        listModel  = []
    ) {
        super(color, intensity, bufferView);

        this._gl          = gl;
        this._opacity     = opacity;
        this._angle       = angle;
        this._direction   = direction;
        this._position    = position;
        this._resolution  = resolution;
        this._pipeline    = pipeline;
        this._listModel   = listModel;
        this._framebuffer = null;

        this._updateView();
        this._updateProjection();
        this._updateFramebuffer();
        this._updateDepth();
    }

    _constructByteData() {
        return new Float32Array(this._color.raw.add(this._intensity).concat(this._angle.raw).add(this._opacity).add(0.0).concat(this._position.raw).add(0.0).concat(this._direction.raw).add(0.0));
    }

    _updateView() {
        this._view = Math.lookAt(this._position, this._position.sum(this._direction), this._direction.abs().isEqual(new V3(0.0, 1.0, 0.0)) ? new V3(0.0, 0.0, 1.0) : new V3(0.0, 1.0, 0.0));
    }

    _updateProjection() {
        this._projection = Math.perspective(0.002, Math.ceil(Math.sqrt(1.0 / (0.002 / this._intensity))), Math.toRadians(this._angle.y), 1.0);
    }

    _updateFramebuffer() {
        if (this._framebuffer !== undefined && this._framebuffer !== null) {
            this._framebuffer.destroy();
        }

        this._framebuffer = new GLFramebuffer(this._gl, [
            [new GLTexture(this._gl, this._gl.LINEAR, this._gl.DEPTH_COMPONENT32F, this._resolution, this._resolution).setStuffDepthCompare(), this._gl.DEPTH_ATTACHMENT]
        ]);
    }

    _updateDepth() {
        if (this._listModel.length === 0.0 || this._opacity === 0.0) {
            return;
        }

        this._framebuffer.bind();
        this._framebuffer.setupViewport();
        this._framebuffer.setupDrawBuffers();

        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.FRONT);

        this._gl.clearDepth(1.0);
        this._gl.clear(this._gl.DEPTH_BUFFER_BIT);

        this._pipeline.bind(
            null,
            this._view.mul(this._projection)
        );

        for (let i = 0; i < this._listModel.length; i++) {
            this._pipeline.setUniform(0, this._listModel[i].model);
            this._listModel[i].draw();
        }
    }

    get opacity() {
        return this._opacity;
    }

    get angle() {
        return this._angle;
    }

    get direction() {
        return this._direction;
    }

    get position() {
        return this._position;
    }

    get resolution() {
        return this._resolution;
    }

    get pipeline() {
        return this._pipeline;
    }

    get listModel() {
        return this._listModel;
    }

    get depth() {
        return this._framebuffer.getAttachment(this._gl.DEPTH_ATTACHMENT);
    }

    get view() {
        return this._view;
    }

    get projection() {
        return this._projection;
    }

    setIntensity(intensity) {
        this._intensity = intensity;
        this._updateProjection();
        this._updateDepth();
        return this;
    }

    setOpacity(opacity) {
        this._opacity = opacity;
        this._updateBufferView();
        return this;
    }

    setAngle() {
        this._angle = arguments[0] instanceof V2 ? arguments[0] : new V2(...arguments);
        this._updateBufferView();
        this._updateProjection();
        this._updateDepth();
        return this;
    }

    setDirection() {
        this._direction = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._updateBufferView();
        this._updateView();
        this._updateDepth();
        return this;
    }

    setPosition() {
        this._position = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._updateBufferView();
        this._updateView();
        this._updateDepth();
        return this;
    }

    setResolution(resolution) {
        this._resolution = resolution;
        this._updateFramebuffer();
        this._updateDepth();
        return this;
    }

    setPipeline(pipeline) {
        this._pipeline = pipeline;
        this._updateDepth();
        return this;
    }

    setListModel(listModel) {
        this._listModel = listModel;
        this._updateDepth();
        return this;
    }
}

SCLightSpot.defaultVS = `#version 300 es
layout(location = 0) in vec3 i_Position;

uniform mat4 u_Model;
uniform mat4 u_ViewProjection;

void main() {
    gl_Position = u_ViewProjection * u_Model * vec4(i_Position, 1.0f);
}`;

SCLightSpot.defaultFS = `#version 300 es
void main() {
}`;
