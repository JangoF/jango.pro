class SCLightPoint extends SCLightAmbient {

    constructor(
        gl,
        color      = undefined,
        intensity  = undefined,
        bufferView = undefined,
        opacity    = 0.0,
        position   = new V3(0.0),
        resolution = 256.0,
        pipeline   = new GLPipeline(gl, SCLightPoint.defaultVS, SCLightPoint.defaultFS),
        listModel  = []
    ) {
        super(color, intensity, bufferView);

        this._gl          = gl;
        this._opacity     = opacity;
        this._position    = position;
        this._resolution  = resolution;
        this._pipeline    = pipeline;
        this._listModel   = listModel;
        this._framebuffer = null;

        this._updateView();
        this._updateProjection();
        this._updateFramebuffer();
        this._updateDepth();
    }

    _constructByteData() {
        return new Float32Array(this._color.raw.add(this._intensity).concat(this._position.raw.add(this._opacity)));
    }

    _updateView() {
        this._view_XP = Math.lookAt(this._position, new V3(1.0, 0.0, 0.0).sum(this._position), new V3(0.0, -1.0, 0.0));
        this._view_XN = Math.lookAt(this._position, new V3(-1.0, 0.0, 0.0).sum(this._position), new V3(0.0, -1.0, 0.0));

        this._view_YP = Math.lookAt(this._position, new V3(0.0, 1.0, 0.0).sum(this._position), new V3(0.0, 0.0, 1.0));
        this._view_YN = Math.lookAt(this._position, new V3(0.0, -1.0, 0.0).sum(this._position), new V3(0.0, 0.0, -1.0));

        this._view_ZP = Math.lookAt(this._position, new V3(0.0, 0.0, 1.0).sum(this._position), new V3(0.0, -1.0, 0.0));
        this._view_ZN = Math.lookAt(this._position, new V3(0.0, 0.0, -1.0).sum(this._position), new V3(0.0, -1.0, 0.0));
    }

    _updateProjection() {
        this._projection = Math.perspective(0.002, Math.ceil(Math.sqrt(1.0 / (0.002 / this._intensity))), Math.toRadians(90.0), 1.0);
    }

    _updateFramebuffer() {
        if (this._framebuffer !== undefined && this._framebuffer !== null) {
            this._framebuffer.destroy();
            this._depthCube.destroy();
        }

        this._depthCube = new GLTexture(this._gl, this._gl.NEAREST, this._gl.R32F, this._resolution); // new GLTexture(this._gl).setTexture(this._gl.TEXTURE_CUBE_MAP, 0, this._gl.R32F, this._resolution, this._gl.NEAREST);
        this._framebuffer = new GLFramebuffer(this._gl);
    }

    _updateDepth() {
        if (this._listModel.length === 0.0 || this._opacity === 0.0) {
            return;
        }

        this._framebuffer.bind();
        this._framebuffer.setupViewport();
        this._gl.drawBuffers([this._gl.COLOR_ATTACHMENT0]);

        this._gl.enable(this._gl.DEPTH_TEST);
        this._gl.enable(this._gl.CULL_FACE);
        this._gl.cullFace(this._gl.FRONT);

        this._gl.clearDepth(1.0);
        this._gl.clearColor(SCLightPoint.maxDistance, SCLightPoint.maxDistance, SCLightPoint.maxDistance, 1.0);

        this._pipeline.bind(
            null,
            null,
            this._position,
        );

        {
            this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X);
            this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
            this._pipeline.setUniform(1, this._view_XP.mul(this._projection));

            for (let i = 0; i < this._listModel.length; i++) {
                this._pipeline.setUniform(0, this._listModel[i].model);
                this._listModel[i].draw();
            }
        }

        {
            this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X);
            this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
            this._pipeline.setUniform(1, this._view_XN.mul(this._projection));

            for (let i = 0; i < this._listModel.length; i++) {
                this._pipeline.setUniform(0, this._listModel[i].model);
                this._listModel[i].draw();
            }
        }

        {
            this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y);
            this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
            this._pipeline.setUniform(1, this._view_YP.mul(this._projection));

            for (let i = 0; i < this._listModel.length; i++) {
                this._pipeline.setUniform(0, this._listModel[i].model);
                this._listModel[i].draw();
            }
        }

        {
            this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y);
            this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
            this._pipeline.setUniform(1, this._view_YN.mul(this._projection));

            for (let i = 0; i < this._listModel.length; i++) {
                this._pipeline.setUniform(0, this._listModel[i].model);
                this._listModel[i].draw();
            }
        }

        {
            this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z);
            this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
            this._pipeline.setUniform(1, this._view_ZP.mul(this._projection));

            for (let i = 0; i < this._listModel.length; i++) {
                this._pipeline.setUniform(0, this._listModel[i].model);
                this._listModel[i].draw();
            }
        }

        {
            this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z);
            this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
            this._pipeline.setUniform(1, this._view_ZN.mul(this._projection));

            for (let i = 0; i < this._listModel.length; i++) {
                this._pipeline.setUniform(0, this._listModel[i].model);
                this._listModel[i].draw();
            }
        }
    }

    get opacity() {
        return this._opacity;
    }

    get position() {
        return this._position;
    }

    get resolution() {
        return this._resolution;
    }

    get pipeline() {
        return this._pipeline;
    }

    get listModel() {
        return this._listModel;
    }

    get depth() {
        return this._depthCube;
    }

    setOpacity(opacity) {
        this._opacity = opacity;
        this._updateBufferView();
        return this;
    }

    setPosition() {
        this._position = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._updateBufferView();
        this._updateView();
        this._updateDepth();
        return this;
    }

    setResolution(resolution) {
        this._resolution = resolution;
        this._updateFramebuffer();
        this._updateDepth();
        return this;
    }

    setPipeline(pipeline) {
        this._pipeline = pipeline;
        this._updateDepth();
        return this;
    }

    setListModel(listModel) {
        this._listModel = listModel;
        this._updateDepth();
        return this;
    }
}

SCLightPoint.maxDistance = Math.pow(2.0, 32.0) - 1.0;

SCLightPoint.defaultVS = `#version 300 es
layout(location = 0) in vec3 i_Position;

uniform mat4 u_Model;
uniform mat4 u_ViewProjection;

out vec3 io_Position;

void main() {
    vec4 position = u_Model * vec4(i_Position, 1.0f);
    io_Position = position.xyz;
    gl_Position = u_ViewProjection * position;
}`;

SCLightPoint.defaultFS = `#version 300 es
precision highp float;

uniform vec3 lightPosition;

in vec3 io_Position;
out vec4 o_Color[6];

void main() {
    o_Color[0] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
    o_Color[1] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
    o_Color[2] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
    o_Color[3] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
    o_Color[4] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
    o_Color[5] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
}`;


// class SCLightPoint extends SCLightAmbient {
//
//     constructor(
//         gl,
//         color      = undefined,
//         intensity  = undefined,
//         bufferView = undefined,
//         opacity    = 0.0,
//         position   = new V3(0.0),
//         resolution = 256.0,
//         pipeline   = new GLPipeline(gl, SCLightPoint.defaultVS, SCLightPoint.defaultFS),
//         listModel  = []
//     ) {
//         super(color, intensity, bufferView);
//
//         this._gl          = gl;
//         this._opacity     = opacity;
//         this._position    = position;
//         this._resolution  = resolution;
//         this._pipeline    = pipeline;
//         this._listModel   = listModel;
//         this._framebuffer = null;
//
//         this._updateView();
//         this._updateProjection();
//         this._updateFramebuffer();
//         this._updateDepth();
//     }
//
//     _constructByteData() {
//         return new Float32Array(this._color.raw.add(this._intensity).concat(this._position.raw.add(this._opacity)));
//     }
//
//     _updateView() {
//         this._view_XP = Math.lookAt(this._position, new V3(1.0, 0.0, 0.0).sum(this._position), new V3(0.0, -1.0, 0.0));
//         this._view_XN = Math.lookAt(this._position, new V3(-1.0, 0.0, 0.0).sum(this._position), new V3(0.0, -1.0, 0.0));
//
//         this._view_YP = Math.lookAt(this._position, new V3(0.0, 1.0, 0.0).sum(this._position), new V3(0.0, 0.0, 1.0));
//         this._view_YN = Math.lookAt(this._position, new V3(0.0, -1.0, 0.0).sum(this._position), new V3(0.0, 0.0, -1.0));
//
//         this._view_ZP = Math.lookAt(this._position, new V3(0.0, 0.0, 1.0).sum(this._position), new V3(0.0, -1.0, 0.0));
//         this._view_ZN = Math.lookAt(this._position, new V3(0.0, 0.0, -1.0).sum(this._position), new V3(0.0, -1.0, 0.0));
//     }
//
//     _updateProjection() {
//         this._projection = Math.perspective(0.002, Math.ceil(Math.sqrt(1.0 / (0.002 / this._intensity))), Math.toRadians(90.0), 1.0);
//     }
//
//     _updateFramebuffer() {
//         if (this._framebuffer !== undefined && this._framebuffer !== null) {
//             this._framebuffer.destroy();
//             this._depthCube.destroy();
//         }
//
//         this._depthCube = new GLTexture(this._gl).setTexture(this._gl.TEXTURE_CUBE_MAP, this._gl.R32F, this._resolution, this._gl.NEAREST);
//         this._framebuffer = new GLFramebuffer(this._gl);
//         // this._framebuffer = new GLFramebuffer(this._gl, [
//         //     [this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
//         //     [this._depthCube, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
//         //     [this._depthCube, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
//         //     [this._depthCube, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
//         //     [this._depthCube, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
//         //     [this._depthCube, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
//         // ]);
//     }
//
//     _updateDepth() {
//         if (this._listModel.length === 0.0 || this._opacity === 0.0) {
//             return;
//         }
//
//         this._framebuffer.bind();
//         this._framebuffer.setupViewport();
//         this._gl.drawBuffers([this._gl.COLOR_ATTACHMENT0]);
//
//         this._gl.enable(this._gl.DEPTH_TEST);
//         this._gl.enable(this._gl.CULL_FACE);
//         this._gl.cullFace(this._gl.FRONT);
//
//         this._gl.clearDepth(1.0);
//         this._gl.clearColor(SCLightPoint.maxDistance, SCLightPoint.maxDistance, SCLightPoint.maxDistance, 1.0);
//
//         this._pipeline.bind(
//             null,
//             null,
//             this._position,
//         );
//
//         {
//             this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X);
//             // this._gl.drawBuffers([this._gl.COLOR_ATTACHMENT0, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//             this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
//
//             for (let i = 0; i < this._listModel.length; i++) {
//                 this._pipeline.setUniform(0, this._listModel[i].model);
//                 this._pipeline.setUniform(1, this._view_XP.mul(this._projection));
//                 this._listModel[i].draw();
//             }
//         }
//
//         {
//             this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X);
//             // this._gl.drawBuffers([this._gl.NONE, this._gl.COLOR_ATTACHMENT1, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//             this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
//
//             for (let i = 0; i < this._listModel.length; i++) {
//                 this._pipeline.setUniform(0, this._listModel[i].model);
//                 this._pipeline.setUniform(1, this._view_XN.mul(this._projection));
//                 this._listModel[i].draw();
//             }
//         }
//
//         {
//             this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y);
//             // this._gl.drawBuffers([this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT2, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//             this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
//
//             for (let i = 0; i < this._listModel.length; i++) {
//                 this._pipeline.setUniform(0, this._listModel[i].model);
//                 this._pipeline.setUniform(1, this._view_YP.mul(this._projection));
//                 this._listModel[i].draw();
//             }
//         }
//
//         {
//             this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y);
//             // this._gl.drawBuffers([this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT3, this._gl.NONE, this._gl.NONE]);
//             this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
//
//             for (let i = 0; i < this._listModel.length; i++) {
//                 this._pipeline.setUniform(0, this._listModel[i].model);
//                 this._pipeline.setUniform(1, this._view_YN.mul(this._projection));
//                 this._listModel[i].draw();
//             }
//         }
//
//         {
//             this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z);
//             // this._gl.drawBuffers([this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT4, this._gl.NONE]);
//             this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
//
//             for (let i = 0; i < this._listModel.length; i++) {
//                 this._pipeline.setUniform(0, this._listModel[i].model);
//                 this._pipeline.setUniform(1, this._view_ZP.mul(this._projection));
//                 this._listModel[i].draw();
//             }
//         }
//
//         {
//             this._framebuffer.setAttachment(this._depthCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z);
//             // this._gl.drawBuffers([this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT5]);
//             this._gl.clear(this._gl.DEPTH_BUFFER_BIT | this._gl.COLOR_BUFFER_BIT);
//
//             for (let i = 0; i < this._listModel.length; i++) {
//                 this._pipeline.setUniform(0, this._listModel[i].model);
//                 this._pipeline.setUniform(1, this._view_ZN.mul(this._projection));
//                 this._listModel[i].draw();
//             }
//         }
//
//
//
//         // SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_XP, this._projection, [this._gl.COLOR_ATTACHMENT0, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//         // SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_XN, this._projection, [this._gl.NONE, this._gl.COLOR_ATTACHMENT1, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//         //
//         // SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_YP, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT2, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//         // SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_YN, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT3, this._gl.NONE, this._gl.NONE]);
//         //
//         // SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_ZP, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT4, this._gl.NONE]);
//         // SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_ZN, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT5]);
//     }
//
//     static _drawDepthToSide(gl, lightPosition, framebuffer, pipeline, listModel, view, projection, drawBuffers) {
//         // framebuffer.bind();
//         // framebuffer.setupViewport();
//         gl.drawBuffers(drawBuffers);
//
//         // gl.enable(gl.DEPTH_TEST);
//         // gl.enable(gl.CULL_FACE);
//         // gl.cullFace(gl.FRONT);
//         //
//         // gl.clearDepth(1.0);
//         // gl.clearColor(SCLightPoint.maxDistance, SCLightPoint.maxDistance, SCLightPoint.maxDistance, 1.0);
//         gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
//
//         pipeline.bind(
//             null,
//             view.mul(projection),
//             lightPosition,
//         );
//
//         for (let i = 0; i < listModel.length; i++) {
//             pipeline.setUniform(0, listModel[i].model);
//             listModel[i].draw();
//         }
//     }
//
//     // _updateDepth() {
//     //     if (this._listModel.length === 0.0 || this._opacity === 0.0) {
//     //         return;
//     //     }
//     //
//     //     SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_XP, this._projection, [this._gl.COLOR_ATTACHMENT0, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//     //     SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_XN, this._projection, [this._gl.NONE, this._gl.COLOR_ATTACHMENT1, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//     //
//     //     SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_YP, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT2, this._gl.NONE, this._gl.NONE, this._gl.NONE]);
//     //     SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_YN, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT3, this._gl.NONE, this._gl.NONE]);
//     //
//     //     SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_ZP, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT4, this._gl.NONE]);
//     //     SCLightPoint._drawDepthToSide(this._gl, this._position, this._framebuffer, this._pipeline, this._listModel, this._view_ZN, this._projection, [this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.NONE, this._gl.COLOR_ATTACHMENT5]);
//     // }
//     //
//     // static _drawDepthToSide(gl, lightPosition, framebuffer, pipeline, listModel, view, projection, drawBuffers) {
//     //     framebuffer.bind();
//     //     framebuffer.setupViewport();
//     //     gl.drawBuffers(drawBuffers);
//     //
//     //     gl.enable(gl.DEPTH_TEST);
//     //     gl.enable(gl.CULL_FACE);
//     //     gl.cullFace(gl.FRONT);
//     //
//     //     gl.clearDepth(1.0);
//     //     gl.clearColor(SCLightPoint.maxDistance, SCLightPoint.maxDistance, SCLightPoint.maxDistance, 1.0);
//     //     gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
//     //
//     //     pipeline.bind(
//     //         null,
//     //         view.mul(projection),
//     //         lightPosition,
//     //     );
//     //
//     //     for (let i = 0; i < listModel.length; i++) {
//     //         pipeline.setUniform(0, listModel[i].model);
//     //         listModel[i].draw();
//     //     }
//     // }
//
//     get opacity() {
//         return this._opacity;
//     }
//
//     get position() {
//         return this._position;
//     }
//
//     get resolution() {
//         return this._resolution;
//     }
//
//     get pipeline() {
//         return this._pipeline;
//     }
//
//     get listModel() {
//         return this._listModel;
//     }
//
//     get depth() {
//         return this._depthCube;
//     }
//
//     setOpacity(opacity) {
//         this._opacity = opacity;
//         this._updateBufferView();
//         return this;
//     }
//
//     setPosition() {
//         this._position = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
//         this._updateBufferView();
//         this._updateView();
//         this._updateDepth();
//         return this;
//     }
//
//     setResolution(resolution) {
//         this._resolution = resolution;
//         this._updateFramebuffer();
//         this._updateDepth();
//         return this;
//     }
//
//     setPipeline(pipeline) {
//         this._pipeline = pipeline;
//         this._updateDepth();
//         return this;
//     }
//
//     setListModel(listModel) {
//         this._listModel = listModel;
//         this._updateDepth();
//         return this;
//     }
// }
//
// SCLightPoint.maxDistance = Math.pow(2.0, 32.0) - 1.0;
//
// SCLightPoint.defaultVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
//
// uniform mat4 u_Model;
// uniform mat4 u_ViewProjection;
//
// out vec3 io_Position;
//
// void main() {
//     vec4 position = u_Model * vec4(i_Position, 1.0f);
//     io_Position = position.xyz;
//     gl_Position = u_ViewProjection * position;
// }`;
//
// SCLightPoint.defaultFS = `#version 300 es
// precision highp float;
//
// uniform vec3 lightPosition;
//
// in vec3 io_Position;
// out vec4 o_Color[6];
//
// void main() {
//     o_Color[0] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
//     o_Color[1] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
//     o_Color[2] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
//     o_Color[3] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
//     o_Color[4] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
//     o_Color[5] = vec4(distance(io_Position, lightPosition), 1.0f, 1.0f, 1.0f);
// }`;
