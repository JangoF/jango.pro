class SCCameraAround extends SCCamera {
    constructor() {
        super(...arguments);
    }

    _update() {
        this._view = Math.rotationZ(this._rotation.z).
            mul(Math.rotationY(this._rotation.y)).
            mul(Math.rotationX(this._rotation.x)).
            mul(Math.translation(this._position.x, this._position.y, this._position.z));

        super._updateProjection();
    }

    getCameraPosition() {
        return Math.rotationZ(this._rotation.z).mul(Math.rotationY(this._rotation.y)).mul(Math.rotationX(this._rotation.x)).mul(new V4(this._position.x, this._position.y, this._position.z, 1.0)).xyz;
    }
}
