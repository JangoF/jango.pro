class SCEssence {
    constructor(translation = new V3(0.0), rotation = new V3(0.0), scaling = new V3(1.0)) {
        this._position = translation;
        this._rotation = rotation;
        this._scaling  = scaling;

        this._update();
    }

    _update() {
        this._model = Math.translation(this._position.x, this._position.y, this._position.z).
            mul(Math.rotation(this._rotation.x, this._rotation.y, this._rotation.z)).
            mul(Math.scaling(this._scaling.x, this._scaling.y, this._scaling.z));
    }

    setPosition() {
        this._position = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    setRotation() {
        this._rotation = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    setScaling() {
        this._scaling = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    addPosition() {
        this._position = this._position.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    addRotation() {
        this._rotation = this._rotation.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    addScaling() {
        this._scaling = this._scaling.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    get model() {
        return this._model;
    }

    get position() {
        return this._position;
    }

    get rotation() {
        return this._rotation;
    }

    get scaling() {
        return this._scaling;
    }
}

// class SCObject {
//     constructor(gl, mesh, world) {
//         this._gl = gl;
//         this._mesh = mesh;
//         this._world = world;
//     }
//
//     draw(mode = this._gl.TRIANGLES) {
//         this._mesh.draw(mode);
//     }
//
//     get world() {
//         return this._world;
//     }
// }
