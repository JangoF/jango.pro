// Простейший источник освещения, не имеет позиции и на вход может принимать цвет, интенсивность и некоторую часть буфера.
// На выходе является источником освещения с возможностью изменения цвета, интенсивности, и автоматическим обновлением данных в заданом uniform буфере.

class SCAmbientLight {

    // Параметры конструктора:
    // (color: (V2 | V3 | V4 | Array), intensity: Number, (?)bufferView: GLBufferView)

    // Байтовая структура источника освещения (каждая переменная 4 байта, упакованы в блоки по 16 байт в каждом):
    // [color.r, color.g, color.b, intensity]

    constructor(color, intensity, bufferView) {
        this._color = new V4(...(new V3(color instanceof Array ? color : color.raw).raw), intensity);
        this._bufferView = bufferView;
    }

    makeRawData() {
        return new Float32Array(this._color.raw);
    }

    _updateBufferView() {
        if (this._bufferView instanceof GLBufferView) {
            this._bufferView.fill(this.makeRawData());
        }
    }

    get color() {
        return this._color.xyz;
    }

    get intensity() {
        return this._color.w;
    }

    set color(color) {
        this._color = new V4(...(new V3(color instanceof Array ? color : color.raw).raw));
        this._updateBufferView();
    }

    set intensity(intensity) {
        this._color.w = intensity;
    }
}

class SCPointLight extends SCAmbientLight {

    // Параметры конструктора:
    // (color: (V2 | V3 | V4 | Array), intensity: Number, position: (V2 | V3 | V4 | Array), shadowOpacity: Number, (?)bufferView: GLBufferView)

    // Байтовая структура источника освещения (каждая переменная 4 байта, упакованы в блоки по 16 байт в каждом):
    // [color.r, color.g, color.b, intensity]
    // [position.x, position.y, position.z, shadowOpacity]

    constructor(color, intensity, position, shadowOpacity, bufferView) {
        super(color, intensity, bufferView);
        this._position = new V4(...(new V3(position instanceof Array ? position : position.raw).raw), shadowOpacity);
    }

    makeRawData() {
        return new Float32Array(this._color.raw.concat(this._position.raw));
    }

    _updateBufferView() {
        if (this._bufferView instanceof GLBufferView) {
            this._bufferView.fill(this.makeRawData());
        }
    }

    get position() {
        return this._position.xyz;
    }

    get shadowOpacity() {
        return this._position.w;
    }

    set position(position) {
        this._position = new V4(...(new V3(position instanceof Array ? position : position.raw).raw), this._position.w);
        this._updateBufferView();
    }

    set shadowOpacity(shadowOpacity) {
        this._position.w = shadowOpacity;
        this._updateBufferView();
    }
}

class SCDirectionalLight extends SCAmbientLight {

    // Параметры конструктора:
    // (color: (V2 | V3 | V4 | Array), intensity: Number, direction: (V2 | V3 | V4 | Array), shadowOpacity: Number, (?)bufferView: GLBufferView)

    // Байтовая структура источника освещения (каждая переменная 4 байта, упакованы в блоки по 16 байт в каждом):
    // [color.r, color.g, color.b, intensity]
    // [direction.x, direction.y, direction.z, shadowOpacity]

    constructor(color, intensity, direction, shadowOpacity, bufferView) {
        super(color, intensity, bufferView);
        this._direction = new V4(...(new V3(direction instanceof Array ? direction : direction.raw).raw), shadowOpacity);
    }

    makeRawData() {
        return new Float32Array(this._color.raw.concat(this._direction.raw));
    }

    _updateBufferView() {
        if (this._bufferView instanceof GLBufferView) {
            this._bufferView.fill(this.makeRawData());
        }
    }

    get direction() {
        return this._direction.xyz;
    }

    get shadowOpacity() {
        return this._direction.w;
    }

    set direction(direction) {
        this._direction = new V4(...(new V3(direction instanceof Array ? direction : direction.raw).raw), this._direction.w);
        this._updateBufferView();
    }

    set shadowOpacity(shadowOpacity) {
        this._direction.w = shadowOpacity;
        this._updateBufferView();
    }
}

class SCSpotLight extends SCAmbientLight {

    // Параметры конструктора:
    // (color: (V2 | V3 | V4 | Array), intensity: Number, innerAngle: Number, outerAngle: Number, position: (V2 | V3 | V4 | Array), direction: (V2 | V3 | V4 | Array), shadowOpacity: Number, (?)bufferView: GLBufferView)

    // Байтовая структура источника освещения (каждая переменная 4 байта, упакованы в блоки по 16 байт в каждом):
    // [color.r, color.g, color.b, intensity]
    // [innerAngle, outerAngle, shadowOpacity, 0.0]
    // [position.x, position.y, position.z, 0.0]
    // [direction.x, direction.y, direction.z, 0.0]

    constructor(color, intensity, innerAngle, outerAngle, position, direction, shadowOpacity, bufferView) {
        super(color, intensity, bufferView);

        this._angle = new V4(innerAngle, outerAngle, shadowOpacity);
        this._position = new V4(...(new V3(position instanceof Array ? position : position.raw).raw));
        this._direction = new V4(...(new V3(direction instanceof Array ? direction : direction.raw).raw));
    }

    makeRawData() {
        return new Float32Array(this._color.raw.concat(this._angle.raw.concat(this._position.raw.concat(this._direction.raw))));
    }

    _updateBufferView() {
        if (this._bufferView instanceof GLBufferView) {
            this._bufferView.fill(this.makeRawData());
        }
    }

    setShadowComponent(gl, resolution) {
        let resolutionL = new V2(resolution instanceof Array ? resolution : resolution.raw);
        this._framebuffer = new GLFramebuffer(gl, [
            [new GLTexture(gl, gl.DEPTH_COMPONENT32F, resolutionL.x, resolutionL.y, gl.NEAREST), gl.DEPTH_ATTACHMENT]
        ]);
    }

    updateShadows() {
        // TODO!
    }

    get innerAngle() {
        return this._angle.x;
    }

    get outerAngle() {
        return this._angle.y;
    }

    get position() {
        return this._position.xyz;
    }

    get direction() {
        return this._direction.xyz;
    }

    get shadowOpacity() {
        return this._angle.z;
    }

    set innerAngle(innerAngle) {
        this._angle.x = innerAngle;
        this._updateBufferView();
    }

    set outerAngle(outerAngle) {
        this._angle.y = outerAngle;
        this._updateBufferView();
    }

    set position(position) {
        this._position = new V4(...(new V3(position instanceof Array ? position : position.raw).raw));
        this._updateBufferView();
    }

    set direction(direction) {
        this._direction = new V4(...(new V3(direction instanceof Array ? direction : direction.raw).raw));
        this._updateBufferView();
    }

    set shadowOpacity(shadowOpacity) {
        this._angle.z = shadowOpacity;
        this._updateBufferView();
    }
}
