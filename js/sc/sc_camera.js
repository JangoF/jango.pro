class SCCamera extends SCCameraBase {
    constructor() {
        super(...arguments);

        this._position = new V3(0.0);
        this._rotation = new V3(0.0);

        this._update();
    }

    _update() {
        this._view = Math.translation(this._position.x, this._position.y, this._position.z).
            mul(Math.rotationZ(this._rotation.z)).
            mul(Math.rotationY(this._rotation.y)).
            mul(Math.rotationX(this._rotation.x));

        super._updateProjection();
    }

    setPosition() {
        this._position = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    setRotation() {
        this._rotation = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    addPosition() {
        this._position = this._position.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    addRotation() {
        this._rotation = this._rotation.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    get position() {
        return this._position;
    }

    get rotation() {
        return this._rotation;
    }

    toCSS(width, height) {
        let view = Math.translation(this._position.x, this._position.y, this._position.z).
            mul(Math.rotationZ(this._rotation.z)).
            mul(Math.rotationY(this._rotation.y)).
            mul(Math.rotationX(-this._rotation.x));

        return view.mul(this.isPerspective == true ? Math.perspectiveCSS(width, height, this._fov) : new M4());
    }
}
