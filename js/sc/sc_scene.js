class SCScene {
    constructor(gl, fbxScene) {
        {
            // Получаем список данных о мешах вида { listIndex: [], listVertex: [], listIndexElementSize: Number, translation: V3, rotation: V3, scaling: V3 }:

            let listMeshRaw = SCScene._makeListMeshRawFromFBX(fbxScene);

            let listTotalIndex = [];
            let listTotalVertex = [];

            // Формируем итоговые массивы индексов и вершин:

            let listIndexMaxElementSize = 0;
            for (let i = 0; i < listMeshRaw.length; i++) {

                listTotalIndex = listTotalIndex.concat(listMeshRaw[i].listIndex);
                listTotalVertex = listTotalVertex.concat(listMeshRaw[i].listVertex);

                listIndexMaxElementSize = Math.max(listIndexMaxElementSize, listMeshRaw[i].listIndexElementSize);
            }

            // Загружаем получившиеся массивы в видеопамять:

            this._bufferIndex = new GLBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, listIndexMaxElementSize === 2 ? new Uint16Array(listTotalIndex) : new Uint32Array(listTotalIndex));
            this._bufferVertex = new GLBuffer(gl, gl.ARRAY_BUFFER, new Float32Array(listTotalVertex));

            this._listModel = [];

            // Формируем список объектов SCModel ссылаясь на диапазоны из итоговых массивов индексов и вершин:

            let currentOffsetIndex = 0;
            let currentOffsetVertex = 0;

            for (let i = 0; i < listMeshRaw.length; i++) {
                let listIndexCurrentSize = listMeshRaw[i].listIndex.length * listIndexMaxElementSize;
                let listVertexCurrentSize = listMeshRaw[i].listVertex.length * 4;

                let currentMesh = new GLMesh(gl, new GLBufferView(this._bufferIndex, currentOffsetIndex, listIndexCurrentSize), new GLBufferView(this._bufferVertex, currentOffsetVertex, listVertexCurrentSize), [3, 3, 3, 3, 2]);
                let currentModel = new SCModel(currentMesh, listMeshRaw[i].translation, listMeshRaw[i].rotation, listMeshRaw[i].scaling);

                this._listModel.push(currentModel);

                currentOffsetIndex += listIndexCurrentSize;
                currentOffsetVertex += listVertexCurrentSize;
            }
        }

        {
            // Получаем шаг выравнивания смещения для uniform буфера:

            let uniformBufferOffsetAlignment = gl.getParameter(gl.UNIFORM_BUFFER_OFFSET_ALIGNMENT);

            // Получаем список данных об объектах типа NodeAttribute:

            let listNodeAttribute = SCScene._makeListNodeAttributeFromFBX(fbxScene);
            let listLightTotal = [];

            let rawDataListPointLightOffset = listLightTotal.length * 4.0; // Смещение указывающее на начало данных о точечтных источниках освещения.
            {
                // Формируем массив сырых данных о точечных источниках освещения:

                for (let i = 0; i < listNodeAttribute.listPointLight.length; i++) {
                    let currentPointLight = listNodeAttribute.listPointLight[i];
                    let rawData = currentPointLight.color.raw.concat([currentPointLight.intensity].concat(currentPointLight.translation.raw.concat([currentPointLight.shadowOpacity])));
                    listLightTotal = listLightTotal.concat(rawData);
                }

                // Дополняем массив пустыми значениями для соответствия выравниванию (32 - размер данных в байтах об одном точечном источнике освещения):

                let listPointLightSize = listNodeAttribute.listPointLight.length * 32.0;
                let additionaElementCount = (Math.ceil(listPointLightSize / uniformBufferOffsetAlignment) * uniformBufferOffsetAlignment - listPointLightSize) / 4.0;
                listLightTotal = listLightTotal.concat([].resized(additionaElementCount, 0.0));
            }

            let rawDataListDirectionalLightOffset = listLightTotal.length * 4.0; // Смещение указывающее на начало данных о точечтных источниках освещения.
            {
                // Формируем массив сырых данных о направленных источниках освещения:

                for (let i = 0; i < listNodeAttribute.listDirectionalLight.length; i++) {
                    let currentDirectionalLight = listNodeAttribute.listDirectionalLight[i];
                    let rawData = currentDirectionalLight.color.raw.concat([currentDirectionalLight.intensity].concat(currentDirectionalLight.direction.raw.concat([currentDirectionalLight.shadowOpacity])));
                    listLightTotal = listLightTotal.concat(rawData);
                }

                // Дополняем массив пустыми значениями для соответствия выравниванию (32 - размер данных в байтах об одном направленном источнике освещения):

                let listDirectionalLightSize = listNodeAttribute.listDirectionalLight.length * 32.0;
                let additionaElementCount = (Math.ceil(listDirectionalLightSize / uniformBufferOffsetAlignment) * uniformBufferOffsetAlignment - listDirectionalLightSize) / 4.0;
                listLightTotal = listLightTotal.concat([].resized(additionaElementCount, 0.0));
            }

            this._listSpotLight = [];
            let rawDataListSpotLightOffset = listLightTotal.length * 4.0; // Смещение указывающее на начало данных о прожекторных источниках освещения.
            {
                // Формируем массив сырых данных о прожекторных источниках освещения:

                for (let i = 0; i < listNodeAttribute.listSpotLight.length; i++) {
                    let currentSpotLight = listNodeAttribute.listSpotLight[i];

                    let rawData = currentSpotLight.color.raw.concat([currentSpotLight.intensity]);
                    rawData = rawData.concat([currentSpotLight.innerAngle].concat([currentSpotLight.outerAngle].concat([currentSpotLight.shadowOpacity, 0.0])));
                    rawData = rawData.concat(currentSpotLight.translation.raw.concat([0.0]));
                    rawData = rawData.concat(currentSpotLight.direction.raw.concat([0.0]));

                    listLightTotal = listLightTotal.concat(rawData);
                }

                // Дополняем массив пустыми значениями для соответствия выравниванию (64 - размер данных в байтах об одном прожекторном источнике освещения):

                let listSpotLightSize = listNodeAttribute.listSpotLight.length * 64.0;
                let additionaElementCount = (Math.ceil(listSpotLightSize / uniformBufferOffsetAlignment) * uniformBufferOffsetAlignment - listSpotLightSize) / 4.0;
                listLightTotal = listLightTotal.concat([].resized(additionaElementCount, 0.0));
            }

            // Загружаем данные о сточниках освещения в видеопамять:

            this._bufferUniform = new GLBuffer(gl, gl.UNIFORM_BUFFER, new Float32Array(listLightTotal));

            {
                // Формируем указатель на блок данных о точечных источниках освещения:

                this._bufferViewListPointLight = new GLBufferView(this._bufferUniform, rawDataListPointLightOffset, listNodeAttribute.listPointLight.length * 32.0);
                this._listPointLight = [];

                // Формируем по указателю на каждый из блоков данных для каждого точечного иточника освещения:

                let currentOffsetPointLight = rawDataListPointLightOffset;
                for (let i = 0; i < listNodeAttribute.listPointLight.length; i++) {
                    let currentPointLight = listNodeAttribute.listPointLight[i];
                    let currentBufferView = new GLBufferView(this._bufferUniform, currentOffsetPointLight, 32.0);

                    this._listPointLight.push(
                        new SCLightPoint(
                            gl,
                            currentPointLight.color,
                            currentPointLight.intensity,
                            currentBufferView,
                            currentPointLight.shadowOpacity,
                            currentPointLight.translation,
                            undefined,
                            undefined,
                            this._listModel,
                        )
                    );
                    currentOffsetPointLight += 32.0;
                }
            }

            {
                // Формируем указатель на блок данных о направленных источниках освещения:

                this._bufferViewListDirectionalLight = new GLBufferView(this._bufferUniform, rawDataListDirectionalLightOffset, listNodeAttribute.listDirectionalLight.length * 32.0);
                this._listDirectionalLight = [];

                // Формируем по указателю на каждый из блоков данных для каждого направленного иточника освещения:

                let currentOffsetDirectionalLight = rawDataListDirectionalLightOffset;
                for (let i = 0; i < listNodeAttribute.listDirectionalLight.length; i++) {
                    let currentDirectionalLight = listNodeAttribute.listDirectionalLight[i];
                    let currentBufferView = new GLBufferView(this._bufferUniform, currentOffsetDirectionalLight, 32.0);

                    this._listDirectionalLight.push(
                        new SCLightDirectional(
                            gl,
                            currentDirectionalLight.color,
                            currentDirectionalLight.intensity,
                            currentBufferView,
                            currentDirectionalLight.shadowOpacity,
                            undefined,
                            currentDirectionalLight.direction,
                            undefined,
                            undefined,
                            undefined,
                            this._listModel,
                        )
                    );

return;

                    currentOffsetDirectionalLight += 32.0;
                }
            }

            {
                // Формируем указатель на блок данных о прожекторных источниках освещения:

                this._bufferViewListSpotLight = new GLBufferView(this._bufferUniform, rawDataListSpotLightOffset, listNodeAttribute.listSpotLight.length * 64.0);
                this._listSpotLight = [];

                // Формируем по указателю на каждый из блоков данных для каждого прожекторного иточника освещения:

                let currentOffsetSpotLight = rawDataListSpotLightOffset;
                for (let i = 0; i < listNodeAttribute.listSpotLight.length; i++) {
                    let currentSpotLight = listNodeAttribute.listSpotLight[i];
                    let currentBufferView = new GLBufferView(this._bufferUniform, currentOffsetSpotLight, 64.0);

                    // console.log(
                    //     new SCLightSpot(gl).setColor(currentSpotLight.color).setIntensity(currentSpotLight.intensity).
                    //     setAngle(currentSpotLight.innerAngle, currentSpotLight.outerAngle).
                    //     setPosition(currentSpotLight.translation).setDirection(currentSpotLight.direction).
                    //     setOpacity(currentSpotLight.shadowOpacity).setBufferView(currentBufferView)
                    // );

                    this._listSpotLight.push(
                        new SCLightSpot(
                            gl,
                            currentSpotLight.color,
                            currentSpotLight.intensity,
                            currentBufferView,
                            currentSpotLight.shadowOpacity,
                            new V2(currentSpotLight.innerAngle, currentSpotLight.outerAngle),
                            currentSpotLight.direction,
                            currentSpotLight.translation,
                            undefined,
                            undefined,
                            this._listModel,
                        )
                    );

                    currentOffsetSpotLight += 64.0;
                }
            }

            {
                this._listCamera = listNodeAttribute.listCamera;
                this._currentCamera = this._listCamera.length > 0 ? this._listCamera[0] : new SCCameraLookAt(0.01, 128.0, 75.0, 1.0).setPosition(new V3(0.0, 4.0, 16.0)).setTarget(new V3(0.0, 0.0, 0.0)).setUp(new V3(0.0, 1.0, 0.0));
            }
        }
    }

    get listModel() {
        return this._listModel;
    }

    get bufferViewListPointLight() {
        return this._bufferViewListPointLight;
    }

    get bufferViewListDirectionalLight() {
        return this._bufferViewListDirectionalLight;
    }

    get bufferViewListSpotLight() {
        return this._bufferViewListSpotLight;
    }

    get listPointLight() {
        return this._listPointLight;
    }

    get listDirectionalLight() {
        return this._listDirectionalLight;
    }

    get listSpotLight() {
        return this._listSpotLight;
    }

    get listCamera() {
        return this._listCamera;
    }

    get currentCamera() {
        return this._currentCamera;
    }

    set currentCamera(currentCamera) {
        this._currentCamera = currentCamera instanceof Number ? this._cameraList[i] : currentCamera;
    }

    static _anyRotationOrderToXYZ(rotationOrder, rotationVector) {
        switch (rotationOrder) {
            case 0: return rotationVector.xyz;
            case 1: return rotationVector.zxy;
            case 2: return rotationVector.yzx;
            case 3: return rotationVector.xzy;
            case 4: return rotationVector.yxz;
            case 5: return rotationVector.zyx;
        }
    }

    static _ejectDefaultObjectTypeFromName(fbxScene, objectType) {
        let listDefaultObjectType = fbxScene.Definitions.ObjectType instanceof Array ? fbxScene.Definitions.ObjectType : [fbxScene.Definitions.ObjectType];
        for (let i = 0; i < listDefaultObjectType.length; i++) {

            if (listDefaultObjectType[i].HEADER === objectType) {
                return listDefaultObjectType[i];
            }
        }
    }

    static _ejectPropertyFromObjectWithDefault(defaultObject, primaryObject, propertyName) {
        let listProperty = primaryObject.Properties70.P.first() instanceof Array ? primaryObject.Properties70.P : [primaryObject.Properties70.P];
        for (let i = 0; i < listProperty.length; i++) {

            if (listProperty[i].first() === propertyName) {
                return listProperty[i];
            }
        }

        let listPropertyDefault = defaultObject.PropertyTemplate.Properties70.P.first() instanceof Array ? defaultObject.PropertyTemplate.Properties70.P : [defaultObject.PropertyTemplate.Properties70.P];
        for (let i = 0; i < listPropertyDefault.length; i++) {

            if (listPropertyDefault[i].first() === propertyName) {
                return listPropertyDefault[i];
            }
        }
    }

    static _makeV3FromProperty(property, rotationOrder) {
        let vector = new V3(property[property.length - 3], property[property.length - 2], property[property.length - 1]);

        if (rotationOrder !== undefined && rotationOrder !== null) {
            return SCScene._anyRotationOrderToXYZ(rotationOrder, vector).inv().toRadians();
        }

        return vector;
    }

    static _ejectModelFromObjectId(fbxScene, objectId) {
        let listConnection = fbxScene.Connections.C instanceof Array ? fbxScene.Connections.C : [fbxScene.Connections.C];
        for (let i = 0; i < listConnection.length; i++) {

            if (listConnection[i][1] === objectId) {
                let listModel = fbxScene.Objects.Model instanceof Array ? fbxScene.Objects.Model : [fbxScene.Objects.Model];

                for (let j = 0; j < listModel.length; j++) {
                    if (listModel[j].HEADER.first() === listConnection[i][2]) {
                        return listModel[j];
                    }
                }
            }
        }
    }

    // Получение списка прочих объектов из FBX файла:

    static _makeListNodeAttributeFromFBX(fbxScene) {
        let listNodeAttribute = fbxScene.Objects.NodeAttribute instanceof Array ? fbxScene.Objects.NodeAttribute : [fbxScene.Objects.NodeAttribute];

        let defaultNodeAttribute = SCScene._ejectDefaultObjectTypeFromName(fbxScene, 'NodeAttribute');
        let defaultModel = SCScene._ejectDefaultObjectTypeFromName(fbxScene, 'Model');

        let listPointLight = [];
        let listDirectionalLight = [];
        let listSpotLight = [];
        let listCamera = [];

        for (let i = 0; i < listNodeAttribute.length; i++) {
            let currentModel = SCScene._ejectModelFromObjectId(fbxScene, listNodeAttribute[i].HEADER.first());

            switch (listNodeAttribute[i].TypeFlags) {
                case 'Light':

                    // Выясняем тип источника освещения:

                    switch (SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, listNodeAttribute[i], 'LightType').last()) {

                        // Точечный источник освещения.

                        case 0:
                            listPointLight.push(SCScene._makePointLightRawFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, listNodeAttribute[i]));
                            break;

                        // Направленный источник освещения.

                        case 1:
                            listDirectionalLight.push(SCScene._makeDirectionalLightRawFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, listNodeAttribute[i]));
                            break;

                        // Прожекторный источник освещения.

                        case 2:
                            listSpotLight.push(SCScene._makeSpotLightRawFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, listNodeAttribute[i]));
                            break;
                    }
                    break;

                case 'Camera':
                    listCamera.push(SCScene._makeListCameraFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, listNodeAttribute[i]));
                    break;
            }
        }

        return { listPointLight: listPointLight, listDirectionalLight: listDirectionalLight, listSpotLight: listSpotLight, listCamera: listCamera };
    }

    // Получение списка источников освещения из FBX файла:

    static _makePointLightRawFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, currentNodeAttribute) {
        let propertyColor          = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'Color'));
        let propertyIntensity      = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'Intensity').last();
        let propertyLclTranslation = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclTranslation'));
        let propertyCastShadows    = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'CastShadows').last();

        return { color: propertyColor, intensity: propertyIntensity / 100.0, translation: propertyLclTranslation, shadowOpacity: propertyCastShadows };
    }

    static _makeDirectionalLightRawFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, currentNodeAttribute) {
        let propertyColor         = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'Color'));
        let propertyIntensity     = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'Intensity').last();
        let propertyRotationOrder = SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'RotationOrder').last();
        let propertyLclRotation   = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclRotation'), propertyRotationOrder);
        let propertyCastShadows   = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'CastShadows').last();

        let lightDirection = Math.rotation(propertyLclRotation.x, propertyLclRotation.y, propertyLclRotation.z).mul(new V4(0.0, 0.0, -1.0, 0.0));
        return { color: propertyColor, intensity: propertyIntensity / 100.0, direction: lightDirection.xyz, shadowOpacity: propertyCastShadows };
    }

    static _makeSpotLightRawFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, currentNodeAttribute) {
        let propertyColor          = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'Color'));
        let propertyIntensity      = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'Intensity').last();
        let propertyInnerAngle     = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'InnerAngle').last();
        let propertyOuterAngle     = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'OuterAngle').last();
        let propertyRotationOrder  = SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'RotationOrder').last();
        let propertyLclRotation    = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclRotation'), propertyRotationOrder);
        let propertyLclTranslation = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclTranslation'));
        let propertyCastShadows    = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'CastShadows').last();

        let lightDirection = Math.rotation(propertyLclRotation.x, propertyLclRotation.y, propertyLclRotation.z).mul(new V4(0.0, 0.0, -1.0, 0.0));
        return { color: propertyColor, intensity: propertyIntensity / 100.0, innerAngle: propertyInnerAngle, outerAngle: propertyOuterAngle, translation: propertyLclTranslation, direction: lightDirection.xyz, shadowOpacity: propertyCastShadows };
    }

    // Получение списка камер из FBX файла:

    static _makeListCameraFromFBX(fbxScene, defaultModel, defaultNodeAttribute, currentModel, currentNodeAttribute) {
        let propertyPosition = SCScene._makeV3FromProperty(currentNodeAttribute.Position);
        let propertyLookAt = SCScene._makeV3FromProperty(currentNodeAttribute.LookAt);
        let propertyUp = SCScene._makeV3FromProperty(currentNodeAttribute.Up);

        let propertyNearPlane = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'NearPlane').last();
        let propertyFarPlane = SCScene._ejectPropertyFromObjectWithDefault(defaultNodeAttribute, currentNodeAttribute, 'FarPlane').last();

        return new SCCameraLookAt(propertyNearPlane, propertyFarPlane, Math.toRadians(75.0), 1.0).setPosition(propertyPosition).setTarget(propertyLookAt).setUp(propertyUp);
    }

    // Получение списка моделей из FBX файла:

    static _makeMeshRawFBXData(currentGeometry, defaultModel, currentModel) {

        // Находит LayerElement в указанном блоке Geometry по указанному типу (LayerElementUV, LayerElementNormal и т.д.) и указанному UVSet:

        function findLayerElementFromTypeAndUVSet(currentGemometry, layerElementType, currentUVSet = '') {
            let listLayerElement = currentGemometry[layerElementType] instanceof Array ? currentGemometry[layerElementType] : [currentGemometry[layerElementType]];
            for (let i = 0; i < listLayerElement.length; i++) {

                if (listLayerElement[i].Name === currentUVSet) {
                    return listLayerElement[i];
                }
            }
        }

        // Находит нужный элемент в заданном массиве учитывая заданные параметры:

        function findLayerElementBlockItem(listValue, listIndexValue, mappingInformationType, referenceInformationType, listIndexGlobal, orderedIndex, dataElementCount) {
            let calculatedListValueIndex = 0;

            switch (mappingInformationType) {

                // Данные определены для каждого полигона:

                case 'ByPolygon':
                calculatedListValueIndex = Math.floor(orderedIndex / 3); // Предполагается что в качестве полигонов могут быть лишь треугольники.
                break;

                // Данные определены для каждого индекса в массиве индексов блока Geometry:

                case 'ByPolygonVertex':
                calculatedListValueIndex = orderedIndex;
                break;

                // Данные определены для каждой вершины в массиве вершин блока Geometry:

                case 'ByVertex', 'ByVertice':
                calculatedListValueIndex = listIndexGlobal[orderedIndex];
                break;
            }

            // В случае если параметр 'ReferenceInformationType' равен 'IndexToDirect', а так же если существует массив индексов текущего блока, данные берутся через его 'призму':

            if (referenceInformationType == 'IndexToDirect' && listIndexValue !== undefined && listIndexValue !== null) {
                calculatedListValueIndex = listIndexValue[calculatedListValueIndex];
            }

            // Формируем результирующий массив данных:

            let result = [];

            for (let i = 0; i < dataElementCount; i++) {
                result.push(listValue[calculatedListValueIndex * dataElementCount + i]);
            }

            return result;
        }

        // Находит в указанном массиве указанный набор элементов и возвращает его позицию, иначе возвращает -1:

        function findDataInDataList(listData, currentData) {
            for (let i = 0; i < listData.length; i += currentData.length) {

                let isFound = true;
                for (let j = 0; j < currentData.length; j++) {

                    if (listData[i + j] !== currentData[j]) {
                        isFound = false;
                        break;
                    }
                }

                if (isFound === true) {
                    return i / currentData.length;
                }
            }

            return -1;
        }

        let currentUVSet = SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'currentUVSet').last();

        // Подготоавливаем массив глобальных индексов и массив глобальных вертексов:

        let listPolygonVertexIndex = currentGeometry.PolygonVertexIndex.a.map(function (item) { return item < 0 ? -item - 1 : item });
        let listVertices = currentGeometry.Vertices.a.reduce((accumulator, item, index) => (index % 3 === 0 ? accumulator.push([item]) : accumulator[accumulator.length - 1].push(item)) && accumulator, []);

        // Получаем списки дополнительных вершинных атрибутов:

        let currentLayerElementNormal = findLayerElementFromTypeAndUVSet(currentGeometry, 'LayerElementNormal');
        let currentLayerElementBinormal = findLayerElementFromTypeAndUVSet(currentGeometry, 'LayerElementBinormal', currentUVSet);
        let currentLayerElementTangent = findLayerElementFromTypeAndUVSet(currentGeometry, 'LayerElementTangent', currentUVSet);
        let currentLayerElementUV = findLayerElementFromTypeAndUVSet(currentGeometry, 'LayerElementUV', currentUVSet);

        // Проходимся по глобальному массиву индексов параллельно формируя новые массивы - вершин и индексов:

        let listVertex = [];
        let listIndex = [];

        for (let i = 0; i < listPolygonVertexIndex.length; i++) {
            let currentPosition = listVertices[listPolygonVertexIndex[i]];

            let currentNormal = findLayerElementBlockItem(currentLayerElementNormal.Normals.a, currentLayerElementNormal.NormalsW.a, currentLayerElementNormal.MappingInformationType, currentLayerElementNormal.ReferenceInformationType, listPolygonVertexIndex, i, 3);
            let currentBinormal = findLayerElementBlockItem(currentLayerElementBinormal.Binormals.a, currentLayerElementBinormal.BinormalsW.a, currentLayerElementBinormal.MappingInformationType, currentLayerElementBinormal.ReferenceInformationType, listPolygonVertexIndex, i, 3);
            let currentTangent = findLayerElementBlockItem(currentLayerElementTangent.Tangents.a, currentLayerElementTangent.TangentsW.a, currentLayerElementTangent.MappingInformationType, currentLayerElementTangent.ReferenceInformationType, listPolygonVertexIndex, i, 3);
            let currentUV = findLayerElementBlockItem(currentLayerElementUV.UV.a, currentLayerElementUV.UVIndex.a, currentLayerElementUV.MappingInformationType, currentLayerElementUV.ReferenceInformationType, listPolygonVertexIndex, i, 2);

            // Формируем данные для текущей вершины:

            let currentVertexData = [
                currentPosition[0], currentPosition[1], currentPosition[2],
                currentNormal[0],   currentNormal[1],   currentNormal[2],
                currentBinormal[0], currentBinormal[1], currentBinormal[2],
                currentTangent[0],  currentTangent[1],  currentTangent[2],
                currentUV[0],       currentUV[1]
            ];

            // В случае если сформированный выше набор данных является уникальным - добавляем его к общему списку вершин, иначе просто добавляем к списку индексов найденный:

            let newPolygonVertexIndex = findDataInDataList(listVertex, currentVertexData);
            if (newPolygonVertexIndex < 0) {

                listIndex.push(listVertex.length / currentVertexData.length);
                listVertex = listVertex.concat(currentVertexData);

            } else {
                listIndex.push(newPolygonVertexIndex);
            }
        }
        // let str = "";
        // for (var i = 0; i < listVertex.length; i++) {
        //     str += String(listVertex[i]) + ","
        // }
        //
        // console.log(str);

        return { listIndex: listIndex, listVertex: listVertex, listIndexElementSize: (listVertex.length / 14) > 65536 ? 4 : 2 };
    }

    static _makeListMeshRawFromFBX(fbxScene) {
        let listGeometry = fbxScene.Objects.Geometry instanceof Array ? fbxScene.Objects.Geometry : [fbxScene.Objects.Geometry];
        let defaultModel = SCScene._ejectDefaultObjectTypeFromName(fbxScene, 'Model');

        // Формируем список данных о геометрии вкупе с данными о их позиционировании:

        let listMeshRaw = [];

        for (let i = 0; i < listGeometry.length; i++) {
            let currentModel = SCScene._ejectModelFromObjectId(fbxScene, listGeometry[i].HEADER.first());

            let propertyRotationOrder = SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'RotationOrder').last();
            let propertyLclTranslation = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclTranslation'));
            let propertyLclRotation = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclRotation'), propertyRotationOrder);
            let propertyLclScaling = SCScene._makeV3FromProperty(SCScene._ejectPropertyFromObjectWithDefault(defaultModel, currentModel, 'LclScaling'));

            let result = SCScene._makeMeshRawFBXData(listGeometry[i], defaultModel, currentModel);
            result['translation'] = propertyLclTranslation;
            result['rotation'] = propertyLclRotation;
            result['scaling'] = propertyLclScaling;

            listMeshRaw.push(result);
        }

        return listMeshRaw;
    }
}

// SCScene.squareMesh = new GLMesh(gl, new Uint16Array([0, 1, 2, 2, 1, 3]), new Float32Array([-1.0, -1.0, 0.0, 0.0,
//                                                                                             1.0, -1.0, 1.0, 0.0,
//                                                                                            -1.0,  1.0, 0.0, 1.0,
//                                                                                             1.0,  1.0, 1.0, 1.0]), [2, 2]);
