class SCCameraBase {
    constructor() {
        this._near   = arguments[0];
        this._far    = arguments[1];

        this._fov    = null;
        this._aspect = null;

        this._top    = null;
        this._left   = null;
        this._right  = null;
        this._bottom = null;

        if (arguments.length == 4) { // nearPlane, farPlane, fov, aspectRatio
            this._fov    = arguments[2];
            this._aspect = arguments[3];
        }
        else if (arguments.length == 6) { // nearPlane, farPlane, top, left, right, bottom
            this._top    = arguments[2];
            this._left   = arguments[3];
            this._right  = arguments[4];
            this._bottom = arguments[5];
        }
    }

    setClip(nearPlane, farPlane) {
        this._near   = nearPlane;
        this._far    = farPlane;

        this._update();
    }

    setProjection() {
        if (arguments.length == 2) { // fov, aspectRatio
            this._fov    = arguments[0];
            this._aspect = arguments[1];

            this._top    = null;
            this._left   = null;
            this._right  = null;
            this._bottom = null;
        }
        else if (arguments.length == 4) { //top, left, right, bottom
            this._fov    = null;
            this._aspect = null;

            this._top    = arguments[0];
            this._left   = arguments[1];
            this._right  = arguments[2];
            this._bottom = arguments[3];
        }

        this._update();
        return this;
    }

    _updateProjection() {
        this._projection = this.isPerspective == true ?
            Math.perspective(this._near, this._far, this._fov, this._aspect) :
            Math.orthogonal(this._left, this._right, this._top, this._bottom, this._near, this._far);

        this._viewProjection = this._view.mul(this._projection);
    }

    get fov() {
        return this._fov;
    }

    get view() {
        return this._view;
    }

    get projection() {
        return this._projection;
    }

    get viewProjection() {
        return this._viewProjection;
    }

    get isPerspective() {
        return this._top === null;
    }

    get isOrthogonal() {
        return this._fov === null;
    }
}
