Array.prototype.resized = function (length, value = null) {
    if (length <= this.length) {
        return this.slice(0, length)
    }

    let sub = length - this.length;
    return this.concat(Array(sub).fill(value, 0, sub));
};

Array.prototype.placed = function (start, array, end = -1) {
    let result = this.slice();
    for (let i = 0; i < Math.min(array.length); i++) {
        if (i == end) {
            break;
        }
        result[start + i] = array[i];
    }
    return result;
};

Array.prototype.first = function () {
    return this[0];
};

Array.prototype.last = function () {
    return this[this.length - 1];
};

function requestAnimationFrameWithSetInterval(code, interval) {
    setInterval(() => requestAnimationFrame(code), interval);
}

String.prototype.first = function () {
    return this[0];
};

String.prototype.last = function () {
    return this[this.length - 1];
};
