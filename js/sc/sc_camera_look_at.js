class SCCameraLookAt extends SCCameraBase {
    constructor() {
        super(...arguments);

        this._position = new V3(0.0);
        this._target   = new V3(0.0, 0.0, -1.0);
        this._up       = new V3(0.0, 1.0,  0.0);

        this._update();
    }

    _update() {
        this._view = Math.lookAt(this._position, this._target, this._up);
        super._updateProjection();
    }

    setPosition() {
        this._position = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    setTarget() {
        this._target = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    setUp() {
        this._up = arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments);
        this._update();

        return this;
    }

    addPosition() {
        this._position = this._position.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    addTarget() {
        this._target = this._target.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    addUp() {
        this._up = this._up.sum(arguments[0] instanceof V3 ? arguments[0] : new V3(...arguments));
        this._update();

        return this;
    }

    get position() {
        return this._position;
    }

    get target() {
        return this._target;
    }

    get up() {
        return this._up;
    }

    getPhyramideVertexList() {
        let xN = Math.sin(this._fov / this._aspect / 2.0) * this._near;
        let yN = Math.sin(this._fov / 2.0) * this._near;
        let zN = this._near; // Math.cos(this._fov * 2.0) * this._near;

        let xF = Math.sin(this._fov / this._aspect / 2.0) * this._far;
        let yF = Math.sin(this._fov / 2.0) * this._far;
        let zF = this._far // Math.cos(this._fov * 2.0) * this._far;

        let NRT = new V4( xN,  yN, zN, 1.0);
        let NRB = new V4( xN, -yN, zN, 1.0);
        let NLT = new V4(-xN,  yN, zN, 1.0);
        let NLB = new V4(-xN, -yN, zN, 1.0);

        let FRT = new V4( xF,  yF, zF, 1.0);
        let FRB = new V4( xF, -yF, zF, 1.0);
        let FLT = new V4(-xF,  yF, zF, 1.0);
        let FLB = new V4(-xF, -yF, zF, 1.0);

        return [
            this._view.mul(NRT).xyz,
            this._view.mul(NRB).xyz,
            this._view.mul(NLT).xyz,
            this._view.mul(NLB).xyz,
            this._view.mul(FRT).xyz,
            this._view.mul(FRB).xyz,
            this._view.mul(FLT).xyz,
            this._view.mul(FLB).xyz
        ];
    }

    toCSS(width, height) {
        // TODO
        // let view = Math.translation(this._position.x, this._position.y, this._position.z).
        //     mul(Math.rotationZ(this._rotation.z)).
        //     mul(Math.rotationY(this._rotation.y)).
        //     mul(Math.rotationX(-this._rotation.x));
        //
        // return view.mul(this.isPerspective == true ? Math.perspectiveCSS(width, height, this._fov) : new M4());
    }
}
