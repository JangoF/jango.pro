class SCSky {

    constructor(
        gl,
        sunDirection        = new V3(0.0, -1.0, 0.0),
        sunColor            = new V4(1.0, 1.0, 1.0, 15.0),
        radius              = new V2(3400e3, 3500e3),
        avangeDensity       = new V2(25e3, 2e3),
        kRay                = new V3(100e-8, 230e-8, 400e-8),
        kMie                = new V3(20e-8, 20e-8, 20e-8),
        gMie                = -0.75,
        resolution          = 32.0,
    ) {
        this._gl            = gl;
        this._sunDirection  = sunDirection,
        this._sunColor      = sunColor,
        this._radius        = radius,
        this._avangeDensity = avangeDensity,
        this._kRay          = kRay,
        this._kMie          = kMie,
        this._gMie          = gMie,
        this._resolution    = resolution;

        this._buffer = new GLBuffer(this._gl, this._gl.UNIFORM_BUFFER, SCSky.directionList);
        this._bufferView = new GLBufferView(this._buffer, 0.0, SCSky.directionList.length * 4.0);

        let skydomeVS = new GLShader(this._gl, SCSky.skydomeVS);

        this._pipelineDraw        = new GLPipeline(gl, skydomeVS, SCSky.defaultDrawFS, undefined, true);
        // this._pipelineConvert     = new GLPipeline(gl, SCSky.defaultConvertVS, SCSky.defaultConvertFS, undefined, true);
        // this._pipelineSpecular    = new GLPipeline(gl, SCSky.defaultSpecularVS, SCSky.defaultSpecularFS, undefined, true);
        // this._pipelineIntegration = new GLPipeline(gl, SCSky.defaultIntegrationVS, SCSky.defaultIntegrationFS, undefined, true);
        this._framebuffer         = null;
        this._skydome             = new GLMesh(this._gl, SCSky.skydomeIndex, SCSky.skydomeVertex, [3]);
        this._mesh                = new GLMesh(this._gl, new Uint16Array([0, 1, 2, 2, 1, 3]), new Float32Array([-1.0, -1.0, 0.0, 0.0,
                                                                                                                 1.0, -1.0, 1.0, 0.0,
                                                                                                                -1.0,  1.0, 0.0, 1.0,
                                                                                                                 1.0,  1.0, 1.0, 1.0]), [2, 2]);

         this._skyIntegration = new GLTexture(this._gl, this._gl.LINEAR, this._gl.RG32F, 64, 64);
         this._framebufferIntegration = new GLFramebuffer(this._gl, [[this._skyIntegration, this._gl.COLOR_ATTACHMENT0]]);

        this._updateFramebuffer();
        this._updateSky();
    }

    _updateFramebuffer() {
        if (this._framebuffer !== undefined && this._framebuffer !== null) {
            this._framebuffer.destroy();
            this._skyCube.destroy();
        }

        this._skyCube = new GLTexture(this._gl, this._gl.LINEAR, this._gl.RGB, this._resolution).setStuffFullSmooth();
        this._framebuffer = new GLFramebuffer(this._gl, [
            [this._skyCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
            [this._skyCube, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
            [this._skyCube, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
            [this._skyCube, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
            [this._skyCube, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
            [this._skyCube, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
        ]);

        this._skyCubeIrradiance = new GLTexture(this._gl, this._gl.LINEAR, this._gl.RGB, this._resolution / 4.0);
        this._framebufferIrradiance = new GLFramebuffer(this._gl, [
            [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
            [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
            [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
            [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
            [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
            [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
        ]);

        this._skyCubeSpecular = new GLTexture(this._gl, this._gl.LINEAR, this._gl.RGB, this._resolution).setStuffPBR();
        this._framebufferSpecular = new GLFramebuffer(this._gl, [
            [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
            [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
            [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
            [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
            [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
            [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
        ]);
    }

    _updateSky() {
        {
            this._framebuffer.bind();
            this._framebuffer.setupViewport();
            this._framebuffer.setupDrawBuffers();

            this._gl.disable(this._gl.DEPTH_TEST);
            this._gl.disable(this._gl.CULL_FACE);

            this._gl.clear(this._gl.COLOR_BUFFER_BIT);

            this._pipelineDraw.bind(
                this._bufferView,
                Math.perspective(0.002, 32.0, Math.toRadians(90.0), 1.0),
                this._sunDirection,
                this._sunColor,
                this._radius,
                this._avangeDensity,
                this._kRay,
                this._kMie.raw.add(this._gMie),
            );
            this._skydome.draw();
        }

        // {
        //     this._framebufferIrradiance.bind();
        //     this._framebufferIrradiance.setupViewport();
        //     this._framebufferIrradiance.setupDrawBuffers();
        //
        //     this._gl.disable(this._gl.DEPTH_TEST);
        //     this._gl.disable(this._gl.CULL_FACE);
        //
        //     this._gl.clear(this._gl.COLOR_BUFFER_BIT);
        //
        //     this._pipelineConvert.bind(
        //         Math.perspective(0.002, 32.0, Math.toRadians(90.0), 1.0),
        //         this._skyCube,
        //     );
        //     this._skydome.draw();
        // }
        //
        // {
        //     this._framebufferSpecular.bind();
        //     this._framebufferSpecular.setupViewport();
        //     this._framebufferSpecular.setupDrawBuffers();
        //
        //     this._gl.disable(this._gl.DEPTH_TEST);
        //     this._gl.disable(this._gl.CULL_FACE);
        //
        //     this._gl.clear(this._gl.COLOR_BUFFER_BIT);
        //
        //     this._pipelineSpecular.bind(
        //         Math.perspective(0.002, 32.0, Math.toRadians(90.0), 1.0),
        //         this._skyCube,
        //         null,
        //     );
        //
        //     let maxMipLevels = 4;
        //     for (let mip = 0; mip < maxMipLevels; mip++) {
        //
        //         let mipWidth  = (this._resolution) * Math.pow(0.5, mip);
        //         let mipHeight = (this._resolution) * Math.pow(0.5, mip);
        //
        //         this._framebufferSpecular.bind();
        //
        //         let roughness = mip / (maxMipLevels - 1);
        //         this._pipelineSpecular.setUniform(2, roughness);
        //
        //         this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X, this._skyCubeSpecular.texture, mip);
        //         this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X, this._skyCubeSpecular.texture, mip);
        //         this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y, this._skyCubeSpecular.texture, mip);
        //         this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, this._skyCubeSpecular.texture, mip);
        //         this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z, this._skyCubeSpecular.texture, mip);
        //         this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, this._skyCubeSpecular.texture, mip);
        //
        //         this._gl.viewport(0, 0, mipWidth, mipHeight);
        //         this._framebufferSpecular.setupDrawBuffers();
        //
        //         this._skydome.draw();
        //     }
        // }
        //
        // {
        //     this._framebufferIntegration.bind();
        //     this._framebufferIntegration.setupViewport();
        //     this._framebufferIntegration.setupDrawBuffers();
        //
        //     this._gl.disable(this._gl.DEPTH_TEST);
        //     this._gl.disable(this._gl.CULL_FACE);
        //
        //     this._gl.clear(this._gl.COLOR_BUFFER_BIT);
        //
        //     this._pipelineIntegration.bind();
        //     this._mesh.draw();
        // }
    }

    get sunDirection() {
        return this._sunDirection;
    }

    get sunColor() {
        return this._sunColor;
    }

    get radius() {
        return this._radius;
    }

    get avangeDensity() {
        return this._avangeDensity;
    }

    get kRay() {
        return this._kRay;
    }

    get kMie() {
        return this._kMie;
    }

    get gMie() {
        return this._gMie;
    }

    get resolution() {
        return this._resolution;
    }

    get pipelineRead() {
        return this._pipelineRead;
    }

    get pipelineDraw() {
        return this._pipelineDraw;
    }

    get skyCube() {
        return this._skyCube;
    }

    get skyCubeIrradiance() {
        return this._skyCubeIrradiance;
    }

    setSunDirection(sunDirection) {
        this._sunDirection = sunDirection;
        this._updateSky();
        return this;
    }

    setSunColor(sunColor) {
        this._sunColor = sunColor;
        this._updateSky();
        return this;
    }

    setRadius(radius) {
        this._radius = radius;
        this._updateSky();
        return this;
    }

    setAvangeDensity(avangeDensity) {
        this._avangeDensity = avangeDensity;
        this._updateSky();
        return this;
    }

    setKRay(kRay) {
        this._kRay = kRay;
        this._updateSky();
        return this;
    }

    setKMie(kMie) {
        this._kMie = kMie;
        this._updateSky();
        return this;
    }

    setGMie(gMie) {
        this._gMie = gMie;
        this._updateSky();
        return this;
    }

    setResolution(resolution) {
        this._resolution = resolution;
        this._updateFramebuffer();
        this._updateSky();
        return this;
    }

    setPipelineRead(pipelineRead) {
        this._pipelineRead = pipelineRead;
        this._updateSky();
        return this;
    }

    setPipelineDraw(pipelineDraw) {
        this._pipelineDraw = pipelineDraw;
        this._updateSky();
        return this;
    }
}

SCSky.directionList = new Float32Array(
    Math.lookAt(new V3(0.0), new V3( 1.0,  0.0,  0.0), new V3(0.0,  1.0,  0.0)).raw.concat(      // X Positive
    Math.lookAt(new V3(0.0), new V3(-1.0,  0.0,  0.0), new V3(0.0,  1.0,  0.0)).raw).concat(     // X Negative
    Math.lookAt(new V3(0.0), new V3( 0.0,  1.0,  0.0), new V3(0.0,  0.0, -1.0)).raw).concat(     // Y Positive
    Math.lookAt(new V3(0.0), new V3( 0.0, -1.0,  0.0), new V3(0.0,  0.0,  1.0)).raw).concat(     // Y Negative
    Math.lookAt(new V3(0.0), new V3( 0.0,  0.0,  1.0), new V3(0.0,  1.0,  0.0)).raw).concat(     // Z Positive
    Math.lookAt(new V3(0.0), new V3( 0.0,  0.0, -1.0), new V3(0.0,  1.0,  0.0)).raw)             // Z Negative
);

SCSky.skydomeIndex = new Uint16Array([0,1,2,1,3,4,1,4,2,2,4,5,0,6,1,6,7,8,6,8,1,1,8,3,0,9,6,9,10,11,9,11,6,6,11,7,0,12,9,12,13,14,12,14,9,9,14,10,0,2,12,2,5,15,2,15,12,12,15,13,5,4,16,4,3,17,4,17,16,16,17,18,3,8,19,8,7,20,8,20,19,19,20,21,7,11,22,11,10,23,11,23,22,22,23,24,10,14,25,14,13,26,14,26,25,25,26,27,13,15,28,15,5,29,15,29,28,28,29,30,5,16,29,16,18,31,16,31,29,29,31,30,3,19,17,19,21,32,19,32,17,17,32,18,7,22,20,22,24,33,22,33,20,20,33,21,10,25,23,25,27,34,25,34,23,23,34,24,13,28,26,28,30,35,28,35,26,26,35,27,30,31,36,31,18,37,31,37,36,36,37,38,18,32,37,32,21,39,32,39,37,37,39,38,21,33,39,33,24,40,33,40,39,39,40,38,24,34,40,34,27,41,34,41,40,40,41,38,27,35,41,35,30,36,35,36,41,41,36,38]);

SCSky.skydomeVertex = new Float32Array([0,0,-1,-0.16245986521244,0.499999970197678,-0.85065084695816,0.42532542347908,0.309016972780228,-0.85065084695816,-0.276393264532089,0.850650906562805,-0.447213649749756,0.262865543365479,0.809016942977905,-0.525731086730957,0.723606824874878,0.525731086730957,-0.447213590145111,-0.525731086730957,-4.59608742175988e-8,-0.85065084695816,-0.894427299499512,-7.81933167104398e-8,-0.447213649749756,-0.688190996646881,0.499999940395355,-0.525731086730957,-0.162459895014763,-0.499999970197678,-0.85065084695816,-0.276393294334412,-0.850650787353516,-0.447213590145111,-0.688190996646881,-0.5,-0.525731086730957,0.425325393676758,-0.30901700258255,-0.85065084695816,0.723606765270233,-0.525731146335602,-0.447213590145111,0.262865513563156,-0.809017062187195,-0.525731146335602,0.85065084695816,-3.50347306721233e-8,-0.525731086730957,0.587785243988037,0.809016942977905,0,-3.50347306721233e-8,0.999999940395355,0,0.276393175125122,0.85065084695816,0.447213590145111,-0.587785243988037,0.809016942977905,0,-0.95105654001236,0.309016913175583,0,-0.723606884479523,0.525731086730957,0.447213649749756,-0.95105642080307,-0.309017091989517,0,-0.587785243988037,-0.80901700258255,0,-0.723606765270233,-0.525731205940247,0.447213590145111,1.75173653360616e-8,-0.999999940395355,0,0.587785303592682,-0.809016942977905,0,0.276393324136734,-0.850650787353516,0.447213590145111,0.95105654001236,-0.309017032384872,0,0.95105654001236,0.309016972780228,0,0.894427299499512,0,0.447213649749756,0.688190937042236,0.5,0.525731086730957,-0.262865573167801,0.809016942977905,0.525731086730957,-0.85065084695816,-1.0510419201637e-7,0.525731086730957,-0.262865453958511,-0.80901700258255,0.525731086730957,0.688190996646881,-0.499999970197678,0.525731086730957,0.525731086730957,0,0.85065084695816,0.162459835410118,0.5,0.85065084695816,0,0,1,-0.42532542347908,0.309016942977905,0.85065084695816,-0.425325393676758,-0.309017032384872,0.85065084695816,0.162459924817085,-0.499999970197678,0.85065084695816]);

SCSky.skydomeVS = `#version 300 es
layout(location = 0) in vec3 i_Position;

layout(std140) uniform VIEW {
    uniform mat4 u_View_XP;
    uniform mat4 u_View_XN;

    uniform mat4 u_View_YP;
    uniform mat4 u_View_YN;

    uniform mat4 u_View_ZP;
    uniform mat4 u_View_ZN;
};

uniform mat4 u_Projection;

out vec3 io_Ray_XP;
out vec3 io_Ray_XN;

out vec3 io_Ray_YP;
out vec3 io_Ray_YN;

out vec3 io_Ray_ZP;
out vec3 io_Ray_ZN;

void main() {
    io_Ray_XP = mat3(u_View_XP) * i_Position;
    io_Ray_XN = mat3(u_View_XN) * i_Position;

    io_Ray_YP = mat3(u_View_YP) * i_Position;
    io_Ray_YN = mat3(u_View_YN) * i_Position;

    io_Ray_ZP = mat3(u_View_ZP) * i_Position;
    io_Ray_ZN = mat3(u_View_ZN) * i_Position;

    gl_Position = u_Projection * vec4(i_Position, 1.0f);
}`;


SCSky.defaultDrawFS = `#version 300 es
precision highp float;

uniform vec3 u_SunDirection;
uniform vec4 u_SunColor;

uniform vec2 u_Radius;
uniform vec2 u_AvangeDensity;

uniform vec3 u_KRay;
uniform vec4 u_KMie;

in vec3 io_Ray_XP;
in vec3 io_Ray_XN;

in vec3 io_Ray_YP;
in vec3 io_Ray_YN;

in vec3 io_Ray_ZP;
in vec3 io_Ray_ZN;

out vec4 o_Color[6];

#define PI 3.1415926535
#define MAX_VALUE 1e10

#define STEP_COUNT_PRIMARY 96
#define STEP_COUNT_SECONDARY 8

// Обычный алгоритм рассчёта точек пересечения луча и сферы, но с учётом того что для origin находящегося внутри сферы - t0 будет равен 0.0:

vec2 intersection(vec3 origin, vec3 direction, float radius) {
    vec3 D = direction;
    vec3 L = vec3(0.0) - origin;

    float tca = dot(D, L);
    float d = dot(L, L) - (tca * tca);
    float R = radius * radius;

    if (d > R) {
        return vec2(MAX_VALUE, -MAX_VALUE);
    }

    float thc = sqrt(R - d);
    float t0 = tca - thc;
    float t1 = tca + thc;

    if (t0 > 0.0 || t1 > 0.0) {
        t0 = max(t0, 0.0);
        t1 = max(t1, 0.0);

        return vec2(min(t0, t1), max(t0, t1));
    }

    return vec2(MAX_VALUE, -MAX_VALUE);
}

// Рассчитываем фазу для ray:

float calculatePhaseRay(float cosineSquare) {
    return 0.75 * (1.0 + cosineSquare);
}

// Рассчитываем фазу для mie:

float calculatePhaseMie(float cosineSquare, float cosine) {

    float RADIUS_PLANET     = u_Radius.x;
    float RADIUS_ATMOSPHERE = u_Radius.y;

    float AVANGE_DENSITY_RAY = u_AvangeDensity.x;
    float AVANGE_DENSITY_MIE = u_AvangeDensity.y;

    vec3 K_RAY = u_KRay;
    vec3 K_MIE = u_KMie.rgb;

    vec3  SUN_COLOR     = u_SunColor.rgb;
    float SUN_INTENSITY = u_SunColor.a;

    float MIE_G = u_KMie.a;

    float vA = 3.0 * (1.0 - MIE_G * MIE_G);
    float vB = 2.0 * (2.0 - MIE_G * MIE_G);

    float vC = 1.0 + cosineSquare;
    float vD = pow(1.0 + MIE_G * MIE_G - 2.0 * MIE_G * cosine, 1.5);

    return (vA / vB) * (vC / vD);
}

// Рассчёт внтуреннего интеграла (оптической глубины) отдельно для ray и mie:

void calculateInternalIntegral(vec3 rayOrigin, vec3 rayDirection, out float depthRay, out float depthMie) {

    float RADIUS_PLANET     = u_Radius.x;
    float RADIUS_ATMOSPHERE = u_Radius.y;

    float AVANGE_DENSITY_RAY = u_AvangeDensity.x;
    float AVANGE_DENSITY_MIE = u_AvangeDensity.y;

    vec3 K_RAY = u_KRay;
    vec3 K_MIE = u_KMie.rgb;

    vec3  SUN_COLOR     = u_SunColor.rgb;
    float SUN_INTENSITY = u_SunColor.a;

    float MIE_G = u_KMie.a;

    float rayStepLength = intersection(rayOrigin, rayDirection, RADIUS_ATMOSPHERE).y / float(STEP_COUNT_SECONDARY);
    vec3  rayStep = rayDirection * rayStepLength;
    vec3  rayStepCurrent = rayOrigin + rayStep * 0.5;

    depthRay = 0.0;
    depthMie = 0.0;

    for (int i = 0; i < STEP_COUNT_SECONDARY; i++) {
        float currentHeight = length(rayStepCurrent) - RADIUS_PLANET;

        depthRay += exp(-currentHeight / AVANGE_DENSITY_RAY) * rayStepLength;
        depthMie += exp(-currentHeight / AVANGE_DENSITY_MIE) * rayStepLength;

        rayStepCurrent += rayStep;
    }
}

vec3 calculateColor(vec3 rayOrigin, vec3 rayDirection, vec3 sunDirection) {

    float RADIUS_PLANET     = u_Radius.x;
    float RADIUS_ATMOSPHERE = u_Radius.y;

    float AVANGE_DENSITY_RAY = u_AvangeDensity.x;
    float AVANGE_DENSITY_MIE = u_AvangeDensity.y;

    vec3 K_RAY = u_KRay;
    vec3 K_MIE = u_KMie.rgb;

    vec3  SUN_COLOR     = u_SunColor.rgb;
    float SUN_INTENSITY = u_SunColor.a;

    float MIE_G = u_KMie.a;

    // Находим точки пересечения луча и атмосферы:

    vec2 aip = intersection(rayOrigin, rayDirection, RADIUS_ATMOSPHERE);
    if (aip.x > aip.y) {
        return vec3(0.0f);
        // return texture(u_StarsColor, rayDirection).rgb; // Если мы промахнулись мимо атмосферы - просто выводим цвет из текстуры звёзд.
    }

    // Находим точки пересечения луча и планеты:

    vec2 pip = intersection(rayOrigin, rayDirection, RADIUS_PLANET);
    aip.y = min(aip.y, pip.x);

    float primaryRayStepLength = (aip.y - aip.x) / float(STEP_COUNT_PRIMARY);
    vec3  primaryRayStep = rayDirection * primaryRayStepLength;
    vec3  primaryRayStepCurrent = rayOrigin + rayDirection * aip.x + primaryRayStep * 0.5;

    vec3 finalRay = vec3(0.0);
    vec3 finalMie = vec3(0.0);

    float primaryDepthRay = 0.0;
    float primaryDepthMie = 0.0;

    vec3 attenuation = vec3(0.0); // Вместо рассчёта света отражёного от поверхности можно воспользоваться последним значением attenuation.

    for (int i = 0; i < STEP_COUNT_PRIMARY; i++) {
        float primaryHeight = length(primaryRayStepCurrent) - RADIUS_PLANET;

        float primaryDepthRayCurrent = exp(-primaryHeight / AVANGE_DENSITY_RAY) * primaryRayStepLength;
        float primaryDepthMieCurrent = exp(-primaryHeight / AVANGE_DENSITY_MIE) * primaryRayStepLength;

        primaryDepthRay += primaryDepthRayCurrent;
        primaryDepthMie += primaryDepthMieCurrent;

        float secondaryDepthRay = 0.0;
        float secondaryDepthMie = 0.0;

        calculateInternalIntegral(primaryRayStepCurrent, sunDirection, secondaryDepthRay, secondaryDepthMie);

        attenuation = exp(-4.0 * PI * (K_RAY * (primaryDepthRay + secondaryDepthRay) + K_MIE * (primaryDepthMie + secondaryDepthMie)));

        finalRay += primaryDepthRayCurrent * attenuation;
        finalMie += primaryDepthMieCurrent * attenuation;

        primaryRayStepCurrent += primaryRayStep;
    }

    float cosine = dot(rayDirection, -sunDirection);
    float cosineSquare = cosine * cosine;

    float phaseRay = calculatePhaseRay(cosineSquare);
    float phaseMie = calculatePhaseMie(cosineSquare, cosine);

    vec3 atmosphereColor = SUN_COLOR * SUN_INTENSITY * (K_RAY * phaseRay * finalRay + K_MIE * phaseMie * finalMie);
    return vec3(atmosphereColor);
}

void main() {
    o_Color[0] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_Ray_XP) * vec3(-1.0f, -1.0f, -1.0f), -u_SunDirection), 1.0);
    o_Color[1] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_Ray_XN) * vec3(-1.0f, -1.0f, -1.0f), -u_SunDirection), 1.0);
    o_Color[2] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_Ray_YP) * vec3( 1.0f, -1.0f,  1.0f), -u_SunDirection), 1.0);
    o_Color[3] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_Ray_YN) * vec3( 1.0f, -1.0f,  1.0f), -u_SunDirection), 1.0);
    o_Color[4] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_Ray_ZP) * vec3( 1.0f, -1.0f,  1.0f), -u_SunDirection), 1.0);
    o_Color[5] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_Ray_ZN) * vec3( 1.0f, -1.0f,  1.0f), -u_SunDirection), 1.0);
}`;







// SCSky.defaultIntegrationVS = `#version 300 es
// layout(location = 0) in vec4 i_Position;
// layout(location = 1) in vec2 i_UV;
//
// out vec2 io_UV;
//
// void main() {
//     io_UV = i_UV;
//     gl_Position = i_Position;
// }`;
//
// SCSky.defaultIntegrationFS = `#version 300 es
// precision highp float;
//
// in vec2 io_UV;
// out vec2 o_Color;
//
// #define PI 3.1415926535
//
// float RadicalInverse_VdC(uint bits)  {
//     bits = (bits << 16u) | (bits >> 16u);
//     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
//     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
//     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
//     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
//     return float(bits) * 2.3283064365386963e-10; // / 0x100000000
// }
//
// vec2 Hammersley(uint i, uint N) {
//     return vec2(float(i)/float(N), RadicalInverse_VdC(i));
// }
//
// float GeometrySchlickGGX(float NdotV, float roughness) {
//     float a = roughness;
//     float k = (a * a) / 2.0;
//
//     float nom   = NdotV;
//     float denom = NdotV * (1.0 - k) + k;
//
//     return nom / denom;
// }
//
// float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
//     float NdotV = max(dot(N, V), 0.0);
//     float NdotL = max(dot(N, L), 0.0);
//     float ggx2 = GeometrySchlickGGX(NdotV, roughness);
//     float ggx1 = GeometrySchlickGGX(NdotL, roughness);
//
//     return ggx1 * ggx2;
// }
//
// vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness) {
//     float a = roughness*roughness;
//
//     float phi = 2.0 * PI * Xi.x;
//     float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
//     float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
//
//     // преобразование из сферических в декартовы координаты
//     vec3 H;
//     H.x = cos(phi) * sinTheta;
//     H.y = sin(phi) * sinTheta;
//     H.z = cosTheta;
//
//     // преобразование из касательного пространства в мировые координаты
//     vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
//     vec3 tangent   = normalize(cross(up, N));
//     vec3 bitangent = cross(N, tangent);
//
//     vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
//     return normalize(sampleVec);
// }
//
// vec2 IntegrateBRDF(float NdotV, float roughness) {
//     vec3 V;
//     V.x = sqrt(1.0 - NdotV*NdotV);
//     V.y = 0.0;
//     V.z = NdotV;
//
//     float A = 0.0;
//     float B = 0.0;
//
//     vec3 N = vec3(0.0, 0.0, 1.0);
//
//     const uint SAMPLE_COUNT = 1024u;
//     for(uint i = 0u; i < SAMPLE_COUNT; ++i)
//     {
//         vec2 Xi = Hammersley(i, SAMPLE_COUNT);
//         vec3 H  = ImportanceSampleGGX(Xi, N, roughness);
//         vec3 L  = normalize(2.0 * dot(V, H) * H - V);
//
//         float NdotL = max(L.z, 0.0);
//         float NdotH = max(H.z, 0.0);
//         float VdotH = max(dot(V, H), 0.0);
//
//         if(NdotL > 0.0)
//         {
//             float G = GeometrySmith(N, V, L, roughness);
//             float G_Vis = (G * VdotH) / (NdotH * NdotV);
//             float Fc = pow(1.0 - VdotH, 5.0);
//
//             A += (1.0 - Fc) * G_Vis;
//             B += Fc * G_Vis;
//         }
//     }
//     A /= float(SAMPLE_COUNT);
//     B /= float(SAMPLE_COUNT);
//     return vec2(A, B);
// }
//
// void main() {
//     o_Color = IntegrateBRDF(io_UV.x, io_UV.y);
// }`;













// SCSky.defaultSpecularVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_Projection;
//
// out vec3 io_NormalXP;
// out vec3 io_NormalXN;
//
// out vec3 io_NormalYP;
// out vec3 io_NormalYN;
//
// out vec3 io_NormalZP;
// out vec3 io_NormalZN;
//
// mat3 lookAt(vec3 inverseTarget, vec3 up) {
//     vec3 FW = inverseTarget;
//     vec3 LF = cross(up, FW);
//     vec3 UP = cross(FW, LF);
//
//     return mat3(
//         LF.x, UP.x, FW.x,
//         LF.y, UP.y, FW.y,
//         LF.z, UP.z, FW.z
//     );
// }
//
// void main() {
//     io_NormalXP = lookAt(vec3(1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalXN = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     io_NormalYP = lookAt(vec3(0.0,  -1.0,  0.0), vec3(0.0,  0.0, 1.0)) * i_Normal;
//     io_NormalYN = lookAt(vec3(0.0, 1.0,  0.0), vec3(0.0,  0.0,  -1.0)) * i_Normal;
//
//     io_NormalZP = lookAt(vec3(0.0,  0.0, 1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalZN = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     gl_Position = u_Projection * vec4(i_Position, 1.0f);
// }`;

// SCSky.defaultConvertVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_Projection;
//
// out vec3 io_NormalXP;
// out vec3 io_NormalXN;
//
// out vec3 io_NormalYP;
// out vec3 io_NormalYN;
//
// out vec3 io_NormalZP;
// out vec3 io_NormalZN;
//
// mat3 lookAt(vec3 inverseTarget, vec3 up) {
//     vec3 FW = inverseTarget;
//     vec3 LF = cross(up, FW);
//     vec3 UP = cross(FW, LF);
//
//     return mat3(
//         LF.x, UP.x, FW.x,
//         LF.y, UP.y, FW.y,
//         LF.z, UP.z, FW.z
//     );
// }
//
// void main() {
//     io_NormalXP = lookAt(vec3(1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalXN = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     io_NormalYP = lookAt(vec3(0.0,  -1.0,  0.0), vec3(0.0,  0.0, 1.0)) * i_Normal;
//     io_NormalYN = lookAt(vec3(0.0, 1.0,  0.0), vec3(0.0,  0.0,  -1.0)) * i_Normal;
//
//     io_NormalZP = lookAt(vec3(0.0,  0.0, 1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalZN = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     gl_Position = u_Projection * vec4(i_Position, 1.0f);
// }`;

// SCSky.defaultDrawVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_Projection;
//
// out vec3 io_NormalXP;
// out vec3 io_NormalXN;
//
// out vec3 io_NormalYP;
// out vec3 io_NormalYN;
//
// out vec3 io_NormalZP;
// out vec3 io_NormalZN;
//
// mat3 lookAt(vec3 inverseTarget, vec3 up) {
//     vec3 FW = inverseTarget;
//     vec3 LF = cross(up, FW);
//     vec3 UP = cross(FW, LF);
//
//     return mat3(
//         LF.x, UP.x, FW.x,
//         LF.y, UP.y, FW.y,
//         LF.z, UP.z, FW.z
//     );
// }
//
// void main() {
//     io_NormalXP = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//     io_NormalXN = lookAt(vec3( 1.0, 0.0,  0.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//
//     io_NormalYP = lookAt(vec3(0.0,  1.0,  0.0), vec3(0.0,  0.0, -1.0)) * i_Normal;
//     io_NormalYN = lookAt(vec3(0.0, -1.0,  0.0), vec3(0.0,  0.0,  1.0)) * i_Normal;
//
//     io_NormalZP = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//     io_NormalZN = lookAt(vec3(0.0,  0.0,  1.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//
//     gl_Position = u_Projection * vec4(i_Position, 1.0f);
// }`;












SCSky.defaultSpecularFS = `#version 300 es
precision highp float;

uniform samplerCube u_OriginalCubemap;
uniform float u_Roughness;

in vec3 io_NormalXP;
in vec3 io_NormalXN;

in vec3 io_NormalYP;
in vec3 io_NormalYN;

in vec3 io_NormalZP;
in vec3 io_NormalZN;

out vec4 o_Color[6];

#define PI 3.1415926535

float RadicalInverse_VdC(uint bits)  {
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
    return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 Hammersley(uint i, uint N) {
    return vec2(float(i)/float(N), RadicalInverse_VdC(i));
}

vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness) {
    float a = roughness*roughness;

    float phi = 2.0 * PI * Xi.x;
    float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
    float sinTheta = sqrt(1.0 - cosTheta*cosTheta);

    // преобразование из сферических в декартовы координаты
    vec3 H;
    H.x = cos(phi) * sinTheta;
    H.y = sin(phi) * sinTheta;
    H.z = cosTheta;

    // преобразование из касательного пространства в мировые координаты
    vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
    vec3 tangent   = normalize(cross(up, N));
    vec3 bitangent = cross(N, tangent);

    vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
    return normalize(sampleVec);
}

vec3 calculate(vec3 normal) {
    vec3 N = normalize(normal);
    vec3 R = N;
    vec3 V = R;

    const uint SAMPLE_COUNT = 1024u;
    float totalWeight = 0.0;
    vec3 prefilteredColor = vec3(0.0);
    for(uint i = 0u; i < SAMPLE_COUNT; ++i)
    {
        vec2 Xi = Hammersley(i, SAMPLE_COUNT);
        vec3 H  = ImportanceSampleGGX(Xi, N, u_Roughness);
        vec3 L  = normalize(2.0 * dot(V, H) * H - V);

        float NdotL = max(dot(N, L), 0.0);
        if(NdotL > 0.0)
        {
            prefilteredColor += texture(u_OriginalCubemap, L).rgb * NdotL;
            totalWeight      += NdotL;
        }
    }
    prefilteredColor = prefilteredColor / totalWeight;

    return prefilteredColor;
}

void main() {
    o_Color[0] = vec4(calculate(normalize(io_NormalXP)), 1.0f);
    o_Color[1] = vec4(calculate(normalize(io_NormalXN)), 1.0f);
    o_Color[2] = vec4(calculate(normalize(io_NormalYP)), 1.0f);
    o_Color[3] = vec4(calculate(normalize(io_NormalYN)), 1.0f);
    o_Color[4] = vec4(calculate(normalize(io_NormalZP)), 1.0f);
    o_Color[5] = vec4(calculate(normalize(io_NormalZN)), 1.0f);
}`;

SCSky.defaultConvertFS = `#version 300 es
precision highp float;

uniform samplerCube u_OriginalCubemap;

in vec3 io_NormalXP;
in vec3 io_NormalXN;

in vec3 io_NormalYP;
in vec3 io_NormalYN;

in vec3 io_NormalZP;
in vec3 io_NormalZN;

out vec4 o_Color[6];

#define PI 3.1415926535

vec3 calculate(vec3 normal) {
    vec3 irradiance = vec3(0.0);

    vec3 up    = vec3(0.0, 1.0, 0.0);
    vec3 right = cross(up, normal);
    up         = cross(normal, right);

    float sampleDelta = 0.025;
    float nrSamples = 0.0;
    for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
    {
        for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
        {
            // перевод сферических коорд. в декартовы (в касательном пр-ве)
            vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
            // из касательного в мировое пространство
            vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * normal;

            irradiance += texture(u_OriginalCubemap, sampleVec).rgb * cos(theta) * sin(theta);
            nrSamples++;
        }
    }
    irradiance = PI * irradiance * (1.0 / float(nrSamples));

    return irradiance;
}

void main() {
    o_Color[0] = vec4(calculate(normalize(io_NormalXP)), 1.0f);
    o_Color[1] = vec4(calculate(normalize(io_NormalXN)), 1.0f);
    o_Color[2] = vec4(calculate(normalize(io_NormalYP)), 1.0f);
    o_Color[3] = vec4(calculate(normalize(io_NormalYN)), 1.0f);
    o_Color[4] = vec4(calculate(normalize(io_NormalZP)), 1.0f);
    o_Color[5] = vec4(calculate(normalize(io_NormalZN)), 1.0f);
}`;































































































SCSky.defaultReadVS = `#version 300 es
layout(location = 0) in vec3 i_Position;
layout(location = 1) in vec3 i_Normal;

uniform mat4 u_View;
uniform mat4 u_Projection;

out vec3 io_Normal;

void main() {
    io_Normal = i_Normal;
    gl_Position = u_Projection * u_View * vec4(i_Position, 1.0f);
}`;

SCSky.defaultReadFS = `#version 300 es
precision highp float;

uniform samplerCube u_Sky;

in vec3 io_Normal;
out vec4 o_Color;

void main() {
    o_Color = vec4(texture(u_Sky, normalize(io_Normal)).rgb, 1.0f);
}`;










// class SCSky {
//
//     constructor(
//         gl,
//         sunDirection        = new V3(0.0, -1.0, 0.0),
//         sunColor            = new V4(1.0, 1.0, 1.0, 15.0),
//         radius              = new V2(3400e3, 3500e3),
//         avangeDensity       = new V2(25e3, 2e3),
//         kRay                = new V3(100e-8, 230e-8, 400e-8),
//         kMie                = new V3(20e-8, 20e-8, 20e-8),
//         gMie                = -0.75,
//         resolution          = 32.0,
//         pipelineRead        = new GLPipeline(gl, SCSky.defaultReadVS, SCSky.defaultReadFS, undefined, true),
//         pipelineDraw        = new GLPipeline(gl, SCSky.defaultDrawVS, SCSky.defaultDrawFS, undefined, true),
//         pipelineConvert     = new GLPipeline(gl, SCSky.defaultConvertVS, SCSky.defaultConvertFS, undefined, true),
//         pipelineSpecular    = new GLPipeline(gl, SCSky.defaultSpecularVS, SCSky.defaultSpecularFS, undefined, true),
//         pipelineIntegration = new GLPipeline(gl, SCSky.defaultIntegrationVS, SCSky.defaultIntegrationFS, undefined, true),
//     ) {
//         this._gl                  = gl;
//         this._sunDirection        = sunDirection,
//         this._sunColor            = sunColor,
//         this._radius              = radius,
//         this._avangeDensity       = avangeDensity,
//         this._kRay                = kRay,
//         this._kMie                = kMie,
//         this._gMie                = gMie,
//         this._resolution          = resolution;
//         this._pipelineRead        = pipelineRead;
//         this._pipelineDraw        = pipelineDraw;
//         this._pipelineConvert     = pipelineConvert;
//         this._pipelineSpecular    = pipelineSpecular;
//         this._pipelineIntegration = pipelineIntegration;
//         this._framebuffer         = null;
//         this._skydome             = new GLMesh(this._gl, SCSky.skydomeIndex, SCSky.skydomeVertex, [3, 3]);
//         this._mesh                = new GLMesh(this._gl, new Uint16Array([0, 1, 2, 2, 1, 3]), new Float32Array([-1.0, -1.0, 0.0, 0.0,
//                                                                                              1.0, -1.0, 1.0, 0.0,
//                                                                                             -1.0,  1.0, 0.0, 1.0,
//                                                                                              1.0,  1.0, 1.0, 1.0]), [2, 2]);
//
//          this._skyIntegration = new GLTexture(this._gl, this._gl.RG32F, 64, 64, this._gl.LINEAR);
//          this._framebufferIntegration = new GLFramebuffer(this._gl, [[this._skyIntegration, this._gl.COLOR_ATTACHMENT0]]);
//
//         this._updateFramebuffer();
//         this._updateSky();
//     }
//
//     _updateFramebuffer() {
//         if (this._framebuffer !== undefined && this._framebuffer !== null) {
//             this._framebuffer.destroy();
//             this._skyCube.destroy();
//         }
//
//         this._skyCube = new GLTexture(this._gl).setTexture(this._gl.TEXTURE_CUBE_MAP, this._gl.RGB, this._resolution, this._gl.LINEAR);
//         this._framebuffer = new GLFramebuffer(this._gl, [
//             [this._skyCube, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
//             [this._skyCube, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
//             [this._skyCube, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
//             [this._skyCube, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
//             [this._skyCube, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
//             [this._skyCube, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
//         ]);
//
//         this._skyCubeIrradiance = new GLTexture(this._gl).setTexture(this._gl.TEXTURE_CUBE_MAP, this._gl.RGB, this._resolution / 4.0, this._gl.LINEAR);
//         this._framebufferIrradiance = new GLFramebuffer(this._gl, [
//             [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
//             [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
//             [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
//             [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
//             [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
//             [this._skyCubeIrradiance, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
//         ]);
//
//         this._skyCubeSpecular = new GLTexture(this._gl).setTexture(this._gl.TEXTURE_CUBE_MAP, this._gl.RGB, this._resolution, this._gl.LINEAR).setPBRMip();
//         this._framebufferSpecular = new GLFramebuffer(this._gl, [
//             [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X],
//             [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
//             [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
//             [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
//             [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
//             [this._skyCubeSpecular, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
//         ]);
//     }
//
//     _updateSky() {
//         {
//             this._framebuffer.bind();
//             this._framebuffer.setupViewport();
//             this._framebuffer.setupDrawBuffers();
//
//             this._gl.disable(this._gl.DEPTH_TEST);
//             this._gl.disable(this._gl.CULL_FACE);
//
//             this._gl.clear(this._gl.COLOR_BUFFER_BIT);
//
//             this._pipelineDraw.bind(
//                 Math.perspective(0.002, 32.0, Math.toRadians(90.0), 1.0),
//                 this._sunDirection,
//                 this._sunColor,
//                 this._radius,
//                 this._avangeDensity,
//                 this._kRay,
//                 this._kMie.raw.add(this._gMie),
//             );
//             this._skydome.draw();
//         }
//
//         {
//             this._framebufferIrradiance.bind();
//             this._framebufferIrradiance.setupViewport();
//             this._framebufferIrradiance.setupDrawBuffers();
//
//             this._gl.disable(this._gl.DEPTH_TEST);
//             this._gl.disable(this._gl.CULL_FACE);
//
//             this._gl.clear(this._gl.COLOR_BUFFER_BIT);
//
//             this._pipelineConvert.bind(
//                 Math.perspective(0.002, 32.0, Math.toRadians(90.0), 1.0),
//                 this._skyCube,
//             );
//             this._skydome.draw();
//         }
//
//         {
//             this._framebufferSpecular.bind();
//             this._framebufferSpecular.setupViewport();
//             this._framebufferSpecular.setupDrawBuffers();
//
//             this._gl.disable(this._gl.DEPTH_TEST);
//             this._gl.disable(this._gl.CULL_FACE);
//
//             this._gl.clear(this._gl.COLOR_BUFFER_BIT);
//
//             this._pipelineSpecular.bind(
//                 Math.perspective(0.002, 32.0, Math.toRadians(90.0), 1.0),
//                 this._skyCube,
//                 null,
//             );
//
//             let maxMipLevels = 4;
//             for (let mip = 0; mip < maxMipLevels; mip++) {
//
//                 let mipWidth  = (this._resolution) * Math.pow(0.5, mip);
//                 let mipHeight = (this._resolution) * Math.pow(0.5, mip);
//
//                 this._framebufferSpecular.bind();
//
//                 let roughness = mip / (maxMipLevels - 1);
//                 this._pipelineSpecular.setUniform(2, roughness);
//
//                 this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT0, this._gl.TEXTURE_CUBE_MAP_POSITIVE_X, this._skyCubeSpecular.texture, mip);
//                 this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT1, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X, this._skyCubeSpecular.texture, mip);
//                 this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT2, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y, this._skyCubeSpecular.texture, mip);
//                 this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT3, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, this._skyCubeSpecular.texture, mip);
//                 this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT4, this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z, this._skyCubeSpecular.texture, mip);
//                 this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT5, this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, this._skyCubeSpecular.texture, mip);
//
//                 this._gl.viewport(0, 0, mipWidth, mipHeight);
//                 this._framebufferSpecular.setupDrawBuffers();
//
//                 this._skydome.draw();
//             }
//         }
//
//         {
//             this._framebufferIntegration.bind();
//             this._framebufferIntegration.setupViewport();
//             this._framebufferIntegration.setupDrawBuffers();
//
//             this._gl.disable(this._gl.DEPTH_TEST);
//             this._gl.disable(this._gl.CULL_FACE);
//
//             this._gl.clear(this._gl.COLOR_BUFFER_BIT);
//
//             this._pipelineIntegration.bind();
//             this._mesh.draw();
//         }
//     }
//
//     // draw(framebuffer, view, projection) {
//     //     framebuffer.bind();
//     //     framebuffer.setupViewport();
//     //     framebuffer.setupDrawBuffers();
//     //
//     //     this._gl.disable(this._gl.DEPTH_TEST);
//     //     this._gl.disable(this._gl.CULL_FACE);
//     //
//     //     this._gl.clear(this._gl.COLOR_BUFFER_BIT);
//     //
//     //     this._pipelineRead.bind(
//     //         new M4(new M3(view)),
//     //         projection,
//     //         this._skyCube,
//     //     );
//     //
//     //     this._skydome.draw();
//     // }
//
//     get sunDirection() {
//         return this._sunDirection;
//     }
//
//     get sunColor() {
//         return this._sunColor;
//     }
//
//     get radius() {
//         return this._radius;
//     }
//
//     get avangeDensity() {
//         return this._avangeDensity;
//     }
//
//     get kRay() {
//         return this._kRay;
//     }
//
//     get kMie() {
//         return this._kMie;
//     }
//
//     get gMie() {
//         return this._gMie;
//     }
//
//     get resolution() {
//         return this._resolution;
//     }
//
//     get pipelineRead() {
//         return this._pipelineRead;
//     }
//
//     get pipelineDraw() {
//         return this._pipelineDraw;
//     }
//
//     get skyCube() {
//         return this._skyCube;
//     }
//
//     get skyCubeIrradiance() {
//         return this._skyCubeIrradiance;
//     }
//
//     setSunDirection(sunDirection) {
//         this._sunDirection = sunDirection;
//         this._updateSky();
//         return this;
//     }
//
//     setSunColor(sunColor) {
//         this._sunColor = sunColor;
//         this._updateSky();
//         return this;
//     }
//
//     setRadius(radius) {
//         this._radius = radius;
//         this._updateSky();
//         return this;
//     }
//
//     setAvangeDensity(avangeDensity) {
//         this._avangeDensity = avangeDensity;
//         this._updateSky();
//         return this;
//     }
//
//     setKRay(kRay) {
//         this._kRay = kRay;
//         this._updateSky();
//         return this;
//     }
//
//     setKMie(kMie) {
//         this._kMie = kMie;
//         this._updateSky();
//         return this;
//     }
//
//     setGMie(gMie) {
//         this._gMie = gMie;
//         this._updateSky();
//         return this;
//     }
//
//     setResolution(resolution) {
//         this._resolution = resolution;
//         this._updateFramebuffer();
//         this._updateSky();
//         return this;
//     }
//
//     setPipelineRead(pipelineRead) {
//         this._pipelineRead = pipelineRead;
//         this._updateSky();
//         return this;
//     }
//
//     setPipelineDraw(pipelineDraw) {
//         this._pipelineDraw = pipelineDraw;
//         this._updateSky();
//         return this;
//     }
// }
//
// SCSky.skydomeIndex = new Uint16Array([0,1,2,2,3,4,2,1,3,1,5,3,0,2,6,6,7,8,6,2,7,2,4,7,0,6,9,9,10,11,9,6,10,6,8,10,0,9,12,12,13,14,12,9,13,9,11,13,0,12,1,1,15,5,1,12,15,12,14,15,5,16,3,3,17,4,3,16,17,16,18,17,4,19,7,7,20,8,7,19,20,19,21,20,8,22,10,10,23,11,10,22,23,22,24,23,11,25,13,13,26,14,13,25,26,25,27,26,14,28,15,15,29,5,15,28,29,28,30,29,5,29,16,16,31,18,16,29,31,29,30,31,4,17,19,19,32,21,19,17,32,17,18,32,8,20,22,22,33,24,22,20,33,20,21,33,11,23,25,25,34,27,25,23,34,23,24,34,14,26,28,28,35,30,28,26,35,26,27,35,30,36,31,31,37,18,31,36,37,36,38,37,18,37,32,32,39,21,32,37,39,37,38,39,21,39,33,33,40,24,33,39,40,39,38,40,24,40,34,34,41,27,34,40,41,40,38,41,27,41,35,35,36,30,35,41,36,41,38,36]);
//
// SCSky.skydomeVertex = new Float32Array([0,0,-1,1.17915142539005e-7,0,0.999999940395355,0.42532542347908,0.309016972780228,-0.85065084695816,-0.425325393676758,-0.30901700258255,0.850650906562805,-0.16245986521244,0.499999970197678,-0.85065084695816,0.162459790706635,-0.499999910593033,0.85065084695816,0.262865543365479,0.809016942977905,-0.525731086730957,-0.262865573167801,-0.80901700258255,0.525731086730957,-0.276393264532089,0.850650906562805,-0.447213649749756,0.276393234729767,-0.850650787353516,0.447213619947433,0.723606824874878,0.525731086730957,-0.447213590145111,-0.723606705665588,-0.525731205940247,0.447213619947433,-0.525731086730957,-4.59608742175988e-8,-0.85065084695816,0.525731027126312,-3.35213634627962e-8,0.85065084695816,-0.688190996646881,0.499999940395355,-0.525731086730957,0.688191056251526,-0.5,0.525731086730957,-0.894427299499512,-7.81933167104398e-8,-0.447213649749756,0.894427239894867,1.88664248668147e-8,0.447213500738144,-0.162459895014763,-0.499999970197678,-0.85065084695816,0.162459880113602,0.499999970197678,0.85065084695816,-0.688190996646881,-0.5,-0.525731086730957,0.688190937042236,0.500000059604645,0.525731086730957,-0.276393294334412,-0.850650787353516,-0.447213590145111,0.276393294334412,0.85065084695816,0.447213470935822,0.425325393676758,-0.30901700258255,-0.85065084695816,-0.425325363874435,0.309016972780228,0.85065084695816,0.262865513563156,-0.809017062187195,-0.525731146335602,-0.262865483760834,0.809017062187195,0.525731086730957,0.723606765270233,-0.525731146335602,-0.447213590145111,-0.723606705665588,0.525731205940247,0.447213560342789,0.85065084695816,-3.50347306721233e-8,-0.525731086730957,-0.85065084695816,-8.9390308488646e-9,0.525731086730957,0.587785243988037,0.809016942977905,0,-0.587785243988037,-0.809017062187195,-7.15122538963442e-8,-3.50347306721233e-8,0.999999940395355,0,-1.89954398877035e-8,-1,-1.25146428331391e-7,0.276393175125122,0.85065084695816,0.447213590145111,-0.2763931453228,-0.850650787353516,-0.447213679552078,-0.587785243988037,0.809016942977905,0,0.587785243988037,-0.809016942977905,1.78780599213724e-8,-0.95105654001236,0.309016913175583,0,0.95105654001236,-0.30901700258255,-2.68170925465938e-8,-0.723606884479523,0.525731086730957,0.447213649749756,0.723606765270233,-0.525731146335602,-0.447213619947433,-0.95105642080307,-0.309017091989517,0,0.951056480407715,0.309017151594162,-5.36341886459013e-8,-0.587785243988037,-0.80901700258255,0,0.587785184383392,0.809017062187195,-2.23475815630536e-8,-0.723606765270233,-0.525731205940247,0.447213590145111,0.723606705665588,0.525731205940247,-0.447213590145111,1.75173653360616e-8,-0.999999940395355,0,-4.02256397080691e-8,1,-1.25146428331391e-7,0.587785303592682,-0.809016942977905,0,-0.587785243988037,0.80901700258255,-5.36341886459013e-8,0.276393324136734,-0.850650787353516,0.447213590145111,-0.276393234729767,0.850650787353516,-0.447213649749756,0.95105654001236,-0.309017032384872,0,-0.951056480407715,0.309017032384872,4.91646758860043e-8,0.95105654001236,0.309016972780228,0,-0.95105654001236,-0.309017032384872,1.34085471614753e-8,0.894427299499512,0,0.447213649749756,-0.894427180290222,-4.71660612788583e-8,-0.447213619947433,0.688190937042236,0.5,0.525731086730957,-0.688190996646881,-0.5,-0.525731146335602,-0.262865573167801,0.809016942977905,0.525731086730957,0.262865573167801,-0.809017062187195,-0.525731027126312,-0.85065084695816,-1.0510419201637e-7,0.525731086730957,0.85065084695816,1.4833781847301e-7,-0.525731146335602,-0.262865453958511,-0.80901700258255,0.525731086730957,0.262865453958511,0.809017062187195,-0.525731086730957,0.688190996646881,-0.499999970197678,0.525731086730957,-0.688190996646881,0.499999970197678,-0.525731146335602,0.525731086730957,0,0.85065084695816,-0.525731146335602,-2.01128216303914e-8,-0.85065084695816,0.162459835410118,0.5,0.85065084695816,-0.162459805607796,-0.5,-0.850650787353516,0,0,1,-5.65992692713735e-8,1.41498173178434e-8,-0.999999940395355,-0.42532542347908,0.309016942977905,0.85065084695816,0.425325393676758,-0.30901700258255,-0.850650906562805,-0.425325393676758,-0.309017032384872,0.85065084695816,0.425325304269791,0.309017032384872,-0.85065084695816,0.162459924817085,-0.499999970197678,0.85065084695816,-0.16245986521244,0.499999970197678,-0.850650787353516]);
//
// SCSky.defaultIntegrationVS = `#version 300 es
// layout(location = 0) in vec4 i_Position;
// layout(location = 1) in vec2 i_UV;
//
// out vec2 io_UV;
//
// void main() {
//     io_UV = i_UV;
//     gl_Position = i_Position;
// }`;
//
// SCSky.defaultIntegrationFS = `#version 300 es
// precision highp float;
//
// in vec2 io_UV;
// out vec2 o_Color;
//
// #define PI 3.1415926535
//
// float RadicalInverse_VdC(uint bits)  {
//     bits = (bits << 16u) | (bits >> 16u);
//     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
//     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
//     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
//     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
//     return float(bits) * 2.3283064365386963e-10; // / 0x100000000
// }
//
// vec2 Hammersley(uint i, uint N) {
//     return vec2(float(i)/float(N), RadicalInverse_VdC(i));
// }
//
// float GeometrySchlickGGX(float NdotV, float roughness) {
//     float a = roughness;
//     float k = (a * a) / 2.0;
//
//     float nom   = NdotV;
//     float denom = NdotV * (1.0 - k) + k;
//
//     return nom / denom;
// }
//
// float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
//     float NdotV = max(dot(N, V), 0.0);
//     float NdotL = max(dot(N, L), 0.0);
//     float ggx2 = GeometrySchlickGGX(NdotV, roughness);
//     float ggx1 = GeometrySchlickGGX(NdotL, roughness);
//
//     return ggx1 * ggx2;
// }
//
// vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness) {
//     float a = roughness*roughness;
//
//     float phi = 2.0 * PI * Xi.x;
//     float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
//     float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
//
//     // преобразование из сферических в декартовы координаты
//     vec3 H;
//     H.x = cos(phi) * sinTheta;
//     H.y = sin(phi) * sinTheta;
//     H.z = cosTheta;
//
//     // преобразование из касательного пространства в мировые координаты
//     vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
//     vec3 tangent   = normalize(cross(up, N));
//     vec3 bitangent = cross(N, tangent);
//
//     vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
//     return normalize(sampleVec);
// }
//
// vec2 IntegrateBRDF(float NdotV, float roughness) {
//     vec3 V;
//     V.x = sqrt(1.0 - NdotV*NdotV);
//     V.y = 0.0;
//     V.z = NdotV;
//
//     float A = 0.0;
//     float B = 0.0;
//
//     vec3 N = vec3(0.0, 0.0, 1.0);
//
//     const uint SAMPLE_COUNT = 1024u;
//     for(uint i = 0u; i < SAMPLE_COUNT; ++i)
//     {
//         vec2 Xi = Hammersley(i, SAMPLE_COUNT);
//         vec3 H  = ImportanceSampleGGX(Xi, N, roughness);
//         vec3 L  = normalize(2.0 * dot(V, H) * H - V);
//
//         float NdotL = max(L.z, 0.0);
//         float NdotH = max(H.z, 0.0);
//         float VdotH = max(dot(V, H), 0.0);
//
//         if(NdotL > 0.0)
//         {
//             float G = GeometrySmith(N, V, L, roughness);
//             float G_Vis = (G * VdotH) / (NdotH * NdotV);
//             float Fc = pow(1.0 - VdotH, 5.0);
//
//             A += (1.0 - Fc) * G_Vis;
//             B += Fc * G_Vis;
//         }
//     }
//     A /= float(SAMPLE_COUNT);
//     B /= float(SAMPLE_COUNT);
//     return vec2(A, B);
// }
//
// void main() {
//     o_Color = IntegrateBRDF(io_UV.x, io_UV.y);
// }`;
//
// SCSky.defaultSpecularVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_Projection;
//
// out vec3 io_NormalXP;
// out vec3 io_NormalXN;
//
// out vec3 io_NormalYP;
// out vec3 io_NormalYN;
//
// out vec3 io_NormalZP;
// out vec3 io_NormalZN;
//
// mat3 lookAt(vec3 inverseTarget, vec3 up) {
//     vec3 FW = inverseTarget;
//     vec3 LF = cross(up, FW);
//     vec3 UP = cross(FW, LF);
//
//     return mat3(
//         LF.x, UP.x, FW.x,
//         LF.y, UP.y, FW.y,
//         LF.z, UP.z, FW.z
//     );
// }
//
// void main() {
//     io_NormalXP = lookAt(vec3(1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalXN = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     io_NormalYP = lookAt(vec3(0.0,  -1.0,  0.0), vec3(0.0,  0.0, 1.0)) * i_Normal;
//     io_NormalYN = lookAt(vec3(0.0, 1.0,  0.0), vec3(0.0,  0.0,  -1.0)) * i_Normal;
//
//     io_NormalZP = lookAt(vec3(0.0,  0.0, 1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalZN = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     gl_Position = u_Projection * vec4(i_Position, 1.0f);
// }`;
//
// SCSky.defaultSpecularFS = `#version 300 es
// precision highp float;
//
// uniform samplerCube u_OriginalCubemap;
// uniform float u_Roughness;
//
// in vec3 io_NormalXP;
// in vec3 io_NormalXN;
//
// in vec3 io_NormalYP;
// in vec3 io_NormalYN;
//
// in vec3 io_NormalZP;
// in vec3 io_NormalZN;
//
// out vec4 o_Color[6];
//
// #define PI 3.1415926535
//
// float RadicalInverse_VdC(uint bits)  {
//     bits = (bits << 16u) | (bits >> 16u);
//     bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
//     bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
//     bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
//     bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
//     return float(bits) * 2.3283064365386963e-10; // / 0x100000000
// }
//
// vec2 Hammersley(uint i, uint N) {
//     return vec2(float(i)/float(N), RadicalInverse_VdC(i));
// }
//
// vec3 ImportanceSampleGGX(vec2 Xi, vec3 N, float roughness) {
//     float a = roughness*roughness;
//
//     float phi = 2.0 * PI * Xi.x;
//     float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
//     float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
//
//     // преобразование из сферических в декартовы координаты
//     vec3 H;
//     H.x = cos(phi) * sinTheta;
//     H.y = sin(phi) * sinTheta;
//     H.z = cosTheta;
//
//     // преобразование из касательного пространства в мировые координаты
//     vec3 up        = abs(N.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
//     vec3 tangent   = normalize(cross(up, N));
//     vec3 bitangent = cross(N, tangent);
//
//     vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
//     return normalize(sampleVec);
// }
//
// vec3 calculate(vec3 normal) {
//     vec3 N = normalize(normal);
//     vec3 R = N;
//     vec3 V = R;
//
//     const uint SAMPLE_COUNT = 1024u;
//     float totalWeight = 0.0;
//     vec3 prefilteredColor = vec3(0.0);
//     for(uint i = 0u; i < SAMPLE_COUNT; ++i)
//     {
//         vec2 Xi = Hammersley(i, SAMPLE_COUNT);
//         vec3 H  = ImportanceSampleGGX(Xi, N, u_Roughness);
//         vec3 L  = normalize(2.0 * dot(V, H) * H - V);
//
//         float NdotL = max(dot(N, L), 0.0);
//         if(NdotL > 0.0)
//         {
//             prefilteredColor += texture(u_OriginalCubemap, L).rgb * NdotL;
//             totalWeight      += NdotL;
//         }
//     }
//     prefilteredColor = prefilteredColor / totalWeight;
//
//     return prefilteredColor;
// }
//
// void main() {
//     o_Color[0] = vec4(calculate(normalize(io_NormalXP)), 1.0f);
//     o_Color[1] = vec4(calculate(normalize(io_NormalXN)), 1.0f);
//     o_Color[2] = vec4(calculate(normalize(io_NormalYP)), 1.0f);
//     o_Color[3] = vec4(calculate(normalize(io_NormalYN)), 1.0f);
//     o_Color[4] = vec4(calculate(normalize(io_NormalZP)), 1.0f);
//     o_Color[5] = vec4(calculate(normalize(io_NormalZN)), 1.0f);
// }`;
//
// SCSky.defaultConvertVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_Projection;
//
// out vec3 io_NormalXP;
// out vec3 io_NormalXN;
//
// out vec3 io_NormalYP;
// out vec3 io_NormalYN;
//
// out vec3 io_NormalZP;
// out vec3 io_NormalZN;
//
// mat3 lookAt(vec3 inverseTarget, vec3 up) {
//     vec3 FW = inverseTarget;
//     vec3 LF = cross(up, FW);
//     vec3 UP = cross(FW, LF);
//
//     return mat3(
//         LF.x, UP.x, FW.x,
//         LF.y, UP.y, FW.y,
//         LF.z, UP.z, FW.z
//     );
// }
//
// void main() {
//     io_NormalXP = lookAt(vec3(1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalXN = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     io_NormalYP = lookAt(vec3(0.0,  -1.0,  0.0), vec3(0.0,  0.0, 1.0)) * i_Normal;
//     io_NormalYN = lookAt(vec3(0.0, 1.0,  0.0), vec3(0.0,  0.0,  -1.0)) * i_Normal;
//
//     io_NormalZP = lookAt(vec3(0.0,  0.0, 1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//     io_NormalZN = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, 1.0,  0.0)) * i_Normal;
//
//     gl_Position = u_Projection * vec4(i_Position, 1.0f);
// }`;
//
// SCSky.defaultConvertFS = `#version 300 es
// precision highp float;
//
// uniform samplerCube u_OriginalCubemap;
//
// in vec3 io_NormalXP;
// in vec3 io_NormalXN;
//
// in vec3 io_NormalYP;
// in vec3 io_NormalYN;
//
// in vec3 io_NormalZP;
// in vec3 io_NormalZN;
//
// out vec4 o_Color[6];
//
// #define PI 3.1415926535
//
// vec3 calculate(vec3 normal) {
//     vec3 irradiance = vec3(0.0);
//
//     vec3 up    = vec3(0.0, 1.0, 0.0);
//     vec3 right = cross(up, normal);
//     up         = cross(normal, right);
//
//     float sampleDelta = 0.025;
//     float nrSamples = 0.0;
//     for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
//     {
//         for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
//         {
//             // перевод сферических коорд. в декартовы (в касательном пр-ве)
//             vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
//             // из касательного в мировое пространство
//             vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * normal;
//
//             irradiance += texture(u_OriginalCubemap, sampleVec).rgb * cos(theta) * sin(theta);
//             nrSamples++;
//         }
//     }
//     irradiance = PI * irradiance * (1.0 / float(nrSamples));
//
//     return irradiance;
// }
//
// void main() {
//     o_Color[0] = vec4(calculate(normalize(io_NormalXP)), 1.0f);
//     o_Color[1] = vec4(calculate(normalize(io_NormalXN)), 1.0f);
//     o_Color[2] = vec4(calculate(normalize(io_NormalYP)), 1.0f);
//     o_Color[3] = vec4(calculate(normalize(io_NormalYN)), 1.0f);
//     o_Color[4] = vec4(calculate(normalize(io_NormalZP)), 1.0f);
//     o_Color[5] = vec4(calculate(normalize(io_NormalZN)), 1.0f);
// }`;
//
// SCSky.defaultReadVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_View;
// uniform mat4 u_Projection;
//
// out vec3 io_Normal;
//
// void main() {
//     io_Normal = i_Normal;
//     gl_Position = u_Projection * u_View * vec4(i_Position, 1.0f);
// }`;
//
// SCSky.defaultReadFS = `#version 300 es
// precision highp float;
//
// uniform samplerCube u_Sky;
//
// in vec3 io_Normal;
// out vec4 o_Color;
//
// void main() {
//     o_Color = vec4(texture(u_Sky, normalize(io_Normal)).rgb, 1.0f);
// }`;
//
// SCSky.defaultDrawVS = `#version 300 es
// layout(location = 0) in vec3 i_Position;
// layout(location = 1) in vec3 i_Normal;
//
// uniform mat4 u_Projection;
//
// out vec3 io_NormalXP;
// out vec3 io_NormalXN;
//
// out vec3 io_NormalYP;
// out vec3 io_NormalYN;
//
// out vec3 io_NormalZP;
// out vec3 io_NormalZN;
//
// mat3 lookAt(vec3 inverseTarget, vec3 up) {
//     vec3 FW = inverseTarget;
//     vec3 LF = cross(up, FW);
//     vec3 UP = cross(FW, LF);
//
//     return mat3(
//         LF.x, UP.x, FW.x,
//         LF.y, UP.y, FW.y,
//         LF.z, UP.z, FW.z
//     );
// }
//
// void main() {
//     io_NormalXP = lookAt(vec3(-1.0, 0.0,  0.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//     io_NormalXN = lookAt(vec3( 1.0, 0.0,  0.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//
//     io_NormalYP = lookAt(vec3(0.0,  1.0,  0.0), vec3(0.0,  0.0, -1.0)) * i_Normal;
//     io_NormalYN = lookAt(vec3(0.0, -1.0,  0.0), vec3(0.0,  0.0,  1.0)) * i_Normal;
//
//     io_NormalZP = lookAt(vec3(0.0,  0.0, -1.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//     io_NormalZN = lookAt(vec3(0.0,  0.0,  1.0), vec3(0.0, -1.0,  0.0)) * i_Normal;
//
//     gl_Position = u_Projection * vec4(i_Position, 1.0f);
// }`;
//
// SCSky.defaultDrawFS = `#version 300 es
// precision highp float;
//
// uniform vec3 u_SunDirection;
// uniform vec4 u_SunColor;
//
// uniform vec2 u_Radius;
// uniform vec2 u_AvangeDensity;
//
// uniform vec3 u_KRay;
// uniform vec4 u_KMie;
//
// in vec3 io_NormalXP;
// in vec3 io_NormalXN;
//
// in vec3 io_NormalYP;
// in vec3 io_NormalYN;
//
// in vec3 io_NormalZP;
// in vec3 io_NormalZN;
//
// out vec4 o_Color[6];
//
// #define PI 3.1415926535
// #define MAX_VALUE 1e10
//
// #define STEP_COUNT_PRIMARY 96
// #define STEP_COUNT_SECONDARY 8
//
// // Обычный алгоритм рассчёта точек пересечения луча и сферы, но с учётом того что для origin находящегося внутри сферы - t0 будет равен 0.0:
//
// vec2 intersection(vec3 origin, vec3 direction, float radius) {
//     vec3 D = direction;
//     vec3 L = vec3(0.0) - origin;
//
//     float tca = dot(D, L);
//     float d = dot(L, L) - (tca * tca);
//     float R = radius * radius;
//
//     if (d > R) {
//         return vec2(MAX_VALUE, -MAX_VALUE);
//     }
//
//     float thc = sqrt(R - d);
//     float t0 = tca - thc;
//     float t1 = tca + thc;
//
//     if (t0 > 0.0 || t1 > 0.0) {
//         t0 = max(t0, 0.0);
//         t1 = max(t1, 0.0);
//
//         return vec2(min(t0, t1), max(t0, t1));
//     }
//
//     return vec2(MAX_VALUE, -MAX_VALUE);
// }
//
// // Рассчитываем фазу для ray:
//
// float calculatePhaseRay(float cosineSquare) {
//     return 0.75 * (1.0 + cosineSquare);
// }
//
// // Рассчитываем фазу для mie:
//
// float calculatePhaseMie(float cosineSquare, float cosine) {
//
//     float RADIUS_PLANET     = u_Radius.x;
//     float RADIUS_ATMOSPHERE = u_Radius.y;
//
//     float AVANGE_DENSITY_RAY = u_AvangeDensity.x;
//     float AVANGE_DENSITY_MIE = u_AvangeDensity.y;
//
//     vec3 K_RAY = u_KRay;
//     vec3 K_MIE = u_KMie.rgb;
//
//     vec3  SUN_COLOR     = u_SunColor.rgb;
//     float SUN_INTENSITY = u_SunColor.a;
//
//     float MIE_G = u_KMie.a;
//
//     float vA = 3.0 * (1.0 - MIE_G * MIE_G);
//     float vB = 2.0 * (2.0 - MIE_G * MIE_G);
//
//     float vC = 1.0 + cosineSquare;
//     float vD = pow(1.0 + MIE_G * MIE_G - 2.0 * MIE_G * cosine, 1.5);
//
//     return (vA / vB) * (vC / vD);
// }
//
// // Рассчёт внтуреннего интеграла (оптической глубины) отдельно для ray и mie:
//
// void calculateInternalIntegral(vec3 rayOrigin, vec3 rayDirection, out float depthRay, out float depthMie) {
//
//     float RADIUS_PLANET     = u_Radius.x;
//     float RADIUS_ATMOSPHERE = u_Radius.y;
//
//     float AVANGE_DENSITY_RAY = u_AvangeDensity.x;
//     float AVANGE_DENSITY_MIE = u_AvangeDensity.y;
//
//     vec3 K_RAY = u_KRay;
//     vec3 K_MIE = u_KMie.rgb;
//
//     vec3  SUN_COLOR     = u_SunColor.rgb;
//     float SUN_INTENSITY = u_SunColor.a;
//
//     float MIE_G = u_KMie.a;
//
//     float rayStepLength = intersection(rayOrigin, rayDirection, RADIUS_ATMOSPHERE).y / float(STEP_COUNT_SECONDARY);
//     vec3  rayStep = rayDirection * rayStepLength;
//     vec3  rayStepCurrent = rayOrigin + rayStep * 0.5;
//
//     depthRay = 0.0;
//     depthMie = 0.0;
//
//     for (int i = 0; i < STEP_COUNT_SECONDARY; i++) {
//         float currentHeight = length(rayStepCurrent) - RADIUS_PLANET;
//
//         depthRay += exp(-currentHeight / AVANGE_DENSITY_RAY) * rayStepLength;
//         depthMie += exp(-currentHeight / AVANGE_DENSITY_MIE) * rayStepLength;
//
//         rayStepCurrent += rayStep;
//     }
// }
//
// vec3 calculateColor(vec3 rayOrigin, vec3 rayDirection, vec3 sunDirection) {
//
//     float RADIUS_PLANET     = u_Radius.x;
//     float RADIUS_ATMOSPHERE = u_Radius.y;
//
//     float AVANGE_DENSITY_RAY = u_AvangeDensity.x;
//     float AVANGE_DENSITY_MIE = u_AvangeDensity.y;
//
//     vec3 K_RAY = u_KRay;
//     vec3 K_MIE = u_KMie.rgb;
//
//     vec3  SUN_COLOR     = u_SunColor.rgb;
//     float SUN_INTENSITY = u_SunColor.a;
//
//     float MIE_G = u_KMie.a;
//
//     // Находим точки пересечения луча и атмосферы:
//
//     vec2 aip = intersection(rayOrigin, rayDirection, RADIUS_ATMOSPHERE);
//     if (aip.x > aip.y) {
//         return vec3(0.0f);
//         // return texture(u_StarsColor, rayDirection).rgb; // Если мы промахнулись мимо атмосферы - просто выводим цвет из текстуры звёзд.
//     }
//
//     // Находим точки пересечения луча и планеты:
//
//     vec2 pip = intersection(rayOrigin, rayDirection, RADIUS_PLANET);
//     aip.y = min(aip.y, pip.x);
//
//     float primaryRayStepLength = (aip.y - aip.x) / float(STEP_COUNT_PRIMARY);
//     vec3  primaryRayStep = rayDirection * primaryRayStepLength;
//     vec3  primaryRayStepCurrent = rayOrigin + rayDirection * aip.x + primaryRayStep * 0.5;
//
//     vec3 finalRay = vec3(0.0);
//     vec3 finalMie = vec3(0.0);
//
//     float primaryDepthRay = 0.0;
//     float primaryDepthMie = 0.0;
//
//     vec3 attenuation = vec3(0.0); // Вместо рассчёта света отражёного от поверхности можно воспользоваться последним значением attenuation.
//
//     for (int i = 0; i < STEP_COUNT_PRIMARY; i++) {
//         float primaryHeight = length(primaryRayStepCurrent) - RADIUS_PLANET;
//
//         float primaryDepthRayCurrent = exp(-primaryHeight / AVANGE_DENSITY_RAY) * primaryRayStepLength;
//         float primaryDepthMieCurrent = exp(-primaryHeight / AVANGE_DENSITY_MIE) * primaryRayStepLength;
//
//         primaryDepthRay += primaryDepthRayCurrent;
//         primaryDepthMie += primaryDepthMieCurrent;
//
//         float secondaryDepthRay = 0.0;
//         float secondaryDepthMie = 0.0;
//
//         calculateInternalIntegral(primaryRayStepCurrent, sunDirection, secondaryDepthRay, secondaryDepthMie);
//
//         attenuation = exp(-4.0 * PI * (K_RAY * (primaryDepthRay + secondaryDepthRay) + K_MIE * (primaryDepthMie + secondaryDepthMie)));
//
//         finalRay += primaryDepthRayCurrent * attenuation;
//         finalMie += primaryDepthMieCurrent * attenuation;
//
//         primaryRayStepCurrent += primaryRayStep;
//     }
//
//     float cosine = dot(rayDirection, -sunDirection);
//     float cosineSquare = cosine * cosine;
//
//     float phaseRay = calculatePhaseRay(cosineSquare);
//     float phaseMie = calculatePhaseMie(cosineSquare, cosine);
//
//     vec3 atmosphereColor = SUN_COLOR * SUN_INTENSITY * (K_RAY * phaseRay * finalRay + K_MIE * phaseMie * finalMie);
//     return vec3(atmosphereColor);
// }
//
// void main() {
//     // o_Color[0] = vec4(calculateColor(vec3(0.0f, 3500e3 * 1.5f, 3500e3 * 2.5f), normalize(io_NormalXP), -u_SunDirection), 1.0);
//     // o_Color[1] = vec4(calculateColor(vec3(0.0f, 3500e3 * 1.5f, 3500e3 * 2.5f), normalize(io_NormalXN), -u_SunDirection), 1.0);
//     // o_Color[2] = vec4(calculateColor(vec3(0.0f, 3500e3 * 1.5f, 3500e3 * 2.5f), normalize(io_NormalYP), -u_SunDirection), 1.0);
//     // o_Color[3] = vec4(calculateColor(vec3(0.0f, 3500e3 * 1.5f, 3500e3 * 2.5f), normalize(io_NormalYN), -u_SunDirection), 1.0);
//     // o_Color[4] = vec4(calculateColor(vec3(0.0f, 3500e3 * 1.5f, 3500e3 * 2.5f), normalize(io_NormalZP), -u_SunDirection), 1.0);
//     // o_Color[5] = vec4(calculateColor(vec3(0.0f, 3500e3 * 1.5f, 3500e3 * 2.5f), normalize(io_NormalZN), -u_SunDirection), 1.0);
//
//     o_Color[0] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_NormalXP), -u_SunDirection), 1.0);
//     o_Color[1] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_NormalXN), -u_SunDirection), 1.0);
//     o_Color[2] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_NormalYP), -u_SunDirection), 1.0);
//     o_Color[3] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_NormalYN), -u_SunDirection), 1.0);
//     o_Color[4] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_NormalZP), -u_SunDirection), 1.0);
//     o_Color[5] = vec4(calculateColor(vec3(0.0f, u_Radius.x + 2.0f, 0.0f), normalize(io_NormalZN), -u_SunDirection), 1.0);
// }`;
