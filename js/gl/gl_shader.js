class GLShader {

    // Принимает на вход экземпляр WebGL а так же исходный код шейдера:

    constructor(gl, shaderSourceCode) {
        this._gl     = gl;
        this._stage  = shaderSourceCode.search('gl_Position') !== -1 ? this._gl.VERTEX_SHADER : this._gl.FRAGMENT_SHADER;
        this._shader = this._gl.createShader(this._stage);

        // Компилирует шейдер:

        this._gl.shaderSource(this._shader, shaderSourceCode);
        this._gl.compileShader(this._shader);

        // Отображает лог компиляции шейдера если оная не была полностью успешной:

        if (!this._gl.getShaderParameter(this._shader, this._gl.COMPILE_STATUS)) {
            console.log(this._gl.getShaderInfoLog(this._shader));
        }

        // Очищает исходный код шейдера как от однострочных, так и от многострочных комментариев:

        shaderSourceCode = shaderSourceCode.replaceAll(/(?:\/\*[^]*?\*\/)|(?:\/\/.*[\n\r\u2028\u2029]*?)/igm, '');

        // Получает список объявлений структур, а так же uniform блоков и uniform переменных:

        const structsSourceCode = shaderSourceCode.match(new RegExp('struct[\\w\\s]+\\{[\\w\\s\\[\\];]+\\}[\\w\\s\\[\\]]*;', 'igm'));
        const uniformsSourceCode = shaderSourceCode.match(new RegExp('(?:uniform[\\w\\s]+\\{[\\w\\s\\[\\];]+\\}[\\w\\s\\[\\]]*;)|(?:uniform[\\w\\s]+(?:\\[[0-9]+\\])?);', 'igm'));

        // Конвертирует структуры из исходного кода в JS объекты (см. _makeJSObjectFromGLSLBlock):

        this._structBlocks = structsSourceCode ? structsSourceCode.map((currentStructSourceCode, index, array) =>
            GLShader._makeJSObjectFromGLSLBlock(currentStructSourceCode)
        ) : [];

        // Конвертирует uniform блоки и uniform переменные в JS объекты (см. _makeJSObjectFromGLSLBlock и _makeJSObjectFromGLSLVariable соответственно):

        this._uniformBlocks = [];
        this._uniformVariables = [];

        if (uniformsSourceCode) {
            uniformsSourceCode.forEach((currentUniformSourceCode) => {
                if (currentUniformSourceCode.match(/{/img)) {
                    this._uniformBlocks.push(GLShader._makeJSObjectFromGLSLBlock(currentUniformSourceCode));
                } else {
                    this._uniformVariables.push(GLShader._makeJSObjectFromGLSLVariable(currentUniformSourceCode));
                }
            })
        }
    }

    // ВНИМАНИЕ - после вызова данного метода текущий GLShader объект станет непригоден к использованию!
    // Удаляет объект шейдера а так же очищает память выделенную под ресурсы:

    destroy() {
        this._gl.deleteShader(this._shader);
        this._gl = undefined;

        this._stage  = undefined;
        this._shader = undefined;

        this._structBlocks     = undefined;
        this._uniformBlocks    = undefined;
        this._uniformVariables = undefined;
    }

    // Конвертирует блок из исходного кода (вида 'blockSpecifier BlockName { /* переменные */ }',
    // 'blockSpecifier BlockName { /* переменные */ } blockVariableName' или же blockSpecifier BlockName { /* переменные */ } blockVariableName[size]) в JS объект вида:
    // {
    //     type: (имя типа блока)
    //     name: (имя переменной блока (если присутствует, иначе null))
    //     size: (количество элементов массива переменной блока, либо 1 если переменная блока не является массивом, или null, если переменная отсутствует)
    //     content: (массив JS объектов (см. _makeJSObjectFromGLSLVariable) описывающих переменные блока)
    // }

    static _makeJSObjectFromGLSLBlock(blockSourceCode) {

        // Очищает исходный код блока от различного мусора:

        blockSourceCode = blockSourceCode.replaceAll(/\s+/igm, ' ').replaceAll(/ ?{ ?/igm, '{').replaceAll(/ ?} ?/igm, '}').
            replaceAll(/ ?; ?/igm, ';').replaceAll(/ ?\[ ?/igm, '[').replaceAll(/ ?\] ?/igm, ']').replaceAll(/; ?$/igm, '');

        // Разбивает строку на логические блоки (до открывающей фигурной скобки; внутри фигурных скобок; после закрывающей фигурной скобки):

        const blockComponents = blockSourceCode.split(/{|}/igm);

        // Разбивает полученные выше строки на новые логические блоки:

        const typeBlockComponents = blockComponents[0].split(' ');
        const bodyBlockComponents = blockComponents[1].split(';').filter(currentBodyBlockComponent => currentBodyBlockComponent !== '');
        const nameBlockComponents = blockComponents[2].split(/\[|\]/igm).filter(currentBodyBlockComponent => currentBodyBlockComponent !== '');

        // Формирует итоговый JS объект, при этом, если блок не является массивом size будет равен 1:

        return {
            type: typeBlockComponents[1],
            name: nameBlockComponents[0],
            size: nameBlockComponents[0] !== undefined ? Number(nameBlockComponents[1] !== undefined ? nameBlockComponents[1] : 1) : undefined,
            content: bodyBlockComponents.map((currentVariableSourceCode, index, array) => { return GLShader._makeJSObjectFromGLSLVariable(currentVariableSourceCode); }),
        };
    }

    // Конвертирует переменную из исходного кода (вида 'TypeName variableName' или же 'TypeName variableName[size]') в JS объект вида:
    // {
    //     type: (тип переменной)
    //     name: (имя переменной)
    //     size: (количество элементов массива переменной, либо 1 если переменная не является массивом)
    // }

    static _makeJSObjectFromGLSLVariable(variableSourceCode) {

        // Очищает исходный код переменной от различного мусора:

        variableSourceCode = variableSourceCode.replaceAll(/\s+/igm, ' ').replaceAll(/ ?{ ?/igm, '{').replaceAll(/ ?} ?/igm, '}').replaceAll(/ ?; ?/igm, ';').
            replaceAll(/ ?\[ ?/igm, '[').replaceAll(/ ?\] ?/igm, ']').replaceAll(/ ?uniform /igm, '').replaceAll(/; ?$/igm, '');

        // Разбивает строку на логические блоки (тип переменной; имя переменной; количество элементов массива переменной (если требуется)):

        const variableComponents = variableSourceCode.split(/ |\[|\]/igm);

        // Формирует итоговый JS объект, при это, если переменная не является массивом size будет равен 1:

        return {
            type: variableComponents[0],
            name: variableComponents[1],
            size: Number(variableComponents[2] !== undefined ? variableComponents[2] : 1),
        };
    }

    // Возвращает объект WebGL шейдера:

    get shader() {
        return this._shader;
    }

    // Возвращает стадию к которой принадлежит текущий шейдер (VERTEX_SHADER либо FRAGMENT_SHADER):

    get stage() {
        return this._stage;
    }

    // Возвращает массив JS объектов (см. _makeJSObjectFromGLSLBlock) описывающих объявленные в шейдере struct блоки:

    get structBlocks() {
        return this._structBlocks;
    }

    // Возвращает массив JS объектов (см. _makeJSObjectFromGLSLBlock) описывающих объявленные в шейдере uniform блоки:

    get uniformBlocks() {
        return this._uniformBlocks;
    }

    // Возвращает массив JS объектов (см. _makeJSObjectFromGLSLVariable) описывающих объявленные в шейдере uniform переменные:

    get uniformVariables() {
        return this._uniformVariables;
    }
}
