// Абстракция вокруг объекта WebGLTexture.
// Включает в себя сам объект а так же информацию о некоторых его параметрах.

class GLTexture {

    // Конструктор может принимать на вход следующие наборы аргументов:

    // gl                                   - Будет создан пустой объект, без выделения памяти под текстуру.
    // gl, magFilter, format, width, height - Будет создан объект двумерной текстуры с выделенной памятью под изображение заданных размеров.
    // gl, magFilter, format, width         - Будет создан объект кубичесокй текстуры с выделенной памятью под шесть квадаратных изображений со стороной заданного размера.
    // gl, magFilter, [image]               - Будет создан объект кубической текстуры с загруженными в память изображениями.
    // gl, magFilter, image                 - Будет создан объект двумерной текстуры с загруженным в память изображением.

    constructor(gl, magFilter) {
        this._gl      = gl;
        this._texture = this._gl.createTexture();

        if (arguments[2] instanceof Array) { // Заполненная кубическая текстура:
            const firstImage = function (images) { for (const image in images) { if (image instanceof Image) { return image; } } }(arguments[2]);
            this.setTexture(this._gl.TEXTURE_CUBE_MAP, this._gl.RGBA, firstImage.naturalWidth, arguments[2]);
        }
        else if (arguments[2] instanceof Image) { // Заполненная двумерная текстура:
            this.setTexture(this._gl.TEXTURE_2D, this._gl.RGBA, arguments[2].naturalWidth, arguments[2].naturalHeight, arguments[2]);
        }
        else if (arguments.length === 5) { // Пустая двумерная текстура:
            this.setTexture(this._gl.TEXTURE_2D, arguments[2], arguments[3], arguments[4]);
        }
        else if (arguments.length === 4) { // Пустая кубическая текстура:
            this.setTexture(this._gl.TEXTURE_CUBE_MAP, arguments[2], arguments[3]);
        }

        this.magFilter = magFilter;
    }

    // Удаляет объект текстуры очищая выделенную под него память.
    // При этом текущий JS объект так же станет непригоден к использованию:

    destroy() {
        this._gl.deleteTexture(this._texture);
        this._texture = null;
    }

    // Метод setTexture выделяет память под текстуру с заданными параметрами.
    // Может принимать один из следующих наборов аргументов:

    // Если target равен TEXTURE_3D либо TEXTURE_2D_ARRAY:

    // target, internalformat, width, height, depth, source
    // target, internalformat, width, height, depth, source, offset

    // Если target равен TEXTURE_2D либо одной из сторон куба:

    // target, internalformat, width, height, source
    // target, internalformat, width, height, source, offset

    // Если target равен TEXTURE_CUBE_MAP:

    // target, internalformat, width, [source]
    // target, internalformat, width, [[source, offset]]

    // Аргумент target может принимать одно из следующих значений:

    // gl.TEXTURE_2D
    // gl.TEXTURE_CUBE_MAP_POSITIVE_X
    // gl.TEXTURE_CUBE_MAP_NEGATIVE_X
    // gl.TEXTURE_CUBE_MAP_POSITIVE_Y
    // gl.TEXTURE_CUBE_MAP_NEGATIVE_Y
    // gl.TEXTURE_CUBE_MAP_POSITIVE_Z
    // gl.TEXTURE_CUBE_MAP_NEGATIVE_Z
    // gl.TEXTURE_CUBE_MAP
    // gl.TEXTURE_2D_ARRAY
    // gl.TEXTURE_3D

    setTexture(target, internalFormat, width) {
        this._isStorage      = false;
        this._internalFormat = internalFormat;
        this._width          = width;

        // Поскольку мы можем получить в качестве параметра target одну из сторон куба, требуется обработать эту ситуацию:

        this._target = target === this._gl.TEXTURE_CUBE_MAP_POSITIVE_X || target === this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X ||
                       target === this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y || target === this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y ||
                       target === this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z || target === this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z ? this._gl.TEXTURE_CUBE_MAP : target;

        const texelDescription = GLTexture._getTexelDescription(this._internalFormat, this._gl); // Получаем описание текселя исходя из внутреннего формата.
        this._gl.bindTexture(this._target, this._texture);

        switch (target) {
            case this._gl.TEXTURE_2D:
            case this._gl.TEXTURE_CUBE_MAP_POSITIVE_X:
            case this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X:
            case this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y:
            case this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y:
            case this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z:
            case this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z: // Любая из двумерных текстур:
            {
                this._height = arguments[3];
                this._depth  = 1;

                const source = arguments[4] === undefined ? [undefined] : (arguments[5] === undefined ? [arguments[4]] : [arguments[4], arguments[5]]);
                this._gl.texImage2D(target, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...source);
            }
            break;

            case this._gl.TEXTURE_CUBE_MAP: // Кубическая текстура:
            {
                this._height = this._width;
                this._depth  = 1;

                const source = arguments[3] === undefined ? [undefined, undefined, undefined, undefined, undefined, undefined] : arguments[3];

                this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_POSITIVE_X, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...(source[0] === undefined ? [undefined] : (source[0] instanceof Array ? source[0] : [source[0]])));
                this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...(source[1] === undefined ? [undefined] : (source[1] instanceof Array ? source[1] : [source[1]])));

                this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...(source[2] === undefined ? [undefined] : (source[2] instanceof Array ? source[2] : [source[2]])));
                this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...(source[3] === undefined ? [undefined] : (source[3] instanceof Array ? source[3] : [source[3]])));

                this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...(source[4] === undefined ? [undefined] : (source[4] instanceof Array ? source[4] : [source[4]])));
                this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, this._internalFormat, this._width, this._height, 0, texelDescription.format, texelDescription.type, ...(source[5] === undefined ? [undefined] : (source[5] instanceof Array ? source[5] : [source[5]])));
            }
            break;

            case this._gl.TEXTURE_2D_ARRAY: // Массив двумерных текстур:
            {
                this._height = arguments[3];
                this._depth  = arguments[4];

                const source = arguments[5] === undefined ? [undefined] : (arguments[6] === undefined ? [arguments[5]] : [arguments[5], arguments[6]]);
                this._gl.texImage3D(this._target, 0, this._internalFormat, this._width, this._height, this._depth, 0, texelDescription.format, texelDescription.type, ...source);
            }
            break;

            case this._gl.TEXTURE_3D: // Трёхмерная текстура:
            {
                this._height = arguments[3];
                this._depth  = arguments[4];

                const source = arguments[5] === undefined ? [undefined] : (arguments[6] === undefined ? [arguments[5]] : [arguments[5], arguments[6]]);
                this._gl.texImage3D(this._target, 0, this._internalFormat, this._width, this._height, this._depth, 0, texelDescription.format, texelDescription.type, ...source);
            }
            break;

            default:
                throw 'Попытка создания текстуры неизвестного назначения: (' + String(target) + ')!';
        }

        return this;
    }

    // Метод setMip, если это требуется, выделяет память под mip уровень текстуры.
    // Может принимать один из следующих наборов аргументов:

    // Если текущая текстура является TEXTURE_2D либо TEXTURE_3D:

    // level, source
    // level, source, offset

    // Если текущая текстура является TEXTURE_2D_ARRAY и мы хотим задать mip для конкретного элемента массива:

    // level, depth, source
    // level, depth, source, offset

    // Если текущая текстура является TEXTURE_CUBE_MAP и мы хотим задать mip для конкретной стороны куба:

    // level, target, source
    // level, target, source, offset

    // Если текущая текстура является TEXTURE_CUBE_MAP либо TEXTURE_2D_ARRAY и мы хотим задать mip для нескольких сторон куба / элементов массива:

    // level, [source]
    // level, [source, offset]

    // Аргумент target может принимать одно из следующих значений:

    // gl.TEXTURE_CUBE_MAP_POSITIVE_X
    // gl.TEXTURE_CUBE_MAP_NEGATIVE_X
    // gl.TEXTURE_CUBE_MAP_POSITIVE_Y
    // gl.TEXTURE_CUBE_MAP_NEGATIVE_Y
    // gl.TEXTURE_CUBE_MAP_POSITIVE_Z
    // gl.TEXTURE_CUBE_MAP_NEGATIVE_Z

    setMip(level) {
        if (this._isStorage === undefined || this._isStorage) {
            throw 'Прежде чем использовать \'setMip\', необходимо вызвать \'setTexture\' или конструктор создающий текстуру!';
        }

        const texelDescription = GLTexture._getTexelDescription(this._internalFormat, this._gl); // Получаем описание текселя исходя из внутреннего формата.
        const factor = level === 0 ? 1 : Math.pow(2, level); // Множитель позволяющий получить разрешение текстуры на некотором mip уровне.

        // Получение разрешения для текущего mip уровня, с учётом того что WebGL округляет его в меньшую сторону:

        const currentWidth  = Math.max(Math.floor(this._width  / factor), 1);
        const currentHeight = Math.max(Math.floor(this._height / factor), 1);
        const currentDepth  = Math.max(Math.floor(this._depth  / factor), 1);

        this._gl.bindTexture(this._target, this._texture);

        switch (this._target) {
            case this._gl.TEXTURE_2D: // Двумерная текстура:
            {
                const source = arguments[1] === undefined ? [undefined] : (arguments[2] === undefined ? [arguments[1]] : [arguments[1], arguments[2]]);
                this._gl.texImage2D(this._target, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...source);
            }
            break;

            case this._gl.TEXTURE_3D: // Трёхмерная текстура:
            {
                const source = arguments[1] === undefined ? [undefined] : (arguments[2] === undefined ? [arguments[1]] : [arguments[1], arguments[2]]);
                this._gl.texImage3D(this._target, level, this._internalFormat, currentWidth, currentHeight, currentDepth, 0, texelDescription.format, texelDescription.type, ...source);
            }
            break;

            case this._gl.TEXTURE_CUBE_MAP: // Кубическая текстура:
            {
                if (argumentsp[1] instanceof Array) { // Установка всех сторон куба:
                    const source = arguments[1] === undefined ? [undefined, undefined, undefined, undefined, undefined, undefined] : arguments[1];

                    this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_POSITIVE_X, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...(source[0] === undefined ? [undefined] : (source[0] instanceof Array ? source[0] : [source[0]])));
                    this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_NEGATIVE_X, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...(source[1] === undefined ? [undefined] : (source[1] instanceof Array ? source[1] : [source[1]])));

                    this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_POSITIVE_Y, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...(source[2] === undefined ? [undefined] : (source[2] instanceof Array ? source[2] : [source[2]])));
                    this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...(source[3] === undefined ? [undefined] : (source[3] instanceof Array ? source[3] : [source[3]])));

                    this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_POSITIVE_Z, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...(source[4] === undefined ? [undefined] : (source[4] instanceof Array ? source[4] : [source[4]])));
                    this._gl.texImage2D(this._gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...(source[5] === undefined ? [undefined] : (source[5] instanceof Array ? source[5] : [source[5]])));
                }
                else { // Установка конкретной стороны куба:
                    const source = arguments[2] === undefined ? [undefined] : (arguments[3] === undefined ? [arguments[2]] : [arguments[2], arguments[3]]);
                    this._gl.texImage2D(arguments[1], level, this._internalFormat, currentWidth, currentHeight, 0, texelDescription.format, texelDescription.type, ...source);
                }
            }
            break;

            case this._gl.TEXTURE_2D_ARRAY: // Массив двумерных текстур:
            {
                if (argumentsp[1] instanceof Array) { // Установка нескольких текстур массива подряд:
                    const maximumElementCount = Math.min(arguments[1].length, this._depth);
                    for (var i = 0; i < maximumElementCount; i++) {

                        const source = arguments[1][i] === undefined ? [undefined] : (image instanceof Array ? arguments[1][i] : [arguments[1][i]]);
                        this._gl.texImage3D(this._target, level, this._internalFormat, currentWidth, currentHeight, i, 0, texelDescription.format, texelDescription.type, ...source);
                    }
                }
                else { // Установка конкретной текстуры массива:
                    const source = arguments[2] === undefined ? [undefined] : (arguments[3] === undefined ? [arguments[2]] : [arguments[2], arguments[3]]);
                    this._gl.texImage3D(this._target, level, this._internalFormat, currentWidth, currentHeight, arguments[1], 0, texelDescription.format, texelDescription.type, ...source);
                }
            }
            break;
        }

        return this;
    }

    // Метод setStorage выделяет память под хранилище с заданными параметрами:

    setStorage(target, levels, internalFormat, width, height, depth) {
        this._isStorage      = true;
        this._target         = target;
        this._internalFormat = internalFormat;
        this._width          = width;
        this._height         = height;
        this._depth          = 1;

        this._gl.bindTexture(this._target, this._texture);

        switch (this._target) {
            case this._gl.TEXTURE_2D:
            case this._gl.TEXTURE_CUBE_MAP: // Двумерное или кубическое хранилище:
                this._gl.texStorage2D(this._target, levels, this._format, this._width, this._height);
                break;

            case this._gl.TEXTURE_3D:
            case this._gl.TEXTURE_2D_ARRAY: // Трёхмерное хранилище или массив двумерных хранилищ:
                this._gl.texStorage3D(this._target, levels, this._format, this._width, this._height, (this._depth = depth));
                break;

            default:
                throw 'Попытка создания хранилища неизвестного назначения: (' + String(target) + ')!';
        }

        return this;
    }

    // Позволяет установить параметы текущей текстуры из заданного объекта GLSampler:

    setParametersFromSampler(sampler) {
        this.minFilter = sampler.minFilter;
        this.magFilter = sampler.magFilter;

        this.wrapS = sampler.wrapS;
        this.wrapT = sampler.wrapT;
        this.wrapR = sampler.wrapR;

        this.compareFunc = sampler.compareFunc;
        this.compareMode = sampler.compareMode;

        this.minLod = sampler.minLod;
        this.maxLod = sampler.maxLod;

        return this;
    }

    // Генерирует mip уровни для текущей текстуры:

    generateMipmap() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.generateMipmap(this._target);

        return this;
    }

    // Задаёт параметры текстуры для достижения максимального визуального качества:

    setStuffFullSmooth() {
        this.generateMipmap();
        this.maxAnisotropy = 16;

        this.minFilter = this._gl.LINEAR_MIPMAP_LINEAR;
        this.magFilter = this._gl.LINEAR;

        return this;
    }

    // Задаёт параметры текстуры для использования с выборкой глубины:

    setStuffDepthCompare() {
        this.compareMode = this._gl.COMPARE_REF_TO_TEXTURE;
        this.compareFunc = this._gl.LEQUAL;

        return this;
    }

    // Задаёт параметры текстуры для использования с PBR шейдером:

    setStuffPBR() {
        this.setStuffFullSmooth();

        this.wrapS = this._gl.CLAMP_TO_EDGE;
        this.wrapT = this._gl.CLAMP_TO_EDGE;
        this.wrapR = this._gl.CLAMP_TO_EDGE;

        return this;
    }

    // Возвращает описание текселя (в виде объекта: { format:, type: }) исходя из заданного внутреннего формата:

    static _getTexelDescription(internalFormat, gl) {
        switch (internalFormat) {
            case gl.RGB:             return { format: gl.RGB,             type: gl.UNSIGNED_BYTE };
            case gl.RGBA:            return { format: gl.RGBA,            type: gl.UNSIGNED_BYTE };
            case gl.LUMINANCE_ALPHA: return { format: gl.LUMINANCE_ALPHA, type: gl.UNSIGNED_BYTE };
            case gl.LUMINANCE:       return { format: gl.LUMINANCE,       type: gl.UNSIGNED_BYTE };
            case gl.ALPHA:           return { format: gl.ALPHA,           type: gl.UNSIGNED_BYTE };
            case gl.R8:              return { format: gl.RED,             type: gl.UNSIGNED_BYTE };
            case gl.R16F:            return { format: gl.RED,             type: gl.FLOAT };
            case gl.R32F:            return { format: gl.RED,             type: gl.FLOAT };
            case gl.R8UI:            return { format: gl.RG_INTEGER,      type: gl.UNSIGNED_BYTE };
            case gl.RG8:             return { format: gl.RG,              type: gl.UNSIGNED_BYTE };
            case gl.RG16F:           return { format: gl.RG,              type: gl.FLOAT };
            case gl.RG32F:           return { format: gl.RG,              type: gl.FLOAT };
            case gl.RG8UI:           return { format: gl.RG_INTEGER,      type: gl.UNSIGNED_BYTE };
            case gl.RGB8:            return { format: gl.RGB,             type: gl.UNSIGNED_BYTE };
            case gl.SRGB8:           return { format: gl.RGB,             type: gl.UNSIGNED_BYTE };
            case gl.RGB565:          return { format: gl.RGB,             type: gl.UNSIGNED_BYTE };
            case gl.R11F_G11F_B10F:  return { format: gl.RGB,             type: gl.FLOAT };
            case gl.RGB9_E5:         return { format: gl.RGB,             type: gl.FLOAT };
            case gl.RGB16F:          return { format: gl.RGB,             type: gl.FLOAT };
            // case gl.RGB32F:          return { format: gl.RGB,             type: gl.FLOAT }; // Данный внутренний формат недоступен!
            case gl.RGB8UI:          return { format: gl.RGB_INTEGER,     type: gl.UNSIGNED_BYTE };
            case gl.RGBA8:           return { format: gl.RGBA,            type: gl.UNSIGNED_BYTE };
            case gl.SRGB8_ALPHA8:    return { format: gl.RGBA,            type: gl.UNSIGNED_BYTE };
            case gl.RGB5_A1:         return { format: gl.RGBA,            type: gl.UNSIGNED_BYTE };
            case gl.RGB10_A2:        return { format: gl.RGBA,            type: gl.UNSIGNED_INT_2_10_10_10_REV };
            case gl.RGBA4:           return { format: gl.RGBA,            type: gl.UNSIGNED_BYTE };
            case gl.RGBA16F:         return { format: gl.RGBA,            type: gl.FLOAT };
            case gl.RGBA32F:         return { format: gl.RGBA,            type: gl.FLOAT };
            case gl.RGBA8UI:         return { format: gl.RGBA_INTEGER,    type: gl.UNSIGNED_BYTE };

            case gl.DEPTH_COMPONENT16:  return { format: gl.DEPTH_COMPONENT, type: gl.UNSIGNED_SHORT };
            case gl.DEPTH_COMPONENT24:  return { format: gl.DEPTH_COMPONENT, type: gl.UNSIGNED_INT };
            case gl.DEPTH_COMPONENT32F: return { format: gl.DEPTH_COMPONENT, type: gl.FLOAT };
            case gl.DEPTH24_STENCIL8:   return { format: gl.DEPTH_STENCIL,   type: gl.UNSIGNED_INT_24_8 };
            case gl.DEPTH32F_STENCIL8:  return { format: gl.DEPTH_STENCIL,   type: gl.FLOAT_32_UNSIGNED_INT_24_8_REV };

            default:
                throw 'Попытка использования недоступного внутреннего формата: (' + String(internalFormat) + ')!';
        }
    }

    // Возвращает объект WebGLTexture

    get texture() {
        return this._texture;
    }

    // Возвращает информацию о том, является ли текущий объект хранилищем или же текстурой:

    get isStorage() {
        return this._isStorage;
    }

    // Возвращает тип текущей текстуры (TEXTURE_2D, TEXTURE_3D и т.д.):

    get target() {
        return this._target;
    }

    // Возвращает внутренний формат текущей текстуры:

    get internalFormat() {
        return this._internalFormat;
    }

    // Возвращает ширину текстуры для нулевого mip уровня:

    get width() {
        return this._width;
    }

    // Возвращает высоту текстуры для нулевого mip уровня:

    get height() {
        return this._height;
    }

    // Возвращает глубину текстуры для нулевого mip уровня:

    get depth() {
        return this._depth;
    }

    // Возвращает текущее состояние переменной TEXTURE_MIN_FILTER для данной текстуры:

    get minFilter() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_MIN_FILTER);
    }

    // Возвращает текущее состояние переменной TEXTURE_MAG_FILTER для данной текстуры:

    get magFilter() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_MAG_FILTER);
    }

    // Возвращает текущее состояние переменной TEXTURE_WRAP_S для данной текстуры:

    get wrapS() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_WRAP_S);
    }

    // Возвращает текущее состояние переменной TEXTURE_WRAP_T для данной текстуры:

    get wrapT() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_WRAP_T);
    }

    // Возвращает текущее состояние переменной TEXTURE_WRAP_R для данной текстуры:

    get wrapR() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_WRAP_R);
    }

    // Возвращает текущее состояние переменной TEXTURE_COMPARE_FUNC для данной текстуры:

    get compareFunc() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_COMPARE_FUNC);
    }

    // Возвращает текущее состояние переменной TEXTURE_COMPARE_MODE для данной текстуры:

    get compareMode() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_COMPARE_MODE);
    }

    // Возвращает текущее состояние переменной TEXTURE_MIN_LOD для данной текстуры:

    get minLod() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_MIN_LOD);
    }

    // Возвращает текущее состояние переменной TEXTURE_MAX_LOD для данной текстуры:

    get maxLod() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_MAX_LOD);
    }

    // Возвращает текущее состояние переменной TEXTURE_MAX_ANISOTROPY_EXT для данной текстуры:

    get maxAnisotropy() {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.getTexParameter(this._target, this._gl.TEXTURE_MAX_ANISOTROPY_EXT);
    }


    // Задаёт текущее состояние переменной TEXTURE_MIN_FILTER для данной текстуры:

    set minFilter(minFilter = this._gl.NEAREST_MIPMAP_LINEAR) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_MIN_FILTER, minFilter);
    }

    // Задаёт текущее состояние переменной TEXTURE_MAG_FILTER для данной текстуры:

    set magFilter(magFilter = this._gl.LINEAR) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_MAG_FILTER, magFilter);
    }

    // Задаёт текущее состояние переменной TEXTURE_WRAP_S для данной текстуры:

    set wrapS(wrapS = this._gl.CLAMP_TO_EDGE) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_WRAP_S, wrapS);
    }

    // Задаёт текущее состояние переменной TEXTURE_WRAP_T для данной текстуры:

    set wrapT(wrapT = this._gl.CLAMP_TO_EDGE) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_WRAP_T, wrapT);
    }

    // Задаёт текущее состояние переменной TEXTURE_WRAP_R для данной текстуры:

    set wrapR(wrapR = this._gl.CLAMP_TO_EDGE) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_WRAP_R, wrapR);
    }

    // Задаёт текущее состояние переменной TEXTURE_COMPARE_FUNC для данной текстуры:

    set compareFunc(compareFunc = this._gl.LEQUAL) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_COMPARE_FUNC, compareFunc);
    }

    // Задаёт текущее состояние переменной TEXTURE_COMPARE_MODE для данной текстуры:

    set compareMode(compareMode = this._gl.NONE) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_COMPARE_MODE, compareMode);
    }

    // Задаёт текущее состояние переменной TEXTURE_MIN_LOD для данной текстуры:

    set minLod(minLod = 0.0) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_MIN_LOD, minLod);
    }

    // Задаёт текущее состояние переменной TEXTURE_MAX_LOD для данной текстуры:

    set maxLod(maxLod = 0.0) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_MAX_LOD, maxLod);
    }

    // Задаёт текущее состояние переменной TEXTURE_MAX_ANISOTROPY_EXT для данной текстуры:

    set maxAnisotropy(maxAnisotropy = 16.0) {
        this._gl.bindTexture(this._target, this._texture);
        this._gl.texParameterf(this._target, this._gl.TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
    }
}
