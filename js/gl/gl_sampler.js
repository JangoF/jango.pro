class GLSampler {

    constructor(
        gl,

        minFilter = gl.LINEAR,
        magFilter = gl.NEAREST_MIPMAP_LINEAR,

        wrapS = gl.REPEAT,
        wrapT = gl.REPEAT,
        wrapR = gl.REPEAT,

        compareFunc = gl.LEQUAL,
        compareMode = gl.NONE,

        minLod = 0.0,
        maxLod = 0.0
    ) {
        this._gl      = gl;
        this._sampler = this._gl.createSampler();

        this.minFilter = minFilter;
        this.magFilter = magFilter;

        this.wrapS = wrapS;
        this.wrapT = wrapT;
        this.wrapR = wrapR;

        this.compareFunc = compareFunc;
        this.compareMode = compareMode;

        this.minLod = minLod;
        this.maxLod = maxLod;
    }

    bind(unit = this._gl.TEXTURE0) {
        this._gl.bindSampler(unit, this._sampler);
    }

    unbind(unit = this._gl.TEXTURE0) {
        this._gl.bindSampler(unit, null);
    }

    // Получает различные состояния из указанной текстуры:

    setParametersFromTexture(texture) {
        this.minFilter = texture.minFilter;
        this.magFilter = texture.magFilter;

        this.wrapS = texture.wrapS;
        this.wrapT = texture.wrapT;
        this.wrapR = texture.wrapR;

        this.compareFunc = texture.compareFunc;
        this.compareMode = texture.compareMode;

        this.minLod = texture.minLod;
        this.maxLod = texture.maxLod;
    }

    // Позволяет получить объект типа WebGLSampler:

    get sampler() {
        return this._sampler;
    }

    // Позволяет получить текущее состояние переменной TEXTURE_MIN_FILTER для данного семплера:

    get minFilter() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_MIN_FILTER);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_MAG_FILTER для данного семплера:

    get magFilter() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_MAG_FILTER);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_WRAP_S для данного семплера:

    get wrapS() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_WRAP_S);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_WRAP_T для данного семплера:

    get wrapT() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_WRAP_T);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_WRAP_R для данного семплера:

    get wrapR() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_WRAP_R);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_COMPARE_FUNC для данного семплера:

    get compareFunc() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_COMPARE_FUNC);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_COMPARE_MODE для данного семплера:

    get compareMode() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_COMPARE_MODE);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_MIN_LOD для данного семплера:

    get minLod() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_MIN_LOD);
    }

    // Позволяет получить текущее состояние переменной TEXTURE_MAX_LOD для данного семплера:

    get maxLod() {
        return this._gl.getSamplerParameter(this._sampler, this._gl.TEXTURE_MAX_LOD);
    }


    // Позволяет задать текущее состояние переменной TEXTURE_MIN_FILTER для данного семплера:

    set minFilter(minFilter = this._gl.NEAREST_MIPMAP_LINEAR) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_MIN_FILTER, minFilter);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_MAG_FILTER для данного семплера:

    set magFilter(magFilter = this._gl.LINEAR) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_MAG_FILTER, magFilter);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_WRAP_S для данного семплера:

    set wrapS(wrapS = this._gl.CLAMP_TO_EDGE) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_WRAP_S, wrapS);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_WRAP_T для данного семплера:

    set wrapT(wrapT = this._gl.CLAMP_TO_EDGE) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_WRAP_T, wrapT);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_WRAP_R для данного семплера:

    set wrapR(wrapR = this._gl.CLAMP_TO_EDGE) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_WRAP_R, wrapR);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_COMPARE_FUNC для данного семплера:

    set compareFunc(compareFunc = this._gl.LEQUAL) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_COMPARE_FUNC, compareFunc);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_COMPARE_MODE для данного семплера:

    set compareMode(compareMode = this._gl.NONE) {
        this._gl.samplerParameteri(this._sampler, this._gl.TEXTURE_COMPARE_MODE, compareMode);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_MIN_LOD для данного семплера:

    set minLod(minLod = 0.0) {
        this._gl.samplerParameterf(this._sampler, this._gl.TEXTURE_MIN_LOD, minLod);
    }

    // Позволяет задать текущее состояние переменной TEXTURE_MAX_LOD для данного семплера:

    set maxLod(maxLod = 0.0) {
        this._gl.samplerParameterf(this._sampler, this._gl.TEXTURE_MAX_LOD, maxLod);
    }
}
