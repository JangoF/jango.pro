class GLRenderbuffer {

    // Принимает на вход экземпляр WebGL, а так же один из следующих наборов аргументов:
    // internalFormat, width, height
    // internalFormat, width, height, samplesCount

    constructor(gl, internalFormat, width, height, samplesCount) {
        this._gl           = gl;
        this._renderbuffer = this._gl.createRenderbuffer();

        if (format !== undefined) {
            this.setStorage(internalFormat, width, height, samplesCount);
        }
    }

    // ВНИМАНИЕ - после вызова данного метода текущий GLRenderbuffer объект станет непригоден к использованию!
    // Удаляет объект рендербуфера:

    destroy() {
        this._gl.deleteRenderbuffer(this._renderbuffer);
        this._renderbuffer = undefined;
    }

    setStorage(internalFormat, width, height, samplesCount = 1) {
        this._gl.bindRenderbuffer(this._gl.RENDERBUFFER, this._renderbuffer);

        if (samples === 1) {
            this._gl.renderbufferStorage(this._gl.RENDERBUFFER, internalFormat, width, height);
        } else {
            this._gl.renderbufferStorageMultisample(this._gl.RENDERBUFFER, samplesCount, internalFormat, width, height);
        }
    }

    // Возвращает объект WebGL рендербуфера:

    get renderbuffer() {
        return this._renderbuffer;
    }

    // Возвращает текущее значение переменной internalFormat:

    get internalFormat() {
        this._gl.bindRenderbuffer(this._gl.RENDERBUFFER, this._renderbuffer);
        return this._gl.getRenderbufferParameter(this._gl.RENDERBUFFER, this._gl.RENDERBUFFER_INTERNAL_FORMAT);
    }

    // Возвращает текущую ширину рендербуфера:

    get width() {
        this._gl.bindRenderbuffer(this._gl.RENDERBUFFER, this._renderbuffer);
        return this._gl.getRenderbufferParameter(this._gl.RENDERBUFFER, this._gl.RENDERBUFFER_WIDTH);
    }

    // Возвращает текущую высоту рендербуфера:

    get height() {
        this._gl.bindRenderbuffer(this._gl.RENDERBUFFER, this._renderbuffer);
        return this._gl.getRenderbufferParameter(this._gl.RENDERBUFFER, this._gl.RENDERBUFFER_HEIGHT);
    }

    // Возвращает текущее количество выборок рендербуфера:

    get samplesCount() {
        this._gl.bindRenderbuffer(this._gl.RENDERBUFFER, this._renderbuffer);
        return this._gl.getRenderbufferParameter(this._gl.RENDERBUFFER, this._gl.RENDERBUFFER_SAMPLES);
    }
}
