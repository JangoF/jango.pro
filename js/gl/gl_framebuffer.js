class GLFramebuffer {

    /* Виды конструктора:
    ** gl
    ** gl, attachment, slot, type
    ** gl, [[attachment, slot, type]]
    */

    constructor(gl) {
        this._gl = gl;
        this._framebuffer = this._gl.createFramebuffer();
        this._attachments = [];

        if (arguments.length > 2) {
            this.setAttachment(arguments[1], arguments[2], arguments[3]);
        }
        else if (arguments.length == 2) {
            for (let i = 0; i < arguments[1].length; i++) {
                this.setAttachment(arguments[1][i][0], arguments[1][i][1], arguments[1][i][2]);
            }
        }
    }

    destroy() {
        this._gl.deleteFramebuffer(this._framebuffer);
    }

    setAttachment(attachment, slot, type = this._gl.TEXTURE_2D, mip = 0.0) {
        this._gl.bindFramebuffer(this._gl.FRAMEBUFFER, this._framebuffer);
        this._attachments[this._getPositionFromAttachment(slot)] = attachment;

        if (attachment instanceof GLTexture) {
            this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, slot, type, attachment._texture, mip);
        }
        else {
            this._gl.framebufferRenderbuffer(this._gl.FRAMEBUFFER, slot, this._gl.RENDERBUFFER, attachment._renderbuffer);
        }
    }

    getAttachment(slot) {
        return this._attachments[this._getPositionFromAttachment(slot)];
    }

    resize(width, height) {
        for (let i = 0; i < this._attachments.length; i++) {
            if (this._attachments[i] instanceof GLTexture) {
                if (this._attachments[i]._isStorage == false) {
                    this._attachments[i].setTexture(this._gl.TEXTURE_2D, this._attachments[i]._format, width, height, null, this._attachments[i].magFilter);
                }
                else {
                    this._attachments[i].setStorage(this._gl.TEXTURE_2D, this._attachments[i]._format, width, height);
                }
            }
            else if (this._attachments[i] instanceof GLFramebuffer) {
                this._attachments[i].setStorage(this._attachments[i]._format, width, height, this._attachments[i]._samples);
            }
        }
    }

    bind(target = this._gl.FRAMEBUFFER) {
        this._gl.bindFramebuffer(target, this._framebuffer);
    }

    setupViewport() {
        for (let i = 0; i < this._attachments.length; i++) {
            if (this._attachments[i] !== undefined && this._attachments[i] !== null) {
                this._gl.viewport(0, 0, this._attachments[i].width, this._attachments[i].height);
                return;
            }
        }
    }

    setupDrawBuffers() {

        // МОДИФИЦИРОВАТЬ ДО ПРАВИЛЬНОГО ПОВЕДЕНИЯ
        let array = [];

        for (let i = 0; i < this._attachments.length; i++) {
            if (this._attachments[i] !== undefined && this._attachments[i] !== null) {
                array.push(this._gl['COLOR_ATTACHMENT' + new String(i)]);
            } else {
                break;
            }
        }

        this._gl.drawBuffers(array.length > 0.0 ? array : [this._gl.NONE]);
    }

    bindComplex() {
        this.bind();
        this.setupViewport();
        this.setupDrawBuffers();
    }

    _getPositionFromAttachment(attachment) {
        switch (attachment) {
            case this._gl.COLOR_ATTACHMENT0:  return 0;
            case this._gl.COLOR_ATTACHMENT1:  return 1;
            case this._gl.COLOR_ATTACHMENT2:  return 2;
            case this._gl.COLOR_ATTACHMENT3:  return 3;
            case this._gl.COLOR_ATTACHMENT4:  return 4;
            case this._gl.COLOR_ATTACHMENT5:  return 5;
            case this._gl.COLOR_ATTACHMENT6:  return 6;
            case this._gl.COLOR_ATTACHMENT7:  return 7;
            case this._gl.COLOR_ATTACHMENT8:  return 8;
            case this._gl.COLOR_ATTACHMENT9:  return 9;
            case this._gl.COLOR_ATTACHMENT10: return 10;
            case this._gl.COLOR_ATTACHMENT11: return 11;
            case this._gl.COLOR_ATTACHMENT12: return 12;
            case this._gl.COLOR_ATTACHMENT13: return 13;
            case this._gl.COLOR_ATTACHMENT14: return 14;
            case this._gl.COLOR_ATTACHMENT15: return 15;

            case this._gl.DEPTH_ATTACHMENT:         return 16;
            case this._gl.STENCIL_ATTACHMENT:       return 17;
            case this._gl.DEPTH_STENCIL_ATTACHMENT: return 18;

            default:
                throw 'Попытка использовать неизвестный тип вложения: (' + String(attachment) + ')!';
        }
    }

    get framebuffer() {
        return this._framebuffer;
    }

    get resolution() {
        for (let i = 0; i < this._attachments.length; i++) {
            if (this._attachments[i] !== undefined && this._attachments[i] !== null) {
                return this._attachments[i].size;
            }
        }
    }
}
