class GLPipeline {

    // Получает на вход экземпляр WebGL а так же вершинный и фрагментный шейдеры,
    // причём каждый из них может быть представлен как исходным кодом, так и объектом GLShader:

    constructor(gl, verterxShader, fragmentShader) {
        this._gl = gl;

        // Создаёт объект GLShader для для шейдеров представленных исходными кодом:

        let isVerterxShaderCreatedLocal = false;
        let isFragmentShaderCreatedLocal = false;

        if (verterxShader instanceof String || typeof verterxShader === 'string') {
            verterxShader = new GLShader(this._gl, verterxShader);
            isVerterxShaderCreatedLocal = true;
        }

        if (fragmentShader instanceof String || typeof fragmentShader === 'string') {
            fragmentShader = new GLShader(this._gl, fragmentShader);
            isFragmentShaderCreatedLocal = true;
        }

        // Создаёт программу:

        this._program = this._gl.createProgram();

        this._gl.attachShader(this._program, verterxShader.shader);
        this._gl.attachShader(this._program, fragmentShader.shader);

        this._gl.linkProgram(this._program);
        this._gl.validateProgram(this._program);

        // Удаляет шейдеры созданные нами:

        if (isVerterxShaderCreatedLocal) {
            verterxShader.destroy();
        }

        if (isFragmentShaderCreatedLocal) {
            fragmentShader.destroy();
        }

        // Отображает лог создания программы если оное не было полностью успешным:

        if (!this._gl.getProgramParameter(this._program, this._gl.VALIDATE_STATUS)) {
            console.log(this._gl.getProgramInfoLog(this._program));
        }

throw 'TODO';

        // Подготавливаем переменные для преобразования:

        this._unifromDescriptions = [];

        this._structDescriptions = vShader.structDescriptions.concat(fShader.structDescriptions);
        let uniforms = vShader.uniformDescriptions.concat(fShader.uniformDescriptions);

        let currentAvalableUnitTexture = 0;
        let currentAvalableUnitUniform = 0;

        // Формируем список описывающий каждую uniform переменную и каждый uniform блок:

        for (const currentUniform of uniforms) {
            if (currentUniform['content'] === undefined) {
                GLPipeline._makeDescriptionFromSingle(currentUniform);
            }
            else {
                GLPipeline._makeDescriptionFromBlock(currentUniform);
            }
        }

        // for (const currentUniform of uniforms) {
        //
        //     let currentUniformInfo = {
        //         type: currentUniform.type,
        //         name: currentUniform.name,
        //         category: GLPipeline._getGLSLTypeCategory(currentUniform.type)
        //     };
        //
        //     switch (currentUniformInfo.category) {
        //         case 'SAMPLER':
        //             currentUniformInfo['location'] = this._gl.getUniformLocation(this._program, currentUniformInfo.name);
        //             currentUniformInfo['unit'] = avalableUnitTexture;
        //             avalableUnitTexture += 1;
        //             break;
        //
        //         case 'UNRECOGNIZED':
        //             currentUniformInfo['location'] = this._gl.getUniformBlockIndex(this._program, currentUniformInfo.type);
        //             currentUniformInfo['unit'] = avalableUnitUniform;
        //             avalableUnitUniform += 1;
        //             break;
        //
        //         default:
        //             currentUniformInfo['location'] = this._gl.getUniformLocation(this._program, currentUniformInfo.name);
        //     }
        //
        //     this._unifromDescriptions.push(currentUniformInfo);
        // }
    }

    _makeDescriptionFromBlock(block, currentName, currentAvalableUnitTexture, currentAvalableUnitUniform) {
    }

    _makeDescriptionFromSingle(single, currentName, currentAvalableUnitTexture, currentAvalableUnitUniform) {
        let category = GLPipeline._getGLSLTypeCategory(single.type);

        switch (currentUniformInfo.category) {
            case 'UNRECOGNIZED':
                currentUniformInfo['location'] = this._gl.getUniformBlockIndex(this._program, currentName + (currentName === '' ? '' : '.') + single.name);
                currentUniformInfo['unit'] = avalableUnitUniform;
                avalableUnitUniform += 1;
                break;

            case 'SAMPLER':
                currentUniformInfo['location'] = this._gl.getUniformLocation(this._program, currentUniformInfo.name);
                currentUniformInfo['unit'] = avalableUnitTexture;
                avalableUnitTexture += 1;
                break;

            default:
                currentUniformInfo['location'] = this._gl.getUniformBlockIndex(this._program, currentName + (currentName === '' ? '' : '.') + single.name);
        }
    }

    // _getCustomTypeDescriptionFormTypeName(typeName) {
    //     for (const currentStructDescription of this._structDescriptions) {
    //         if (currentStructDescription.type === typeName) {
    //             return currentStructDescription;
    //         }
    //     }
    //
    //     throw 'Запрашиваемый тип не найден!';
    // }
    //
    // _makeSamplerRecursiveList(uniform) {
    //
    // }

    // Удаляет объект программы очищая выделенную под неё память.
    // При этом текущий JS объект так же станет непригоден к использованию:

    destroy() {
        this._gl.deleteProgram(this._program);
        this._program = null;
    }

    // Функция позволяет подключить текущую программу с указанными значениями переменных:

    bind() {
        this._gl.useProgram(this._program);
        const valueCount = Math.min(arguments.length, this._unifromDescriptions.length);

        for (let i = 0; i < valueCount; i++) {
            if (arguments[i] !== undefined && arguments[i] !== null) {
                this.setUniform(i, arguments[i]);
            }
        }
    }

    // Позволяет установить значение конкретной uniform переменной, как по её порядковому номеру, так и по её имени:
    // Поряковый номер высчитывается с учётом как вершинного так и фрагментного шейдеров:

    // uniform, value
    // uniform, value, isTranspose

    setUniform(uniform, value) {

        // Если переменная uniform является строкой - получаем её порядковый номер:

        if (uniform instanceof String || typeof fShader === 'string') {
            uniform = function (unifromDescriptions, requiredName) {

                for (let i = 0; i < unifromDescriptions.length; i++) {
                    if (this._unifromDescriptions[i].name === requiredName) {
                        return i;
                    }
                }
            }(this._unifromDescriptions, uniform);
        }

        switch (this._unifromDescriptions[uniform].category) {
            case 'SINGLE':
                GLPipeline._setSingleValue(this._gl, this._unifromDescriptions[uniform], value);
                break;

            case 'VECTOR':
                GLPipeline._setVectorValue(this._gl, this._unifromDescriptions[uniform], value instanceof V1 ? value.raw : (value instanceof Array ? value : [].slice.call(arguments).dropFirst()));
                break;

            case 'MATRIX':
                GLPipeline._setMatrixValue(this._gl, arguments[2] || false, this._unifromDescriptions[uniform], value instanceof M1 ? value.raw : (value instanceof Array ? value : [].slice.call(arguments).dropFirst()));
                break;

            case 'SAMPLER':
                GLPipeline._setSamplerValue(this._gl, this._unifromDescriptions[uniform], value instanceof GLTexture ? value.texture : value);
                break;

            case 'UNRECOGNIZED':
                GLPipeline._setBufferValue(this._gl, this._program, this._unifromDescriptions[uniform], ...(value instanceof GLBuffer ? [value.buffer, 0, value.size] : (value instanceof GLBufferView ? [value.buffer.buffer, value.offset, value.length] : [value, arguments[2], arguments[3]])));
                break;
        }
    }

    static _setSingleValue(gl, uniformDescription, value) {
        switch (uniformDescription.type) {
            case 'bool':  gl.uniform1i(uniformDescription.location,  value); break;
            case 'int':   gl.uniform1i(uniformDescription.location,  value); break;
            case 'uint':  gl.uniform1ui(uniformDescription.location, value); break;
            case 'float': gl.uniform1f(uniformDescription.location,  value); break;
        }
    }

    // Задаёт значение uniform переменной являющейся вектором. Аргумент value должен быть массивом:

    static _setVectorValue(gl, uniformDescription, value) {
        switch (uniformDescription.type) {
            case 'vec2':  gl.uniform2fv(uniformDescription.location,  value.resized(2, 0)); break;
            case 'vec3':  gl.uniform3fv(uniformDescription.location,  value.resized(3, 0)); break;
            case 'vec4':  gl.uniform4fv(uniformDescription.location,  value.resized(4, 0)); break;
            case 'bvec2': gl.uniform2iv(uniformDescription.location,  value.resized(2, 0)); break;
            case 'bvec3': gl.uniform3iv(uniformDescription.location,  value.resized(3, 0)); break;
            case 'bvec4': gl.uniform4iv(uniformDescription.location,  value.resized(4, 0)); break;
            case 'ivec2': gl.uniform2iv(uniformDescription.location,  value.resized(2, 0)); break;
            case 'ivec3': gl.uniform3iv(uniformDescription.location,  value.resized(3, 0)); break;
            case 'ivec4': gl.uniform4iv(uniformDescription.location,  value.resized(4, 0)); break;
            case 'uvec2': gl.uniform2uiv(uniformDescription.location, value.resized(2, 0)); break;
            case 'uvec3': gl.uniform3uiv(uniformDescription.location, value.resized(3, 0)); break;
            case 'uvec4': gl.uniform4uiv(uniformDescription.location, value.resized(4, 0)); break;
        }
    }

    // Задаёт значение uniform переменной являющейся матрицей. Аргумент value должен быть массивом:

    static _setMatrixValue(gl, uniformDescription, isTranspose, value) {
        switch (uniformDescription.type) {
            case 'mat2': case 'mat2x2': gl.uniformMatrix2fv(uniformDescription.location, isTranspose, value.resized(4,  0)); break;
            case 'mat3': case 'mat3x3': gl.uniformMatrix3fv(uniformDescription.location, isTranspose, value.resized(9,  0)); break;
            case 'mat4': case 'mat4x4': gl.uniformMatrix4fv(uniformDescription.location, isTranspose, value.resized(16, 0)); break;
            case 'mat2x3': gl.uniformMatrix2x3fv(uniformDescription.location, isTranspose, value.resized(6,  0)); break;
            case 'mat2x4': gl.uniformMatrix2x4fv(uniformDescription.location, isTranspose, value.resized(8,  0)); break;
            case 'mat3x2': gl.uniformMatrix3x2fv(uniformDescription.location, isTranspose, value.resized(6,  0)); break;
            case 'mat3x4': gl.uniformMatrix3x4fv(uniformDescription.location, isTranspose, value.resized(12, 0)); break;
            case 'mat4x2': gl.uniformMatrix4x2fv(uniformDescription.location, isTranspose, value.resized(8,  0)); break;
            case 'mat4x3': gl.uniformMatrix4x3fv(uniformDescription.location, isTranspose, value.resized(12, 0)); break;
        }
    }

    // Устанавливает текстуру исходя из заданных параметров:

    static _setSamplerValue(gl, uniformDescription, value) {
        gl.activeTexture(gl['TEXTURE' + new String(uniformDescription.unit)]);
        gl.bindTexture(GLPipeline._convertSamplerNameToTextureTarget(gl, uniformDescription.type), value);
        gl.uniform1i(uniformDescription.location, uniformDescription.unit);
    }

    // Устанавливает uniform буфер исходя из заданных параметров:

    static _setBufferValue(gl, program, uniformDescription, value, offset, length) {
        gl.uniformBlockBinding(program, uniformDescription.location, uniformDescription.unit);
        gl.bindBufferRange(gl.UNIFORM_BUFFER, uniformDescription.unit, value, offset, length);
    }

    // Исходя из названия sampler объекта возвращает требуемый для него тип текстуры:

    static _convertSamplerNameToTextureTarget(gl, samplerName) {
        switch (samplerName) {
            case 'sampler3D':      case 'isampler3D':           case 'usampler3D':                              return gl.TEXTURE_3D;
            case 'sampler2D':      case 'sampler2DShadow':      case 'isampler2D':      case 'usampler2D':      return gl.TEXTURE_2D;
            case 'samplerCube':    case 'samplerCubeShadow':    case 'isamplerCube':    case 'usamplerCube':    return gl.TEXTURE_CUBE_MAP;
            case 'sampler2DArray': case 'sampler2DArrayShadow': case 'isampler2DArray': case 'usampler2DArray': return gl.TEXTURE_2D_ARRAY;
        }
    }

    // Возвращает категорию заданного имени типа:

    static _getGLSLTypeCategory(typeName) {
        switch (typeName) {
            case 'bool': case 'int':
            case 'uint': case 'float':
                return 'SINGLE';

            case 'vec2':  case 'vec3':  case 'vec4':
            case 'bvec2': case 'bvec3': case 'bvec4':
            case 'ivec2': case 'ivec3': case 'ivec4':
            case 'uvec2': case 'uvec3': case 'uvec4':
                return 'VECTOR';

            case 'mat2':   case 'mat3':   case 'mat4':
            case 'mat2x2': case 'mat2x3': case 'mat2x4':
            case 'mat3x2': case 'mat3x3': case 'mat3x4':
            case 'mat4x2': case 'mat4x3': case 'mat4x4':
                return 'MATRIX';

            case 'sampler2D':       case 'sampler3D':            case 'samplerCube': case 'samplerCubeShadow': case 'sampler2DShadow':
            case 'sampler2DArray':  case 'sampler2DArrayShadow': case 'isampler2D':  case 'isampler3D':        case 'isamplerCube':
            case 'isampler2DArray': case 'usampler2D':           case 'usampler3D':  case 'usamplerCube':      case 'usampler2DArray':
                return 'SAMPLER';

            default:
                return 'UNRECOGNIZED';
        }
    }
}


// // Абстракция вокруг объекта WebGLProgram.
// // Включает в себя сам объект а так же информацию о различных его состояниях.
//
// class GLPipeline {
//
//     // Каждый из получаемых констркутором шейдеров может быть как строкой, так и готовым GLShader объектом.
//     // Каждый элемент массива replaceInfo должен иметь вид { from: String, to: (String или любой тип что можно преобразовать в строку) }.
//
//     constructor(gl, vShader, fShader, replaceInfo = [], printDebugInfo = false) {
//         this._gl = gl;
//
//         // Если вместо объекта шейдера нам приходит строка - создаём объект GLShader вручную:
//
//         let isVShaderLocal = false;
//         let isFShaderLocal = false;
//
//         if (vShader instanceof String || typeof vShader === 'string') {
//             vShader = new GLShader(this._gl, vShader, replaceInfo);
//             isVShaderLocal = true;
//         }
//
//         if (fShader instanceof String || typeof fShader === 'string') {
//             fShader = new GLShader(this._gl, fShader, replaceInfo);
//             isFShaderLocal = true;
//         }
//
//         // Формируем программу:
//
//         this._program = this._gl.createProgram();
//         this._gl.attachShader(this._program, vShader.shader);
//
//         this._gl.attachShader(this._program, fShader.shader);
//         this._gl.linkProgram(this._program);
//
//         // Если требуется - отображаем дополнительную информацию:
//
//         if (printDebugInfo) {
//             console.log(this._gl.getShaderInfoLog(vShader.shader));
//             console.log(this._gl.getShaderInfoLog(fShader.shader));
//         }
//
//         // Если объект шейдера создали мы - убираем за собой мусор:
//
//         if (isVShaderLocal) {
//             vShader.destroy();
//         }
//
//         if (isFShaderLocal) {
//             fShader.destroy();
//         }
//
//         // Формируем список описывающий используемые uniform переменные:
//
//         let avalableUnitTexture = 0; // Первый по порядку доступный текстурный юнит.
//         let avalableUnitUniform = 0; // Первый по порядку доступный юнит uniform буфера.
//
//         this._unifromDescriptions = [];
//
//         let uniforms = vShader.uniformDescriptions.concat(fShader.uniformDescriptions); // Полный список с описаниями uniform блоков для всех стадий.
//         for (let i = 0; i < uniforms.length; i++) {
//
//             let currentUniformInfo = {
//                 type: uniforms[i].type,
//                 name: uniforms[i].name,
//                 category: GLPipeline._getGLSLTypeCategory(currentUniformInfo.type)
//             };
//
//             // В зависимости от типа uniform переменной заполняем структуру currentUniformInfo:
//
//             switch (currentUniformInfo['category']) {
//                 case 'SAMPLER': // Если текущая переменная является текстурой - сохраняем её расположение и доступный текстурный юнит:
//                     currentUniformInfo['location'] = this._gl.getUniformLocation(this._program, currentUniformInfo.name);
//                     currentUniformInfo['unit'] = avalableUnitTexture;
//                     avalableUnitTexture += 1;
//                     break;
//
//                 case 'UNRECOGNIZED': // Если текущая переменная имеет пользовательский тип - сохраняем её расположение и доступный юнит uniform буфера:
//                     currentUniformInfo['location'] = this._gl.getUniformBlockIndex(this._program, currentUniformInfo.type);
//                     currentUniformInfo['unit'] = avalableUnitUniform;
//                     avalableUnitUniform += 1;
//                     break;
//
//                 default: // Во всех остальных случаях - просто сохраняем расположение переменной:
//                     currentUniformInfo['location'] = this._gl.getUniformLocation(this._program, currentUniformInfo.name);
//             }
//
//             this._unifromDescriptions.push(currentUniformInfo);
//         }
//     }
//
//     // Удаляет объект программы очищая выделенную под неё память.
//     // При этом текущий JS объект так же станет непригоден к использованию:
//
//     destroy() {
//         this._gl.deleteProgram(this._program);
//         this._program = null;
//     }
//
//     // Функция позволяет подключить текущую программу с указанными значениями переменных:
//
//     bind() {
//         this._gl.useProgram(this._program);
//         const valueCount = Math.min(arguments.length, this._unifromDescriptions.length);
//
//         for (let i = 0; i < valueCount; i++) {
//             if (arguments[i] !== undefined && arguments[i] !== null) {
//                 this.setUniform(i, arguments[i]);
//             }
//         }
//     }
//
//     // Позволяет установить значение конкретной uniform переменной, как по её порядковому номеру, так и по её имени:
//     // Поряковый номер высчитывается с учётом как вершинного так и фрагментного шейдеров:
//
//     setUniform(uniform, value) {
//
//         // Если переменная uniform является строкой - получаем её порядковый номер:
//
//         if (uniform instanceof String || typeof fShader === 'string') {
//             uniform = function (unifromDescriptions, requiredName) {
//
//                 for (let i = 0; i < unifromDescriptions.length; i++) {
//                     if (this._unifromDescriptions[i].name === requiredName) {
//                         return i;
//                     }
//                 }
//             }(this._unifromDescriptions, uniform);
//         }
//
//         // В зависимости от типа uniform переменной задаём её значение:
//
//         switch (this._unifromDescriptions[uniform].type) {
//             case 'int': // Если мы задаём значение единичной int переменной:
//                 this._gl.uniform1i(this._unifromDescriptions[uniform].location, value);
//                 break;
//
//             case 'float': // Если мы задаём значение единичной float переменной:
//                 this._gl.uniform1f(this._unifromDescriptions[uniform].location, value);
//                 break;
//
//             case 'vec2': // Для двумерного вектора, в качестве параметров, мы можем получить либо сам вектор, либо то из чего его можно создать:
//                 this._gl.uniform2fv(this._unifromDescriptions[uniform].location, (value instanceof V2 ? value : new V2([].slice.call(arguments).dropFirst())).raw);
//                 break;
//
//             case 'vec3': // Для трёхмерного вектора, в качестве параметров, мы можем получить либо сам вектор, либо то из чего его можно создать:
//                 this._gl.uniform3fv(this._unifromDescriptions[uniform].location, (value instanceof V3 ? value : new V3([].slice.call(arguments).dropFirst())).raw);
//                 break;
//
//             case 'vec4': // Для четырёхмерного вектора, в качестве параметров, мы можем получить либо сам вектор, либо то из чего его можно создать:
//                 this._gl.uniform4fv(this._unifromDescriptions[uniform].location, (value instanceof V4 ? value : new V4([].slice.call(arguments).dropFirst())).raw);
//                 break;
//
//             case 'mat3': // Для матрицы 3 * 3, в качестве параметров, мы можем получить либо саму матрицу, либо то из чего её можно создать:
//                 this._gl.uniformMatrix3fv(this._unifromDescriptions[uniform].location, false, (value instanceof M3 ? value : new M3([].slice.call(arguments).dropFirst())).raw);
//                 break;
//
//             case 'mat4': // Для матрицы 4 * 4, в качестве параметров, мы можем получить либо саму матрицу, либо то из чего её можно создать:
//                 this._gl.uniformMatrix4fv(this._unifromDescriptions[uniform].location, false, (value instanceof M4 ? value : new M4([].slice.call(arguments).dropFirst())).raw);
//                 break;
//
//             case 'sampler2D':
//             case 'sampler2DShadow':
//             case 'samplerCube': // Если мы задаём значение текстуры, в качестве параметра value допускается лишь объект типа GLTexture:
//                 this._gl.activeTexture(this._gl['TEXTURE' + new String(this._unifromDescriptions[uniform].unit)]);
//                 this._gl.bindTexture(this._unifromDescriptions[uniform].type === 'samplerCube' ? this._gl.TEXTURE_CUBE_MAP : this._gl.TEXTURE_2D, value.texture);
//                 this._gl.uniform1i(this._unifromDescriptions[uniform].location, this._unifromDescriptions[uniform].unit);
//                 break;
//
//             default: // Если мы задаём значение uniform буфера:
//                 this._gl.uniformBlockBinding(this._program, this._unifromDescriptions[uniform].location, this._unifromDescriptions[uniform].unit);
//
//                 if (value instanceof GLBuffer) { // Если в качестве параметра value у нас GLBuffer:
//                     this._gl.bindBufferBase(this._gl.UNIFORM_BUFFER, this._unifromDescriptions[uniform].unit, value.buffer);
//                 }
//                 else if (value instanceof GLBufferView) { // Если в качестве параметра value у нас GLBufferView:
//                     this._gl.bindBufferRange(this._gl.UNIFORM_BUFFER, this._unifromDescriptions[uniform].unit, value.buffer.buffer, value.offset, value.length);
//                 }
//                 else { // Если в качестве параметра value у нас нечто иное:
//                     this._gl.bindBufferBase(this._gl.UNIFORM_BUFFER, this._unifromDescriptions[uniform].unit, value);
//                 }
//                 break;
//         }
//     }
//
//     // Позволяет получить категорию для заданного типа:
//
//     static _getGLSLTypeCategory(typeName) {
//         switch (typeName) {
//             case 'bool':
//             case 'int':
//             case 'uint':
//             case 'float':
//                 return 'SINGLE';  // Если категория типа - простое значение.
//
//             case 'vec2':
//             case 'vec3':
//             case 'vec4':
//             case 'bvec2':
//             case 'bvec3':
//             case 'bvec4':
//             case 'ivec2':
//             case 'ivec3':
//             case 'ivec4':
//             case 'uvec2':
//             case 'uvec3':
//             case 'uvec4':
//                 return 'VECTOR'; // Если категория типа - вектор.
//
//             case 'mat2':
//             case 'mat3':
//             case 'mat4':
//             case 'mat2x2':
//             case 'mat2x3':
//             case 'mat2x4':
//             case 'mat3x2':
//             case 'mat3x3':
//             case 'mat3x4':
//             case 'mat4x2':
//             case 'mat4x3':
//             case 'mat4x4':
//                 return 'MATRIX'; // Если категория типа - матрица.
//
//             case 'sampler2D':
//             case 'sampler3D':
//             case 'samplerCube':
//             case 'samplerCubeShadow':
//             case 'sampler2DShadow':
//             case 'sampler2DArray':
//             case 'sampler2DArrayShadow':
//             case 'isampler2D':
//             case 'isampler3D':
//             case 'isamplerCube':
//             case 'isampler2DArray':
//             case 'usampler2D':
//             case 'usampler3D':
//             case 'usamplerCube':
//             case 'usampler2DArray':
//                 return 'SAMPLER'; // Если категория типа - текстура.
//
//             default:
//                 return 'UNRECOGNIZED'; // Если категория типа не распознана.
//         }
//     }
// }
