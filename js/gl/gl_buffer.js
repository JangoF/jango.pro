class GLBuffer {

    // Принимает на вход экземпляр WebGL а так же один из следующих наборов аргументов:
    // target - в данном случае перед первым использованием буфера необходимо вызвать create.
    // target, data
    // target, data, usage
    // target, length
    // target, length, usage

    constructor(gl, target, data_or_length, usage = gl.STATIC_DRAW) {
        this._gl     = gl;
        this._target = target;
        this._length = data_or_length instanceof ArrayBuffer ? data_or_length.byteLength : data_or_length;
        this._usage  = usage;
        this._buffer = this._gl.createBuffer();

        if length_or_data !== undefined {
            this.bind();
            this._gl.bufferData(this._target, length_or_data, usage);
        }
    }

    // ВНИМАНИЕ - после вызова данного метода текущий GLBuffer объект станет непригоден к использованию!
    // Удаляет объект шейдера а так же очищает память выделенную под ресурсы:

    destroy() {
        this._gl.deleteBuffer(this._buffer);
        this._gl = undefined;

        this._target = undefined;
        this._length = undefined;

        this._usage  = undefined;
        this._buffer = undefined;
    }

    // Привязывает текущий буфер к соответствующему слоту WebGL:

    bind() {
        this._gl.bindBuffer(this._target, this._buffer);
    }

    // Отвязывает текущий буфер от соответствующего слота WebGL:

    unbind() {
        this._gl.bindBuffer(this._target, null);
    }

    // Создаёт (или пересоздаёт) буфер с учётом одного из следующих наборов аргументов:
    // target, data
    // target, data, usage
    // target, length
    // target, length, usage

    create(target, data_or_length, usage = gl.STATIC_DRAW) {
    }

    fill(offset, data) {
        this.bind();
        if ()
        this._gl.bufferSubData(this._target, destinationOffset, sourceData, 0.0, 0.0);
    }

    // Позволяет получить цель (gl.UNIFORM_BUFFER, gl.ARRAY_BUFFER и т.д.):

    get target() {
        return this._target;
    }

    // Позволяет получить объект типа WebGLBuffer:
    get buffer() {
        return this._buffer;
    }
}
