class GLMesh {

    /* Виды конструктора:
    ** gl, indexBuffer, vertexBuffer, vertexDescription, instanceBuffer = null, instanceDescription = null
    ** vertexDescription и instanceDescription могут иметь либо форму объекта { type: (тип компоненты атрибута), count: (количество компонент атрибута) },
    ** либо форму массива [(количество компонент атрибута), ... (количество компонент атрибута)] с типом FLOAT по умолчанию.
    */

    constructor(gl, indexBuffer, vertexBuffer, vertexDescription, instanceBuffer, instanceDescription) {
        this._gl = gl;

        // Создаём Vertex Array Object:

        this._vertexArray = this._gl.createVertexArray();
        this._gl.bindVertexArray(this._vertexArray);

        let additionalVertexOffset = 0;
        if (vertexBuffer instanceof GLBufferView) { // Если в качестве вершинного буфера нам передан GLBufferView:

            this._vertexBuffer = vertexBuffer.buffer.buffer;
            vertexBuffer.buffer.bind();
            additionalVertexOffset = vertexBuffer.offset;

        } else { // Если в качестве вершинного буфера нам переданы обычные данные - создаём объект буфера и загружаем в него эти данные:
            this._vertexBuffer = this._gl.createBuffer();
            this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._vertexBuffer);
            this._gl.bufferData(this._gl.ARRAY_BUFFER, vertexBuffer, this._gl.STATIC_DRAW);
        }

        if (vertexDescription instanceof Array) {
            vertexDescription = this._convertDescriptionFromArrayToObject(vertexDescription, this._gl.FLOAT);
        }

        let vertexOffsetInfo = this._getVertexOffsetInfo(vertexDescription);
        for (let i = 0; i < vertexDescription.length; i++) {
            this._gl.enableVertexAttribArray(i);
            this._gl.vertexAttribPointer(i, vertexDescription[i].count, vertexDescription[i].type, false, vertexOffsetInfo.total, vertexOffsetInfo.offset[i] + additionalVertexOffset);
        }

        if (instanceBuffer != null) {
            let additionalInstanceOffset = 0;
            if (instanceBuffer instanceof GLBufferView) {

                this._instanceBuffer = instanceBuffer.buffer.buffer;
                instanceBuffer.buffer.bind();
                additionalVertexOffset = instanceBuffer.offset;

            } else {
                this._instanceBuffer = this._gl.createBuffer();
                this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._instanceBuffer);
                this._gl.bufferData(this._gl.ARRAY_BUFFER, instanceBuffer, this._gl.STATIC_DRAW);
            }

            this._instanceBuffer = this._gl.createBuffer();
            this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._instanceBuffer);
            this._gl.bufferData(this._gl.ARRAY_BUFFER, instanceBuffer, this._gl.STATIC_DRAW);

            if (instanceDescription instanceof Array) {
                instanceDescription = this._convertDescriptionFromArrayToObject(instanceDescription, this._gl.FLOAT);
            }

            let instanceOffsetInfo = this._getVertexOffsetInfo(instanceDescription);
            for (let i = 0; i < instanceDescription.length; i++) {
                let attributeIndex = i + vertexDescription.length;

                this._gl.enableVertexAttribArray(attributeIndex);
                this._gl.vertexAttribPointer(attributeIndex, instanceDescription[i].count, instanceDescription[i].type, false, instanceOffsetInfo.total, instanceOffsetInfo.offset[i] + additionalInstanceOffset);
                this._gl.vertexAttribDivisor(attributeIndex, 1);
            }

            this._instanceCount = instanceBuffer.length / instanceOffsetInfo.count;
        }

        this._indexBufferType = (vertexBuffer.length / vertexOffsetInfo.count) < 65536 ? this._gl.UNSIGNED_SHORT : this._gl.UNSIGNED_INT;
        if (indexBuffer instanceof GLBufferView) {
            this._indexBuffer = indexBuffer.buffer.buffer;
            this._gl.bindBuffer(this._gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
            this._indexBufferLength = indexBuffer.length / (this._indexBufferType === this._gl.UNSIGNED_SHORT ? 2.0 : 4.0);
            this._indexBufferOffset = indexBuffer.offset;

        } else {
            this._indexBuffer = this._gl.createBuffer();
            this._gl.bindBuffer(this._gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
            this._gl.bufferData(this._gl.ELEMENT_ARRAY_BUFFER, indexBuffer, this._gl.STATIC_DRAW);
            this._indexBufferLength = indexBuffer.length;
        }
    }

    destroy() {
        this._gl.deleteBuffer(this._vertexBuffer);
        this._gl.deleteBuffer(this._instanceBuffer);
        this._gl.deleteBuffer(this._indexBuffer);
        this._gl.deleteVertexArray(this._vertexArray);
    }

    draw(mode = this._gl.TRIANGLES) {
        this._gl.bindVertexArray(this._vertexArray);

        if (this._instanceBuffer != null) {
            this._gl.drawElementsInstanced(mode, this._indexBufferLength, this._indexBufferType, 0, this._instanceCount);
        }
        else {
            this._gl.drawElements(mode, this._indexBufferLength, this._indexBufferType, this._indexBufferOffset);
        }
    }

    _getTypeSize(type) {
        switch (type) {
            case this._gl.BYTE:           return 1;
            case this._gl.SHORT:          return 2;
            case this._gl.UNSIGNED_BYTE:  return 1;
            case this._gl.UNSIGNED_SHORT: return 2;
            case this._gl.FLOAT:          return 4;
            case this._gl.HALF_FLOAT:     return 2;
        }
    }

    _getVertexOffsetInfo(vertexDescription) {
        let offsetInfo = { offset: [], total: 0, count: 0 };

        for (let i = 0; i < vertexDescription.length; i++) {
            offsetInfo.offset.push(offsetInfo.total);
            offsetInfo.total += vertexDescription[i].count * this._getTypeSize(vertexDescription[i].type);
            offsetInfo.count += vertexDescription[i].count;
        }

        return offsetInfo;
    }

    _convertDescriptionFromArrayToObject(description, type) {
        let result = [];

        for (let i = 0; i < description.length; i++) {
            result.push({ type: type, count: description[i] });
        }

        return result;
    }
}

// class GLMesh {
//
//     /* Виды конструктора:
//     ** gl, indexBuffer, vertexBuffer, vertexDescription, instanceBuffer = null, instanceDescription = null
//     ** vertexDescription и instanceDescription могут иметь либо форму объекта { type: (тип компоненты атрибута), count: (количество компонент атрибута) },
//     ** либо форму массива [(количество компонент атрибута), ... (количество компонент атрибута)] с типом FLOAT по умолчанию.
//     */
//
//     constructor(gl, indexBuffer, vertexBuffer, vertexDescription, instanceBuffer, instanceDescription) {
//         this._gl = gl;
//
//         // Создаём Vertex Array Object:
//
//         this._vertexArray = this._gl.createVertexArray();
//         this._gl.bindVertexArray(this._vertexArray);
//
//         let additionalVertexOffset = 0;
//         if (vertexBuffer instanceof GLBufferView) { // Если в качестве вершинного буфера нам передан GLBufferView:
//
//             this._vertexBuffer = vertexBuffer.buffer.buffer;
//             vertexBuffer.buffer.bind();
//             additionalVertexOffset = vertexBuffer.offset;
//
//         } else { // Если в качестве вершинного буфера нам переданы обычные данные - создаём объект буфера и загружаем в него эти данные:
//             this._vertexBuffer = this._gl.createBuffer();
//             this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._vertexBuffer);
//             this._gl.bufferData(this._gl.ARRAY_BUFFER, vertexBuffer, this._gl.STATIC_DRAW);
//         }
//
//         if (vertexDescription instanceof Array) {
//             vertexDescription = this._convertDescriptionFromArrayToObject(vertexDescription, this._gl.FLOAT);
//         }
//
//         let vertexOffsetInfo = this._getVertexOffsetInfo(vertexDescription);
//         for (let i = 0; i < vertexDescription.length; i++) {
//             this._gl.enableVertexAttribArray(i);
//             this._gl.vertexAttribPointer(i, vertexDescription[i].count, vertexDescription[i].type, false, vertexOffsetInfo.total, vertexOffsetInfo.offset[i] + additionalVertexOffset);
//         }
//
//         if (instanceBuffer != null) {
//             let additionalInstanceOffset = 0;
//             if (instanceBuffer instanceof GLBufferView) {
//
//                 this._instanceBuffer = instanceBuffer.buffer.buffer;
//                 instanceBuffer.buffer.bind();
//                 additionalVertexOffset = instanceBuffer.offset;
//
//             } else {
//                 this._instanceBuffer = this._gl.createBuffer();
//                 this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._instanceBuffer);
//                 this._gl.bufferData(this._gl.ARRAY_BUFFER, instanceBuffer, this._gl.STATIC_DRAW);
//             }
//
//             this._instanceBuffer = this._gl.createBuffer();
//             this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._instanceBuffer);
//             this._gl.bufferData(this._gl.ARRAY_BUFFER, instanceBuffer, this._gl.STATIC_DRAW);
//
//             if (instanceDescription instanceof Array) {
//                 instanceDescription = this._convertDescriptionFromArrayToObject(instanceDescription, this._gl.FLOAT);
//             }
//
//             let instanceOffsetInfo = this._getVertexOffsetInfo(instanceDescription);
//             for (let i = 0; i < instanceDescription.length; i++) {
//                 let attributeIndex = i + vertexDescription.length;
//
//                 this._gl.enableVertexAttribArray(attributeIndex);
//                 this._gl.vertexAttribPointer(attributeIndex, instanceDescription[i].count, instanceDescription[i].type, false, instanceOffsetInfo.total, instanceOffsetInfo.offset[i] + additionalInstanceOffset);
//                 this._gl.vertexAttribDivisor(attributeIndex, 1);
//             }
//
//             this._instanceCount = instanceBuffer.length / instanceOffsetInfo.count;
//         }
//
//         this._indexBufferType = (vertexBuffer.length / vertexOffsetInfo.count) < 65536 ? this._gl.UNSIGNED_SHORT : this._gl.UNSIGNED_INT;
//         if (indexBuffer instanceof GLBufferView) {
//             this._indexBuffer = indexBuffer.buffer.buffer;
//             this._gl.bindBuffer(this._gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
//             this._indexBufferLength = indexBuffer.length / (this._indexBufferType === this._gl.UNSIGNED_SHORT ? 2.0 : 4.0);
//             this._indexBufferOffset = indexBuffer.offset;
//
//         } else {
//             this._indexBuffer = this._gl.createBuffer();
//             this._gl.bindBuffer(this._gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
//             this._gl.bufferData(this._gl.ELEMENT_ARRAY_BUFFER, indexBuffer, this._gl.STATIC_DRAW);
//             this._indexBufferLength = indexBuffer.length;
//         }
//     }
//
//     destroy() {
//         this._gl.deleteBuffer(this._vertexBuffer);
//         this._gl.deleteBuffer(this._instanceBuffer);
//         this._gl.deleteBuffer(this._indexBuffer);
//         this._gl.deleteVertexArray(this._vertexArray);
//     }
//
//     draw(mode = this._gl.TRIANGLES) {
//         this._gl.bindVertexArray(this._vertexArray);
//
//         if (this._instanceBuffer != null) {
//             this._gl.drawElementsInstanced(mode, this._indexBufferLength, this._indexBufferType, 0, this._instanceCount);
//         }
//         else {
//             this._gl.drawElements(mode, this._indexBufferLength, this._indexBufferType, this._indexBufferOffset);
//         }
//     }
//
//     _getTypeSize(type) {
//         switch (type) {
//             case this._gl.BYTE:           return 1;
//             case this._gl.SHORT:          return 2;
//             case this._gl.UNSIGNED_BYTE:  return 1;
//             case this._gl.UNSIGNED_SHORT: return 2;
//             case this._gl.FLOAT:          return 4;
//             case this._gl.HALF_FLOAT:     return 2;
//         }
//     }
//
//     _getVertexOffsetInfo(vertexDescription) {
//         let offsetInfo = { offset: [], total: 0, count: 0 };
//
//         for (let i = 0; i < vertexDescription.length; i++) {
//             offsetInfo.offset.push(offsetInfo.total);
//             offsetInfo.total += vertexDescription[i].count * this._getTypeSize(vertexDescription[i].type);
//             offsetInfo.count += vertexDescription[i].count;
//         }
//
//         return offsetInfo;
//     }
//
//     _convertDescriptionFromArrayToObject(description, type) {
//         let result = [];
//
//         for (let i = 0; i < description.length; i++) {
//             result.push({ type: type, count: description[i] });
//         }
//
//         return result;
//     }
// }
