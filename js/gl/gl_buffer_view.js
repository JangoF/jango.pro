class GLBufferView {

    // Принимает на вход экземпляр WebGL а так же смещение (в байтах) и длину участка (в байтах):

    constructor(buffer, offset, length) {
        this._buffer = buffer;
        this._offset = offset;
        this._length = length;
    }

    // Заполняет данными участок на который ссылается текущий GLBufferView:

    fill(data) {
        this._buffer.fill(data, this._offset);
    }

    // Возвращает объект типа GLBuffer на который ссылается текущий GLBufferView:

    get buffer() {
        return this._buffer;
    }

    // Возвращает смещение, в байтах, до начала участка на который ссылается текущий GLBufferView:

    get offset() {
        return this._offset;
    }

    // Возвращает длину участка, в байтах, на который ссылается текущий GLBufferView:

    get length() {
        return this._length;
    }
}
