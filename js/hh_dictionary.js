class HHDictionary {

    constructor() {
    }

    add() {
        this._lastCategory = this;

        for (var i = 0; i < arguments.length - 2; i++) {
            if (this._lastCategory[arguments[i]] == undefined) {
                this._lastCategory[arguments[i]] = {};
            }

            this._lastCategory = this._lastCategory[arguments[i]]
        }

        this._lastCategory[arguments[arguments.length - 2]] = arguments[arguments.length - 1];
    }
}
